﻿using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;
using EPiServer.Validation;
using Norgesportalen.Business.EditorDescriptors;
using Norgesportalen.Business.Publishing;
using Norgesportalen.Models.Pages.BaseClasses;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Norgesportalen.Models.Pages
{
    [SiteContentType(
      GroupName = Global.PageTypeGroupNames.Newsletter,
      DisplayName = "Newsletter unsubscribe page",
      GUID = "C9159A6A-9201-4CFA-9E19-347BD1653AE6",
      Description = "Pagetype for unsubscribing to a specific list")]
    [SiteImageUrl("~/Models/SiteImagesForPageTypesAndBlocks/newsletter-unsub.png")]

    public class NewsletterUnsubscribePage : BasicPageBase, IAbleToFetchFromCentral
    {
        [Display(Name = "Select your country's name on the list.",
                   Description = "",
                   Order = 10,
                   GroupName = Global.GroupNames.NewsletterList)]
        [EditorDescriptor(EditorDescriptorType = typeof(NewsletterEditorDescriptor))]   
        public virtual string NewsLetterList { get; set; }

        [Display(
            Name = Global.PropertyDescriptorValues.FetchDataFromHelperName,
            Description = Global.PropertyDescriptorValues.FetchDataFromHelperDescription,
            GroupName = Global.GroupNames.Content,
            Order = 1)]
        [CultureSpecific]
        [AllowedTypes(new[] { typeof(NewsletterUnsubscribePage) })]
        public override ContentReference FetchDataFromHelper { get; set; }
    }

    public class NewsletterUnsubscribePageValidator : IValidate<NewsletterUnsubscribePage>
    {
        IEnumerable<ValidationError> IValidate<NewsletterUnsubscribePage>.Validate(NewsletterUnsubscribePage page)
        {
            var validationErrors = new List<ValidationError>();

            if (page.NewsLetterList == null)
            {
                validationErrors.Add(
                    new ValidationError()
                    {
                        ErrorMessage = "NewsLetter List to unsubsribe from must be set",
                        PropertyName = page.GetPropertyName(pg => pg.NewsLetterList),
                        Severity = ValidationErrorSeverity.Error,
                        ValidationType = ValidationErrorType.AttributeMatched
                    }
                    );
            }

            return validationErrors;
        }

    }
}