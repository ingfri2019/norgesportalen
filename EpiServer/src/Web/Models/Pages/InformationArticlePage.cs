﻿using EPiServer.DataAnnotations;
using Norgesportalen.Business.Publishing;
using Norgesportalen.Models.Pages.BaseClasses;
using System.ComponentModel.DataAnnotations;

namespace Norgesportalen.Models.Pages
{
    [ContentType(
        DisplayName = "Information page",
        GroupName = Global.PageTypeGroupNames.OtherPageTypes,
        GUID = "F9E2C628-22BE-43D5-B1D0-8AB17A650E5D", 
        Description = "Information articles for the embassy, delegation or consulate general. Used for information articles like work in Norway, study in Norway, invest in Norway and passport renewal. The information articles are produced centrally and should include local elements."
    )]
    [Access(Roles = "WebAdmins, Administrators")]
    [SiteImageUrl("~/Models/SiteImagesForPageTypesAndBlocks/informationpage.png")]

    public class InformationArticlePage : BasicPageBase, IAbleToFetchFromCentral
    {
        [Ignore]
        [CultureSpecific]
        [Display(
        Name = "Location",
        Description = "Geographic place",
        GroupName = Global.GroupNames.Content,
        Order = 30)]
        public override string Location { get; set; }

    }
}