﻿using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Validation;
using EPiServer.Web;
using Norgesportalen.Models.Pages.BaseClasses;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net.Mail;

namespace Norgesportalen.Models.Pages
{
    [SiteContentType(
        GroupName = Global.PageTypeGroupNames.Newsletter,
        DisplayName = "Newsletter Confirm Email page",
        GUID = "3ebbe387-6edf-4a00-84db-b126babb8e73",
        Description = "Pagetype for confirming email for newsletter subscription")]
    [SiteImageUrl("~/Models/SiteImagesForPageTypesAndBlocks/newsletter-unsub.png")]

    public class NewsletterConfirmPage : PageBase
    {
        [CultureSpecific]
        [UIHint(UIHint.Textarea)]
        [Display(Name = "Add a Thank you note",
         Description = "Thank you message to user for confirming email address",
         GroupName = Global.GroupNames.Content,
         Order = 20)]
        public override string MainIntro { get; set; }

        [CultureSpecific]
        [UIHint(UIHint.Textarea)]
        [Display(
                Name = "Error message",
                Description = "If confirmation of email fails for some reason, define a general error message that replaces Thank you message.",
                GroupName = Global.GroupNames.Content,
                Order = 151)]
        public virtual string ErrorMsg { get; set; }

        [Ignore]
        public bool ShowErrormsg { get; set; }

        [Display(
            Name = "From Address",
            GroupName = Global.GroupNames.Email,
            Description = "The email address that will show as the from field in the confirm email in most email clients.", 
            Order = 160)]
        public virtual string MailSender { get; set; }

        [CultureSpecific]        
        [Display(
            Name = "Email Subject",
            GroupName = Global.GroupNames.Email,
            Description = "The subject line of the confirm email. If empty, the name of the site will be used.", 
            Order = 170)]
        public virtual string MailSubject { get; set; }       

        [CultureSpecific]
        [Display(
            Name = "Logo title",
            Prompt = "Norway and ...",
            GroupName = Global.GroupNames.Email,
            Description = "Header text inside email",
            Order = 180)]
        public virtual string MailHeader { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Logo mission name",
            Prompt = "Royal Norwegian...",
            GroupName = Global.GroupNames.Email,
            Description = "Header subtext inside email",
            Order = 182)]
        public virtual string MailSubHeader { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Confirmation email heading",
            Prompt = "Thank you...",
            GroupName = Global.GroupNames.Email,
            Description = "Title text inside email",
            Order = 184)]
        public virtual string MailTitle { get; set; }

       

        [CultureSpecific]
        [UIHint(UIHint.Textarea)]
        [Display(
            Name = "First text area",
            Prompt = "Displays the text in the email the user receives.",
            GroupName = Global.GroupNames.Email,
            Description = "First paragraph inside email",
            Order = 190)]
        public virtual string MailPara1 { get; set; }

        [CultureSpecific]
        [UIHint(UIHint.Textarea)]
        [Display(
            Name = "Second text area",
            Prompt = "Displays the text in the email the user receives.",
            GroupName = Global.GroupNames.Email,
            Description = "Second paragraph inside email",
            Order = 200)]
        public virtual string MailPara2 { get; set; }

        [CultureSpecific]
        [Display(
          Name = "Email button intro",
          GroupName = Global.GroupNames.Email,
          Description = "Text above the confirm email-adr button",
          Order = 205)]
        public virtual string MailButtonIntro { get; set; }

        [CultureSpecific]
        [Display(
          Name = "Email button text",
          GroupName = Global.GroupNames.Email,
          Description = "Confirm email-adr button text",
          Order = 210)]
        public virtual string MailButton { get; set; }

        public override void SetDefaultValues(ContentType contentType)
        {
            base.SetDefaultValues(contentType);

            MailSender = "noreply@" + SiteDefinition.Current.SiteUrl.Host;
            MailButtonIntro = "Please click the link below to confirm your Newsletter subscription.";
            MailButton = "Confirm Subscription";            
        }

        public class NewsletterConfirmPageValidator : IValidate<NewsletterConfirmPage>
        {
            IEnumerable<ValidationError> IValidate<NewsletterConfirmPage>.Validate(NewsletterConfirmPage page)
            {
                var validationErrors = new List<ValidationError>();

                if (string.IsNullOrEmpty(page.MailSubject))
                {
                    validationErrors.Add(
                        new ValidationError()
                        {
                            ErrorMessage = "Without subject text the site name will be used.",
                            PropertyName = page.GetPropertyName(pg => pg.MailSubject),
                            Severity = ValidationErrorSeverity.Warning,
                            ValidationType = ValidationErrorType.AttributeMatched
                        }
                    );
                }
                if (string.IsNullOrEmpty(page.MailSender))
                {
                    validationErrors.Add(
                        new ValidationError()
                        {
                            ErrorMessage = "Please enter the 'From' email address.",
                            PropertyName = page.GetPropertyName(pg => pg.MailSender),
                            Severity = ValidationErrorSeverity.Error,
                            ValidationType = ValidationErrorType.AttributeMatched
                        }
                    );
                }
                else
                {
                    try
                    {
                        var adrTest = new MailAddress(page.MailSender);
                    }
                    catch
                    {
                        validationErrors.Add(
                            new ValidationError()
                            {
                                ErrorMessage = "Please enter a valid 'From' email address.",
                                PropertyName = page.GetPropertyName(pg => pg.MailSender),
                                Severity = ValidationErrorSeverity.Error,
                                ValidationType = ValidationErrorType.AttributeMatched
                            }
                        );
                    }
                }
                if (string.IsNullOrEmpty(page.MailHeader))
                {
                    validationErrors.Add(
                        new ValidationError()
                        {
                            ErrorMessage = "Please enter the email header.",
                            PropertyName = page.GetPropertyName(pg => pg.MailHeader),
                            Severity = ValidationErrorSeverity.Error,
                            ValidationType = ValidationErrorType.AttributeMatched
                        }
                    );
                }
                if (string.IsNullOrEmpty(page.MailPara1))
                {
                    validationErrors.Add(
                        new ValidationError()
                        {
                            ErrorMessage = "Please enter the first paragraph inside the email.",
                            PropertyName = page.GetPropertyName(pg => pg.MailPara1),
                            Severity = ValidationErrorSeverity.Error,
                            ValidationType = ValidationErrorType.AttributeMatched
                        }
                    );
                }
                if (string.IsNullOrEmpty(page.MailButton))
                {
                    validationErrors.Add(
                        new ValidationError()
                        {
                            ErrorMessage = "Please enter the text for email confirm button.",
                            PropertyName = page.GetPropertyName(pg => pg.MailButton),
                            Severity = ValidationErrorSeverity.Error,
                            ValidationType = ValidationErrorType.AttributeMatched
                        }
                    );
                }
                return validationErrors;
            }
        }
    }
}