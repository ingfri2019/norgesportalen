using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;
using EPiServer.Web;
using Norgesportalen.Business;
using Norgesportalen.Business.EditorDescriptors;
using Norgesportalen.Business.Publishing;
using Norgesportalen.Models.Blocks;
using Norgesportalen.Models.Interfaces;
using Norgesportalen.Models.Pages.BaseClasses;
using System.ComponentModel.DataAnnotations;
using EPiServer.SpecializedProperties;
using Newtonsoft.Json;

namespace Norgesportalen.Models.Pages
{
    [ContentType(
        DisplayName = "Core article",
        GroupName = Global.PageTypeGroupNames.NewsAndEvents,
        GUID = "2F90476E-36B5-4C24-90D0-3D5C3D72D024",
        Description = "The most common page, that can be used as a news page, statement page and information page.The page can also change to a section page and list page layout.")]
    [SiteImageUrl("~/Models/SiteImagesForPageTypesAndBlocks/corearticle.png")]
    public class CoreArticlePage : BasicPageBase, IAbleToFetchFromCentral, ICanBeInNewsList
    {
        [Ignore]
        public override string TeaserText { get; set; }

        [Ignore]
        public override ContentArea RightColumContentArea { get; set; }
        
        [CultureSpecific]
        [Display(
          Name = "Exchange ingress with image in lists",
          Description = "Show ingress or teasertext in listings instead of image",
          GroupName = Global.GroupNames.NotInUse,
          Order = 3)]
        public override bool ShowIngressOrTeaserText { get; set; }

        [CultureSpecific]
        [Display(Name = "Alternative heading",
                Description = "",
                Prompt = "Page heading",
                Order = 5,
                GroupName = Global.GroupNames.NotInUse)]
        public override string Heading { get; set; }

        [Display(Name = "Preferred resolution for the main image is 1044X400px.",
        Description = "Preferred resolution for the main image is 1044X400px.",
        GroupName = Global.GroupNames.Content,
        Order = 9)]
        [UIHintExtended("ExtraEditorInformationProperty", UIHintExtendedAttribute.DisplayConditions.ShowAlways)]
        public virtual string MainImageInfo { get; set; }

        [UIHint(UIHint.Image)]
        [Display(
           Name = "Add a main image",
           Description = "Preferred resolution for the main image is 1044X400px.",
           Prompt = "Preferred resolution for the main image is 1044X400px.",
           GroupName = Global.GroupNames.Content,
           Order = 10)]
        public override ContentReference MainImage { get; set; }

        [CultureSpecific]
        [UIHint(UIHint.Textarea)]
        [Display(
             Name = "Image text",
             Description = "",
             GroupName = Global.GroupNames.NotInUse)]
        public override string ImageText { get; set; }

        [CultureSpecific]
        [Display(Name = "First field Main body",
                    Description = "Will be shown in the first main content area of the page, using the XHTML-editor you can insert for example text, images and tables.",
                    GroupName = Global.GroupNames.Content,
                    Order = 50)]
        public override XhtmlString MainBody { get; set; }

        [UIHint(UIHint.Image)]
        [Display(
        Name = "Add a MIDDLE image",
        Description = "Add a MIDDLE image",
        GroupName = Global.GroupNames.Content,
        Order = 55)]
        public virtual ContentReference MainImageMiddle { get; set; }

        [CultureSpecific]
        [Display(Name = "Second field heading",
                 Description = "",
                 Order = 60,
                 GroupName = Global.GroupNames.Content)]
        public virtual string HeadingSecond { get; set; }

        [CultureSpecific]
        [Display(Name = "Second field Main body",
                    Description = "Will be shown in the second content area of the page, using the XHTML-editor you can insert for example text, images and tables.",
                    GroupName = Global.GroupNames.Content,
                    Order = 65)]
        public virtual XhtmlString MainBodySecond { get; set; }

        [CultureSpecific]
        [Display(
           Name = "Related articles",
           GroupName = Global.GroupNames.Content,
           Order = 70,
           Description = "")]
        [AllowedTypes(new[] {
            typeof(PageBase),
            typeof(MediaData),
            typeof(RelatedLinkBlock)
        })]
        public virtual ContentArea LinkContentArea { get; set; }

        [CultureSpecific]
        [AllowedTypes(new[] { typeof(AccordionItemFreetextBlock),
            typeof(AccordionItemFreeTextTwoColumns),
            typeof(AccordionItemLinkListBlock),
            typeof(AccordionItemContactsBlock), 
            typeof(AccordionContainerBlock),
            typeof(LinkWithTextContainerBlock),
            typeof(LinkWithTitleContainerBlock)})]
        [Display(
            Name = "Add accordions",
            GroupName = Global.GroupNames.Content,
            Order = 80,
            Description = "")]
        public override ContentArea MainContentArea { get; set; }

        [CultureSpecific]
        [AllowedTypes(new[]
        {
            typeof(NewsletterSubscriptionBlock),
            typeof(ContactBlock)
        })]
        [Display(
            Name = "Add address or newsletter block",
            GroupName = Global.GroupNames.Content,
            Order = 90,
            Description = "")]
        public virtual ContentArea AddressNewsletterArea { get; set; }

        [Display(
            Name = Global.PropertyDescriptorValues.FetchDataFromHelperName,
            Description = Global.PropertyDescriptorValues.FetchDataFromHelperDescription,
            GroupName = Global.GroupNames.Content,
            Order = 1000
        )]
        [CultureSpecific]
        [AllowedTypes(new[] { typeof(CoreArticlePage), typeof(SectionsPage), typeof(ThemePage) })]
        public override ContentReference FetchDataFromHelper { get; set; }

        [Display(
            Name = "Choose view for listed subcontent",
            Description = "",
            GroupName = Global.GroupNames.ListAlternatives,
            Order = 120)]
        [SelectOne(SelectionFactoryType = typeof(ListAlternativesSelectionFactory))]
        [BackingType(typeof(PropertyString))]
        public virtual string ListAlternatives { get; set; }

        [Display(
            Name = "Heading for listed sub-content",
            Description = "",
            GroupName = Global.GroupNames.ListAlternatives,
            Order = 120)]
        public virtual string RelatedContentHeader { get; set; }

        public override void SetDefaultValues(ContentType contentType)
        {
            base.SetDefaultValues(contentType);
            ListAlternatives = Constants.ListAlternatives.ListView;
        }

        [JsonIgnore]
        [UIHint("RelatedLinkList")]
        [Display(Name = "Related links to external content",
            Description = "Used to show external links in right column. Note!  Husk å informere om hvor lenken leder i feltet \"Koblingens navn\", for eksempel \"Flere bilder på Flickr\".",
            Order = 270,
            GroupName = Global.GroupNames.Content)]
        public virtual LinkItemCollection RelatedLinks { get; set; }
    }
}
