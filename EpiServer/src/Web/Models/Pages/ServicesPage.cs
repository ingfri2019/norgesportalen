﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Web;
using Norgesportalen.Business.Publishing;
using Norgesportalen.Models.Pages.BaseClasses;

namespace Norgesportalen.Models.Pages
{
    [ContentType(
        DisplayName = "Services page",
        GroupName = Global.PageTypeGroupNames.OtherPageTypes,
        GUID = "53105DA6-D2E2-4C07-BDB0-E51C6C6E9B9A", 
        Description = "This page type is meant to present services the mission provides foreigners. This is an admin-page and there should only be one of these pr mission. It is designed to present services-type content."
    )]
    [SiteImageUrl("~/Models/SiteImagesForPageTypesAndBlocks/ServicesPage.png")]
    [Access(Roles = "WebAdmins, Administrators")]
    public class ServicesPage : PageBase, IAbleToFetchFromCentral
    {
        // Helper property for setting "Fetch from" on page
        [Display(
            Name = Global.PropertyDescriptorValues.FetchDataFromHelperName,
            Description = Global.PropertyDescriptorValues.FetchDataFromHelperDescription,
            GroupName = Global.GroupNames.Content,
            Order = 1)]
        [AllowedTypes(new[] { typeof(ServicesPage) })]
        [CultureSpecific]
        public virtual ContentReference FetchDataFromHelper { get; set; }

        [Ignore]
        public virtual List<PageBase> Pages { get; set; }

        #region LOCAL CONTENT

        [CultureSpecific]
        [UIHint(UIHint.Textarea)]
        [Display(
            Name = Global.PropertyDescriptorValues.AddALocalIngress,
            Description = "",
            GroupName = Global.GroupNames.AdditionalContent,
            Order = 140)]
        public virtual string LocalMainIntro { get; set; }

        [CultureSpecific]
        [Display(
            Name = Global.PropertyDescriptorValues.AddALocalMainBody,
            Description = "",
            GroupName = Global.GroupNames.AdditionalContent,
            Order = 150)]
        public virtual XhtmlString LocalXHtml { get; set; }


        #endregion

    }
}
