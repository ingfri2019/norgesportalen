﻿using EPiServer.DataAnnotations;

namespace Norgesportalen.Models.Pages
{
    public class SiteImageUrl : ImageUrlAttribute
    {
        public SiteImageUrl(string path)
            : base(path)
        {

        }

    }
}