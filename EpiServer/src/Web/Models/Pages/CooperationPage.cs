﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Web;
using Norgesportalen.Business.Publishing;
using Norgesportalen.Models.Pages.BaseClasses;

namespace Norgesportalen.Models.Pages
{
    [ContentType(
        DisplayName = "Cooperation page",
        GroupName = Global.PageTypeGroupNames.OtherPageTypes,
        GUID = "3FD6F56F-0034-46CA-BA5E-B2E365C9E132", 
        Description = "This page type is meant to present information and general content towards the mission residing nation. The page is designed to present general items of all types. This is an admin-page and there should only be one of these pr mission."
    )]
    [SiteImageUrl("~/Models/SiteImagesForPageTypesAndBlocks/CooperationPage.png")]
    [Access(Roles = "WebAdmins, Administrators")]
    public class CooperationPage : PageBase, IAbleToFetchFromCentral
    {
        // Helper property for setting "Fetch from" on page
        [Display(
            Name = Global.PropertyDescriptorValues.FetchDataFromHelperName,
            Description = Global.PropertyDescriptorValues.FetchDataFromHelperDescription,
            GroupName = Global.GroupNames.Content,
            Order = 1)]
        [AllowedTypes(new[] { typeof(CooperationPage) })]
        [CultureSpecific]
        public virtual ContentReference FetchDataFromHelper { get; set; }

        [Ignore]
        public virtual List<PageBase> Pages { get; set; }

        #region LOCAL CONTENT

        [CultureSpecific]
        [UIHint(UIHint.Textarea)]
        [Display(
            Name = Global.PropertyDescriptorValues.AddALocalIngress,
            Description = "",
            GroupName = Global.GroupNames.AdditionalContent,
            Order = 140)]
        public virtual string LocalMainIntro { get; set; }

        [CultureSpecific]
        [Display(
            Name = Global.PropertyDescriptorValues.AddALocalMainBody,
            Description = "",
            GroupName = Global.GroupNames.AdditionalContent,
            Order = 150)]
        public virtual XhtmlString LocalXHtml { get; set; }


        #endregion

    }
}
