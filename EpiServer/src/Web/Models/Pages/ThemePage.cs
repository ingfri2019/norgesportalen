﻿using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.ServiceLocation;
using EPiServer.Validation;
using EPiServer.Web;
using Norgesportalen.Business.Publishing;
using Norgesportalen.Business.Validation;
using Norgesportalen.Helpers;
using Norgesportalen.Models.Blocks;
using Norgesportalen.Models.Media;
using Norgesportalen.Models.Pages.BaseClasses;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Norgesportalen.Models.Pages
{
    [ContentType(
        DisplayName = "Theme page", 
        GroupName = Global.PageTypeGroupNames.OtherPageTypes,
        GUID = "73625216-2110-4955-B593-3A01508E2F1F", 
        Description = "Theme page for themes like the High north and Business cooperation. The theme pages are produced centrally and should include local elements.")]
    [SiteImageUrl("~/Models/SiteImagesForPageTypesAndBlocks/themepage.png")]

    public class ThemePage : PageBase, IAbleToFetchFromCentral
    {
        // Helper property for setting "Fetch from" on page
        [Display(
            Name = Global.PropertyDescriptorValues.FetchDataFromHelperName,
            Description = Global.PropertyDescriptorValues.FetchDataFromHelperDescription,
            GroupName = Global.GroupNames.Content,
            Order = 1)]
        [CultureSpecific]
        [AllowedTypes(new[] { typeof(ThemePage) })]
        public virtual ContentReference FetchDataFromHelper { get; set; }

        [Display(
            Name = "Background image", 
            Description = "",
            GroupName = Global.GroupNames.Content, 
            Order = 60)]
        [AllowedTypes(new[] { typeof(ImageFile) })]
        public virtual ContentReference Image { get; set; }

        [UIHint(UIHint.Image)]
        [Display(
            Name = "Introduction image for listing",
            Description = "",
            GroupName = Global.GroupNames.Content, Order = 61)]
        public virtual ContentReference Thumbnail
        {
            get
            {
                var thumbnail = this.GetPropertyValue(page => page.Thumbnail);

                if (thumbnail != null)
                {
                    return thumbnail;
                }

                // Fallback to page name when title isn't set
                return Image;
            }
            set
            {
                this.SetPropertyValue(page => page.Thumbnail, value);
            }
        }

        [UIHint(UIHint.Textarea)]
        [CultureSpecific]
        [Display(
            Name = "Alternative ingress", 
            Description = "", 
            Order = 62)]
        public virtual string Introduction
        {
            get
            {
                var introduction = this.GetPropertyValue(page => page.Introduction);

                if (!string.IsNullOrWhiteSpace(introduction))
                {
                    return introduction;
                }
                return MainIntro;
            }
            set
            {
                this.SetPropertyValue(page => page.Introduction, value);
            }
        }

        #region Infographic Area Light

        [CultureSpecific]
        [Display(
            Name = "Area 1 - heading",
            Description = "",
            GroupName = Global.GroupNames.Content,
            Order = 70)]
        public virtual string LightInfographicHeading { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Area 1 - Main body",
            Description = "",
            GroupName = Global.GroupNames.Content,
            Order = 71)]
        public virtual XhtmlString LightInfographicText { get; set; }

        [Display(
            Name = "Area 1- Info blocks", 
            Description = "",
            GroupName = Global.GroupNames.Content, 
            Order = 72)]
        [AllowedTypes(new[] {
            typeof(InfographicBlock)
        })]
        [MaxItemCount(3, ErrorMessage = "Too many blocks in area 1 infographic elements. Maximum 3.")]
        public virtual ContentArea LightInfographicContentArea { get; set; }

        #endregion

        #region Infographic Area Dark

        [CultureSpecific]
        [Display(
            Name = "Area 2 - Heading",
            Description = "",
            GroupName = Global.GroupNames.Content,
            Order = 80)]
        public virtual string DarkInfographicHeading { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Area 2 - Main body",
            Description = "",
            GroupName = Global.GroupNames.Content,
            Order = 81)]
        public virtual XhtmlString DarkInfographicText { get; set; }

        [Display(
            Name = "Area 2- Info blocks", 
            Description = "",
            GroupName = Global.GroupNames.Content, 
            Order = 82)]
        [AllowedTypes(new[] {
            typeof(InfographicBlock)
        })]
        [MaxItemCount(3, ErrorMessage = "Too many blocks in area 2 infographic elements. Maximum 3.")]
        public virtual ContentArea DarkInfographicContentArea { get; set; }

        #endregion

        #region Local Content

        [CultureSpecific]
        [Display(
            Name = "Link to local relevant content", 
            Description = "",
            GroupName = Global.GroupNames.AdditionalContent, 
            Order = 50)]
        [AllowedTypes(new[] { typeof(PageData)})]
        public virtual PageReference LocalContentReference { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Local content heading",
            Description = "",
            GroupName = Global.GroupNames.AdditionalContent)]
        public virtual string LocalHeading { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Local ingress",
            Description = "",
            GroupName = Global.GroupNames.AdditionalContent,
            Order = 51)]
        public virtual string LocalIntro { get; set; }


        [CultureSpecific]
        [Display(  
            Name = "Local additions to the central text",
            Description = "",
            GroupName = Global.GroupNames.AdditionalContent,
            Order = 51)]
        public virtual XhtmlString LocalContent { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Local link to more information", 
            Description = "",
            GroupName = Global.GroupNames.AdditionalContent)]
        public virtual string LocalLinkText { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Link to relevant news section page",
            Description = "",
            GroupName = Global.GroupNames.AdditionalContent,
            Order = 90)]
        public virtual PageReference NewsReference { get; set; }

        [Display(
            Name = "Add additional news",
            Description = "",
            GroupName = Global.GroupNames.AdditionalContent,
            Order = 91)]
        [AllowedTypes(new[] {
            typeof(BasicPageBase),
            typeof(PageData)
        })]
        [MaxItemCount(6, ErrorMessage = "Too many blocks in local news, events and statements. Maximum 6.")]
        public virtual ContentArea NewsContentArea { get; set; }

        [Display(
            Name = "Add additional links",
            Description = "",
            GroupName = Global.GroupNames.AdditionalContent,
            Order = 100)]
        [AllowedTypes(new[] {
            typeof(AccordionContainerBlock),
            typeof(LinkWithTextContainerBlock),
            typeof(LinkWithTitleContainerBlock)
        })]
        [MaxItemCount(4, ErrorMessage = "Too many container blocks in local links. Maximum 4.")]
        public virtual ContentArea LocalLinksContentArea { get; set; }

        #endregion


        #region Links Area
        [Display(
        Name = "Main contentarea",
        GroupName = Global.GroupNames.Content,
        Order = 100)]
        [AllowedTypes(new[] {
            typeof(AccordionContainerBlock),
            typeof(LinkWithTextContainerBlock),
            typeof(LinkWithTitleContainerBlock)
        })]
        [MaxItemCount(4, ErrorMessage = "Too many blocks in main contentarea. Maximum 4.")]
        [Ignore]
        public virtual ContentArea MainContentArea { get; set; }


        [Display(
            Name = "Add additional links here",
            Description = "",
            GroupName = Global.GroupNames.Content,
            Order = 100)]
        [AllowedTypes(new[] {
            typeof(PageData),
            typeof(MediaData),
            typeof(RelatedLinkBlock),
            typeof(AccordionContainerBlock),
            typeof(LinkWithTextContainerBlock),
            typeof(LinkWithTitleContainerBlock)
        })]
        public virtual ContentArea LinksContentArea { get; set; }

        #endregion

        #region General Functions

        public BaseClasses.PageBase GetLocalContentPage(PageReference pageReference)
        {
            var repository = ServiceLocator.Current.GetInstance<EPiServer.IContentRepository>();
            return repository.Get<BaseClasses.PageBase>(pageReference);
        }

        public string getImageCredit(ContentReference contentReference)
        {
            var repository = ServiceLocator.Current.GetInstance<EPiServer.IContentRepository>();
            if (ContentReference.IsNullOrEmpty(contentReference))
            {
                return string.Empty;
            } else
            {
                var image = repository.Get<Media.ImageFile>(contentReference);

                if(image == null)
                {
                    return string.Empty;
                }
                if(string.IsNullOrEmpty(image.Credit))
                {
                    return string.Empty;
                } else
                {
                    return image.Credit;
                }
            }
        }

        #endregion
    }

    public class ThemePagePageValidator : IValidate<ThemePage>
    {
        IEnumerable<ValidationError> IValidate<ThemePage>.Validate(ThemePage page)
        {

            var validationErrors = new List<ValidationError>();
           
            var fetchDataFromHelper = page.Property["FetchDataFromHelper"]?.Value as PageReference;
            if (fetchDataFromHelper == null && page.Image == null)
            {
                validationErrors.Add(
                    new ValidationError()
                    {
                        ErrorMessage = "Image must be set.",
                        PropertyName = page.GetPropertyName(pg => pg.Image),
                        Severity = ValidationErrorSeverity.Error,
                        ValidationType = ValidationErrorType.AttributeMatched
                    }
                );
            }
            var val = new ImageValidatorHelper(page);
            var imgError = val.ValidateImage(page.Image, "Image");
            if (imgError != null)
            {
                validationErrors.Add(imgError);
            }

            imgError = val.ValidateImage(page.Thumbnail, "Thumbnail");
            if (imgError != null)
            {
                validationErrors.Add(imgError);
            }            

            return validationErrors;
        }
    }
}