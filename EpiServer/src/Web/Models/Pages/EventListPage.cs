﻿using EPiServer.Core;
using EPiServer.DataAnnotations;
using Norgesportalen.Business.EditorDescriptors;
using Norgesportalen.Models.Pages.BaseClasses;
using System.ComponentModel.DataAnnotations;
using Norgesportalen.Business.Validation;

namespace Norgesportalen.Models.Pages
{
    [ContentType(
        DisplayName = "Event list page",
        GroupName = Global.PageTypeGroupNames.NewsAndEvents,
        GUID = "d6f4c159-4233-408f-a0c4-4ce983c039f7", 
        Description = "Page that lists events. The list is sorted by the start date of the event.")]
    [SiteImageUrl("~/Models/SiteImagesForPageTypesAndBlocks/Event_list_page.png")]
    [AvailableContentTypes(Include = new[] { typeof(EventPage) })]
    public class EventListPage : PageBase
    {
        [Display(Name = "",
            Description = "This page is intended to present event pages only. It lists content according to event start date. Please see the users manual for more on this pagetype.",
            GroupName = Global.GroupNames.Content,
            Order = 0)]
        [UIHintExtended("ExtraEditorInformationProperty", UIHintExtendedAttribute.DisplayConditions.ShowAlways)]
        public virtual string EventListPageInfo { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Add one focus event to the page",
            Description = "Add one focus event to the page, use this to promote an event. The promoted event will not be part of the listing.",
            GroupName = Global.GroupNames.Content,
            Order = 10000)]
        [AllowedTypes(new[] { typeof(EventPage) })]
        [MaxItemCount(1, "Only 1 event can be promoted/focused")]
        public virtual ContentArea FocusEventContent { get; set; }

        [Ignore]
        public override string MainIntro { get; set; }

        [Ignore]
        public override XhtmlString MainBody { get; set; }

        [Ignore]
        public override string Heading { get => base.Heading; set => base.Heading = value; }

        [Ignore]
        public override string SuggestedPageTypes { get => base.SuggestedPageTypes; set => base.SuggestedPageTypes = value; }

        [Ignore]
        public override bool HideDate { get; set; }

        [Ignore]
        public override string TabInfo_AdditionalContent_NotFetchedContentTabReadonly { get; set; }

        [Ignore]
        public override string TabInfo_AdditionalContent_NotFetchedContentTabNotReadonly { get; set; }
    }
}