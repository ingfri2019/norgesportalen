﻿using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.ServiceLocation;
using EPiServer.Validation;
using EPiServer.Web;
using Norgesportalen.Business.Publishing;
using Norgesportalen.Business.Validation;
using Norgesportalen.Helpers;
using Norgesportalen.Models.Blocks;
using Norgesportalen.Models.Interfaces;
using Norgesportalen.Models.Pages.BaseClasses;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Norgesportalen.Models.Pages
{
    [ContentType(
        DisplayName = "Section page",
        GroupName = Global.PageTypeGroupNames.OtherPageTypes,
        GUID = "B7255F9F-1B6B-4D59-9886-0F789DB7CE8F",
        Description = "Section page for offical services like visa & residence permit or study and research or work in Norway or doing business in Norway or visit Norway or EEA funds. The section pages are produced centrally and should include local elements."
    )]
    [SiteImageUrl("~/Models/SiteImagesForPageTypesAndBlocks/sectionpage.png")]
    [Access(Roles = "WebAdmins, Administrators")]
    //[AvailableContentTypes(Include = new[] { typeof(SectionsPage), typeof(ProcessPage), typeof(InformationArticlePage), typeof(EventPage), typeof(StatementPage), typeof(NewsPage) })]
    public class SectionsPage : PageBase, IAbleToFetchFromCentral, IHasMainImage
    {
        // Helper property for setting "Fetch from" on page
        [Display(
            Name = Global.PropertyDescriptorValues.FetchDataFromHelperName,
            Description = Global.PropertyDescriptorValues.FetchDataFromHelperDescription,
            GroupName = Global.GroupNames.Content,
            Order = 1
        )]
        [CultureSpecific]
        [AllowedTypes(new[] { typeof(SectionsPage) })]
        public virtual ContentReference FetchDataFromHelper { get; set; }


        [UIHint(UIHint.Image)]
        [Display(
            Name = "Main image",
            Description = "",
            GroupName = Global.GroupNames.Content,
            Order = 10)]
        public virtual ContentReference MainImage { get; set; }

        [Ignore]
        public virtual List<PageBase> ServicePages { get; set; }

        [Display(
            Name = "Links",
            Description = "",
            GroupName = Global.GroupNames.Content,
            Order = 70)]
        [AllowedTypes(new[] {
            typeof(AccordionContainerBlock),
            typeof(LinkWithTextContainerBlock),
            typeof(LinkWithTitleContainerBlock),
            typeof(CoreArticlePage),
            typeof(AccordionItemContactsBlock),
            typeof(AccordionItemFreetextBlock),
            typeof(AccordionItemLinkListBlock),
            typeof(ContactBlock),
            typeof(ContactPersonBlockForAccordion),
            typeof(EmergencyBlock),
            typeof(GoogleMapsBlock),
            typeof(ImageBlock),
            typeof(InfographicBlock),
            typeof(InformationBlock),
            typeof(LogoBlock),
            typeof(NewsletterSubscriptionBlock),
            typeof(RelatedLinkBlock),
            typeof(SocialMediaBlock),
            typeof(TopTaskLinkListBlock)
        })]
        [MaxItemCount(4, ErrorMessage = "Too many blocks in links. Maximum 4.")]
        public virtual ContentArea MainContentArea { get; set; }

        [CultureSpecific]
        [UIHint(UIHint.Textarea)]
        [Display(
            Name = Global.PropertyDescriptorValues.AddALocalIngress,
            Description = "",
            GroupName = Global.GroupNames.AdditionalContent,
            Order = 150)]
        public virtual string LocalMainIntro { get; set; }

        [CultureSpecific]
        [Display(
            Name = Global.PropertyDescriptorValues.AddALocalMainBody,
            Description = "",
            GroupName = Global.GroupNames.AdditionalContent,
            Order = 151)]
        public virtual XhtmlString LocalMainBody { get; set; }

        [CultureSpecific]
        [Display(
            Name = Global.PropertyDescriptorValues.AddLocalRelevantLinks,
            GroupName = Global.GroupNames.AdditionalContent,
            Order = 160,
            Description = "")]
        [AllowedTypes(new[] {typeof(PageData),
            typeof(MediaData),
            typeof(RelatedLinkBlock),
            typeof(AccordionContainerBlock),
            typeof(LinkWithTextContainerBlock),
            typeof(LinkWithTitleContainerBlock)
        })]
        [MaxItemCount(4, ErrorMessage = "Too many blocks in local links. Maximum 4.")]
        public virtual ContentArea LocalMainContentArea { get; set; }

        public string getImageCredit(ContentReference contentReference)
        {
            var repository = ServiceLocator.Current.GetInstance<EPiServer.IContentRepository>();
            if (ContentReference.IsNullOrEmpty(contentReference))
            {
                return string.Empty;
            }
            else
            {
                var image = repository.Get<Media.ImageFile>(contentReference);

                if (image == null)
                {
                    return string.Empty;
                }
                if (string.IsNullOrEmpty(image.Credit))
                {
                    return string.Empty;
                }
                else
                {
                    return image.Credit;
                }
            }
        }

        public class SectionsPageValidator : IValidate<SectionsPage>
        {
            IEnumerable<ValidationError> IValidate<SectionsPage>.Validate(SectionsPage page)
            {

                var validationErrors = new List<ValidationError>();
               
                var val = new ImageValidatorHelper(page);
                var imgError = val.ValidateImage(page.MainImage, "MainImage");
                if (imgError != null)
                {
                    validationErrors.Add(imgError);
                }                

                return validationErrors;
            }
        }

        public static int GetPageTypeId()
        {
            return new SectionsPage().ContentTypeID;
        }
    }
}
