﻿using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Validation;
using EPiServer.Web;
using Norgesportalen.Helpers;
using Norgesportalen.Models.Blocks;
using Norgesportalen.Models.Interfaces;
using Norgesportalen.Models.Media;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;

namespace Norgesportalen.Models.Pages.BaseClasses
{
    public class BasicPageBase : PageBase, IHasMainImage
    {
        // Helper property for setting "Fetch from" on page
        [Display(
            Name = Global.PropertyDescriptorValues.FetchDataFromHelperName,
            Description = Global.PropertyDescriptorValues.FetchDataFromHelperDescription,
            GroupName = Global.GroupNames.AdditionalContent,
            Order = 100)]
        [CultureSpecific]
        [AllowedTypes(new[] { typeof(BasicPageBase) })]
        public virtual ContentReference FetchDataFromHelper { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Exchange ingress with image in lists",
            Description = "Show ingress or teasertext in listings instead of image",
            GroupName = Global.GroupNames.Content,
            Order = 3)]
        public virtual bool ShowIngressOrTeaserText { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Where is it?",
            Description = "Geographic place",
            GroupName = Global.GroupNames.Content,
            Order = 4)]
        public virtual string Location { get; set; }

        [CultureSpecific]
        [StringLength(200)]
        [UIHint(UIHint.Textarea)]
        [Display(
            Name = "Alternative ingress",
            Description = "",
            GroupName = Global.GroupNames.Content,
            Order = 6)]
        public virtual string TeaserText { get; set; }

        [UIHint(UIHint.Image)]
        [Display(
            Name = "Add a main image",
            Description = "Top Image",
            GroupName = Global.GroupNames.Content,
            Order = 10)]
        public virtual ContentReference MainImage { get; set; }

        [CultureSpecific]
        [UIHint(UIHint.Textarea)]
        [Display(
            Name = "Image text",
            Description = "",
            GroupName = Global.GroupNames.Content,
            Order = 20)]
        public virtual string ImageText { get; set; }

        [CultureSpecific]
        [AllowedTypes(new[]
        {
            typeof(GoogleMapsBlock), typeof(AccordionContainerBlock), typeof(ButtonBlock),
            typeof(ContactBlock), typeof(ContactAmbassadorBlockForAccordion), typeof(ContactFocusBlock),
            typeof(ContactPersonBlockForAccordion), typeof(AccordionItemContactsBlock), typeof(AccordionItemFreetextBlock),
            typeof(AccordionItemFreeTextTwoColumns), typeof(AccordionItemLinkListBlock), typeof(EmergencyBlock),
            typeof(ExclamationMarkBlock), typeof(InformationBlock),
            typeof(LinkWithTextBlock), typeof(LinkWithTextContainerBlock), typeof(LinkWithTitleBlock),
            typeof(LinkWithTitleContainerBlock), typeof(LogoBlock), typeof(NewsletterSubscriptionBlock), typeof(SocialMediaBlock),
            typeof(TopTaskLinkListBlock)
        })]
        [Display(
            Name = "Add blocks to the page",
            GroupName = Global.GroupNames.Content,
            Order = 70,
            Description = "")]
        public virtual ContentArea MainContentArea { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Side image area",
            GroupName = Global.GroupNames.Content,
            Order = 80,
            Description = "")]
        [AllowedTypes(new[] { typeof(ImageBlock), typeof(ImageFile), typeof(GoogleMapsBlock) })]
        public virtual ContentArea RightColumContentArea { get; set; }


        #region Local content

        [CultureSpecific]
        [UIHint(UIHint.Textarea)]
        [Display(
            Name = Global.PropertyDescriptorValues.AddALocalIngress,
            Description = "",
            GroupName = Global.GroupNames.AdditionalContent,
            Order = 140)]
        public virtual string LocalMainIntro { get; set; }

        [CultureSpecific]
        [Display(
            Name = Global.PropertyDescriptorValues.AddALocalMainBody,
            Description = "",
            GroupName = Global.GroupNames.AdditionalContent,
            Order = 153)]
        public virtual XhtmlString LocalMainBody { get; set; }

        [CultureSpecific]
        [Display(
            Name = Global.PropertyDescriptorValues.AddLocalRelevantLinks,
            GroupName = Global.GroupNames.AdditionalContent,
            Order = 160,
            Description = "")]
        [AllowedTypes(new[] {
            typeof(AccordionContainerBlock),
            typeof(PageData),
            typeof(MediaData),
            typeof(RelatedLinkBlock),
            typeof(AccordionContainerBlock),
            typeof(LinkWithTextContainerBlock),
            typeof(LinkWithTitleContainerBlock)
        })]
        public virtual ContentArea LocalMainContentArea { get; set; }

        #endregion

        public static string GetDateFormatString()
        {
            return CultureInfo.CurrentCulture.Equals(new CultureInfo("cs-CZ")) ? "dd. MM. yyyy" : "dd. MMM yyyy";
        }

        public override void SetDefaultValues(ContentType contentType)
        {
            base.SetDefaultValues(contentType);
            Heading = PageName;
        }

        public static string TeaserOrFallbackToIntro(string teaser, string intro)
        {
            string retVal = string.Empty;

            if (teaser != null && teaser != string.Empty)
            {
                retVal = teaser;
            }
            else if (intro != null && intro != string.Empty)
            {
                retVal = new string(intro.Take(250).ToArray());
                int lastP = retVal.LastIndexOf('.');
                if (lastP > 0)
                {
                    retVal = retVal.Substring(0, lastP + 1);
                }
            }
            return retVal;
        }
  
        public class BasicPageBaseValidator : IValidate<BasicPageBase>
        {
            IEnumerable<ValidationError> IValidate<BasicPageBase>.Validate(BasicPageBase page)
            {
                var validationErrors = new List<ValidationError>();

                var val = new ImageValidatorHelper(page);
                var imgError = val.ValidateImage(page.MainImage, "MainImage");
                if (imgError != null)
                {
                    validationErrors.Add(imgError);
                }

                return validationErrors;
            }
        }
    }
}
