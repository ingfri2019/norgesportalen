﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;
using Norgesportalen.Business.EditorDescriptors;
using Norgesportalen.Business.Validation;
using Norgesportalen.Models.Interfaces;

namespace Norgesportalen.Models.Pages.BaseClasses
{
    public class BaseFormsPage : CoreArticlePage, IHasPageEmail

    {
        [CultureSpecific]
        [Display(Name = "Response email",
            Description = "Enter email for responses",
            GroupName = Global.GroupNames.Forms,
            Order = 1)]
        [Required]
        [EmailAddress]
        public virtual string PageEmail { get; set; }

        [UIHint("FloatingString")]
        [CultureSpecific]
        [Display(Name = "Select form",
            Description = "Add form to be visible on page",
            GroupName = Global.GroupNames.Forms,
            Order = 2)]
        [MaxItemCount(1, "Maximum number of forms is 1")]
        [SelectOne(SelectionFactoryType = typeof(GlobalFormsSelectionFactory))]
        public virtual string FormSelection { get; set; }

        #region Ignore unnecessary properties

        [Ignore]
        public override bool ShowIngressOrTeaserText { get; set; }

        [Ignore]
        public override string Heading { get; set; }

        [Ignore]
        public override string MainImageInfo { get; set; }

        [Ignore]
        public override ContentReference MainImageMiddle { get; set; }

        [Ignore]
        public override string HeadingSecond { get; set; }

        [Ignore]
        public override XhtmlString MainBodySecond { get; set; }

        [Ignore]
        public override ContentArea LinkContentArea { get; set; }

        [Ignore]
        public override ContentArea MainContentArea { get; set; }

        [Ignore]
        public override ContentArea AddressNewsletterArea { get; set; }

        [Ignore]
        public override string ListAlternatives { get; set; }

        [Ignore]
        public override string RelatedContentHeader { get; set; }

        #endregion
    }

}