﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.ServiceLocation;
using EPiServer.Web;
using EPiServer.Shell.ObjectEditing;
using EPiServer.Validation;
using Norgesportalen.Business.EditorDescriptors;
using Norgesportalen.Helpers;
using RestSharp.Extensions;


namespace Norgesportalen.Models.Pages.BaseClasses
{
    public class PageBase : SitePageData
    {
        [CultureSpecific]
        [Display(
            Name = "Do not show article date",
            Description = "",
            GroupName = Global.GroupNames.Content,
            Order = 2)]
        public virtual bool HideDate { get; set; }

        [Display(
            Name = "Icon shown in list views",
            Description = "",
            GroupName = Global.GroupNames.Content,
            Order = 3)]
        [SelectOne(SelectionFactoryType = typeof(IconSelectionFactory))]
        [BackingType(typeof(PropertyString))]
        [UIHint("FloatingString")]
        public virtual string IconImage { get; set; }

        [CultureSpecific]
        [Display(Name = "Alternative heading",
                 Description = "",
                 Prompt = "Page heading",
                 Order = 5,
                 GroupName = Global.GroupNames.Content)]
        public virtual string Heading {
            get
            {
                var title = this.GetPropertyValue(page => page.Heading);

                if (!string.IsNullOrWhiteSpace(title))
                {
                    return title;
                }

                // Fallback to page name when title isn't set
                return PageName;
            }
            set
            {
                this.SetPropertyValue(page => page.Heading, value);
            }
        }

        [DocumentTypeSelection]
        [Display(
            Name = "Suggested page types",
            Description = "Choose which page types will be suggested when creating a new page 'under' the current page.",
            GroupName = Global.GroupNames.SuggestedPageTypes,
            Order = 6)]
        [BackingType(typeof(PropertyString))]
        public virtual string SuggestedPageTypes { get; set; }

        public string HeadingOrPageName
        {
            get
            {
                if (string.IsNullOrEmpty(Heading))
                {
                    return Name;
                }
                return Heading;
            }
        }

      

        public class IconAssetFolder : IImageSelectorSettings
        {
            public string AssetFolderName()
            {
                return "Icons";
            }
        }

        [CultureSpecific]        
        [UIHint(UIHint.Textarea)]
        [Display(Name = "Ingress",
                Description = "",
                GroupName = Global.GroupNames.Content,
                Order = 40)]
        public virtual string MainIntro { get; set; }

        [CultureSpecific]
        [Display(Name = "Main body",
                    Description = Global.PropertyDescriptorValues.MainBodyDescription,
                    GroupName = Global.GroupNames.Content,
                    Order = 50)]
        public virtual XhtmlString MainBody { get; set; }

        public string ShortIntro
        {
            get
            {
                if (MainIntro == null)
                    return string.Empty;
                if (MainIntro.Length <= 200)
                    return MainIntro;

                var spaceIndex = MainIntro.IndexOf(" ", 200, StringComparison.Ordinal);

                if (spaceIndex < 0 || spaceIndex >= (MainIntro.Length - 1))
                    return MainIntro;

                var remainingText = MainIntro.Substring(spaceIndex, MainIntro.Length - spaceIndex);
                if (string.IsNullOrWhiteSpace(remainingText))
                    return MainIntro;

                var retVal = MainIntro.Substring(0, spaceIndex);

                if (retVal.EndsWith(".") || retVal.EndsWith(",") || retVal.EndsWith(":") || retVal.EndsWith(";") || retVal.EndsWith("-"))
                    retVal = retVal.Remove(retVal.Length - 1);

                retVal += "&nbsp;&hellip;";
                return retVal.HtmlDecode();
            }
        }


    }

    public class PageBaseValidator : IValidate<PageBase>
    {
        IEnumerable<ValidationError> IValidate<PageBase>.Validate(PageBase basePage)
        {
            var validationErrors = new List<ValidationError>();

            // the property FetchDataFromHelper in the base class return null, so using this method in stead
            var fetchDataFromHelper = basePage.Property["FetchDataFromHelper"]?.Value as PageReference;
            if (fetchDataFromHelper != null)
            {
                var repository = ServiceLocator.Current.GetInstance<IContentRepository>();
                var fetcedhPg = repository.Get<PageData>(fetchDataFromHelper);
                if (basePage.PageName != fetcedhPg.PageName)
                {
                    validationErrors.Add(
                        new ValidationError()
                        {
                            ErrorMessage = "Page name will not be changed locally when using fetch content.",
                            PropertyName = basePage.GetPropertyName(p => p.PageName),
                            Severity = ValidationErrorSeverity.Warning,
                            ValidationType = ValidationErrorType.AttributeMatched
                        }
                    );
                }
                if (basePage.URLSegment != fetcedhPg.URLSegment)
                {
                    validationErrors.Add(
                        new ValidationError()
                        {
                            ErrorMessage = "Url will not be changed locally when using fetch content.",
                            PropertyName = basePage.GetPropertyName(p => p.URLSegment),
                            Severity = ValidationErrorSeverity.Warning,
                            ValidationType = ValidationErrorType.AttributeMatched
                        }
                    );
                }
            }

            var imageValidator = new ImageValidatorHelper(basePage);
            var mainBodyValidationErrors = imageValidator.ValidateImage(basePage.MainBody, "MainBody");
            validationErrors.AddRange(mainBodyValidationErrors);

            if (basePage is BasicPageBase)
            {
                var rightColumnValidationErrors = imageValidator.ValidateImage(((BasicPageBase)basePage).RightColumContentArea, "RightColumContentArea");
                validationErrors.AddRange(rightColumnValidationErrors);
            }      

            return validationErrors;
        }
    }   
}