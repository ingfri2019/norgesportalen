using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Framework.Localization;
using EPiServer.Web;
using EPiServer.Web.Routing;
using Newtonsoft.Json;
using Norgesportalen.Business.EditorDescriptors;
using System.ComponentModel.DataAnnotations;
using EPiServer;

namespace Norgesportalen.Models.Pages.BaseClasses
{
    /// <summary>
    /// Base class for all page types
    /// </summary>
    public abstract class SitePageData : PageData
    {
        [Display(
            GroupName = Global.GroupNames.MetaData,
            Order = 400)]
        [CultureSpecific]
        public virtual bool DisableIndexing { get; set; }

        #region Info-texts used in the edit interface:

        /// <summary>
        /// This property is not used for content purposes, but is used to display an info text in the content tab regarding the fetched 
        /// content and why properties are set to readonly.
        /// </summary>
        [Display(Name = "",
            Description = Global.PropertyDescriptorValues.FetchDataReadOnlyTabInfo,
            GroupName = Global.GroupNames.Content,
            Order = 0)]
        [UIHintExtended("ExtraEditorInformationProperty", UIHintExtendedAttribute.DisplayConditions.ShowOnReadonlyTab)]
        // ReSharper disable once InconsistentNaming
        public virtual string TabInfo_Content_FetchedContentTabReadonly { get; set; }

        /// <summary>
        /// This property is not used for content purposes, but is used to display an info text in the content tab regarding the fetched 
        /// content and why properties are set to readonly.
        /// </summary>
        [Display(Name = "",
            Description = "Additional content can only be edited when \"fetch content\" is set. To add content to this page use the \"<strong>Content</strong>\"  tab. Read more about \"central content\" in the <a href=\"https://edit.norway.no/en/tum\"><span style=\"color: #2880b9;\"><strong>users manual<strong></span></a>.",
            GroupName = Global.GroupNames.AdditionalContent,
            Order = 0)]
        [UIHintExtended("ExtraEditorInformationProperty", UIHintExtendedAttribute.DisplayConditions.ShowOnReadonlyTab)]
        // ReSharper disable once InconsistentNaming
        public virtual string TabInfo_AdditionalContent_NotFetchedContentTabReadonly { get; set; }

        /// <summary>
        /// This property is not used for content purposes, but is used to display an info text in the content tab regarding the fetched 
        /// content and why properties are set to readonly.
        /// </summary>
        [Display(Name = "",
            Description = "This page is set to fetch content from another page. Because of this setting local editors can only add <i>additional</i> contents here. Read more about \"central content\" in the <a href=\"https://edit.norway.no/en/tum\"><span style=\"color: #2880b9;\"><strong>users manual<strong></span></a>.",
            GroupName = Global.GroupNames.AdditionalContent,
            Order = 0)]
        [UIHintExtended("ExtraEditorInformationProperty", UIHintExtendedAttribute.DisplayConditions.ShowOnNotReadonlyTab)]
        // ReSharper disable once InconsistentNaming
        public virtual string TabInfo_AdditionalContent_NotFetchedContentTabNotReadonly { get; set; }

        /// <summary>
        /// This property is not used for content purposes, but is used to display an info text in the content tab regarding the fetched 
        /// content and why properties are set to readonly.
        /// </summary>
        [Display(Name = "",
            Description = Global.PropertyDescriptorValues.FetchDataReadOnlyTabInfo,
            GroupName = Global.GroupNames.MetaData,
            Order = 0)]
        [UIHintExtended("ExtraEditorInformationProperty", UIHintExtendedAttribute.DisplayConditions.ShowOnReadonlyTab)]
        // ReSharper disable once InconsistentNaming
        public virtual string TabInfo_SoMeTab_FetchedContentTabReadonly { get; set; }

        #endregion

        [Display(
            GroupName = Global.GroupNames.MetaData, 
            Name = "Add an alternative heading when linking to SOME	", 
            Description = "Title of page when used in Facebook links etc, when not set this defaults to Logo title or finally Page Name.", 
            Order = 100)]
        [CultureSpecific]
        public virtual string MetaTitle
        {
            get
            {
                var metaTitle = this.GetPropertyValue(p => p.MetaTitle);
                if (!string.IsNullOrWhiteSpace(metaTitle))
                {
                    return metaTitle;
                }

                if (this is HomePage homePage)
                {
                        metaTitle = homePage.GetPropertyValue(h => h.HeaderLogo.LogoTitle);
                }

                if (string.IsNullOrWhiteSpace(metaTitle))
                {                
                    metaTitle = this.PageName;
                }            
                return metaTitle;
            }

            set { this.SetPropertyValue(p => p.MetaTitle, value); }
        }

        [Display(GroupName = Global.GroupNames.MetaData, Name = "SoMe description", Description = "Description of page when used in e.g. Facebook links, when not set this defaults to Logo subtitle.", Order = 105)]
        [CultureSpecific]
        public virtual string MetaDescription
        {
            get
            {
                var metaDescription = this.GetPropertyValue(p => p.MetaDescription);
                if (!string.IsNullOrWhiteSpace(metaDescription))
                {
                    return metaDescription;
                }

                switch (this)
                {
                    case HomePage homePage:
                        {
                            metaDescription = homePage.GetPropertyValue(h => h.HeaderLogo.LogoSubText);
                            break;
                        }

                    case SitePageData sitePage:
                        {
                            var hPage = GetHomePage(sitePage.PageLink);
                            if (hPage != null)
                            {
                                metaDescription = hPage.GetPropertyValue(pp => pp.MetaDescription);
                                if (string.IsNullOrWhiteSpace(metaDescription))
                                {
                                    metaDescription = hPage.GetPropertyValue(h => h.HeaderLogo.LogoSubText);
                                }
                            }

                            break;
                        }
                }
                return metaDescription;
            }

            set { this.SetPropertyValue(p => p.MetaDescription, value); }
        }

        [Display(GroupName = Global.GroupNames.MetaData, Name = "SEO Title", Description = "Title of page used by search engines, when not set this defaults to Logo title or finally Page Name.", Order = 108)]
        [CultureSpecific]
        public virtual string SeoTitle
        {
            get
            {
                var seoTitle = this.GetPropertyValue(p => p.SeoTitle);
                if (!string.IsNullOrWhiteSpace(seoTitle))
                {
                    return seoTitle;
                }

                if (this is HomePage homePage)
                {
                    seoTitle = homePage.GetPropertyValue(h => h.HeaderLogo.LogoTitle);
                }
                
                if (string.IsNullOrWhiteSpace(seoTitle))
                {
                    seoTitle = this.PageName;
                }
                return seoTitle;
            }

            set { this.SetPropertyValue(p => p.SeoTitle, value); }
        }

        [Display(GroupName = Global.GroupNames.MetaData, Name = "SEO description", Description = "Description of page used by search engines, when not set this defaults to Logo subtitle.", Order = 109)]
        [CultureSpecific]
        public virtual string SeoDescription
        {
            get
            {
                var seoDescription = this.GetPropertyValue(p => p.SeoDescription);
                if (!string.IsNullOrWhiteSpace(seoDescription))
                {
                    return seoDescription;
                }

                switch (this)
                {
                    case HomePage homePage:
                        {
                            seoDescription = homePage.GetPropertyValue(h => h.HeaderLogo.LogoSubText);
                            break;
                        }

                    case SitePageData sitePage:
                        {
                            var hPage = GetHomePage(sitePage.PageLink);
                            if (hPage != null)
                            {
                                seoDescription = hPage.GetPropertyValue(pp => pp.SeoDescription);
                                if (string.IsNullOrWhiteSpace(seoDescription))
                                {
                                    seoDescription = hPage.GetPropertyValue(h => h.HeaderLogo.LogoSubText);
                                }
                            }

                            break;
                        }
                }
                return seoDescription;
            }
            set { this.SetPropertyValue(p => p.SeoDescription, value); }
        }


        [UIHint(UIHint.Image)]
        [Display(
            Name = "Facebook-bilde",
            Description = "Bilde for deling til Facebook. Bilder b�r v�re minimum 800px bredt og i 16:9 format. Maks 8MB.",
            GroupName = Global.GroupNames.MetaData,
            Order = 110)]
        public virtual ContentReference FaceBookImage
        {
            get
            {
                var faceBookImage = this.GetPropertyValue(p => p.FaceBookImage);
                if (faceBookImage != null)
                {
                    return faceBookImage;
                }
                var homePage = this as HomePage;
                if (homePage == null)
                {
                    if (this is SitePageData sitePage)
                    {
                        homePage = GetHomePage(sitePage.PageLink);
                    }
                }
                faceBookImage = homePage?.GetPropertyValue(p => p.FaceBookImage);
                return faceBookImage;
            }
            set { this.SetPropertyValue(p => p.FaceBookImage, value); }
        }

        [Ignore]
        public virtual string CanonicalUrl
        {
            get
            {
                var url = UrlResolver.Current.GetUrl(ContentLink, Language.Name);
                var siteurl = SiteDefinition.Current.SiteUrl.ToString();
                if (siteurl.EndsWith("/") && url.StartsWith("/"))
                {
                    siteurl = siteurl.TrimEnd('/');
                }
                return $"{siteurl}{url}";
            }
        }


    private class LeadingTexts
        {
            public LeadingTexts()
            {
                menu = new Menu();
            }

            // ReSharper disable once NotAccessedField.Local
            // ReSharper disable once InconsistentNaming
            public Menu menu;

            public struct Menu
            {
                // ReSharper disable NotAccessedField.Local
                // ReSharper disable InconsistentNaming
                public string close;
                public string open;
                // ReSharper restore InconsistentNaming
                // ReSharper restore NotAccessedField.Local
            }
        }

        public string GetLeadingTexts()
        {
            var leadTexts = new LeadingTexts();
            leadTexts.menu.open = LocalizationService.Current.GetString("/Menu/menu");
            leadTexts.menu.close = LocalizationService.Current.GetString("/Menu/close");

            return JsonConvert.SerializeObject(leadTexts);
        }

        public static HomePage GetHomePage(PageReference pgRef)
        {
            if (pgRef.ID == 0)
            {
                return null;
            }
            var pg = DataFactory.Instance.GetPage(pgRef);
            if (pg is HomePage)
            {
                return pg as HomePage;
            }
            if (pg is CountriesOverviewPage || pg.ParentLink == null)
            {
                return null;
            }

            //Recusive call
            return GetHomePage(pg.ParentLink);
        }
    }
}
