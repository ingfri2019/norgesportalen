﻿using EPiServer.DataAnnotations;
using Norgesportalen.Business.Publishing;
using Norgesportalen.Models.Pages.BaseClasses;
using System.ComponentModel.DataAnnotations;

namespace Norgesportalen.Models.Pages
{
    [ContentType(DisplayName = "Statement page",
        GroupName = Global.PageTypeGroupNames.OtherPageTypes,
        GUID = "95663C7A-70F0-4584-A1C2-F84B977FCC07", 
        Description = "Statements from official meetings, signings or visits where the delegation is involved.")]
    [SiteImageUrl("~/Models/SiteImagesForPageTypesAndBlocks/statementpage.png")]
    [Access(Roles = "WebAdmins, Administrators")]
    public class StatementPage : BasicPageBase, IAbleToFetchFromCentral
    {
        [Ignore]
        [CultureSpecific]
        [Display(
        Name = "Show ingress or teasertext in listings instead of image, for listing on Service Frontpage only",
        Description = "Show ingress or teasertext in listings instead of image",
        GroupName = Global.GroupNames.Content,
        Order = 3)]
        public override bool ShowIngressOrTeaserText { get; set; }

    }
}