﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Web;
using Norgesportalen.Business.Publishing;
using Norgesportalen.Models.Pages.BaseClasses;

namespace Norgesportalen.Models.Pages
{
    [ContentType(
        DisplayName = "Profiling page",
        GroupName = Global.PageTypeGroupNames.OtherPageTypes,
        GUID = "23D977F4-0810-4907-9A9E-F263102A7EE7", 
        Description = "This page type is meant to contain articles presenting Norway commercially. Both owned and external content. This is an admin-page and there should only be one of these pr mission. It is designed to present Commercial-type content."
    )]
    [SiteImageUrl("~/Models/SiteImagesForPageTypesAndBlocks/ProfilingPage.png")]
    [Access(Roles = "WebAdmins, Administrators")]
    public class ProfilingPage : PageBase, IAbleToFetchFromCentral
    {
        // Helper property for setting "Fetch from" on page
        [Display(
            Name = Global.PropertyDescriptorValues.FetchDataFromHelperName,
            Description = Global.PropertyDescriptorValues.FetchDataFromHelperDescription,
            GroupName = Global.GroupNames.Content,
            Order = 1)]
        [AllowedTypes(new[] { typeof(ProfilingPage) })]
        [CultureSpecific]
        public virtual ContentReference FetchDataFromHelper { get; set; }

        [Ignore]
        public virtual List<PageBase> Pages { get; set; }

        #region LOCAL CONTENT

        [CultureSpecific]
        [UIHint(UIHint.Textarea)]
        [Display(
            Name = Global.PropertyDescriptorValues.AddALocalIngress,
            Description = "",
            GroupName = Global.GroupNames.AdditionalContent,
            Order = 140)]
        public virtual string LocalMainIntro { get; set; }

        [CultureSpecific]
        [Display(
            Name = Global.PropertyDescriptorValues.AddALocalMainBody,
            Description = "",
            GroupName = Global.GroupNames.AdditionalContent,
            Order = 150)]
        public virtual XhtmlString LocalXHtml { get; set; }


        #endregion

    }
}
