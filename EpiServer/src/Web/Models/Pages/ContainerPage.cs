﻿using EPiServer.Core;
using EPiServer.DataAnnotations;
using Norgesportalen.Business.Rendering;

namespace Norgesportalen.Models.Pages
{
    [ContentType(
        DisplayName = "Container page",
        GroupName = Global.PageTypeGroupNames.OtherPageTypes,
        GUID = "FB235E91-172A-4E13-80CE-7B32335C8B57", 
        Description = "Container page which is not viewed by visitors, the page is used as a \"folder\" to gather e.g. news or events or statements so they can be viewed filtered in the list news, events and statements list page."
    )]
    [SiteImageUrl("~/Models/SiteImagesForPageTypesAndBlocks/containerpage.png")]
    [Access(Roles = "WebAdmins, Administrators")]

    public class ContainerPage : PageData, IContainerPage
    {

    }
}