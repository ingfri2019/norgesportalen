﻿using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using Norgesportalen.Business.Rendering;

namespace Norgesportalen.Models.Pages
{
    [ContentType(
        DisplayName = "Folder page",
        GroupName = Global.PageTypeGroupNames.OtherPageTypes,
        GUID = "2f527fcf-2216-4448-b0f8-20367a6fe15b", 
        Description = "Folder page which is not viewed by visitors, the page is used as a folder to structure content for the web editors."
    )]
    [SiteImageUrl("~/Models/SiteImagesForPageTypesAndBlocks/folderpage.png")]
    
    public class FolderPage : PageData, IContainerPage
    {
       
    }
}