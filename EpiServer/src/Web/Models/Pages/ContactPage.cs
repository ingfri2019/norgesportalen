﻿using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Editor;
using EPiServer.Web;
using Norgesportalen.Business.Validation;
using Norgesportalen.Models.Blocks;
using Norgesportalen.Models.Pages.BaseClasses;
using System.ComponentModel.DataAnnotations;

namespace Norgesportalen.Models.Pages
{
    [ContentType(
        DisplayName = "Contact page",
        GroupName = Global.PageTypeGroupNames.OtherPageTypes,
        GUID = "3E537D80-BE8B-4CD7-B319-687DB339963E", 
        Description = "Contact page for the embassy, delegation or consulate general that contains detailed information about employees, opening hours and holidays."
    )]
    [SiteImageUrl("~/Models/SiteImagesForPageTypesAndBlocks/contactpage.png")]
    [Access(Roles = "WebAdmins, Administrators")]
    public class ContactPage : PageBase
    {
        [UIHint(UIHint.Image)]
        [Display(
            Name = "Heading image",
            Description = "",
            GroupName = Global.GroupNames.Content,
            Order = 4)]
        public virtual ContentReference HeadingImage { get; set; }

        [CultureSpecific]
        [Display(
           Name = "Heading image text",
           Description = "",
           GroupName = Global.GroupNames.Content,
           Order = 4)]
        public virtual string HeadingImageText { get; set; }

        [Display(Name = "Mission address field",
            Description = "",
            GroupName = Global.GroupNames.Content,
            Order = 45)]
        public virtual XhtmlString MissionAddressField { get; set; }

        [Display(
            Name = "Google maps block",
            Description = "",
            GroupName = Global.GroupNames.Content,
            Order = 47)]
        [AllowedTypes(new[] {
            typeof(GoogleMapsBlock)
        })]
        [MaxItemCount(1, ErrorMessage = "You can only add one Google maps block.")]
        public virtual ContentArea GoogleMapsBlock { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Add internal or external links to the article",
            GroupName = Global.GroupNames.Content,
            Order = 70,
            Description = "")]
        [AllowedTypes(new[] {
            typeof(PageData),
            typeof(MediaData),
            typeof(RelatedLinkBlock),
            typeof(AccordionContainerBlock),
            typeof(LinkWithTextContainerBlock),
            typeof(LinkWithTitleContainerBlock)
        })]
        public virtual ContentArea MainContentArea { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Add accordions to the page",
            Description = "",
            GroupName = Global.GroupNames.Content,
            Order = 90)]
        [AllowedTypes(new[] 
            {typeof(AccordionItemLinkListBlock),
            typeof(AccordionItemFreetextBlock),
            typeof(AccordionItemContactsBlock),
            typeof(RelatedLinkBlock),
            typeof(AccordionContainerBlock),
            typeof(LinkWithTextContainerBlock),
            typeof(LinkWithTitleContainerBlock)})]
        public virtual ContentArea AccordionContentArea { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Heading-link to news section page",
            Description = "",
            GroupName = Global.GroupNames.Content,
            Order = 100)]
        public virtual PageReference NewsListPageRef { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Link relevant articles to the page",
            Description = "",
            GroupName = Global.GroupNames.Content,
            Order = 110)]
        [AllowedTypes(new[] {
            typeof(InformationArticlePage),
            typeof(NewsPage),
            typeof(StatementPage),
            typeof(EventPage),
            typeof(CoreArticlePage)
        })]
        public virtual ContentArea NewsContentArea { get; set; }

        #region SOCIAL MEDIA

        [CultureSpecific]
        [Display(
            Name = "Add social media blocks",
            Description = "",
            GroupName = Global.GroupNames.Content,
            Order = 700)]
        [AllowedTypes(new[] { typeof(SocialMediaBlock) })]
        [MaxItemCount(3, ErrorMessage = "Maximum 3 social media blocks in social media contentarea")]
        public virtual ContentArea SocialMediaContentArea { get; set; }

        [CultureSpecific]
        [Display(
            Name = "SOME section list page",
            Description = "",
            GroupName = Global.GroupNames.Content,
            Order = 701)]
        [AllowedTypes(new[] { typeof(SocialMediaListpage) })]
        public virtual PageReference SocialmediaViewMorePageRef { get; set; }

        #endregion

        [CultureSpecific]
        [UIHint(UIHint.Textarea)]
        [Display(
           Name = "Teaser text",
           Description = "Short teaser text, typically no longer than 200 characters, and used in listings like on the countries and delegations overview page.",
           GroupName = EPiServer.DataAbstraction.SystemTabNames.Settings,
           Order = 1000)]
        public virtual string TeaserText { get; set; }


        /// <summary>
        /// Overridden in tests
        /// </summary>
        /// <returns></returns>
        public virtual bool IsInEditMode()
        {
            return PageEditing.PageIsInEditMode;
        }

    }
}