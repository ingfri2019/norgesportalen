﻿using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.SpecializedProperties;
using EPiServer.Web;
using Norgesportalen.Business.Publishing;
using Norgesportalen.Models.Pages.BaseClasses;
using System.ComponentModel.DataAnnotations;

namespace Norgesportalen.Models.Pages
{
    [ContentType(
        GroupName = Global.PageTypeGroupNames.VisaResidence,
        DisplayName = "Process step page",
        GUID = "BF8F15BE-F059-432E-95C1-DFE5C9720EB7",
        Description = "Process step page for the visa process. The process step pages are produced centrally and should include local elements."
    )]
    [Access(Roles = "WebAdmins, Administrators")]
    [SiteImageUrl("~/Models/SiteImagesForPageTypesAndBlocks/processsteppage.png")]

    public class ProcessStepPage : PageBase, IAbleToFetchFromCentral
    {

        // Helper property for setting "Fetch from" on page
        [Display(
            Name = Global.PropertyDescriptorValues.FetchDataFromHelperName,
            Description = Global.PropertyDescriptorValues.FetchDataFromHelperDescription,
            GroupName = Global.GroupNames.Content,
            Order = 1)]
        [CultureSpecific]
        [AllowedTypes(new[] { typeof(ProcessStepPage) })]
        public virtual ContentReference FetchDataFromHelper { get; set; }

        [Ignore]
        public override string MainIntro { get; set; }

        [CultureSpecific]
        [Display(Name = "Main body",
                 Description = "",
                 GroupName = Global.GroupNames.Content,
                 Order = 151)]
        public override XhtmlString MainBody { get; set; }


        [CultureSpecific]
        [UIHint("ProcessPageLinkCollection")]
        [Display(Name = "Links",
          Description = "",
          GroupName = Global.GroupNames.Content,
          Order = 152)]
        public virtual LinkItemCollection Links { get; set; }

        [CultureSpecific]
        [Display(
            Name = Global.PropertyDescriptorValues.AddALocalMainBody,
            Description = "",
            GroupName = Global.GroupNames.AdditionalContent,
            Order = 153)]
        public virtual XhtmlString LocalMainBody { get; set; }

        [CultureSpecific]
        [UIHint("ProcessPageLinkCollection")]
        [Display(
            Name = Global.PropertyDescriptorValues.AddLocalRelevantLinks,
            Description = "",
            GroupName = Global.GroupNames.AdditionalContent,
            Order = 154)]
        public virtual LinkItemCollection LocalLinks { get; set; }
    }
}
