﻿using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Web;
using Norgesportalen.Business.Publishing;
using Norgesportalen.Models.Blocks;
using System.ComponentModel.DataAnnotations;
using PageBase = Norgesportalen.Models.Pages.BaseClasses.PageBase;

namespace Norgesportalen.Models.Pages
{
    [ContentType(
        DisplayName = "Social media list page",
        GroupName = Global.PageTypeGroupNames.OtherPageTypes,
        Description = "A list of all social media channels for the embassy, delegation or consulate general.",
        GUID = "223FB47C-ACCE-4042-B68C-AB63CD535C71")]
    [SiteImageUrl("~/Models/SiteImagesForPageTypesAndBlocks/socialmedialistpage.png")]
    [Access(Roles = "WebAdmins, Administrators")]
    public  class SocialMediaListpage : PageBase, IAbleToFetchFromCentral
    {

        // Helper property for setting "Fetch from" on page
        [Display(
            Name = Global.PropertyDescriptorValues.FetchDataFromHelperName,
            Description = Global.PropertyDescriptorValues.FetchDataFromHelperDescription,
            GroupName = Global.GroupNames.Content,
            Order = 10000)]
        [AllowedTypes(new[] { typeof(SocialMediaListpage) })]
        [CultureSpecific]
        public virtual ContentReference FetchDataFromHelper { get; set; }

        [CultureSpecific]
        [Display(
           Name = "Social media",
           Description = "",
           GroupName = Global.GroupNames.Content,
           Order = 60)]
        [AllowedTypes(typeof(SocialMediaBlock))]
        public virtual ContentArea BlocksContentArea { get; set; }

        [CultureSpecific]
        [UIHint(UIHint.Textarea)]
        [Display(
            Name = Global.PropertyDescriptorValues.AddALocalIngress,
            Description = "",
            GroupName = Global.GroupNames.AdditionalContent,
            Order = 41)]
        public virtual string LocalMainIntro { get; set; }

        [CultureSpecific]
        [Display(
            Name = Global.PropertyDescriptorValues.AddALocalMainBody,
            Description = "",
            GroupName = Global.GroupNames.AdditionalContent,
            Order = 51)]
        public virtual XhtmlString LocalMainBody { get; set; }

        [CultureSpecific]
        [Display(
           Name = "Additional social media",
           Description = "",
           GroupName = Global.GroupNames.AdditionalContent,
           Order = 61)]
        [AllowedTypes(typeof(SocialMediaBlock))]
        public virtual ContentArea LocalSocialMediaContentArea { get; set; }
    }
}
