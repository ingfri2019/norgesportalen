﻿using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using Norgesportalen.Models.Blocks;
using Norgesportalen.Models.Pages.BaseClasses;
using System.ComponentModel.DataAnnotations;
using EPiServer.Web;

namespace Norgesportalen.Models.Pages
{

    // Page Model
    [ContentType(
        DisplayName = "Countries and delegations overview page",
        GroupName = Global.PageTypeGroupNames.OtherPageTypes,
        Description = "Overview of all countries and delegations",
        GUID = "4b695872-94fd-4039-a4b9-11e5e416b8b7",
        AvailableInEditMode = true)]
    [SiteImageUrl("~/Models/SiteImagesForPageTypesAndBlocks/countryanddelegationoverviewpage.png")]

    public class CountriesOverviewPage : SitePageData
    {

        [CultureSpecific]
        [Display(
            Name = "Main body",
            Description = Global.PropertyDescriptorValues.MainBodyDescription,
            GroupName = SystemTabNames.Content,
            Order = 1)]
        public virtual XhtmlString MainBody { get; set; }

        [CultureSpecific]
        [Display(
               Name = "Central info screen content",
               Description =
                   "An 'Info screen' page from which all local Info screen pages will fetch contents.",
               GroupName = Global.GroupNames.Config,
               Order = 3)]
        public virtual ContentReference CentralInfoScreenPage { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Global forms location",
            Description =
                "Location of globals forms that can be selected from a Forms page",
            GroupName = Global.GroupNames.Config,
            Order = 4)]
        [UIHint(UIHint.AssetsFolder)]
        public virtual ContentReference GlobalFormsLocation { get; set; }

        [CultureSpecific]
        [Display(
        Name = "National Emergency",
        Description = "This alert banner will be shown above any country-specific banners.",
        GroupName = Global.GroupNames.EmergencyAndInformation,
        Order = 10)]
        [AllowedTypes(new[] {
            typeof(EmergencyBlock) })]
        public virtual ContentArea NatEmergencyContentArea { get; set; }

        [Display(
        Name = "Show alert-banner on subpages",
        Description = "Show emergency banner on all the subpages for all countries as well.",
        GroupName = Global.GroupNames.EmergencyAndInformation,
        Order = 15)]
        public virtual bool ShowEmergencyOnSubpages { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Delegations listing header",
            Description = "",
            GroupName = Global.GroupNames.Content,
            Order = 20)]
        public virtual string DelegationsLinkListHeading { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Delegations listing",
            Description = "Pages that will be listed in the delegations listing with title, link and teaser text on the portal page. Typically of type HomePage (FrontPage).",
            GroupName = Global.GroupNames.Content,
            Order = 30)]
        [AllowedTypes(new[] { typeof(PageData) })]
        public virtual ContentArea DelegationsLinkList { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Consulate generals listing header",
            Description = "",
            GroupName = Global.GroupNames.Content,
            Order = 40)]
        public virtual string ConsulateLinkListHeading { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Consulate generals listing",
            Description = "Pages that will be listed in the consulate generals listing with title, link and teaser text on the portal page. Typically of type ContactPage.",
            GroupName = Global.GroupNames.Content,
            Order = 50)]
        [AllowedTypes(new[] { typeof(PageData) })]
        public virtual ContentArea ConsulatesLinkList { get; set; }


        #region FOOTER

        [CultureSpecific]
        [Display(
            Name = "Footer heading",
            Description = "",
            GroupName = Global.GroupNames.Footer,
            Order = 300)]
        public virtual string FooterTitle { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Footer left column",
            Description = "",
            GroupName = Global.GroupNames.Footer,
            Order = 320)]
        public virtual XhtmlString FooterLeft { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Footer center column",
            Description = "",
            GroupName = Global.GroupNames.Footer,
            Order = 330)]
        public virtual XhtmlString FooterCenter { get; set; }

        // --- Footer right column: -------------------------

        [CultureSpecific]
        [Display(
            Name = "Social media optional heading",
            Description = "Optional heading used above the social media links. If nothing is set, the text \"follow us\" will be displayed",
            GroupName = Global.GroupNames.Footer,
            Order = 340)]
        public virtual string SocialMediaOptionalHeading { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Social media blocks only",
            Description = "These blocks will appear in the right column",
            GroupName = Global.GroupNames.Footer,
            Order = 350)]
        [AllowedTypes(new[] {
            typeof(SocialMediaBlock)
        })]
        public virtual ContentArea FooterRight { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Newsletter subscription field",
            Description = "",
            GroupName = Global.GroupNames.Footer,
            Order = 360)]
        [AllowedTypes(new[] {
            typeof(NewsletterSubscriptionBlock)
        })]
        public virtual ContentArea NewsletterSubscriptionField { get; set; }

        #endregion FOOTER

    }
}
