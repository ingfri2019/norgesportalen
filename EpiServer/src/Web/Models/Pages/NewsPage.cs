﻿using EPiServer.DataAnnotations;
using Norgesportalen.Business.Publishing;
using Norgesportalen.Models.Interfaces;
using Norgesportalen.Models.Pages.BaseClasses;
using System.ComponentModel.DataAnnotations;

namespace Norgesportalen.Models.Pages
{
    [ContentType(DisplayName = "News page", GUID = "2044BE30-757F-4FF8-BA4F-6DDEFC7266B2",
        GroupName = Global.PageTypeGroupNames.OtherPageTypes,
        Description = "News articles for the embassy, delegation or consulate general.")]
    [SiteImageUrl("~/Models/SiteImagesForPageTypesAndBlocks/newspage.png")]
    [Access(Roles = "WebAdmins, Administrators")]
    public class NewsPage : BasicPageBase, IAbleToFetchFromCentral, ICanBeInNewsList
    {
        [Ignore]
        [CultureSpecific]
        [Display(
        Name = "Show ingress or teaser text in section page listing instead of image.",
        Description = "",
        GroupName = Global.GroupNames.Content,
        Order = 3)]
        public override bool ShowIngressOrTeaserText { get; set; }

    }
}   