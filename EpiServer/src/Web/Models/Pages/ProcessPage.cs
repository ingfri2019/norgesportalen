﻿using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using Norgesportalen.Business.Publishing;
using Norgesportalen.Models.Blocks;
using Norgesportalen.Models.Pages.BaseClasses;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Norgesportalen.Models.Pages
{
    [ContentType(
        GroupName = Global.PageTypeGroupNames.VisaResidence,
        DisplayName = "Process page",
        GUID = "B43C1B00-FE21-4F62-A2F6-4B3C9755572E",
        Description = "Process page for the visa process."
    )]
    [Access(Roles = "WebAdmins, Administrators")]
    [AvailableContentTypes(Include = new[] { typeof(ProcessStepPage) })]
    [SiteImageUrl("~/Models/SiteImagesForPageTypesAndBlocks/processpage.png")]

    public class ProcessPage : BasicPageBase, IAbleToFetchFromCentral
    {
        public override void SetDefaultValues(ContentType contentType)
        {
            base.SetDefaultValues(contentType);
            ShowIngressOrTeaserText = true;
        }

        // Helper property for setting "Fetch from" on page
        [Display(
            Name = Global.PropertyDescriptorValues.FetchDataFromHelperName,
            Description = Global.PropertyDescriptorValues.FetchDataFromHelperDescription,
            GroupName = Global.GroupNames.Content,
            Order = 1)]
        [CultureSpecific]
        [AllowedTypes(new[] { typeof(ProcessPage) })]
        public override ContentReference FetchDataFromHelper { get; set; }

        [CultureSpecific]
        [AllowedTypes(new[]
        {
            typeof(AccordionItemContactsBlock),
            typeof(AccordionItemFreetextBlock)
        })]
        [Display(
                Name = "Create or add blocks to the article",
                GroupName = Global.GroupNames.Content,
                Order = 70,
                Description = "")]
        public override ContentArea MainContentArea { get; set; }

        [CultureSpecific]
        [Display(Name = "Anchor link text",
                 Description = "",
                 GroupName = Global.GroupNames.Content,
                 Order = 100)]
        public virtual string AnchorLinkText { get; set; }

        [CultureSpecific]
        [Display(Name = "Links",
                 Description = "",
                 GroupName = Global.GroupNames.Content,
                 Order = 153)]
        [AllowedTypes(new[] {
            typeof(RelatedLinkBlock)
        })]
        public virtual ContentArea LinkContentArea { get; set; }

        [Ignore]
        public virtual List<ProcessStepPage> Pages { get; set; }

        #region Properties overridden in ProcessPage, and set to not in use:

        [ScaffoldColumn(false)]
        public override bool HideDate { get; set; }

        [ScaffoldColumn(false)]
        public override string Location { get; set; }

        [ScaffoldColumn(false)]
        public override ContentReference MainImage { get; set; }

        [ScaffoldColumn(false)]
        public override string ImageText { get; set; }

        [ScaffoldColumn(false)]
        public override ContentArea RightColumContentArea { get; set; }

        [ScaffoldColumn(false)]
        public override string SuggestedPageTypes { get; set; }

        [ScaffoldColumn(false)]
        public override string IconImage { get; set; }

        [ScaffoldColumn(false)]
        public virtual ContentArea LocalLinkContentArea { get; set; }

        #endregion
    }
}

