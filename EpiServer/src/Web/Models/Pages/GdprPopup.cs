﻿using EPiServer.Core;
using EPiServer.DataAnnotations;
using System.ComponentModel.DataAnnotations;

namespace Norgesportalen.Models.Pages
{
    [ContentType(
        GroupName = Global.PageTypeGroupNames.Admin, 
        DisplayName = "GDPR popup", 
        GUID = "4987410d-b23e-482a-878b-0ee62c65bd66", 
        Description = "Content for the GDPR info popup footer shown on all pages.")]
    [Access(Roles = "WebAdmins, Administrators")]
    [SiteImageUrl("~/Models/SiteImagesForPageTypesAndBlocks/gdpr.png")]
    public class GdprPopupPage : PageData
    {
        [CultureSpecific]
        [Required]
        [Display(
            Name = "Logo text",
            Description = "",
            GroupName = Global.GroupNames.Content,
            Order = 105)]
        public virtual string LogoText { get; set; }

        [CultureSpecific]
        [Required]
        [Display(
            Name = "Content",
            Description = "",
            GroupName = Global.GroupNames.Content,
            Order = 115)]
        public virtual XhtmlString MainColumn { get; set; }

        [CultureSpecific]
        [Required]
        [Display(
            Name = "Agree button text",
            Description = "",
            GroupName = Global.GroupNames.Content,
            Order = 120)]
        public virtual string AgreeText { get; set; }

    }
}   