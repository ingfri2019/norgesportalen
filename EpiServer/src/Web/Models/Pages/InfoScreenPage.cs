﻿using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using Norgesportalen.Models.Blocks;
using Norgesportalen.Models.Pages.BaseClasses;
using System.ComponentModel.DataAnnotations;

namespace Norgesportalen.Models.Pages
{
    [ContentType(
        DisplayName = "Info screen", GroupName = Global.PageTypeGroupNames.OtherPageTypes,
        GUID = "f5938ac1-a52f-4040-8b97-40606c2e28ea",
        Description = "Creates a presentation screen that allows the mission to add social, external and internal links to a page that automatically flips between added pages.")]
    [Access(Roles = "WebAdmins, Administrators")]
    [SiteImageUrl("~/Models/SiteImagesForPageTypesAndBlocks/InfoScreen.png")]
    public class InfoScreenPage : SitePageData
    {
        [ScaffoldColumn(false)]
        public override string MetaTitle { get; set; }

        [ScaffoldColumn(false)]
        public override string MetaDescription { get; set; }

        [ScaffoldColumn(false)]
        public override string SeoTitle { get; set; }

        [ScaffoldColumn(false)]
        public override string SeoDescription { get; set; }

        [ScaffoldColumn(false)]
        public override ContentReference FaceBookImage { get; set; }

        [ScaffoldColumn(false)]
        public override bool DisableIndexing { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Header text",
            Description = "",
            Order = 10)]
        public virtual string HeaderText { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Change interval (seconds)",
            Description = "",
            Order = 30)]
        [Range(15, 90)]
        public virtual int ChangeInterval { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Internal pages",
            Order = 40,
            Description = "")]
        [AllowedTypes(new[] { typeof(InfoScreenInternalLinkBlock) })]
        public virtual ContentArea InternalLinks { get; set; }

        [CultureSpecific]
        [Display(
            Name = "External pages",
            Order = 50,
            Description = "")]
        [AllowedTypes(new[] { typeof(InfoScreenExternalLinkBlock) })]
        public virtual ContentArea ExternalLinks { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Social media pages (only Facebook and Twitter supported)",
            Order = 60,
            Description = "")]
        [AllowedTypes(new[] { typeof(SocialMediaBlock) })]
        public virtual ContentArea SocialMediaLinks { get; set; }

        public override void SetDefaultValues(EPiServer.DataAbstraction.ContentType contentType)
        {
            base.SetDefaultValues(contentType);
            this.VisibleInMenu = false;
            this.DisableIndexing = true;
        }
    }
}