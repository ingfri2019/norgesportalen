﻿using BVNetwork.EPiSendMail;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.SpecializedProperties;
using EPiServer.Validation;
using EPiServer.Web;
using Norgesportalen.Models.Blocks;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Norgesportalen.Models.Pages
{
    [SiteContentType(
        GroupName = Global.PageTypeGroupNames.Newsletter,
        DisplayName = "Newsletter page",
        GUID = "C810AC68-43A7-4046-9322-2AA1163A6880",
        Description = "Pagetype for creation of a newsletter")]
    [SiteImageUrl("~/Models/SiteImagesForPageTypesAndBlocks/newsletter.png")]

    public class NewsletterPage : NewsletterBase
    {
        [Display(Name = "Logo",
            Order = 10,
            GroupName = SystemTabNames.Content)]
        public virtual LogoBlock LogoBlock { get; set; }


        [CultureSpecific]
        [Display(Name = "Unsubscription page",
            Order = 10,
            GroupName = SystemTabNames.Content)]
        [AllowedTypes(typeof(NewsletterUnsubscribePage))]
        public virtual PageReference UnsubscribePage { get; set; }

        [CultureSpecific]
        [UIHint(UIHint.Textarea)]
        [StringLength(400, ErrorMessage = "\"Ingress\" cannot be longer than 400 characters.")]
        [Display(Name = "Ingress",
            Description = "This field is limited to 400 characters.",
            GroupName = Global.GroupNames.Content,
            Order = 200)]
        public virtual string MainIntro { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Add articles here",
            GroupName = Global.GroupNames.Content,
            Order = 300,
            Description = "Articles Content Area")]
        [AllowedTypes(typeof(NewsPage), typeof(EventPage), typeof(StatementPage), typeof(InformationArticlePage), typeof(CoreArticlePage), typeof(AccordionContainerBlock),
            typeof(LinkWithTextContainerBlock),
            typeof(LinkWithTitleContainerBlock))]
        public virtual ContentArea MainContentArea { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Heading for added links",
            Description = "Title above linklist",
            GroupName = Global.GroupNames.Content,
            Order = 400)]
        public virtual string LinkCollectionTitle { get; set; }

        [CultureSpecific]
        [Display(Name = "Links",
            Description = "",
            GroupName = Global.GroupNames.Content,
            Order = 500)]
        public virtual LinkItemCollection Links { get; set; }

        [Ignore]
        [Display( Name = "From Address (read-only)",
            Description = "The email address that will show as the from field in most email clients",
            GroupName = "Information",             
            Order = 600)]
        public override string MailSender { get; set; }

    }

    public class NewsletterPageValidator : IValidate<NewsletterPage>
    {
        IEnumerable<ValidationError> IValidate<NewsletterPage>.Validate(NewsletterPage page)
        {

            var validationErrors = new List<ValidationError>();

            var fetchDataFromHelper = page.Property["FetchDataFromHelper"]?.Value as PageReference;
            if (fetchDataFromHelper == null && page.UnsubscribePage == null)           
            {
                validationErrors.Add(
                    new ValidationError()
                    {
                        ErrorMessage = "Unsubscription page must be set.",
                        PropertyName = page.GetPropertyName(pg => pg.UnsubscribePage),
                        Severity = ValidationErrorSeverity.Error,
                        ValidationType = ValidationErrorType.AttributeMatched
                    }
                );
            }

            LogoBlock logoBlock = page.LogoBlock;
            if (logoBlock == null || string.IsNullOrWhiteSpace(logoBlock.LogoTitle))
            {
                validationErrors.Add(
                    new ValidationError() {
                        ErrorMessage = "LogoBlock with logo and page title must be set.",
                        PropertyName = page.GetPropertyName(pg => pg.LogoBlock.LogoTitle),
                        Severity = ValidationErrorSeverity.Error,
                        ValidationType = ValidationErrorType.AttributeMatched
                    }
                );
            }


            return validationErrors;
        }

    }
}