﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;
using EPiServer.Validation;
using EPiServer.Web;
using Norgesportalen.Business;
using Norgesportalen.Business.Validation;
using Norgesportalen.Helpers;
using Norgesportalen.Models.Blocks;
using Norgesportalen.Models.Interfaces;
using Norgesportalen.Models.Media;
using Norgesportalen.Models.Pages.BaseClasses;

namespace Norgesportalen.Models.Pages
{
    [ContentType(
        DisplayName = "Event page",
        GroupName = Global.PageTypeGroupNames.NewsAndEvents,
        GUID = "4E40267F-1036-486E-A295-134905AA8A23",
        Description = "Events where the embassy, delegation or consulate general is involved."
    )]
    [AvailableContentTypes(Exclude = new[] { typeof(PageData) })]
    [SiteImageUrl("~/Models/SiteImagesForPageTypesAndBlocks/eventpage.png")]
    public class EventPage : BasicPageBase , ICanBeInNewsList
    {
        [Required]
        // ReSharper disable once Mvc.TemplateNotResolved
        [UIHint(Constants.UIHint.FloatingString)]
        [SelectOne(SelectionFactoryType = typeof(TimezoneHelpers.TzSelectionFactory))]
        [Display(
            Name = "Timezone",
            Description = "Timezone event takes place, Start/End time use this value to display correct local time",
            GroupName = Global.GroupNames.Content,
            Order = 1)]
        public virtual string EventTimezoneId { get; set; }

        [Required]
        [Display(
            Name = "When does the event start?",
            Description = "Note: If input is given in a different timezone than the event, adjust time accordingly and output will be correct",
            GroupName = SystemTabNames.Content,
            Order = 5)]
        [Range(typeof(DateTime), "1/1/1814", "1/1/9999")]
        public virtual DateTime EventStartDate { get; set; }

        [Required]
        [Display(
            Name = "When does the event end?",
            Description = "Note: If input is given in a different timezone than the event, adjust time accordingly and output will be correct",
            GroupName = SystemTabNames.Content,
            Order = 10)]

        [Range(typeof(DateTime), "1/1/1814", "1/1/9999")]
        public virtual DateTime EventStopDate { get; set; }

        [UIHint(Constants.UIHint.LineBreakString)]
        [StringLength(200)]
        [CultureSpecific]
        [Display(
            Name = "Where is the event?",
            Description = "Geographic place",
            GroupName = Global.GroupNames.Content,
            Order = 15)]
        public override string Location { get; set; }

        [CultureSpecific]
        [Display(
            Name = "What type of event is it?",
            Description = "",
            GroupName = Global.GroupNames.Content,
            Order = 20)]
        public virtual string What { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Link to dependent page or information",
            Description = "",
            GroupName = Global.GroupNames.Content,
            Order = 25)]
        public virtual Url EventWebsiteUrl { get; set; }

        [CultureSpecific]
        [AllowedTypes(typeof(GoogleMapsBlock))]
        [MaxItemCount(1, ErrorMessage = "You can only add a single map element to the field")]
        [Display(
            Name = "Add a map to the location with a google map block",
            GroupName = Global.GroupNames.Content,
            Order = 30,
            Description = "")]
        public override ContentArea MainContentArea { get; set; }

        [UIHint(UIHint.Image)]
        [Display(
            Name = "Add a TOP (main) image",
            GroupName = Global.GroupNames.Content,
            Order = 35,
            Description = "")]
        public override ContentReference MainImage { get; set; }

        //Ingress has order 40

        [CultureSpecific]
        [Display(Name = "First field main body",
            Description = Global.PropertyDescriptorValues.MainBodyDescription,
            GroupName = Global.GroupNames.Content,
            Order = 50)]
        public override XhtmlString MainBody { get; set; }

        [UIHint(UIHint.Image)]
        [CultureSpecific]
        [Display(
            Name = "MIDDLE image location",
            GroupName = Global.GroupNames.Content,
            Order = 60,
            Description = "")]
        public virtual ContentReference MainImageMiddle { get; set; }

        [CultureSpecific]
        [Display(Name = "Second field heading",
            Description = "",
            Prompt = "Page heading",
            Order = 70,
            GroupName = Global.GroupNames.Content)]
        public virtual string HeadingSecond { get; set; }

        [CultureSpecific]
        [Display(Name = "Second field main body",
            Description = Global.PropertyDescriptorValues.MainBodyDescription,
            GroupName = Global.GroupNames.Content,
            Order = 80)]
        public virtual XhtmlString MainBodySecond { get; set; }

        [CultureSpecific]
        [AllowedTypes(typeof(ImageFile))]
        [Display(
            Name = "Image carousel",
            GroupName = Global.GroupNames.Content,
            Order = 90,
            Description = "")]
        [MaxItemCount(6, ErrorMessage = "You can add maximum 6 images")]
        public virtual ContentArea ImageCarouselArea { get; set; }

        [CultureSpecific]
        [Display(Name = "If you add related events please add a related heading",
            Description = "",
            Prompt = "Other related events",
            Order = 100,
            GroupName = Global.GroupNames.Content)]
        public virtual string RelatedEventsHeading { get; set; }
        
        [CultureSpecific]
        [AllowedTypes(typeof(EventPage))]
        [Display(
            Name = "Add related events to this event",
            GroupName = SystemTabNames.Content,
            Order = 110)]
        public virtual ContentArea RelatedEventPages { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Image text",
            Description = "",
            GroupName = Global.GroupNames.NotInUse,
            Order = 200)]
        public override string ImageText { get; set; }

        [CultureSpecific]
        [Display(Name = "Alternative heading",
            Description = "",
            Prompt = "Page heading",
            Order = 5,
            GroupName = Global.GroupNames.NotInUse)]
        public override string Heading { get; set; }


        #region [ Ignore properties in base classes ]

        #region [ BasicPageBase ]
        [Ignore]
        public override ContentReference FetchDataFromHelper { get; set; }

        [Ignore]
        public override bool ShowIngressOrTeaserText { get; set; }

        [Ignore]
        public override string TeaserText { get; set; }

        [Ignore]
        public override ContentArea RightColumContentArea { get; set; }

        [Ignore]
        public override string LocalMainIntro { get; set; }  
        
        [Ignore]
        public override XhtmlString LocalMainBody { get; set; }

        [Ignore]
        public override ContentArea LocalMainContentArea { get; set; }

        #endregion [ BasicPageBase ]

        [Ignore]
        public virtual DateTime StartTimeConverted { get; set; }
        [Ignore]
        public virtual DateTime EndTimeConverted { get; set; }

        // Ignore unnecessary properties
        [Ignore]
        public override bool HideDate { get; set; }
        [Ignore]
        public override string TabInfo_AdditionalContent_NotFetchedContentTabReadonly { get; set; }
        [Ignore]
        public override string TabInfo_AdditionalContent_NotFetchedContentTabNotReadonly { get; set; }
        [Ignore]
        public override string TabInfo_Content_FetchedContentTabReadonly { get; set; }

        #endregion [ Ignore properties in base classes ]

        public override void SetDefaultValues(ContentType contentType)
        {
            base.SetDefaultValues(contentType);
            var homePg = GetHomePage(this.ParentLink);
            if (homePg != null)
            {
                EventTimezoneId = homePg.DeaultTimezoneId;
            }
        }

        // Moved to SitePageData
        //public static HomePage GetHomePage(PageReference pgRef)
        //{
        //    var pg = DataFactory.Instance.GetPage(pgRef);
        //    if (pg is HomePage)
        //        return pg as HomePage;
        //    if (pg is CountriesOverviewPage || pg.ParentLink == null)
        //        return null;
        //    return GetHomePage(pg.ParentLink);
        //}
    }

    public class EventPageValidator : IValidate<EventPage>
    {
        IEnumerable<ValidationError> IValidate<EventPage>.Validate(EventPage page)
        {
            var validationErrors = new List<ValidationError>();

            if (page.EventStartDate.Year < 2000)
            {
                validationErrors.Add(
                    new ValidationError()
                    {
                        ErrorMessage = "Event start date must be set, and must be newer than 1/1 2000.",
                        PropertyName = page.GetPropertyName(pg => pg.EventStartDate),
                        Severity = ValidationErrorSeverity.Error,
                        ValidationType = ValidationErrorType.AttributeMatched
                    }
                );

            }

            if (page.EventStopDate.Year < 2000)
            {
                validationErrors.Add(
                    new ValidationError()
                    {
                        ErrorMessage = "Event end date must be set, and must be newer than 1/1 2000.",
                        PropertyName = page.GetPropertyName(pg => pg.EventStopDate),
                        Severity = ValidationErrorSeverity.Error,
                        ValidationType = ValidationErrorType.AttributeMatched
                    }
                 );
            }

            // Even if the user adds a valid date outside of the allowed range, we can still inform him/her that stop date is before start date:
            if (page.EventStartDate.Year > 0 && page.EventStopDate.Year > 0 && page.EventStartDate > page.EventStopDate)
            {
                validationErrors.Add(
                    new ValidationError()
                    {
                        ErrorMessage = "Event end date can not be before the start date.",
                        PropertyName = page.GetPropertyName(pg => pg.EventStopDate),
                        Severity = ValidationErrorSeverity.Error,
                        ValidationType = ValidationErrorType.AttributeMatched
                    }
                );
            }

            if (page.MainImageMiddle!=null)
            {
                var val = new ImageValidatorHelper(page);
                var imgError = val.ValidateImage(page.MainImageMiddle, "MainImageMiddle");
                if (imgError != null)
                {
                    validationErrors.Add(imgError);
                }
            }

            if (page.ImageCarouselArea != null && page.ImageCarouselArea.Items.Any())
            {
                var list = page.ImageCarouselArea.Items.Select(item => item.ContentLink)
                    .Where(image => image != null).ToList();
                
                var val = new ImageValidatorHelper(page);
                var imgErrorList = val.ValidateImages(list, "ImageCarouselArea");

                if (imgErrorList != null)
                {
                    validationErrors.AddRange(imgErrorList);
                }
            }

            return validationErrors;
        }
    }
}