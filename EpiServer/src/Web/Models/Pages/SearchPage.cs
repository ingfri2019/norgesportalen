﻿using EPiServer.DataAnnotations;
using Norgesportalen.Models.Pages.BaseClasses;

namespace Norgesportalen.Models.Pages
{
    [ContentType(
        GroupName = Global.PageTypeGroupNames.Admin,
        DisplayName = "Search page", 
        GUID = "49A9F954-BCDA-4CEC-9800-FFAE5D22A4EF", 
        Description = "Search page for the embassy, delegation or consulate general."
    )]
    [SiteImageUrl("~/Models/SiteImagesForPageTypesAndBlocks/searchpage.png")]
    [Access(Roles = "WebAdmins, Administrators")]
    public class SearchPage : SitePageData
    {

    }
}