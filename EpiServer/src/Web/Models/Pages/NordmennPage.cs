﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Web;
using Norgesportalen.Business.Publishing;
using Norgesportalen.Models.Pages.BaseClasses;

namespace Norgesportalen.Models.Pages
{
    [ContentType(
        DisplayName = "Nordmenn page",
        GroupName = Global.PageTypeGroupNames.OtherPageTypes,
        GUID = "9D2A78BD-5A7D-4AC2-BEDB-D81EF00FFA6A", 
        Description = "This page type is meant to present Information and services directed towards Norwegian citizens. Ex-pats, visiting or permanently resided Norwegians. This is an admin-page and there should only be one of these pr mission. It is designed to present content in Norwegian."
    )]
    [SiteImageUrl("~/Models/SiteImagesForPageTypesAndBlocks/NordmennPage.png")]
    [Access(Roles = "WebAdmins, Administrators")]
    public class NordmennPage : PageBase, IAbleToFetchFromCentral
    {
        // Helper property for setting "Fetch from" on page
        [Display(
            Name = Global.PropertyDescriptorValues.FetchDataFromHelperName,
            Description = Global.PropertyDescriptorValues.FetchDataFromHelperDescription,
            GroupName = Global.GroupNames.Content,
            Order = 1)]
        [AllowedTypes(new[] { typeof(NordmennPage) })]
        [CultureSpecific]
        public virtual ContentReference FetchDataFromHelper { get; set; }

        [Ignore]
        public virtual List<PageBase> Pages { get; set; }

        #region LOCAL CONTENT

        [CultureSpecific]
        [UIHint(UIHint.Textarea)]
        [Display(
            Name = Global.PropertyDescriptorValues.AddALocalIngress,
            Description = "",
            GroupName = Global.GroupNames.AdditionalContent,
            Order = 140)]
        public virtual string LocalMainIntro { get; set; }

        [CultureSpecific]
        [Display(
            Name = Global.PropertyDescriptorValues.AddALocalMainBody,
            Description = "",
            GroupName = Global.GroupNames.AdditionalContent,
            Order = 150)]
        public virtual XhtmlString LocalXHtml { get; set; }


        #endregion

    }
}
