﻿using EPiServer.DataAnnotations;
using Norgesportalen.Models.Pages.BaseClasses;

namespace Norgesportalen.Models.Pages
{
    [ContentType(
        DisplayName = "Forms page",
        GroupName = Global.PageTypeGroupNames.OtherPageTypes,
        GUID = "CF55526F-A815-4DC8-B1E9-E8511E49F678",
        Description = "Page type for form")]

    public class BasicFormsPage : BaseFormsPage
    {
    }
}