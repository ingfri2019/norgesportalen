﻿using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using Norgesportalen.Models.Pages.BaseClasses;
using System.ComponentModel.DataAnnotations;

namespace Norgesportalen.Models.Pages
{
    [ContentType(
        DisplayName = "List page", 
        GroupName = Global.PageTypeGroupNames.OtherPageTypes,
        GUID = "FD180801-A5DC-4C32-BCEF-44EC54366C74",
        Description = "A list of all news, events and statements for an embassy, delegation or consulate general. The list can be filtered by the visitor to only show news and/or events and/or statements."
    )]
    [SiteImageUrl("~/Models/SiteImagesForPageTypesAndBlocks/ListPage.png")]
    // aka NewsStatementsArticlesAndEventsListPage
    public class FilteredListPage : PageBase
    {
        [Display(Name = "Number of articles pr page", Order = 1)]
        [Range(1, 1000)]
        public virtual int PageSize
        {
            get
            {
                var v = this.GetPropertyValue(p => p.PageSize);

                if (v > 0)
                    return v;
                else
                    return 12;
            }
            set { this.SetPropertyValue(p => p.PageSize, value); }
        }

        [Ignore]
        public override string MainIntro { get; set; }

        [Ignore]
        public override XhtmlString MainBody { get; set; }

        public override void SetDefaultValues(ContentType contentType)
        {
            base.SetDefaultValues(contentType);
            this.PageSize = 12;
        }
    }

}