﻿using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Web;
using Norgesportalen.Business.Publishing;
using Norgesportalen.Models.Pages.BaseClasses;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Norgesportalen.Models.Pages
{
    [ContentType(
        DisplayName = "Section list page",
        GroupName = Global.PageTypeGroupNames.OtherPageTypes,
        GUID = "3A4C1C64-97CB-4901-9E8B-7BC9253BD88D", 
        Description = "An automated list of all underlying section pages like visa and residence permit, study and research and work in Norway. "
    )]
    [SiteImageUrl("~/Models/SiteImagesForPageTypesAndBlocks/sectionlistpage.png")]
    [Access(Roles = "WebAdmins, Administrators")]
    public class SectionListPage : PageBase, IAbleToFetchFromCentral
    {
        // Helper property for setting "Fetch from" on page
        [Display(
            Name = Global.PropertyDescriptorValues.FetchDataFromHelperName,
            Description = Global.PropertyDescriptorValues.FetchDataFromHelperDescription,
            GroupName = Global.GroupNames.Content,
            Order = 1)]
        [AllowedTypes(new[] { typeof(SectionListPage) })]
        [CultureSpecific]
        public virtual ContentReference FetchDataFromHelper { get; set; }

        [Ignore]
        public virtual List<PageBase> Pages { get; set; }

        #region LOCAL CONTENT

        [CultureSpecific]
        [UIHint(UIHint.Textarea)]
        [Display(
            Name = Global.PropertyDescriptorValues.AddALocalIngress,
            Description = "",
            GroupName = Global.GroupNames.AdditionalContent,
            Order = 140)]
        public virtual string LocalMainIntro { get; set; }

        [CultureSpecific]
        [Display(
            Name = Global.PropertyDescriptorValues.AddALocalMainBody,
            Description = "",
            GroupName = Global.GroupNames.AdditionalContent,
            Order = 150)]
        public virtual XhtmlString LocalXHtml { get; set; }


        #endregion

    }
}
