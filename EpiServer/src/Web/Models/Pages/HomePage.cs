﻿using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Editor;
using EPiServer.Shell.ObjectEditing;
using EPiServer.SpecializedProperties;
using Norgesportalen.Business.EditorDescriptors;
using Norgesportalen.Business.Validation;
using Norgesportalen.Helpers;
using Norgesportalen.Models.Blocks;
using Norgesportalen.Models.Pages.BaseClasses;
using System.ComponentModel.DataAnnotations;

namespace Norgesportalen.Models.Pages
{
    [SiteContentType(
        DisplayName = "Front page",
        GroupName = Global.PageTypeGroupNames.OtherPageTypes,
        GUID = "820c222d-46e0-48b7-98e9-ffa6fad20aab",
        Description = "Front page for the embassy or \"side akkrediterte land\" or missions.")]
    [SiteImageUrl("~/Models/SiteImagesForPageTypesAndBlocks/frontpage.png")]
    [Access(Roles = "WebAdmins, Administrators")]
    public class HomePage : SitePageData
    {
        [SelectOne(SelectionFactoryType = typeof(TimezoneHelpers.TzSelectionFactory))]
        [Display(
            Name = "Timezone",
            Description = "Default timezone linked to this countries events etc.",
            GroupName = Global.GroupNames.Content,
            Order = 5)]
        public virtual string DeaultTimezoneId { get; set; }

        #region TOP TASKS

        /// Services Area --------------------------------------------
        [Display(
            Name = "Front page background image",
            Description = "",
            Order = 10)]
        [BackingType(typeof(PropertyString))]
        [SelectOne(SelectionFactoryType = typeof(BackgroundSelectionFactory))]
        public virtual string TopTasksBackground { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Top tasks",
            GroupName = Global.GroupNames.TopTasks,
            Order = 15,
            Description = "")]
        [AllowedTypes(new[] {
            typeof(SectionsPage),
            typeof(InformationArticlePage),
            typeof(SectionListPage),
            typeof(CoreArticlePage)
        })]
        [MaxItemCount(4, ErrorMessage = "Too many top tasks. Maximum 4.")]
        public virtual ContentArea TopTasksContentArea { get; set; }

        #region TOP TASKS LIST

        [Display(
            Name = "Link list title",
            Description = "",
            GroupName = Global.GroupNames.TopTasks,
            Order = 18)]
        public virtual string TopTasksTitle { get; set; } // --> Former "Title"

        [Display(
            Name = "Link list",
            Description = "This list can have a maximum of four items.",
            GroupName = Global.GroupNames.TopTasks,
            Order = 20)]
        [MaxItemCount(4, ErrorMessage = "Too many items in Link list. Maximum 4.")]
        public virtual LinkItemCollection TopTasksLinkList { get; set; }

        [Display(
            Name = "Link to read more",
            Description = "Add maximum one item here.",
            GroupName = Global.GroupNames.TopTasks,
            Order = 24)]
        [MaxItemCount(1, ErrorMessage = "Too many items in \"link to read more\". Maximum 1.")]
        public virtual LinkItemCollection TopTasksTitleAndReadmoreLink { get; set; }

        #endregion TOP TASKS LIST

        #endregion TOP TASKS

        #region NEWS

        [CultureSpecific]
        [Display(
            Name = "Link to news, events and statements list page",
            Description = "",
            GroupName = Global.GroupNames.Content,
            Order = 25)]
        public virtual PageReference NewsListPageRef { get; set; }

        [CultureSpecific]
        [Display(
            Name = "News, events and statements",
            Description = "",
            GroupName = Global.GroupNames.Content,
            Order = 30)]
        [AllowedTypes(new[] {
            typeof(NewsPage),
            typeof(EventPage),
            typeof(CoreArticlePage) })]
        [MaxItemCount(3, ErrorMessage = "Too many news/statements/events in News. Maximum 3.")]
        public virtual ContentArea NewsContentArea { get; set; }

        #endregion NEWS

        #region SOCIAL MEDIA

        [CultureSpecific]
        [Display(
            Name = "Social media",
            Description = "",
            GroupName = Global.GroupNames.Content,
            Order = 40)]
        [AllowedTypes(new[] { typeof(SocialMediaBlock) })]
        [MaxItemCount(3, ErrorMessage = "Maximum 3 social media blocks in social media area")]
        public virtual ContentArea SocialMediaContentArea { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Link to social media list page",
            Description = "",
            GroupName = Global.GroupNames.Content,
            Order = 45)]
        [AllowedTypes(new[] { typeof(SocialMediaListpage) })]
        public virtual PageReference SocialmediaViewMorePageRef { get; set; }

        #endregion

        #region THEMES

        [CultureSpecific]
        [Display(
            Name = "Link to themes sectionlist page",
            Description = "",
            GroupName = Global.GroupNames.Content,
            Order = 50)]
        public virtual PageReference ThemesSectionListPageRef { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Themes first row",
            Description = "",
            GroupName = Global.GroupNames.Content,
            Order = 55)]
        [AllowedTypes(new[] { typeof(ThemePage),
            typeof(CoreArticlePage) })]
        [MaxItemCount(3, ErrorMessage = "Too many items in themes first row (Max 3)")]
        public virtual ContentArea ThemesLargeContentArea { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Themes second row",
            Description = "",
            GroupName = Global.GroupNames.Content,
            Order = 60)]
        [AllowedTypes(new[] { typeof(ThemePage),
            typeof(CoreArticlePage) })]
        [MaxItemCount(3, ErrorMessage = "Too many items in themes second row. (Max 3)")]
        public virtual ContentArea ThemesContentArea { get; set; }

        #endregion THEMES

        #region EmergencyAndInformation

        [CultureSpecific]
        [Display(
            Name = "Emergency",
            Description = "",
            GroupName = Global.GroupNames.EmergencyAndInformation,
            Order = 100)]
        [AllowedTypes(new[] {typeof(EmergencyBlock) })]
        [PropertyEditRestriction(new string[] { "WebAdmins", "Administrators" })]
        public virtual ContentArea EmergencyContentArea { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Information",
            Description = "Contentarea for informationblocks",
            GroupName = Global.GroupNames.EmergencyAndInformation,
            Order = 110)]
        [AllowedTypes(new[] {typeof(InformationBlock) })]
        [PropertyEditRestriction(new string[] { "WebAdmins", "Administrators" })]
        public virtual ContentArea InformationContentArea { get; set; }

        #endregion

        #region HEADER

        [Display(
            Name = "Logo",
            GroupName = Global.GroupNames.Header,
            Order = 200)]
        public virtual LogoBlock HeaderLogo { get; set; }

        #endregion HEADER

        #region FOOTER

        [CultureSpecific]
        [Display(
            Name = "Footer heading",
            Description = "",
            GroupName = Global.GroupNames.Footer,
            Order = 300)]
        public virtual string FooterTitle { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Footer left column",
            Description = "",
            GroupName = Global.GroupNames.Footer,
            Order = 320)]
        public virtual XhtmlString FooterLeft { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Footer center column",
            Description = "",
            GroupName = Global.GroupNames.Footer,
            Order = 330)]
        public virtual XhtmlString FooterCenter { get; set; }

        // --- Footer right column: -------------------------

        [CultureSpecific]
        [Display(
            Name = "Social media optional heading",
            Description = "Optional heading used above the social media links. If nothing is set, the text \"follow us\" will be displayed",
            GroupName = Global.GroupNames.Footer,
            Order = 340)]
        public virtual string SocialMediaOptionalHeading { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Social media blocks only",
            Description = "These blocks will appear in the right column",
            GroupName = Global.GroupNames.Footer,
            Order = 350)]
        [AllowedTypes(new[] {
            typeof(SocialMediaBlock)
        })]
        public virtual ContentArea Footer3 { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Newsletter subscription field",
            Description = "",
            GroupName = Global.GroupNames.Footer,
            Order = 360)]
        [AllowedTypes(new[] {
            typeof(NewsletterSubscriptionBlock)
        })]
        public virtual ContentArea NewsletterSubscriptionField { get; set; }


        #endregion FOOTER

        #region MENU

        [CultureSpecific]
        [Display(
        Name = "Hide main menu",
        Description = "When this box is checked, the main menu will be hidden. This is useful on pages with few or no sub-pages.",
        GroupName = Global.GroupNames.Menu,
        Order = 400)]
        public virtual bool HideMainMenu { get; set; }


        [CultureSpecific]
        [Display(
        Name = "Link to search page",
        Description = "",
        GroupName = Global.GroupNames.Menu,
        Order = 405)]
        [AllowedTypes(new[] { typeof(SearchPage) })]
        public virtual PageReference SearchPageLink { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Link to horizontal menu page",
            Description = "",
            GroupName = Global.GroupNames.Menu,
            Order = 410)]
        public virtual PageReference HorizontalMenuPage { get; set; }

        [Display(
            Name = "Maximum number of horizontal menu items (0-30)",
            Description = "",
            GroupName = Global.GroupNames.Menu,
            Order = 420)]
        [Range(0, 30)]
        public virtual int MaximumNumberOfHorizontalMenuItems { get; set; }


        [Display(
            Name = "Maximum number of first level menu items",
            Description = "",
            GroupName = Global.GroupNames.Menu,
            Order = 430)]
        [Range(0, 4)]
        public virtual int MaximumNumberOfFirstLevelMenuItems { get; set; }

        [Display(
            Name = "Maximum number of second level menu items",
            Description = "",
            GroupName = Global.GroupNames.Menu,
            Order = 440)]
        [Range(0, 14)]
        public virtual int MaximumNumberOfSecondLevelMenuItems { get; set; }

        #endregion Menu

        [Display(
            Name = "Name in countries and missions overview page",
            Description = "",
            GroupName = SystemTabNames.Settings,
            Order = 600)]
        public virtual string NameInCountriesOverview { get; set; }

        // General
        [CultureSpecific]
        [Display(
            Name = "Teaser text",
            Description = "",
            GroupName = SystemTabNames.Settings,
            Order = 610)]
        public virtual string ShortText { get; set; }

        public override void SetDefaultValues(ContentType contentType)
        {
            base.SetDefaultValues(contentType);
            MaximumNumberOfFirstLevelMenuItems = 3;
            MaximumNumberOfSecondLevelMenuItems = 7;
            NameInCountriesOverview = PageName;
        }

        /// <summary>
        /// Overridden in tests
        /// </summary>
        /// <returns></returns>
        public virtual bool IsInEditMode()
        {
            return PageEditing.PageIsInEditMode;
        }

        // todo: move to view model
        public virtual string GetUrlForContentID(string contentId)
        {
            return UrlHelpers.GetUrlForContentId(contentId);
        }
    }
}
