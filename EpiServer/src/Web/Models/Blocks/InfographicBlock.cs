﻿using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Validation;
using EPiServer.Web;
using Norgesportalen.Helpers;
using Norgesportalen.Models.Pages;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Norgesportalen.Models.Blocks
{
    [ContentType(
        DisplayName = "Infographic block",
        GroupName = Global.BlockGroupNames.Admin,
        GUID = "1004B050-9CF7-4EA2-87B0-779376AE9F9F",
        Description = "Infographic block for graphical elements with description."
    )]
    [SiteImageUrl("~/Models/SiteImagesForPageTypesAndBlocks/infographicblock.png")]
    [Access(Roles = "WebAdmins, Administrators")]
    public class InfographicBlock : BlockData
    {
        [UIHint(UIHint.Image)]
        [Display(
            Name = "Image",
            Description = "",
            GroupName = Global.GroupNames.Content,
            Order = 1)]
        public virtual ContentReference Image { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Heading",
            Description = "",
            GroupName = Global.GroupNames.Content,
            Order = 2)]
        public virtual string Heading { get; set; }

        [UIHint(UIHint.Textarea)]
        [CultureSpecific]
        [Display(
            Name = "Text",
            Description = "",
            GroupName = Global.GroupNames.Content,
            Order = 3)]
        public virtual string Text { get; set; }

        public class InfographicBlockValidator : IValidate<InfographicBlock>
        {
            IEnumerable<ValidationError> IValidate<InfographicBlock>.Validate(InfographicBlock page)
            {
                var validationErrors = new List<ValidationError>();

                var val = new ImageValidatorHelper(page);
                var imgError = val.ValidateImage(page.Image, "Image");
                if (imgError != null)
                {
                    validationErrors.Add(imgError);
                }                

                return validationErrors;
            }
        }

    }
}