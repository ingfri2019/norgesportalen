﻿using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Validation;
using EPiServer.Web;
using Norgesportalen.Helpers;
using Norgesportalen.Models.Pages;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Norgesportalen.Models.Blocks
{
    [ContentType(
        DisplayName = "Contact ambassador block",
        GroupName = Global.BlockGroupNames.Admin,
        GUID = "0D6F2A38-E743-4FFE-A4B4-63AA391D87E0",
        Description = "Contact ambassador block",
        AvailableInEditMode = false
    )]
    [SiteImageUrl("~/Models/SiteImagesForPageTypesAndBlocks/contactambassadorblock.png")]
    [Access(Roles = "WebAdmins, Administrators")]
    public class ContactAmbassadorBlockForAccordion : BlockData
    {
        [CultureSpecific]
        [Display(
            Name = "Contactperson name",
            Description = "",
            GroupName = SystemTabNames.Content,
            Order = 10)]
        public virtual string Name { get; set; }

        [CultureSpecific]
        [UIHint(UIHint.Image)]
        [Display(
            Name = "Contactperson image",
              Description = "",
              GroupName = Global.GroupNames.Content,
              Order = 20)]
        public virtual ContentReference MainImage { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Title",
            Description = "",
            GroupName = SystemTabNames.Content,
            Order = 30)]
        public virtual string Title { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Main body",
            Description = Global.PropertyDescriptorValues.MainBodyDescription,
            GroupName = Global.GroupNames.Content,
            Order = 60)]
        public virtual XhtmlString MainBody { get; set; }

        public class ContactAmbassadorBlockForAccordionValidator : IValidate<ContactAmbassadorBlockForAccordion>
        {
            IEnumerable<ValidationError> IValidate<ContactAmbassadorBlockForAccordion>.Validate(ContactAmbassadorBlockForAccordion page)
            {
                var validationErrors = new List<ValidationError>();

                var val = new ImageValidatorHelper(page);
                var imgError = val.ValidateImage(page.MainImage, "MainImage");
                if (imgError != null)
                {
                    validationErrors.Add(imgError);
                }
                return validationErrors;
            }
        }
    }
}