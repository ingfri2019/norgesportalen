﻿using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using Norgesportalen.Models.Pages;
using System.ComponentModel.DataAnnotations;

namespace Norgesportalen.Models.Blocks
{
    [ContentType(
        DisplayName = "Link without introduction text block",
        GroupName = Global.BlockGroupNames.Admin,
        GUID = "5b9c17ab-93c0-4243-85d6-38e1c76443e0",
        Description = "Link block without an introduction text."
    )]
    [SiteImageUrl("~/Models/SiteImagesForPageTypesAndBlocks/linkwithintroductiontextblock.png")]
    [Access(Roles = "WebAdmins, Administrators")]
    public class LinkWithTitleBlock : BlockData
    {

        [CultureSpecific]
        [Display(
            Name = "Heading",
            Description = "",
            GroupName = SystemTabNames.Content,
            Order = 1)]
        public virtual string Title { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Link",
            Description = "Link URL",
            GroupName = Global.GroupNames.Content,
            Order = 2)]
        public virtual Url LinkUrl { get; set; }
    }
}