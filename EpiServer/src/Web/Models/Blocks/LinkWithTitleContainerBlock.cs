﻿using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using Norgesportalen.Models.Pages;
using System.ComponentModel.DataAnnotations;

namespace Norgesportalen.Models.Blocks
{
    [ContentType(
        DisplayName = "Container for links without introduction text block",
        GroupName = Global.BlockGroupNames.Admin,
        GUID = "E7239F6A-357F-472B-86FC-F6B6869EC6C6", 
        Description = "Container block which is not viewed by visitors, the container block is used as a \"folder\" to gather links without an introduction text to be viewed in one link list."
    )]
    [SiteImageUrl("~/Models/SiteImagesForPageTypesAndBlocks/containerforlinkswithoutintroductiontextblock.png")]
    [Access(Roles = "WebAdmins, Administrators")]
    public class LinkWithTitleContainerBlock : BlockData
    {
        [CultureSpecific]
        [Display(
            Name = "Heading",
            Description = "",
            GroupName = SystemTabNames.Content,
            Order = 1)]
        public virtual string Title { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Links without introduction text ",
            Description = " ",
           GroupName = Global.GroupNames.Content,
           Order = 2)]
        [AllowedTypes(new[] { typeof(LinkWithTitleBlock) })]
        public virtual ContentArea LinkWithTitleContentArea { get; set; }
    }
}