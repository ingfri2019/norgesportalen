﻿using EPiServer.Core;
using EPiServer.DataAnnotations;
using Norgesportalen.Models.Pages;
using System.ComponentModel.DataAnnotations;

namespace Norgesportalen.Models.Blocks
{
    [ContentType(
        DisplayName = "Container for accordion block",
        GroupName = Global.BlockGroupNames.Admin,
        GUID = "F2EEA148-A591-466F-A930-755D52B54C5C",
        Description = "Container block which is not viewed by visitors, the container block is used as a \"folder\" to gather accordion items to be viewed gathered in an expanded accordion."
    )]
    [SiteImageUrl("~/Models/SiteImagesForPageTypesAndBlocks/containerforaccordionblock.png")]
    [Access(Roles = "WebAdmins, Administrators")]
    public class AccordionContainerBlock : BlockData
    {
        [CultureSpecific]
        [Display(
            Name = "Accordions",
            Description = "",
            GroupName = Global.GroupNames.Content,
            Order = 1)]
        [AllowedTypes(new[] { typeof(AccordionItemFreetextBlock), typeof(AccordionItemLinkListBlock), typeof(AccordionItemContactsBlock), typeof(AccordionItemFreeTextTwoColumns) })]
        public virtual ContentArea AccordionItemsContentArea { get; set; }
    }
}