﻿using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;
using Norgesportalen.Business.EditorDescriptors;
using Norgesportalen.Models.Pages;
using System.ComponentModel.DataAnnotations;

namespace Norgesportalen.Models.Blocks
{
    [ContentType(
        DisplayName = "Google Maps Block",
        GroupName = Global.BlockGroupNames.Media,
        GUID = "096FA4FA-D037-4FB1-AF39-CA1F8E5AB592",
        Description = "Adds Google map to your article. You can add a location, link and adjust zoom level in the block."
    )]
    [SiteImageUrl("~/Models/SiteImagesForPageTypesAndBlocks/GoogleMapsBlock.png")]
    public class GoogleMapsBlock : BlockData
    {
        [Display(
            Name = "Kordinater",
            Description = "",
            GroupName = Global.GroupNames.Content,
            Order = 5)]
        [EditorDescriptor(EditorDescriptorType = typeof(CustomGoogleMapsEditorDescriptor))]
        public virtual string Coordinates { get; set; }

        [Display(
            Name = "Zoom nivå",
            Description = "Sett zoomnivå på kartet. 0-20. Default er 14.",
            GroupName = Global.GroupNames.Content,
            Order = 10)]
        [Range(1,20)]
        public virtual int ZoomLevel { get; set; }

        [Display(
            Name = "Bruk lenke til GoogleMaps",
            Description = "",
            GroupName = Global.GroupNames.Content,
            Order = 15)]
        public virtual bool LinkToGoogleMaps { get; set; }

        public override void SetDefaultValues(ContentType contentType)
        {
            base.SetDefaultValues(contentType);
            ZoomLevel = 14;
        }
    }
}