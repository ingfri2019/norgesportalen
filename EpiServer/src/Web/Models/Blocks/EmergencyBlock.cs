﻿using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using Norgesportalen.Models.Pages;
using System.ComponentModel.DataAnnotations;
using EPiServer.Shell.ObjectEditing;
using Norgesportalen.Business.EditorDescriptors;

namespace Norgesportalen.Models.Blocks
{
    [ContentType(
        DisplayName = "Emergency block",
        GroupName = Global.BlockGroupNames.Admin,
        GUID = "63C0F369-20D1-42DC-B944-9359D60AFFE5",
        Description = "Emergency block on the front page."
    )]
    [SiteImageUrl("~/Models/SiteImagesForPageTypesAndBlocks/emergencyblock.png")]
    [Access(Roles = "WebAdmins, Administrators")]
    public class EmergencyBlock : BlockData

    {
        [CultureSpecific]
        [Display(
            Name = "Heading",
            Description = "",
            GroupName = SystemTabNames.Content,
            Order = 10)]
        public virtual string Title { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Banner Color",
            GroupName = Global.GroupNames.Content,
            Order = 15)]
        [BackingType(typeof(PropertyString))]
        [SelectOne(SelectionFactoryType = typeof(EmergencyBannerColorSelectionFactory))]
        public virtual string BannerColor { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Main body",
            Description = "Using the XHTML-editor you can insert for example text, images and tables.",
            GroupName = Global.GroupNames.Content,
            Order = 20)]
        public virtual XhtmlString MainBody { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Link title",
            Description = "Link URL text. If none is given, the URL itself will be used.",
            GroupName = Global.GroupNames.Content,
            Order = 30)]
        public virtual string LinkText { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Link",
            Description = "Link URL",
            GroupName = Global.GroupNames.Content,
            Order = 40)]
        public virtual Url Url { get; set; }

    }
}