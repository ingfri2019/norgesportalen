﻿using EPiServer.Core;
using EPiServer.DataAnnotations;
using Norgesportalen.Models.Pages;
using System.ComponentModel.DataAnnotations;

namespace Norgesportalen.Models.Blocks
{
    [ContentType(
        DisplayName = "Exclamation mark block",
        GroupName = Global.BlockGroupNames.Admin,
        GUID = "2B11C2A0-C6F3-45C9-9246-7A6E98C8C351",
        Description = "Exclamation mark block on the process page and process step page.")]
    [SiteImageUrl("~/Models/SiteImagesForPageTypesAndBlocks/exclamationmarkblock.png")]
    [Access(Roles = "WebAdmins, Administrators")]
    public class ExclamationMarkBlock : BlockData
    {
        [CultureSpecific]
        [Display(
            Name = "Main body",
            Description = "Using the XHTML-editor you can insert for example text, images and tables.",
            GroupName = Global.GroupNames.Content,
            Order = 2)]
        public virtual XhtmlString Content { get; set; }
    }
}