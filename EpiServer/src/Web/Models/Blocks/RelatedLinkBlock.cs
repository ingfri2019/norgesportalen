﻿using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Web;
using Norgesportalen.Models.Pages;
using System.ComponentModel.DataAnnotations;

namespace Norgesportalen.Models.Blocks
{
    [ContentType(
        DisplayName = "Related link block",
        GroupName = Global.BlockGroupNames.Links,
        GUID = "2C044000-86BF-42AB-BB91-7C4092238119",
        Description = ""
    )]
    [SiteImageUrl("~/Models/SiteImagesForPageTypesAndBlocks/containerforlinkswithintroductiontext.png")]

    public class RelatedLinkBlock : BlockData
    {

        [CultureSpecific]
        [Display(
            Name = "Text to display",
            Description = "",
            GroupName = SystemTabNames.Content,
            Order = 1)]
        public virtual string LinkName { get; set; }

        [CultureSpecific]
        [UIHint(UIHint.Textarea)]
        [Display(
            Name = "Intro",
            Description = "",
            GroupName = SystemTabNames.Content,
            Order = 2)]
        public virtual string Intro { get; set; }

        [Required]
        [Display(
            Name = "Link",
            Description = "Link URL",
            GroupName = Global.GroupNames.Content,
            Order = 3)]
        public virtual Url Url { get; set; }
    }
}
