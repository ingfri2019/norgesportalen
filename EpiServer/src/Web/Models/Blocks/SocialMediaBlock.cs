﻿using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;
using EPiServer.Web;
using Norgesportalen.Business.EditorDescriptors;
using Norgesportalen.Models.Pages;
using System.ComponentModel.DataAnnotations;

namespace Norgesportalen.Models.Blocks
{
    [ContentType(
        DisplayName = "Social media block",
        GroupName = Global.BlockGroupNames.Media,
        GUID = "36509597-5669-4E4C-8EC4-73AE19189E82",
        Description = "Social media block on the front page, contact page and social media list page."
    )]
    [SiteImageUrl("~/Models/SiteImagesForPageTypesAndBlocks/socialmediablock.png")]

    public class SocialMediaBlock : InfoScreenLinkBlock
    {
        [Display(
            Name = "Social media icon",
            GroupName = SystemTabNames.Content,
            Order = 2)]
        [CultureSpecific]
        [BackingType(typeof(PropertyNumber))]
        [EditorDescriptor(EditorDescriptorType = typeof(DropDownEditorDescriptor<SocialmediaIcons>))]
        public virtual SocialmediaIcons Icons { get; set; }

        [CultureSpecific]
        [Required]
        [Display(
            Name = "Heading",
            Description = "",
            GroupName = Global.GroupNames.Content,
            Order = 3)]
        public virtual string Heading { get; set; }

        [UIHint(UIHint.Textarea)]
        [CultureSpecific]
        [Display(
            Name = "Text",
            Description = "",
            GroupName = Global.GroupNames.Content,
            Order = 4)]
        public virtual string Text { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Link",
            Description = "Link URL",
            GroupName = Global.GroupNames.Content,
            Order = 2)]
        public virtual Url LinkUrl { get; set; }
    }
}