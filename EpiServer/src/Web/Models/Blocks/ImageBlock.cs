﻿using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Validation;
using EPiServer.Web;
using Norgesportalen.Helpers;
using Norgesportalen.Models.Pages;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Norgesportalen.Models.Blocks
{
    [ContentType(
        DisplayName = "Image block",
        GroupName = Global.BlockGroupNames.Admin,
        GUID = "6CDA572F-AE4E-4330-AD5A-B82449487208", 
        Description = "Image block on the contact page and in main body fields on other pages.",
        AvailableInEditMode = false
    )]
    [SiteImageUrl("~/Models/SiteImagesForPageTypesAndBlocks/imageblock.png")]
    [Access(Roles = "WebAdmins, Administrators")]
    public class ImageBlock : BlockData
    {
        [UIHint(UIHint.Image)]
        [Display(
            Name = "Image",
            Description = "",
            GroupName = Global.GroupNames.Content,
            Order = 1)]
        public virtual ContentReference Image { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Image text",
            Description = "",
            GroupName = Global.GroupNames.Content,
            Order = 2)]
        public virtual string ImageText { get; set; }

        public class ImageBlockValidator : IValidate<ImageBlock>
        {
            IEnumerable<ValidationError> IValidate<ImageBlock>.Validate(ImageBlock page)
            {
                var validationErrors = new List<ValidationError>();

                var val = new ImageValidatorHelper(page);
                var imgError = val.ValidateImage(page.Image, "Image");
                if (imgError != null)
                {
                    validationErrors.Add(imgError);
                }                

                return validationErrors;
            }
        }
    }
}