﻿using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Validation;
using Norgesportalen.Models.Pages;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Norgesportalen.Models.Blocks
{
    [ContentType(
        DisplayName = "Info screen external link",
        GroupName = Global.BlockGroupNames.Admin,
        GUID = "a193e956-a984-4cbe-a017-0c877212fecd",
        Description = "Adds a link to an external page to the list."
    )]
    [Access(Roles = "WebAdmins, Administrators")]
    [SiteImageUrl("~/Models/SiteImagesForPageTypesAndBlocks/InforScreenExternalLink.png")]
    public class InfoScreenExternalLinkBlock : InfoScreenLinkBlock
    {
        [CultureSpecific]
        [Display(
            Name = "URL (only https addresses)",
            Description = "You will need the full URL, for example https://www.stortinget.no.",
            GroupName = Global.GroupNames.InfoScreen,
            Order = 1)]
        public virtual string Url { get; set; }

        public class InfoScreenExternalLinkBlockValidator : IValidate<InfoScreenExternalLinkBlock>
        {
            IEnumerable<ValidationError> IValidate<InfoScreenExternalLinkBlock>.Validate(InfoScreenExternalLinkBlock page)
            {
                var validationErrors = new List<ValidationError>();

                if (page.Url != null && !page.Url.Trim().StartsWith("https"))
                {
                    validationErrors.Add(
                        new ValidationError()
                        {
                            ErrorMessage = "Only secure (https) URLs supported.",
                            PropertyName = page.GetPropertyName(pg => pg.Url),
                            Severity = ValidationErrorSeverity.Error,
                            ValidationType = ValidationErrorType.AttributeMatched
                        }
                    );
                }
              
                return validationErrors;
            }
        }
    }
}