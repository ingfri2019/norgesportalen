﻿using EPiServer.Core;
using EPiServer.DataAnnotations;
using Norgesportalen.Models.Pages;
using System.ComponentModel.DataAnnotations;

namespace Norgesportalen.Models.Blocks
{
    [ContentType(
        DisplayName = "Info screen internal link",
        GroupName = Global.BlockGroupNames.Admin,
        GUID = "e87bc30d-fa9f-46a8-92ef-4bbc26c2609a",
        Description = "Adds a link to an internal page to the list."
    )]
    [Access(Roles = "WebAdmins, Administrators")]
    [SiteImageUrl("~/Models/SiteImagesForPageTypesAndBlocks/InforScreenInternalLink.png")]
    public class InfoScreenInternalLinkBlock : InfoScreenLinkBlock
    {
        [CultureSpecific]
        [Display(
            Name = "Page reference",
            Description = "Reference to an internal page to display on the info screen.",
            GroupName = Global.GroupNames.InfoScreen,
            Order = 1)]
        public virtual PageReference PageReference { get; set; }
    }
}