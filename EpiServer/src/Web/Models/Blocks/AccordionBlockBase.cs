﻿using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using System.ComponentModel.DataAnnotations;

namespace Norgesportalen.Models.Blocks
{
    public class AccordionBlockBase : BlockData
    {

        [CultureSpecific]
        [Display(
           Name = "Heading",
           Description = "",
           GroupName = SystemTabNames.Content,
           Order = 10)]
        public virtual string Title { get; set; }

        public string TitleWithNoWhiteSpaces()
        {
            if (string.IsNullOrWhiteSpace(Title))
                return string.Empty;

            var titleWithNoWhitespace = string.Empty;
            foreach (char c in Title)
            {
                if (!char.IsWhiteSpace(c))
                {
                    titleWithNoWhitespace += c;
                }
            }
            return titleWithNoWhitespace;
        }

        public string ContentLink()
        {
            return (this as IContent).ContentLink.ToString();
        }

    }
}