﻿using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Web;
using Norgesportalen.Models.Pages;
using System.ComponentModel.DataAnnotations;

namespace Norgesportalen.Models.Blocks
{
    [ContentType(
        DisplayName = "Contact person block",
        GUID = "85DB3FC9-349D-45EC-A412-8857A7495E19",
        Description = "Contact person block"
    )]
    [SiteImageUrl("~/Models/SiteImagesForPageTypesAndBlocks/contactpersonblock.png")]

    public class ContactPersonBlockForAccordion : BlockData
    {
        [CultureSpecific]
        [Display(
            Name = "Contactperson name",
            Description = "",
            GroupName = SystemTabNames.Content,
            Order = 10)]
        public virtual string Name { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Title",
            Description = "",
            GroupName = SystemTabNames.Content,
            Order = 30)]
        public virtual string Title { get; set; }

        [CultureSpecific]
        [UIHint(UIHint.Image)]
        [Display(
            Name = "Contactperson image",
            Description = "",
            GroupName = Global.GroupNames.Content,
            Order = 40)]
        public virtual ContentReference MainImage { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Main body",
            Description = Global.PropertyDescriptorValues.MainBodyDescription,
            GroupName = Global.GroupNames.Content,
            Order = 60)]
        public virtual XhtmlString MainBody { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Email",
            Description = "",
            GroupName = SystemTabNames.Content,
            Order = 30)]
        public virtual string Email { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Phone",
            Description = "",
            GroupName = SystemTabNames.Content,
            Order = 30)]
        public virtual string Phone { get; set; }
    }
}