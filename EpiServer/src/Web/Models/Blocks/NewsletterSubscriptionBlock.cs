﻿using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;
using EPiServer.Validation;
using EPiServer.Web;
using Norgesportalen.Business.EditorDescriptors;
using Norgesportalen.Models.Pages;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Norgesportalen.Models.Blocks
{
    [ContentType(
        DisplayName = "Newsletter subscription block",
        GroupName = Global.BlockGroupNames.Media,
        GUID = "54D26246-A2EB-47EC-ADCB-8662E3983DB8",
        Description = "Allows user to subscripe to a newsletter list of the editors choosing."
    )]
    [SiteImageUrl("~/Models/SiteImagesForPageTypesAndBlocks/newslettersubscriptionblock.png")]

    public class NewsletterSubscriptionBlock : BlockData
    {
        [Display(Name = "Newsletter list",
                Description = "Choose the newsletter list which users will subscribe to.",
                Order = 10,
                GroupName = SystemTabNames.Content)]
        [EditorDescriptor(EditorDescriptorType = typeof(NewsletterEditorDescriptor))]
        public virtual string NewsLetterList { get; set; }

        [Display(Name = "Enable Email verification",
                Description = "If enabled, user has to click link in sent email to verify",
                Order = 14,
                GroupName = SystemTabNames.Content)]        
        public virtual bool EmailVerificationEnabled { get; set; }

        [CultureSpecific]
        [Display(Name = "Confirm subscription page",
            Description = "Page that handles user email confirmation, must also be set to enable email verification.",
           Order = 16,
           GroupName = SystemTabNames.Content)]
        [AllowedTypes(typeof(NewsletterConfirmPage))]
        public virtual PageReference ConfirmSubscriptionPage { get; set; }

        [CultureSpecific]
        [Display(GroupName = SystemTabNames.Content,
                 Order = 20,
                 Name = "Title")]
        public virtual string Title { get; set; }

        [CultureSpecific]
        [Display(GroupName = SystemTabNames.Content,
               Order = 30,
               Name = "Short description for the email input field")]
        [UIHint(UIHint.Textarea)]
        public virtual string MainIntro { get; set; }

        [CultureSpecific]
        [Display(GroupName = SystemTabNames.Content,               
               Order = 40,
               Name = "Thank you message")]
        [UIHint(UIHint.Textarea)]
        public virtual string ThankYouMsg { get; set; }

        public class NewsletterSubscriptionBlockValidator : IValidate<NewsletterSubscriptionBlock>
        {
            IEnumerable<ValidationError> IValidate<NewsletterSubscriptionBlock>.Validate(NewsletterSubscriptionBlock page)
            {
                var validationErrors = new List<ValidationError>();

                if (page.EmailVerificationEnabled && page.ConfirmSubscriptionPage == null)
                {
                    validationErrors.Add(
                        new ValidationError()
                        {
                            ErrorMessage = "Confirm subscription page must be set to enable Email verification",
                            PropertyName = page.GetPropertyName(pg => pg.EmailVerificationEnabled),
                            Severity = ValidationErrorSeverity.Error,
                            ValidationType = ValidationErrorType.AttributeMatched
                        }
                    );
                }
                if (page.NewsLetterList == null)
                {
                    validationErrors.Add(
                        new ValidationError()
                        {
                            ErrorMessage = "NewsLetter List to subscribe to must be set.",
                            PropertyName = page.GetPropertyName(pg => pg.NewsLetterList),
                            Severity = ValidationErrorSeverity.Error,
                            ValidationType = ValidationErrorType.AttributeMatched
                        }
                        );
                }

                return validationErrors;
            }
        }
    }
}