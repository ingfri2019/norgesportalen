﻿using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using System.ComponentModel.DataAnnotations;

namespace Norgesportalen.Models.Blocks
{
    [ContentType(
        DisplayName = "Info screen link",
        GroupName = Global.BlockGroupNames.Admin,
        GUID = "d7ca77d3-205c-48f8-bdfd-ac16c5d02f06",
        Description = ""
    )]
    public abstract class InfoScreenLinkBlock : BlockData
    {
        [CultureSpecific]
        [Display(Name = "Displays",
                 Description = "Number of times the linked page will be displayed in each rotation of the links.",
                 GroupName = Global.GroupNames.InfoScreen,
                 Order = 1)]
        [Range(0, 100)]
        public virtual int NumberOfDisplaysPerRotation
        {
            get { return this.Property["NumberOfDisplaysPerRotation"].IsNull ? 1 : this.GetPropertyValue(page => page.NumberOfDisplaysPerRotation); }
            set { this.SetPropertyValue(page => page.NumberOfDisplaysPerRotation, value); }
        }

        public override void SetDefaultValues(ContentType contentType)
        {
            base.SetDefaultValues(contentType);

            NumberOfDisplaysPerRotation = 1;
        }
    }
}