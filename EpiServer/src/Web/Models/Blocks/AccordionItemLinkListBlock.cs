﻿using EPiServer.DataAnnotations;
using EPiServer.SpecializedProperties;
using Norgesportalen.Models.Pages;
using System.ComponentModel.DataAnnotations;

namespace Norgesportalen.Models.Blocks
{
    [ContentType(
        DisplayName = "Links accordion item",
        GroupName = Global.BlockGroupNames.Accordions,
        GUID = "D0BC97AA-77EF-4284-9477-F9C5FA752FB3",
        Description = "Accordion item with a link list."
    )]
    [SiteImageUrl("~/Models/SiteImagesForPageTypesAndBlocks/linksaccordionitem.png")]

    public class AccordionItemLinkListBlock : AccordionBlockBase
    {
        [CultureSpecific]        
        [UIHint("LinkItemCollectionWithArrows")]
        [Display(
            Name = "Links",
            Description = "",
            GroupName = Global.GroupNames.Content,
            Order = 20)]
        public virtual LinkItemCollection LinkList { get; set; }
    }
}