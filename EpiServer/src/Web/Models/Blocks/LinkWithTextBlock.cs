﻿using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Web;
using Norgesportalen.Models.Pages;
using System.ComponentModel.DataAnnotations;

namespace Norgesportalen.Models.Blocks
{
    [ContentType(
        DisplayName = "Link with introduction text block",
        GroupName = Global.BlockGroupNames.Admin,
        GUID = "A4B5308E-0A55-42A3-ADF4-50AF782542E9", 
        Description = "Link block with an introduction text."
    )]
    [SiteImageUrl("~/Models/SiteImagesForPageTypesAndBlocks/containerforlinkswithintroductiontext.png")]
    [Access(Roles = "WebAdmins, Administrators")]
    public class LinkWithTextBlock : BlockData
    {

        [CultureSpecific]
        [Display(
            Name = "Link title",
            Description = "",
            GroupName = SystemTabNames.Content,
            Order = 1)]
        public virtual string Title { get; set; }

        [CultureSpecific]
        [UIHint(UIHint.Textarea)]
        [Display(
             Name = "Text",
            Description = "",
            GroupName = SystemTabNames.Content,
            Order = 2)]
        public virtual string Text { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Link",
            Description = "Link URL",
            GroupName = Global.GroupNames.Content,
            Order = 3)]
        public virtual Url Url { get; set; }
    }
}