﻿using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using Norgesportalen.Models.Pages;
using System.ComponentModel.DataAnnotations;

namespace Norgesportalen.Models.Blocks
{
    [ContentType(
        DisplayName = "Button block",
        GroupName = Global.BlockGroupNames.Admin,
        GUID = "6116BAA6-2EE0-4FC7-86A3-1A8B9873863D",
        Description = "Button block on the process page and process step page."
    )]
    [SiteImageUrl("~/Models/SiteImagesForPageTypesAndBlocks/buttonblock.png")]
    [Access(Roles = "WebAdmins, Administrators")]
    public class ButtonBlock : BlockData
    {
        [CultureSpecific]
        [Display(
            Name = "Heading",
            Description = "",
            GroupName = SystemTabNames.Content,
            Order = 1)]
        public virtual string Title { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Link",
            Description = "Link URL",
            GroupName = Global.GroupNames.Content,
            Order = 2)]
        public virtual Url LinkUrl { get; set; }
    }
}