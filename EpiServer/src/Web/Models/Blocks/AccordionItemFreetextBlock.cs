﻿using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using Norgesportalen.Models.Pages;
using System.ComponentModel.DataAnnotations;

namespace Norgesportalen.Models.Blocks
{
    [ContentType(
        DisplayName = "Text accordion item",
        GroupName = Global.BlockGroupNames.Accordions,
        GUID = "7257982A-7C38-4BA0-8A0D-3C6535263C48",
        Description = "Accordion item with one column."
    )]
    [SiteImageUrl("~/Models/SiteImagesForPageTypesAndBlocks/textaccordionitem.png")]
    public class AccordionItemFreetextBlock : AccordionBlockBase
    {
        [CultureSpecific]
        [Display(
            Name = "Main body",
            Description = "",
            GroupName = SystemTabNames.Content,
            Order = 20)]
        public virtual XhtmlString MainBody { get; set; }
    }
}