﻿using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;
using EPiServer.SpecializedProperties;
using Norgesportalen.Business.Validation;
using Norgesportalen.Helpers;
using Norgesportalen.Models.Pages;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Norgesportalen.Models.Blocks
{
    [ContentType(
        DisplayName = "Top task link list block",
        GroupName = Global.BlockGroupNames.Admin,
        GUID = "DAA7D8C3-8B79-47DD-B160-6B2AB496A419", 
        Description = "Top task link list on the front page."
    )]
    [SiteImageUrl("~/Models/SiteImagesForPageTypesAndBlocks/toptasklinklistblock.png")]
    [Access(Roles = "WebAdmins, Administrators")]
    public class TopTaskLinkListBlock : BlockData
    {
        [Display(
            Name = "Heading",
            Description = "",
            GroupName = Global.GroupNames.Content,
            Order = 1)]
        public virtual string Title { get; set; }

        [Display(
            Name = "Links", 
            Description = "",
            GroupName = Global.GroupNames.Content, 
            Order = 2)]
        [MaxItemCount(4, ErrorMessage = "Too many items in Links. Maximum 4.")]
        public virtual LinkItemCollection LinkList { get; set; }

        [Display(
            Name = "Read more heading",
            Description = "",
            GroupName = Global.GroupNames.Content,
            Order = 3)]
        public virtual string ReadMoreLinkTitle { get; set; }

        [Display(
            Name = "Link to read more",
            Description = "",
            GroupName = Global.GroupNames.Content,
            Order = 4)]
        [DefaultDragAndDropTarget]
        public virtual PageReference TitleAndReadmoreLink { get; set; }

        #region Rendering helpers

        // view model
        // TODO: Replace view data
        [Ignore]
        public virtual bool IsSmall { get; set; }

        public struct TopTaskLink
        {
            public string Title;
            public string UnderTitle;
            public string Href;
            public string Target;
        }

        [Ignore]
        public List<TopTaskLink> ItemsToRender
        {
            get
            {
                var itemsToRender = new List<TopTaskLink>();
                // 4 for large view, 2 for small view
                int maxItemsToDisplay = IsSmall ? 2 : 4;

                if (LinkList == null)
                    return itemsToRender;

                foreach (var linkItem in LinkList)
                {
                    
                    itemsToRender.Add(
                        new TopTaskLink()
                        {
                            Title = linkItem.Text,
                            UnderTitle = linkItem.Title,
                            Href = linkItem.Href,
                            Target = linkItem.Target
                        });

                    maxItemsToDisplay--;
                    if (maxItemsToDisplay == 0)
                        break;
                }

                return itemsToRender;
            }
        }

        public virtual string GetUrlForContentID(string contentId)
        {
            return UrlHelpers.GetUrlForContentId(contentId);
        }

        #endregion rendering helpers
    }
}