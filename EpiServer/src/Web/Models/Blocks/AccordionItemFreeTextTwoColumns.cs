﻿using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using Norgesportalen.Models.Pages;
using System.ComponentModel.DataAnnotations;

namespace Norgesportalen.Models.Blocks
{
    [ContentType(
        DisplayName = "Free text two columns accordion item",
        GroupName = Global.BlockGroupNames.Admin,
        GUID = "82F4106B-7304-4BC0-BAE6-0EFF1FBD2931",
        Description = "Accordion item with two columns."
    )]
    [SiteImageUrl("~/Models/SiteImagesForPageTypesAndBlocks/freetexttwocolumnaccordionitem.png")]
    [Access(Roles = "WebAdmins, Administrators")]
    public class AccordionItemFreeTextTwoColumns : AccordionBlockBase
    {
        [CultureSpecific]
        [Display(
            Name = "Left main body",
            Description = "The main body will be shown in the main content area of the block, using the XHTML-editor you can insert for example text, images and tables.",
            GroupName = SystemTabNames.Content,
            Order = 20)]
        public virtual XhtmlString MainBody { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Right main body",
            Description = "The main body will be shown in the main content area of the block, using the XHTML-editor you can insert for example text, images and tables.",
            GroupName = SystemTabNames.Content,
            Order = 30)]
        public virtual XhtmlString MainBody2 { get; set; }
    }
}