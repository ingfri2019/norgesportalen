﻿using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using Norgesportalen.Models.Pages;
using System.ComponentModel.DataAnnotations;

namespace Norgesportalen.Models.Blocks
{
    [ContentType(
        DisplayName = "Container for links with introduction text block",
        GroupName = Global.BlockGroupNames.Admin,
        GUID = "E4727452-D4F4-4709-A53F-D465A5550F8A", 
        Description = "Container block which is not viewed by visitors, the container block is used as a \"folder\" to gather links with an introduction text to be viewed in one link list."
    )]
    [SiteImageUrl("~/Models/SiteImagesForPageTypesAndBlocks/containerforlinkswithintroductiontext.png")]
    [Access(Roles = "WebAdmins, Administrators")]
    public class LinkWithTextContainerBlock  : BlockData
    {
        [CultureSpecific]
        [Display(
            Name = "Heading",
            Description = "",
            GroupName = SystemTabNames.Content,
            Order = 1)]
        public virtual string Title { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Links with introduction text ",
            Description = " ",
           GroupName = Global.GroupNames.Content,
           Order = 2)]
        [AllowedTypes(new[] { typeof(LinkWithTextBlock) })]
        public virtual ContentArea LinkWithTextContentArea { get; set; }
    }
}