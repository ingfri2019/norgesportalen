﻿using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using Norgesportalen.Models.Pages;
using System.ComponentModel.DataAnnotations;

namespace Norgesportalen.Models.Blocks
{
    [ContentType(
        DisplayName = "Information block",
        GroupName = Global.BlockGroupNames.Admin,
        GUID = "7AEBDD00-8EFB-4886-A990-CD0BF546EEF7",
        Description = "Information block on the front page."
    )]
    [Access(Roles = "WebAdmins, Administrators")]
    [SiteImageUrl("~/Models/SiteImagesForPageTypesAndBlocks/informationblock.png")]
    public class InformationBlock : BlockData
    {
        [CultureSpecific]
        [Display(
            Name = "Heading",
            Description = "",
            GroupName = SystemTabNames.Content,
            Order = 10)]
        public virtual string Title { get; set; }

        [CultureSpecific]
        [Display(Name = "Main body",
                  Description = "Using the XHTML-editor you can insert for example text, images and tables.",
                  GroupName = Global.GroupNames.Content,
                  Order = 20)]
        public virtual XhtmlString MainBody { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Link title",
            Description = "Link URL text. If none is given, the URL itself will be used.",
            GroupName = Global.GroupNames.Content,
            Order = 30)]
        public virtual string LinkText { get; set; }

        [CultureSpecific]
        [Display(
           Name = "Link",
           Description = "Link URL",
           GroupName = Global.GroupNames.Content,
           Order = 40)]
        public virtual Url Url { get; set; }

        [Ignore]
        public virtual string OwnerCountry { get; set; }

    }
}