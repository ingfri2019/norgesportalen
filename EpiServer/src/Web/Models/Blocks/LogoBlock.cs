﻿using EPiServer.Core;
using EPiServer.DataAnnotations;
using Norgesportalen.Models.Pages;
using System.ComponentModel.DataAnnotations;

namespace Norgesportalen.Models.Blocks
{
    [ContentType(
        DisplayName = "Logo block",
        GroupName = Global.BlockGroupNames.Admin,
        GUID = "c20cac60-7b0d-4a92-bbfb-41c566fc24a3", 
        Description = "Station name on the header."
    )]
    [SiteImageUrl("~/Models/SiteImagesForPageTypesAndBlocks/logoblock.png")]
    [Access(Roles = "WebAdmins, Administrators")]
    public class LogoBlock : BlockData
    {

        [CultureSpecific]
        [Display(
            Name = "Logo heading",
            Description = "",
            GroupName = Global.GroupNames.Header
            )]
        public virtual string LogoTitle { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Logo mission name",
            Description = "",
            GroupName = Global.GroupNames.Header
            )]
        public virtual string LogoSubText { get; set; }
    }
}