﻿using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using Norgesportalen.Models.Pages;
using System.ComponentModel.DataAnnotations;

namespace Norgesportalen.Models.Blocks
{
    [ContentType(
        DisplayName = "Contact block",
        GroupName = Global.BlockGroupNames.Admin,
        GUID = "0CB45937-578A-4210-BACC-B59EF20138F1",
        Description = "Contact block for gathering address, phone number, fax number, email address, business hours and holiday information on the footer, contact page, consulate general page and visa page."
    )]
    [SiteImageUrl("~/Models/SiteImagesForPageTypesAndBlocks/contactblock.png")]
    [Access(Roles = "WebAdmins, Administrators")]
    public class ContactBlock : BlockData
    {
        [CultureSpecific]
        [Display(
            Name = "Heading",
            Description = "",
            GroupName = SystemTabNames.Content,
            Order = 1)]
        public virtual string Title { get; set; }

        [CultureSpecific]
        [Display(Name = "Main body",
                    Description = "Using the XHTML-editor you can insert for example text, images and tables.",
                    GroupName = Global.GroupNames.Content,
                    Order = 2)]
        public virtual XhtmlString MainBody { get; set; }
    }
}