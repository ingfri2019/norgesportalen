﻿using EPiServer.Core;
using EPiServer.DataAnnotations;
using Norgesportalen.Models.Pages;
using System.ComponentModel.DataAnnotations;

namespace Norgesportalen.Models.Blocks
{
    [ContentType(
        DisplayName = "Contacts accordion item",
        GroupName = Global.BlockGroupNames.Accordions,
        GUID = "688F4D9B-FB49-4937-A7A7-2ED6AE2A149B",
        Description = "Accordion item with contact person information."
    )]
    [SiteImageUrl("~/Models/SiteImagesForPageTypesAndBlocks/contactaccordionitem.png")]

    public class AccordionItemContactsBlock : AccordionBlockBase
    {
        [CultureSpecific]
        [Display(
            Name = "Contacts",
            Description = "",
            GroupName = Global.GroupNames.Content,
            Order = 20)]
        [AllowedTypes(new[] { typeof(ContactPersonBlockForAccordion) })]
        public virtual ContentArea ContactContentArea { get; set; }
    }
}