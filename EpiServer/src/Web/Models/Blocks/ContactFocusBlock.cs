﻿using EPiServer.Core;
using EPiServer.DataAnnotations;
using Norgesportalen.Models.Pages;
using System.ComponentModel.DataAnnotations;

namespace Norgesportalen.Models.Blocks
{
    [ContentType(
        DisplayName = "Contact focus block",
        GroupName = Global.BlockGroupNames.Admin,
        GUID = "1C875D3D-2BB0-4C74-89E1-2412ACEDC29D", 
        Description = "Focus block on the contact page."
    )]
    [SiteImageUrl("~/Models/SiteImagesForPageTypesAndBlocks/contactforcusblock.png")]
    [Access(Roles = "WebAdmins, Administrators")]
    public class ContactFocusBlock :BlockData
    {
        [CultureSpecific]
        [Display(
            Name = "Main body",
            Description = "",
            GroupName = Global.GroupNames.Content,
            Order = 2)]
        public virtual XhtmlString MainBody { get; set; }
    }
}