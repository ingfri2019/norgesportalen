using System.Collections.Generic;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Framework.DataAnnotations;
using System.ComponentModel.DataAnnotations;
using EPiServer.Validation;
using EPiServer.Web;
using Norgesportalen.Business.EditorDescriptors;
using Norgesportalen.Models.PropertyTypes;
using NuGet;
using RestSharp.Extensions;

namespace Norgesportalen.Models.Media
{
    
    [ContentType(GUID = "E8220BC4-7149-4A70-8AF4-926ECD65F89B")]
    [MediaDescriptor(ExtensionString = "jpg,jpeg,jpe,ico,gif,bmp,png,svg")]
    public class ImageFile : ImageData 
    {

        [Display(Name = "Alternative text",
            Description = "Appears to the blind or partially sighted who use screen readers and others who have turned off image viewing in the browser. Alternative text should describe the subject's image.",
            Order = 1)]
        [StringLength(Business.Constants.ImageFile.AltTextLength)]
        public virtual string AltText { get; set; }

        [UIHint(UIHint.Textarea)]
        [Display(Name = "Caption",
            Order = 3)]
        [StringLength(Business.Constants.ImageFile.ImageTextLength)]
        public virtual string ImageText { get; set; }

        [Display(Name = "Photo Credit",
            Description = "Shows who has taken and owns the photo. Appears along with the caption.",
            Order = 4)]
        [StringLength(Business.Constants.ImageFile.CreditLength)]
        public virtual string Credit { get; set; }

        [Display(Name = "Use as icon",
            Order = 215,
            GroupName = Global.GroupNames.Icons)]
        public virtual bool IsIcon { get; set; }

        [DocumentTypeSelection]
        [Display(Name = "Can be icon on the following page types",
            Order = 215,
            GroupName = Global.GroupNames.Icons)]
        [BackingType(typeof(PropertyIconPages))]
        public virtual string FilterIconPages { get; set; }

        [Display(Name = "Use as background",
            Order = 215,
            GroupName = Global.GroupNames.Backgrounds)]
        public virtual bool IsBackground { get; set; }

        [Display(Order = 99999,
            GroupName = Global.GroupNames.Content)]
        [UIHint("HelpImage")]
        public virtual string ImagePreview { get; set; }

        public class ImageFileValidator : IValidate<ImageFile>
        {
            IEnumerable<ValidationError> IValidate<ImageFile>.Validate(ImageFile imageFile)
            {
                var errorList = new List<ValidationError>();
                if (imageFile.AltText.IsEmpty())
                {
                    errorList.Add
                    (
                        new ValidationError
                        {
                            ErrorMessage = "Please add Alternative text for the image",
                            PropertyName = imageFile.GetPropertyName(page => page.AltText),
                            Severity = ValidationErrorSeverity.Warning,
                            ValidationType = ValidationErrorType.AttributeMatched
                        }
                    );
                }

                if (!imageFile.IsIcon && !string.IsNullOrEmpty(imageFile.FilterIconPages))
                {
                    errorList.Add
                    (
                        new ValidationError
                        {
                            ErrorMessage = "If the image is not an icon, it cannot have selected pagetypes",
                            PropertyName = imageFile.GetPropertyName(page => page.IsIcon),
                            Severity = ValidationErrorSeverity.Error,
                            ValidationType = ValidationErrorType.AttributeMatched
                        }
                    );
                }
                return errorList;
            }
        }
    }
}
