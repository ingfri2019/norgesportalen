using EPiServer.Core;
using EPiServer.DataAnnotations;
using System;

namespace Norgesportalen.Models.Media
{
    [ContentType(GUID = "1E3A6566-A718-42D2-A0C7-59A844B0312E")]
    public class GenericMedia : MediaData
    {
        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public virtual String Description { get; set; }
    }
}
