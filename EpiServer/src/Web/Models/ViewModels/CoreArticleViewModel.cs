﻿using System.Collections.Generic;
using EPiServer.Core;
using Norgesportalen.Models.Pages;

namespace Norgesportalen.Models.ViewModels
{
    public class CoreArticleViewModel : PageViewModel<CoreArticlePage>
    {
        public CoreArticleViewModel(CoreArticlePage currentPage) : base(currentPage)
        {
        }

        public IEnumerable<PageData> Children { get; set; }
        public bool ShowChildrenList { get; set; }
        public bool UseListViewForChildren { get; set; }
    }
}