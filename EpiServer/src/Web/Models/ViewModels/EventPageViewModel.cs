﻿using System.Collections.Generic;
using Norgesportalen.Models.Media;
using Norgesportalen.Models.Pages;

namespace Norgesportalen.Models.ViewModels
{
    public class EventPageViewModel : PageViewModel<EventPage>
    {
        public IEnumerable<ImageFile> CarouselImages;
        public IEnumerable<EventPage> RelatedEvents;

        public EventPageViewModel(EventPage currentPage) : base(currentPage)
        {
        }
    }
}