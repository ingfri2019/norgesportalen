﻿using Norgesportalen.Models.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;

namespace Norgesportalen.Models.ViewModels
{
    // View Model
    public class CountriesOverviewViewModel : PageViewModel<CountriesOverviewPage>
    {
        public CountriesOverviewViewModel(CountriesOverviewPage currentPage) : base(currentPage)
        {
        }

        private ObjectCache _cache = MemoryCache.Default;
        private object _lock = new object();

        public List<CountrySiteCategory> AlphaCategoriesEmbassyList { get; set; }

        public List<SimplePageInfo> DelegationList { get; set; }

        public List<SimplePageInfo> ConsulateGeneralsList { get; set; }

        // ToDo: Test
        public IList<IGrouping<int, CountrySiteCategory>> CountrySitesGroups
        {
            get
            {
                var key = "CachedCountrySitesGroups";

                var countrySitesGroups = _cache[key] as List<IGrouping<int, CountrySiteCategory>>;

                // Check whether the value exists
                if (countrySitesGroups == null)
                {
                    lock (_lock)
                    {
                        // Try to get the object from the cache again
                        countrySitesGroups = _cache[key] as List<IGrouping<int, CountrySiteCategory>>;

                        // Double-check that another thread did 
                        // not call the DB already and load the cache
                        if (countrySitesGroups == null)
                        {
                            var embassyCount = AlphaCategoriesEmbassyList.Count;
                            int tre = (embassyCount / 3); // Div by 0
                            if (tre == 0) { tre = 1; }
                            countrySitesGroups = AlphaCategoriesEmbassyList.GroupBy(r => AlphaCategoriesEmbassyList.IndexOf(r) / tre).ToList();

                            // Add the list to the cache
                            _cache.Set(key, countrySitesGroups, DateTimeOffset.Now.AddMinutes(5));
                        }
                    }
                }
                return countrySitesGroups;
            }



        }

        public class CountrySiteCategory
        {
            public class CountrySite
            {
                public string Name;
                public string Website;
            }

            public char Letter;
            public List<CountrySite> Embassies;

        }

        public class SimplePageInfo
        {
            public string Name;
            public string Website;
            public string ShortText;
        }


    }

}