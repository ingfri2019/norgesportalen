﻿using System;
using System.Collections.Generic;
using System.Web.Routing;

namespace Norgesportalen.Models.ViewModels
{
    public class PagingModel
    {
        public List<PagerLink> PagerLinks;
        public PagerLink PreviousPage;
        public PagerLink NextPage;
        public int CurrentPageNo;

        public class PagerLink
        {
            public PagerLink(string linkText,int page, KeyValuePair<string, string>? addionalParam, bool active = false)
            {
                LinkText = linkText;
                var route = new Dictionary<string, object>();
                route.Add("page", page);
                if (addionalParam.HasValue)
                {
                    route.Add(addionalParam.Value.Key, addionalParam.Value);
                }
                Route = new RouteValueDictionary(route);
                IsActivePage = active;
            }
            public bool IsActivePage { get; set; }
            public string LinkText { get; set; }
            public RouteValueDictionary Route { get; set; }
        }

        public PagingModel(int numberOfHits, int pageSize, int activePage, KeyValuePair<string, string>? addionalParam = null)
        {
            if (numberOfHits <= pageSize)
                return;

            if (pageSize == 0)
                return;

            PagerLinks = new List<PagerLink>();

            if (numberOfHits > 0)
            {
                int totalPages = (int)Math.Ceiling((double)numberOfHits / pageSize);

                if (totalPages > 1)
                {
                    for (int index = 1; index < totalPages + 1; index++)
                    {
                        PagerLinks.Add(new PagerLink((index).ToString(), index, addionalParam, activePage == index));
                    }

                    if (activePage - 1 > 0)
                    {
                        PreviousPage = new PagerLink("<<",activePage - 1, addionalParam);
                    }

                    if (activePage < totalPages)
                    {
                        NextPage = new PagerLink(">>",activePage + 1, addionalParam);
                    }
                }
            }
        }
    }
}
