﻿namespace Norgesportalen.Models.ViewModels
{
    public class ImageViewModel
    {
        public string Url { get; set; }
        public string AltText { get; set; }
        public string Credits { get; set; }
        public string ImageText { get; set; }
    }
}