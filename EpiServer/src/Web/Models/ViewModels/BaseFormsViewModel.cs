﻿using System.Collections.Generic;
using EPiServer.Core;
using Norgesportalen.Models.Pages.BaseClasses;

namespace Norgesportalen.Models.ViewModels
{
    public class BaseFormsViewModel: PageViewModel<BaseFormsPage>
    {
        public BaseFormsViewModel(BaseFormsPage currentPage) : base(currentPage)
        {
        }
        public IEnumerable<PageData> Children { get; set; }
        public bool ShowChildrenList { get; set; }
        public bool UseListViewForChildren { get; set; }
        public virtual ContentArea FormsArea { get; set; }
    }
}