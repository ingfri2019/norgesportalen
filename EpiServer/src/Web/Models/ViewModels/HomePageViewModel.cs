﻿using Norgesportalen.Models.Pages;
using System.Linq;
using EPiServer.Core;
using Norgesportalen.Business;
using Norgesportalen.Business.Extensions;

namespace Norgesportalen.Models.ViewModels
{

    public class HomePageViewModel : PageViewModel<HomePage>
    {
        public HomePageViewModel(HomePage homepage) : base(homepage)
        {
        }


        public bool IsInEditMode()
        {
            return CurrentPage.IsInEditMode();
        }

        public string TopTaskBackgroundStyle
        {
            get
            {
                int id;
                if (int.TryParse(CurrentPage.TopTasksBackground, out id))
                {
                    ContentReference imageReference = new ContentReference(id);

                    if (imageReference == ContentReference.EmptyReference)
                        return string.Empty;

                    return string.Format("Style=background-image:url('{0}');", imageReference.GetImageUrl(Constants.ImageSize.Xlarge));
                }
                return string.Empty;
            }
        }

        public string ThemesLargeCssLayout
        {
            get
            {
                if (CurrentPage.ThemesLargeContentArea != null)
                {

                    /// for editor every items should count
                    if (IsInEditMode() && CurrentPage.ThemesLargeContentArea.Items.Count() == 3)
                        return "panel-medium panel";

                    // Only take into account items that are visible
                    if (CurrentPage.ThemesLargeContentArea.FilteredItems.Count() == 3)
                        return "panel-medium panel";
                }

                return "panel-large panel";

            }
        }

        public string ThemesPanelsCssLayout
        {
            get
            {
                if (CurrentPage.ThemesContentArea != null)
                {

                    /// for editor every items should count
                    if (IsInEditMode() && CurrentPage.ThemesContentArea.Items.Count() == 3)
                        return "panel-medium panel";

                    // Only take into account items that are visible
                    if (CurrentPage.ThemesContentArea.FilteredItems.Count() == 3)
                        return "panel-medium panel";
                }

                return "panel-large panel";

            }
        }

        /// <summary>
        /// Returns how many top task items is published, for presentation logic
        /// </summary>
        public int TopTasksCount()
        {
            if (IsInEditMode()) return 5; // Changing this will also cause the test TopTasksCount_Test to fail

            int count = 0;

            if (CurrentPage.TopTasksContentArea != null)
            {
                count += CurrentPage.TopTasksContentArea.FilteredItems.Count();
            }

            if (CurrentPage.TopTasksLinkList != null)
            {
                count += CurrentPage.TopTasksLinkList.Count();
            }

           if (CurrentPage.TopTasksTitleAndReadmoreLink != null)
           {
                count += CurrentPage.TopTasksTitleAndReadmoreLink.Count();
           }

            return count;
        }

        public int LeftTopTasksCount()
        {
            
            int count = 0;

            if (CurrentPage.TopTasksContentArea != null)
            {
                count += CurrentPage.TopTasksContentArea.FilteredItems.Count();
            }

            return count;
        }

        public ContentArea AutomaticNewsList { get; set; }
    }
}