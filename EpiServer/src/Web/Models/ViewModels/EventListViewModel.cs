using Norgesportalen.Models.Pages;
using System;
using System.Collections.Generic;
using EPiServer.Core;
using Norgesportalen.Business.Extensions;
using Norgesportalen.Business.Helpers;

namespace Norgesportalen.Models.ViewModels
{
    public class EventListViewModel : PageViewModel<EventListPage>
    {
        public PagingModel Paging;

        public IEnumerable<ListedEvent> EventsOnPage { get; set; }

        public int SelectedPage { get; set; }

        public EventListViewModel(EventListPage currentPage) : base(currentPage)
        {
        }

        public bool ShowFocusedEvent { get; set; }

        public static string FormatLocation(EventPage page)
        {
            if (!string.IsNullOrEmpty(page.Location))
            {
                return $" | {page.Location}";
            }

            return string.Empty;
        }

        public enum EventListType
        {
            Active,
            Archived
        }

        public class ListedEvent
        {
            public string LinkUrl;
            public string Location;
            public string Name;
            public string Text;
            public DateTime EventStartDate;
            public DateTime EventStopDate;
            public string FormattedEventDate;
            public ContentReference MainImage;
            public EventListType EventList;
           

            public ListedEvent(EventPage page)
            {
                var timeZoneId = page.EventTimezoneId;

                var tz = TimeZoneInfo.FindSystemTimeZoneById(timeZoneId);

                var eventStartDate = TimeZoneInfo.ConvertTimeFromUtc(page.EventStartDate.ToUniversalTime(), tz);
                var eventStopDate = TimeZoneInfo.ConvertTimeFromUtc(page.EventStopDate.ToUniversalTime(), tz);

                LinkUrl = page.LinkURL;
                Location = FormatLocation(page);
                Name = page.HeadingOrPageName;
                Text = page.MainIntro;
                EventStartDate = eventStartDate;
                EventStopDate = eventStopDate;
                FormattedEventDate = HtmlHelpers.FormatDateTimeForEventCard(eventStartDate, eventStopDate);
                MainImage = page.MainImage;
                EventList = ResolveEventList(page);
            }

            private EventListType ResolveEventList(EventPage page)
            {
                var timeZoneId = page.EventTimezoneId;
                if (page.EventStopDate.GetTimeZoneConvertedDateTime(timeZoneId) < DateTime.UtcNow.GetTimeZoneConvertedDateTime(timeZoneId))
                {
                    return EventListType.Archived;
                }
                return EventListType.Active;
            }
        }
    }
}