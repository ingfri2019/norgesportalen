﻿using System.Collections.Generic;
using EPiServer.ServiceLocation;
using Norgesportalen.Models.Pages;
using Norgesportalen.Models.Pages.BaseClasses;

namespace Norgesportalen.Models.ViewModels
{
    public class NewsletterPageViewModel
    {
        public NewsletterPage NewsletterPageData { get; set; }

        public bool HasContentInMainContentArea { get; set; }

        public bool HasLinks { get; set; }

        public bool DisplayUnsubscribeLink { get; set; }

        public List<BasicPageBase> MainContentPages { get; set; }

        public string SiteUrl { get; set; }

        public HomePage HomePage { get; set; }

        public NewsletterPageViewModel(NewsletterPage page)
        {
            DisplayUnsubscribeLink = page.UnsubscribePage != null;
            MainContentPages = new List<BasicPageBase>();
            SiteUrl = EPiServer.Web.SiteDefinition.Current.SiteUrl.ToString();
            NewsletterPageData = page;
            HasContentInMainContentArea = page.MainContentArea != null;
            HasLinks = page.Links != null;

            var contentLoader = ServiceLocator.Current.GetInstance<EPiServer.IContentLoader>();
            if (HasContentInMainContentArea)
            {
                foreach (var contentItem in NewsletterPageData.MainContentArea.Items)
                {
                    if (contentItem.ContentLink != null && contentItem.ContentLink.ID > 0)
                    {
                        var item = contentLoader.Get<BasicPageBase>(contentItem.ContentLink);
                        MainContentPages.Add(item);
                    }
                }
            }
        }
    }
}