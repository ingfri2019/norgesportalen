﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Norgesportalen.Business.Search;
using Norgesportalen.Models.Pages;

namespace Norgesportalen.Models.ViewModels
{
    public class SearchContentModel : PageViewModel<SearchPage>
    {
        public SearchContentModel(SearchPage currentPage) : base(currentPage)
        {
            this.PagerLinks = new List<CustomLink>();
            this.Hits = new List<SearchHit>();
        }

        public bool SearchServiceDisabled { get; set; }
        public string SearchedQuery { get; set; }
        public int NumberOfHits { get; set; }
        public IEnumerable<SearchHit> Hits { get; set; }

        //Paging
        public List<CustomLink> PagerLinks { get; set; }
        public CustomLink PreviousPage { get; set; }
        public CustomLink NextPage { get; set; }
        public int CurrentPageNo { get; set; }
        public int TotalPages { get; set; }
        public IEnumerable<CustomLink> Items { get; set; }

        public class SearchHit
        {
            public string Title { get; set; }
            public string Url { get; set; }
            public string Excerpt { get; set; }
        }
    }
}