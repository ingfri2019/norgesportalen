﻿using Norgesportalen.Models.Pages;
using Norgesportalen.Models.Pages.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Routing;

namespace Norgesportalen.Models.ViewModels
{
    public class FilteredListViewModel : PageViewModel<FilteredListPage>
    {
        public IEnumerable<ListedArticle> ArticlesOnPage { get; set; }

        public PagingModel Paging;


        public FilteredListViewModel(FilteredListPage currentPage) : base(currentPage)
        {

        }


        public class ListedArticle
        {
            public ListedArticle(BasicPageBase page, Type type)
            {
                LinkUrl = page.LinkURL;
                Changed = page.HideDate == false ? page.Changed.ToString(BasicPageBase.GetDateFormatString()) : string.Empty;
                Published = page.HideDate == false && page.StartPublish.HasValue ? page.StartPublish.Value.ToString(BasicPageBase.GetDateFormatString()) : string.Empty;
                Location = FormatLocation(page);
                Name = page.HeadingOrPageName;
                Text = BasicPageBase.TeaserOrFallbackToIntro(page.TeaserText, page.MainIntro);
                Type = type.Name.Replace("Proxy","");
            }

            public string LinkUrl;
            public string Changed;
            public string Published;
            public string Location;
            public string Name;
            public string Text;
            public string Type;
        }

        // funker ikke så bra med on page edit
        public static string FormatLocation(BasicPageBase page)
        {
            if (page.Location != null && page.Location != string.Empty)
            {
                if (!page.HideDate)
                {
                    return string.Format(" | {0}", page.Location);
                }
                else return page.Location;
            }
            else
                return string.Empty;
        }


        #region Filters

        public class ListPageFilter
        {
            public ListPageFilter(string filterType, string filterQuery)
            {
                if (filterQuery == null)
                    filterQuery = string.Empty;


                List<string> filterValues = new List<string> { };
                if (!string.IsNullOrEmpty(filterQuery))
                    if (filterQuery.Contains(','))
                        filterValues = filterQuery.Split(',').ToList();
                    else
                        filterValues.Add(filterQuery);

                if (filterQuery.Contains(filterType))
                    filterValues.Remove(filterType);
                else
                    filterValues.Add(filterType);


                RouteValueDictionary route = null;
                if (filterValues.Count() > 0)
                {
                    route = new RouteValueDictionary();
                    string value = filterValues.Count == 1 ? filterValues.First() : string.Join(",", filterValues);
                    route.Add("filter", value);
                }
                Route = route;
                IsChecked = filterQuery.Contains(filterType);

            }

            public RouteValueDictionary Route;
            public bool IsChecked;
            public bool IsVisilbe;
            public IDictionary<string, object> HtmlAttributes
            {
                get
                {
                    var css = new Dictionary<string, object>();
                    if (IsChecked)
                    {
                        css.Add("Class", "checkbox checked");
                    }
                    else
                    {
                        css.Add("Class", "checkbox unchecked");
                    }
                    return css;
                }
            }
        }

        #endregion

    }
}