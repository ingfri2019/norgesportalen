﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EPiServer.Core;

namespace Norgesportalen.Models.ViewModels
{
    public class GenericMediaViewModel
    {
        public string Url{ get; set; }
        public string Intro { get; set; }
        public string Name { get; set; }
    }
}