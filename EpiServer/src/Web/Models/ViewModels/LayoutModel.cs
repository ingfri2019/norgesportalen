using System.Web;
using System.Web.Routing;
using Norgesportalen.Business.Language;
using Norgesportalen.Business.Menu;
using Norgesportalen.Models.Blocks;
using Norgesportalen.Models.Pages;
using DSS.PageSurvey.Models.ViewModels;
using EPiServer.Core;

namespace Norgesportalen.Models.ViewModels
{
    public class LayoutModel
    {
        //Header
        public IHtmlString LogotypeLinkUrl { get; set; }
        public LogoBlock Logo { get; set; }
        public Menu Menu { get; set; }
        public HomePage HomePage { get; set; }
        public string HomePageTitle { get; set; }
        public bool CurrentPageIsHomePage { get; set; }
        public bool CurrentPageIsCountriesOverviewPage { get; set; }
        public LanguageOptions LanguageOptions { get; set; }
        public RouteValueDictionary SearchPageRouteValues { get; set; }
        public ContentArea NationalEmergencyArea { get; set; }       
        public SurveyViewModel SurveyViewModel { get; set; }
        public GdprPopupPage GdprPopupPage { get; set; }
    }
}
