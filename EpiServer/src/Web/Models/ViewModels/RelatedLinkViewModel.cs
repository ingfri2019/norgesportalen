﻿namespace Norgesportalen.Models.ViewModels
{
    public class RelatedLinkViewModel
    {
        public string Text { get; set; }
        public string Intro { get; set; }
        public string Url { get; set; }
    }
}