﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EPiServer.Core;
using Norgesportalen.Models.Pages;
using Norgesportalen.Models.Pages.BaseClasses;

namespace Norgesportalen.Models.ViewModels
{
    public class InfoScreenViewModel : PageViewModel<InfoScreenPage>
    {
        public InfoScreenViewModel(InfoScreenPage currentPage) : base(currentPage)
        {

        }

        public InfoScreenPage PageData { get; set; }

        public string LinksJsList { get; set; }

    }
}