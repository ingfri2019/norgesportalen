﻿using System;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Forms.Core;
using EPiServer.Forms.EditView.DataAnnotations;
using EPiServer.Forms.Helpers.Internal;
using EPiServer.Forms.Implementation.Elements.BaseClasses;
using EPiServer.Forms.Implementation.Validation;
using EPiServer.ServiceLocation;
using EPiServer.Web.Routing;

namespace Norgesportalen.Models.Elements
{
    [ContentType(
        DisplayName = "Hidden email page property",
        Description = "Reads the email value stored in page property \"FormsEmail\"",
        GroupName = "Custom Elements",
        GUID = "DD764635-621D-4EE5-81B5-459616F2B129")]
    [AvailableValidatorTypes(Include = new Type[] { typeof(RequiredValidator), typeof(EmailValidator) })]
    [ImageUrl("~/Frontend/dist-web/Static/images/form-hiddenelementblock.png")]
    public class PagePropertyHiddenEmailElementBlock : DataElementBlockBase, IViewModeInvisibleElement
    {
        /// <inheritdoc />
        /// <summary>Hidden element does not have label, because it is not displayed for Visitor</summary>
        [Ignore]
        public override string Label { get; set; }

        /// <inheritdoc />
        /// <summary>Hidden element does not have desc, because it is not displayed for Visitor</summary>
        [Ignore]
        public override string Description { get; set; }


        public virtual string Email
        {
            get
            {
                try
                {
                    var pageHandler = ServiceLocator.Current.GetInstance<IPageRouteHelper>();
                    var email = pageHandler.Page.GetPropertyValue<string>("PageEmail");
                    var returnValue = (email != null ? email : ((PredefinedValue != null ? PredefinedValue : string.Empty)));
                    return returnValue;
                }
                catch
                {
                    return string.Empty;
                }
            }
        }

        public override void SetDefaultValues(ContentType contentType)
        {
            base.SetDefaultValues(contentType);

            // Set default validators to required and email
            var validators = new string[] { typeof(EPiServer.Forms.Implementation.Validation.RequiredValidator).FullName, typeof(EPiServer.Forms.Implementation.Validation.EmailValidator).FullName };
            Validators = string.Join(EPiServer.Forms.Constants.RecordSeparator, validators);
        }


        public virtual string GetDefaultViewLocation()
        {
            var defaultPathToBlockViews = "~" + ModuleHelper.ToResource(this.GetType(), "Views/Shared/ElementBlocks");
            return defaultPathToBlockViews;
        }

        public override string EditViewFriendlyTitle
        {
            get
            {
                if (Email != null && !Email.Contains("@"))
                {
                    return $"Property is not configured with an email. Value is set to : {Email}";
                }
                return Email;
            }
        }
    }
}