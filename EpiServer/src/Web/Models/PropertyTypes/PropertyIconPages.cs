﻿using System;
using EPiServer.Core;
using EPiServer.PlugIn;

namespace Norgesportalen.Models.PropertyTypes
{
    [Serializable]
    [PropertyDefinitionTypePlugIn(DisplayName = "Icon available on pages", Description = "Velg en eller flere")]
    public class PropertyIconPages : PropertyString
    {

    }
}
