﻿namespace Norgesportalen.Models.Interfaces
{
    public interface IHasPageEmail
    {
        string PageEmail { get; set; }
    }
}
