﻿using EPiServer.Core;

namespace Norgesportalen.Models.Interfaces
{
    public interface IHasMainImage
    {
        ContentReference MainImage { get; set; }
    }
}
