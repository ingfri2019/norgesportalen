using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Security;
using System.ComponentModel.DataAnnotations;

namespace Norgesportalen
{
    public class Global
    {
        /// <summary>
        /// Group names for content types and properties
        /// </summary>
        [GroupDefinitions()]
        public static class GroupNames
        {
            [Display(Name = "Content", Order = 1)]
            public const string Content = SystemTabNames.Content;

            [Display(Name = "Default", Order = 2)]
            public const string Default = "Default";

            [Display(Name = "Header", Order = 4)]
            [RequiredAccess(AccessLevel.Administer)]
            public const string Header = "Header";

            [Display(Name = "Top tasks", Order = 5)] // Only used on front page (HomePage.cs)
            public const string TopTasks = "TopTasks";

            [Display(Name = "Footer", Order = 5)]
            public const string Footer = "Footer";

            [Display(Name = "Menu", Order = 6)]
            public const string Menu = "Menu";

            [Display(Name = "Emergency & Information", Order = 7)]
            [RequiredAccess(AccessLevel.Administer)]
            public const string EmergencyAndInformation = "EmergencyAndInformation";

            [Display(Name = "Additional content", Order = 8)]
            public const string AdditionalContent = "Additional content";

            [Display(Name = "Form", Order = 9)]
            public const string Forms = "Form";

            [Display(Name = "Newsletter list", Order = 9)]
            public const string NewsletterList = "NewsletterList";

            [Display(Name = "SoMe & Metadata", Order = 10)]
            public const string MetaData = "SoMeAndMetadata";

            [Display(Name = "Icon", Order = 12)]
            [RequiredAccess(AccessLevel.Administer)]
            public const string Icons = "Icons";

            [Display(Name = "Backgrounds", Order = 13)]
            [RequiredAccess(AccessLevel.Administer)]
            public const string Backgrounds = "Backgrounds";

            [Display(Name = "Email", Order = 14)]
            public const string Email = "Email";

            [Display(Name = "List alternatives", Order = 14)]
            public const string ListAlternatives = "ListAlternatives";

            [Display(Name = "Suggested page types", Order = 16)]
            [RequiredAccess(AccessLevel.Administer)]
            public const string SuggestedPageTypes = "SuggestedPageTypes";

            [Display(Name = "Not in use", Order = 17)]
            [RequiredAccess(AccessLevel.Administer)]
            public const string NotInUse = "NotInUse";

            [Display(Name = "Info screen", Order = 18)]
            public const string InfoScreen = "Info screen";

            [Display(Name = "Config", Order = 1000)]
            [RequiredAccess(AccessLevel.Administer)]
            public const string Config = "Config";


        }

        /// <summary>
        /// Group names for page types
        /// </summary>
        [GroupDefinitions()]
        public static class PageTypeGroupNames
        {
            // Group names used for grouping and sorting page types (on create)
            [Display(Name = "News and event articles", Order = 100)]
            public const string NewsAndEvents = "News and event articles";

            [Display(Name = "Newsletter", Order = 200)]
            public const string Newsletter = "Newsletter";

            [Display(Name = "Visa and residence", Order = 300)]
            public const string VisaResidence = "Visa and residence";

            [Display(Name = "Admin", Order = 400)]
            public const string Admin = "Admin";

            [Display(Name = "Other page types", Order = 500)]
            public const string OtherPageTypes = "Other page types";
        }

        /// <summary>
        /// Group names for blocks
        /// </summary>
        [GroupDefinitions()]
        public static class BlockGroupNames
        {
            [Display(Name = "Accordions", Order = 100)]
            public const string Accordions = "Accordions";

            [Display(Name = "Links", Order = 200)]
            public const string Links = "Links";

            [Display(Name = "Media", Order = 300)]
            public const string Media = "Media";

            [Display(Name = "Admin", Order = 400)]
            public const string Admin = "Admin blocks";
        }

        /// <summary>
        /// Contains static data typically reused for several properties
        /// </summary>
        public static class PropertyDescriptorValues
        {
            public const string MainBodyDescription = "The main body will be shown in the main content area of the page, using the XHTML-editor you can insert for example text, images and tables.";

            public const string FetchDataFromHelperName = "Fetch data from central content";
            public const string FetchDataFromHelperDescription = "Replaces key properties on the page with properties from the chosen replacement page (typically from \"central content\"). You can edit this in the on page editor.";
            public const string FetchDataReadOnlyTabInfo = "This page is set to fetch content from another page. Because of this setting local editors cannot edit this page. If you want to add <i>additional</i> content to this page, select the \"<strong>Additional content</strong>\" tab and add it there. Read more about \"central content\" in the <a href=\"https://edit.norway.no/en/tum\"><span style=\"color: #2880b9;\"><strong>users manual<strong></span></a>.";

            public const string AddALocalIngress = "Add a local ingress";
            public const string AddALocalMainBody = "Add a local main body to the text area";
            public const string AddLocalRelevantLinks = "Add local relevant links to the article";
        }

        /// <summary>
        /// Indicates tabs where the properties should be disabled when content is "fetched" (and enabled when it's not).<para />
        /// 
        /// On certain pages you can "fetch content" from a different page. This renders the existing content in certain
        /// tabs unnecessary, and should thus be disabled for editing. In other words, when the functionality is used, 
        /// some tabs have editable content and some should be disabled. And vice versa when fetch content is not used.
        /// </summary>
        public static readonly string[] DisableTabContentWhenFetchContent = {
            Global.GroupNames.Content,
            Global.GroupNames.MetaData,
            Global.GroupNames.ListAlternatives
        };

        /// <summary>
        /// Indicates tabs where the properties should be enabled when content is "fetched" (and disabled when it's not).<para />
        /// 
        /// On certain pages you can "fetch content" from a different page. When this functionality is NOT used, certain
        /// tabs should be disabled for editing. In other words, when the functionality is used, some tabs have editable 
        /// content and some should be disabled. And vice versa when fetch content is not used.
        /// </summary>
        public static readonly string[] DisableTabContentWhenNotFetchContent = {
            Global.GroupNames.AdditionalContent
        };


        public static class ContentAreaTags
        {
            public const string HomePageTopTasks = "TopTasksArea";
            public const string RelatedEvents = "RelatedEvents";
            public const string HomePageThemes = "ThemesArea";
            public const string HomePageManualNews = "ManualNewsArea";
            public const string NewsList = "NewsList";
            public const string HomePageAutomaticNews = "AutomaticNewsArea";
            public const string Footer = "Footer";
            public const string SectionsPage = "SectionsPage";
            public const string RelatedLinks = "RelatedLinks";
            public const string TopContactAccordion = "TopContactAccordion";
            public const string ContactAccordion = "ContactAccordion";
            public const string Newsletter = "Newsletter";
            public const string FocusedEvent = "FocusedEvent";
            

            public static class SocialMedia
            {
                public const string Footer = "Footer";
                public const string List = "List";
                public const string Panel = "Panel";
                public const string Inline = "Inline";
                public const string Newsletter = "Newsletter";
            }
        }

        /// <summary>
        /// Virtual path to folder with static graphics, such as "~/Static/gfx/"
        /// </summary>
        public const string StaticGraphicsFolderPath = "~/Static/gfx/";
    }
}
