'use strict';

var pageSurvey = document.querySelector('[data-page-survey]');

if (pageSurvey) {
    (function () {

        // fn to set multiple attributes on an element

        var setAttributes = function setAttributes(el, attrs) {
            for (var key in attrs) {
                el.setAttribute(key, attrs[key]);
            }
        }

        // fn to insert element after another element
        ;

        var insertAfter = function insertAfter(el, referenceNode) {
            referenceNode.parentNode.insertBefore(el, referenceNode.nextSibling);
        }

        // fieldset
        ;

        // fn to asynchronously post survey answer using

        var post = function post(data, success, failed) {
            var xhr = new XMLHttpRequest();
            var url = surveyApi;
            xhr.open('POST', url, true);
            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            xhr.send(data);
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        if (xhr.responseText === '"Success"') {
                            success();
                        } else {
                            failed();
                        }
                    } else {
                        failed();
                    }
                }
            };
            return xhr;
        };

        var surveyId = pageSurvey.dataset.pageSurvey;
        var surveyApi = pageSurvey.dataset.pageSurveyApi;
        var str_legend = pageSurvey.dataset.textHiddenTitle;
        var str_question = pageSurvey.dataset.textQuestion;
        var arr_options = JSON.parse(pageSurvey.dataset.textOptions);
        var str_comment = pageSurvey.dataset.textComment;
        var str_send = pageSurvey.dataset.textSend;
        var str_thanks = pageSurvey.dataset.textThanks;
        var str_failed = pageSurvey.dataset.textFailed;
        var err_illegal = pageSurvey.dataset.textIllegalCharacters;var fieldset = document.createElement('fieldset');
        fieldset.setAttribute('data-page-survey-fieldset', '');

        // legend
        var legend = document.createElement('legend');
        legend.className = 'visuallyhidden';
        legend.innerHTML = str_legend;

        // heading
        var heading = document.createElement('h2');
        heading.innerHTML = str_question;

        // options
        var optionsWrapper = document.createElement('div');
        optionsWrapper.className = 'options';

        // comments field
        var commentsWrapper = document.createElement('div');
        commentsWrapper.className = 'comments';
        commentsWrapper.setAttribute('data-page-survey-comments', '');

        // comments label
        var commentsLabel = document.createElement('label');
        commentsLabel.setAttribute('for', 'PageSurveyAnswerComment' + surveyId);
        commentsLabel.innerHTML = str_comment;
        var commentsField = document.createElement('textarea');
        setAttributes(commentsField, {
            'id': 'PageSurveyAnswerComment_' + surveyId,
            'name': 'PageSurveyAnswerComment_' + surveyId,
            'maxlength': '255',
            'cols': '24',
            'rows': '4',
            'placeholder': ''
        });

        // loop through options array

        var _loop = function (i) {
            var index = i;
            var humanId = Number(index) + 1;

            // create option elements
            var option = document.createElement('input');
            setAttributes(option, {
                'type': 'radio',
                'name': 'pageSurveyQuestion_' + surveyId,
                'id': 'PageSurveyOption_' + surveyId + '_' + humanId,
                'value': arr_options[i].response,
                'data-text': arr_options[i].feedback
            });

            // assign click fn to option elements
            option.addEventListener('click', function () {
                commentsField.setAttribute('placeholder', arr_options[index].feedback);
                commentsWrapper.classList.add('shown');
                submitWrapper.classList.add('shown');
            });

            // option label
            var optionLabel = document.createElement('label');
            optionLabel.setAttribute('for', 'PageSurveyOption_' + surveyId + '_' + humanId);
            optionLabel.innerHTML = arr_options[i].response;

            var optionWrapper = document.createElement('div');
            optionWrapper.className = 'option';

            optionWrapper.appendChild(option);
            optionWrapper.appendChild(optionLabel);

            optionsWrapper.appendChild(optionWrapper);
        };

        for (var i in arr_options) {
            _loop(i);
        }

        // submit
        var submitWrapper = document.createElement('div');
        submitWrapper.className = 'submit';
        var submitButton = document.createElement('button');
        setAttributes(submitButton, {
            'type': 'submit',
            'id': 'PageSurveyAnswerSubmitButton_' + surveyId,
            'value': str_send
        });
        submitButton.innerHTML = str_send;
        submitButton.addEventListener('click', function () {
            var element = document.createElement('div');
            var comment = commentsField.value;
            if (comment && typeof comment === 'string') {
                // strip script/html tags
                comment = comment.replace(/<script[^>]*>([\S\s]*?)<\/script>/gmi, '');
                comment = comment.replace(/<\/?\w(?:[^"'>]|"[^"]*"|'[^']*')*>/gmi, '');
                comment = comment.replace(/<\/?\w(?:[^"'>]|"[^"]*"|'[^']*')*>/g, '');
                element.innerHTML = comment;
                comment = element.textContent;
                element.textContent = '';
            } else {
                comment = '';
            }
            var commentEncoded = encodeURI(comment);

            var pathname = document.location.href;
            var optionName = 'pageSurveyQuestion_' + surveyId;
            var optionSelectedId = Array.from(document.getElementsByName(optionName)).find(function (r) {
                return r.checked;
            }).id;
            var optionSelectedIndex = optionSelectedId.substring(optionSelectedId.length - 1, optionSelectedId.length);
            var optionSelectedText = Array.from(document.getElementsByName(optionName)).find(function (r) {
                return r.checked;
            }).value;

            // callback handlers
            var callBackFailed = function callBackFailed() {
                fieldset.style.display = 'none';
                failed.classList.add('shown');
            };

            var callBackSuccess = function callBackSuccess() {
                fieldset.style.display = 'none';
                thanks.classList.add('shown');
                if (typeof dataLayer !== 'undefined') {
                    dataLayer.push({
                        'feedback_text': comment,
                        'event': 'page_feedback_text'
                    });
                }
            };

            var pageSurveyInfo = 'PageSurveyId=' + surveyId + '&Option=' + optionSelectedIndex + '&OptionText=' + optionSelectedText + '&Comment=' + commentEncoded + '&Pathname=' + pathname;

            post(pageSurveyInfo, callBackSuccess, callBackFailed);
            submitButton.setAttribute('disabled', 'disabled');
        });
        submitWrapper.appendChild(submitButton);

        fieldset.appendChild(legend);
        fieldset.appendChild(heading);
        fieldset.appendChild(optionsWrapper);
        fieldset.appendChild(commentsWrapper);
        fieldset.appendChild(submitWrapper);

        pageSurvey.appendChild(fieldset);

        var thanks = document.createElement('div');
        thanks.className = 'thankyounote';
        thanks.innerHTML = str_thanks;
        thanks.setAttribute('data-thank-you-note', '');

        var failed = document.createElement('div');
        failed.className = 'failednote';
        failed.innerHTML = str_failed;
        failed.setAttribute('data-failed-note', '');

        insertAfter(failed, fieldset);
        insertAfter(thanks, fieldset);

        // validate comments
        commentsField.addEventListener('keyup', function () {
            validateComment(commentsField, submitButton);
        });

        // error message
        var errorMessageWrapper = document.createElement('div');
        errorMessageWrapper.className = 'error-message';

        commentsWrapper.appendChild(commentsLabel);
        commentsWrapper.appendChild(commentsField);
        commentsWrapper.appendChild(errorMessageWrapper);

        // fn to validate comment
        var validateComment = function validateComment(textarea) {
            var commentsRegex = /^[a-zA-Z0-9-+æøåÆØÅČčdĐđÁáŊŋŠšŦŧŽž .,!?()"':;\r\n]+$/;
            var comment = textarea.value;
            var errorMessage = errorMessageWrapper;

            if (comment === '') {
                return true;
            } else {
                if (!commentsRegex.test(comment) || comment.length > 256) {
                    errorMessage.innerHTML = err_illegal;
                    errorMessage.classList.add('shown');
                    submitButton.setAttribute('disabled', 'disabled');
                    return false;
                } else {
                    errorMessage.innerHTML = '';
                    errorMessage.className = 'error-message';
                    submitButton.removeAttribute('disabled');
                    return true;
                }
            }
        };;
    })();
}