﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="PageSurveys.aspx.cs" EnableEventValidation="false" Inherits="DSS.PageSurvey.Plugins.Admin.PageSurveys" %>
<%@ Register tagPrefix="EPiServerUI" namespace="EPiServer.UI.WebControls" assembly="EPiServer.UI" %>

<asp:Content ContentPlaceHolderID="FullRegion" runat="server">
    <div class="epi-padding epi-contentArea">
        <asp:GridView ID="gwPageSurveysList" runat="server" DataKeyNames="PageSurveyId" AutoGenerateColumns="false" CssClass="epi-default">
            <Columns>
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <EPiServerUI:ToolButton ID="btSelectSurvey" runat="server" CausesValidation="False" OnClick="btSelectSurvey_Click"
                                CommandArgument='<%# Eval("PageSurveyId") %>' Text="Se svar" ImageUrl="~/App_Themes/Default/Images/Tools/ViewMode.gif" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="AnswerCounter" HeaderText="Ant svar" />
                <asp:BoundField DataField="Svar1" HeaderText="Svar 1" />
                <asp:BoundField DataField="Svar2" HeaderText="Svar 2" />
                <asp:BoundField DataField="Name" HeaderText="Navn" />
                <asp:BoundField DataField="Activated" HtmlEncode="false" HeaderText="Aktiv fra" DataFormatString="{0:MM-dd-yyyy}" />
                <asp:BoundField DataField="Deactivated" HeaderText="Aktiv til" HtmlEncode="false" DataFormatString="{0:MM-dd-yyyy}" />              

                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <EPiServerUI:ToolButton ID="btDeleteAllAnswer" runat="server" CausesValidation="False" OnClientClick='return confirm("Slette alle svar for denne undersøkelsen?");'
                                                OnClick="btDeleteAllAnswers_Click" CommandArgument='<%# Eval("PageSurveyId") %>'
                                                Text="Slett alle svar" ImageUrl="~/App_Themes/Default/Images/Tools/Delete.gif" />
                    </ItemTemplate>  
                </asp:TemplateField>
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <EPiServerUI:ToolButton ID="btDeleteSurvey" runat="server" CausesValidation="False" OnClientClick='return confirm("Slette hele undersøkelsen?");'
                                                OnClick="btDeleteSurvey_Click" CommandArgument='<%# Eval("PageSurveyId") %>'
                                                Text="Slett Survey" ImageUrl="~/App_Themes/Default/Images/Tools/Delete.gif" />
                    </ItemTemplate>  
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <hr />
        <div>
            <asp:Panel ID="panelDownloadSurveyAnswers" runat="server">
                <asp:HiddenField ID="hiddenPageSurveyAnswerID" runat="server" />
                
                <EPiServerUI:ToolButton ID="btDownloadSurveyAnswers" CausesValidation="False" runat="server" 
                    ImageUrl="/App_Themes/Default/Images/Tools/Export.gif" OnClick="btDownloadSurveyAnswers_Click" Text="Eksportér til Excel"></EPiServerUI:ToolButton><br/><br/>

            </asp:Panel>
            <asp:GridView ID="gwPageSurveyAnswers" runat="server" AutoGenerateColumns="false"
                DataKeyNames="PageSurveyAnswerID" AllowPaging="false" AllowSorting="false">
                <Columns>
                    <asp:BoundField DataField="PageSurveyAnswerID" Visible="false" />
                    <asp:BoundField DataField="PageSurveyID" Visible="false" />
                    <asp:BoundField  DataField="AnswerTime" HtmlEncode="false" HeaderText="Sendt" DataFormatString="{0:dd.MM.yy&nbsp;HH:mm}" />
                    <asp:BoundField DataField="CheckedAnswer" HeaderText="Valgt svar" ItemStyle-HorizontalAlign="Center" />
                    <asp:TemplateField HeaderText="Kommentar">
                        <ItemTemplate>
                            <asp:Label ID="CommentLabel" runat="server" Text='<%# Bind("Comment") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="PageUrl" HeaderText="Fra side" HtmlEncode="false" />
                    <asp:BoundField DataField="PageId" HeaderText="Side ID" HtmlEncode="false" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <EPiServerUI:ToolButton ID="btDeleteOneAnswer" runat="server" OnClick="btDeleteOneAnswer_Click" CausesValidation="False"
                                                    Text="Slett svar" CommandArgument='<%# Eval("PageSurveyAnswerId") %>' ImageUrl="~/App_Themes/Default/Images/Tools/Delete.gif"/>
                        </ItemTemplate>                                             
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </div>
</asp:Content>