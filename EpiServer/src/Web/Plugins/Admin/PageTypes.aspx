﻿<%@ Page Language="C#" AutoEventWireup="True" EnableViewState="True" Inherits="Norgesportalen.Plugins.Admin.PageTypes" CodeBehind="PageTypes.aspx.cs" %>
<%@ Import Namespace="EPiServer.Editor" %>
<%@ Import Namespace="EPiServer.Core" %>

<asp:content id="Content2" contentplaceholderid="FullRegion" runat="server">  

    <div class="epi-padding epi-contentArea">
        
        <div>
            <h1>List ut sider av angitt type</h1>
            <p style="border: solid 1px; padding: 0.2em;">
                Dette verktøyet brukes til å vise en liste med lenker til sider av en angitt sidetype.<br />
                Velg rotside for å begrense til sider som ligger under rotsiden.<br />
                Velg en egenskap for å kun liste ut sider der denne egenskapen er tom.<br />
            </p>
            <div>
                <br/>
                Velg språk: <br/>
                <asp:DropDownList ID="ddlLanguage" AutoPostBack="true" OnSelectedIndexChanged="OnChange_LanguageUpdated" runat="server" />
            </div>
            <div>
                <br/>
                Velg sidetype: <br/>
                <asp:DropDownList ID="ddlPageTypes" AutoPostBack="true" OnSelectedIndexChanged="OnChange_PageTypeUpdated" runat="server" />
            </div>
            <div>
                <br/>
                Velg rotside: <EPiServer:InputPageReference ID="iprRootPage" OnValueChanged="OnChange_RootPageUpdated" AutoPostBack="true" runat="server" />
            </div>
            <div>
                <br/>
                Velg egenskap som ikke er utfylt: <br/>
                <asp:DropDownList ID="ddlProperty" OnSelectedIndexChanged="OnChange_PropertyUpdated" AutoPostBack="True" runat="server" />
            </div>

            <div>
                <br/>
                <br/>
                <asp:Button Text="Eksporter alle sider (CSV)" OnClick="ExportAllPages" runat="server" />
            </div>
        </div>
        
        
        <asp:placeholder ID="phPages" Visible="False" runat="server">
            <div> 
                <p>
                    Antall sider: <asp:literal ID="litCount" runat="server"/><br/>
                </p>      
                
                <asp:PlaceHolder runat="server" ID="phPagesMoreThan1000">
                    <p>Dette verktøyet viser maks 1000 sider.</p>
                </asp:PlaceHolder>
                <table id="pages">
                    <tr>
                        <td>Navn</td>
                    </tr>
                    <asp:Repeater ID="plPages" runat="server">
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <a href="<%# PageEditing.GetEditUrlForLanguage((ContentReference)Eval("ContentLink"), ((System.Globalization.CultureInfo)Eval("Language")).Name) %>" target="_blank">
                                        <%#  Eval("PageName") %> (<%# ((PageData)Container.DataItem).ContentLink.ID %>)
                                    </a>                                    
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </table>
            </div>
        </asp:placeholder>     
             
    </div>   
</asp:content>