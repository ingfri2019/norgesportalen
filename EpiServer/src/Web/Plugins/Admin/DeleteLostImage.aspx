﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DeleteLostImage.aspx.cs" Inherits="Norgesportalen.Plugins.Admin.DeleteLostImage" %>

<asp:content id="Content2" contentplaceholderid="FullRegion" runat="server">  

    <div class="epi-padding epi-contentArea">
        <div>
           <h1>Slette korrupt bildefil</h1>
            <br/>
            <p>
                Dette verktøyet brukes til å slette en bildefil som har blitt lastet opp ufullstendig.<br/>
                Merk: Hvis bildefilen er i bruk vil den ikke bli slettet.<br/><br/>
                Episerver bug fikset i CMS 11, dette er en midlertidig workaround.<br/>
                Se forøvrig https://world.episerver.com/support/Bug-list/bug/CMS-6995
            </p>            
            <div>
                <label for="txtPageId">Imagefile page id: </label>
                <asp:TextBox runat="server" ID="txtPageId" Width="95"></asp:TextBox>
            </div>
            <br/> 
                <div>
                 <span class="epi-cmsButton" id="epiCmsButton">
                    <asp:Button runat="server" Text="Slett imagefil" OnClick="DeleteLostImg"
                        OnClientClick=" javascript:DisplayProgressBar(); "
                        CssClass="epi-cmsButton-text epi-cmsButton-tools epi-cmsButton-NewPage"/>
                 </span>
                  </div>
            <br/>
          <hr/>
          <h2>Statusvindu</h2>
          <asp:TextBox ID="txtStats" runat="server" Width="800px" Height="380px" TextMode="MultiLine"></asp:TextBox>
        </div>        
    </div>
</asp:content>


