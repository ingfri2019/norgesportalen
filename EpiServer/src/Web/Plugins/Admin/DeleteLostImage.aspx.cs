﻿using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAccess;
using EPiServer.Logging.Compatibility;
using EPiServer.PlugIn;
using EPiServer.Security;
using EPiServer.ServiceLocation;
using EPiServer.Shell.WebForms;
using EPiServer.Web;
using Norgesportalen.Models.Media;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Settings = EPiServer.Configuration.Settings;

namespace Norgesportalen.Plugins.Admin
{
    [GuiPlugIn(DisplayName = "[Admin] Slette tapt bildefil",
              Area = PlugInArea.AdminMenu,
              Url = "~/Plugins/Admin/DeleteLostImage.aspx",
              SortIndex = 4002,
              Category = "Tools")]
    public partial class DeleteLostImage : ContentWebFormsBase
    {
        private static readonly ILog Log = LogManager.GetLogger("DeleteLostImage");

        protected override void OnInit(EventArgs e)
        {
            if (PrincipalInfo.HasAdminAccess == false)
                AccessDenied();

            base.OnInit(e);
        }

        public void DeleteLostImg(object sender, EventArgs eventArgs)
        {
            var status = new StringBuilder();

            try
            {
                if (string.IsNullOrWhiteSpace(txtPageId.Text))
                {
                    throw new Exception("Error: Mangler page id");
                }
                int pageid = int.Parse(txtPageId.Text);
                status.AppendLine("Page id:"+pageid);

                var contentRepo = ServiceLocator.Current.GetInstance<IContentRepository>();
                var contentRef =  new ContentReference(pageid);
                if (contentRef == null)
                {
                    throw new Exception("Error: page id "+pageid+" not found");
                }
                var currPage = contentRepo.Get<ImageFile>(contentRef);

                if (currPage == null)
                {
                    throw new Exception("Error: content with page id " + pageid + " is not an ImageFile");
                }
                var deleteItemIfItIsReferencedByOtherContent = false; // Set to true if you want to force the delete. This will create broken links if anything references the content.
                contentRepo.Delete(contentRef, deleteItemIfItIsReferencedByOtherContent, AccessLevel.Delete);
               
                status.AppendLine("ImageFile deleted ok");

                /* The code below was copied off the net to fix problem, but gives another error so couldn't use it
                //Use something more specific than IContent if you have a lot of children to the ContentAssetsRoot
                var contentAssetChildren = contentRepo.GetChildren<IContent>(SiteDefinition.Current.ContentAssetsRoot);
                
                foreach (var child in contentAssetChildren.Where(c => !(c is ContentAssetFolder)))
                {
                    status.AppendLine("Imagefile id:" + child.ContentLink.ID);
                    //contentRepo.Delete(child.ContentLink, deleteItemIfItIsReferencedByOtherContent, AccessLevel.Delete);
                }
                */
            }
            catch (Exception ex)
            {
                status.AppendLine(String.Format("Error: {0}", ex));
            }

            txtStats.Text = status.ToString();
            txtStats.DataBind();
        }
    

        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}