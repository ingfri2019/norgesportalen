﻿using EPiServer.DataAbstraction;

namespace Norgesportalen.Plugins.Admin.ContentTypeOverview
{
    public class PropertyTranslation
    {
        public string Description { get; set; }
        public string DisplayName { get; set; }
        public LanguageBranch Language { get; set; }
    }
}