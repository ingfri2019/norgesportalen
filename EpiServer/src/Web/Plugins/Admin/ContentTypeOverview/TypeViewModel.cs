﻿
namespace Norgesportalen.Plugins.Admin.ContentTypeOverview
{
    public class TypeViewModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string GroupName { get; set; }
        public string DisplayName { get; set; }
        public int? SortIndex { get; set; }
        public int GroupSortIndex { get; set; }
        public string DescriptionButton { get; set; }
        public string PropertyName { get; set; }
        public string Type { get; set; }
        public string CultureSpecific { get; set; }
        public string IsRequired { get; set; }
        public string InheritedFrom { get; set; }
        public string Id { get; set; }
    }
}