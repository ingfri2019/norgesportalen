﻿
namespace Norgesportalen.Plugins.Admin.ContentTypeOverview
{
    public enum TranslatedPropertyType
    {
        Description,
        DisplayName
    }
}