﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TypeViewUserControl.ascx.cs" Inherits="Norgesportalen.Plugins.Admin.ContentTypeOverview.TypeViewUserControl" %>

<tr class="spacer">
    <td colspan="8">
        <h3>
            <asp:Label style="font-weight:bold;" runat="server" ID="tblGroupName" />  
        </h3>
    </td>
</tr>

<asp:Repeater runat="server" ID="rptTypeView">
    <ItemTemplate>
        <tr>
            <td style="padding-top: 5px;">
                <%# Eval("Name")%>
            </td>
            <td style="padding-top: 5px;">
                <%# Eval("Type")%>
            </td>
             <td style="padding-top: 5px;">
                <%# Eval("InheritedFrom")%>
            </td>
            <td style="padding-top: 5px;">
                <%# Eval("CultureSpecific")%>
            </td>
            <td style="padding-top: 5px;">
                <%# Eval("IsRequired")%>
            </td>
            <td style="padding-left: 10px; padding-top: 5px;">
                <%# Eval("DisplayName")%>
            </td>
            <td style="padding-left: 10px; padding-top: 5px; max-width: 600px;">
                <%# Eval("Description")%>
            </td>
            <td style="padding-left: 10px; padding-top: 5px; padding-right: 10px; width: 65px;">
                <%# Eval("DescriptionButton")%>
            </td>
        </tr>
    </ItemTemplate>
</asp:Repeater>


