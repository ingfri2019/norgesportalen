﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web.UI.WebControls;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.PlugIn;
using EPiServer.Security;
using EPiServer.ServiceLocation;
using EPiServer.Shell.WebForms;

namespace Norgesportalen.Plugins.Admin.ContentTypeOverview
{
    [GuiPlugIn(DisplayName = "[Admin] Informasjon om blokk- og sidetyper",
              Area = PlugInArea.AdminMenu,
              Url = "~/Plugins/Admin/ContentTypeOverview/ContentTypeView.aspx",
              SortIndex = 4002,
              Category = "Tools")]
    public partial class ContentTypeView : ContentWebFormsBase
    {
        protected override void OnInit(EventArgs e)
        {
            if (PrincipalInfo.HasAdminAccess == false)
                AccessDenied();

            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                Populate();
        }

        #region Populate
        private void Populate()
        {
            var list = ContentTypeRepository.List().ToList();
            th.Visible = false;
            contentTypeList.Visible = false;

            PopulatePageDropdownList(list);
            PopulateBlockDropdownList(list);
            PopulateBaseDropdownList(list);
        }

        private void PopulatePageDropdownList(IEnumerable<ContentType> list)
        {
            ddlPageTypes.Items.Add("-- Velg sidetype --");
            var pageTypes = list.Where(i => typeof(PageData).IsAssignableFrom(i.ModelType))
                                .Select(x => new
                                {
                                    Name = x.DisplayName ?? x.Name + " (slettet)",
                                    Id = x.ID,
                                    Modeltype = x.ModelType
                                })
                                .OrderBy(x => x.Name);

            foreach (var pageType in pageTypes)
            {
                if (GetProperties(pageType.Modeltype).Length > 0)
                {
                    ddlPageTypes.Items.Add(new ListItem(pageType.Name, pageType.Id.ToString()));
                }
            }
        }

        private void PopulateBlockDropdownList(IEnumerable<ContentType> list)
        {
            ddlBlockTypes.Items.Add("-- Velg blokktype --");
            var blockTypes = list.Where(i => typeof(BlockData).IsAssignableFrom(i.ModelType))
                                .Select(x => new
                                {
                                    Name = x.DisplayName ?? x.Name + " (slettet)",
                                    Id = x.ID,
                                    Modeltype = x.ModelType
                                })
                                .OrderBy(x => x.Name);

            foreach (var blockType in blockTypes)
            {
                if (GetProperties(blockType.Modeltype).Length > 0)
                {
                    ddlBlockTypes.Items.Add(new ListItem(blockType.Name, blockType.Id.ToString()));
                }
            }
        }

        private void PopulateBaseDropdownList(IEnumerable<ContentType> list)
        {
            ddlBaseTypes.Items.Add("-- Velg basetype --");
            var baseTypes = list.Where(i => typeof(PageData).IsAssignableFrom(i.ModelType) || typeof(BlockData).IsAssignableFrom(i.ModelType))
                                .Select(x => x.ModelType.BaseType)
                                .Distinct()
                                .OrderBy(x => x.Name);

            foreach (var baseType in baseTypes)
            {
                ddlBaseTypes.Items.Add(new ListItem(baseType.Name, baseType.FullName + "," + baseType.Assembly.FullName));
            }
        }
        #endregion

        #region Events
        protected void OnChange_BlockTypeUpdated(object sender, EventArgs e)
        {
            ResetView(ddlBlockTypes.SelectedValue);
            Update(ddlBlockTypes.SelectedValue);
        }

        protected void OnChange_PageTypeUpdated(object sender, EventArgs e)
        {
            ResetView(ddlPageTypes.SelectedValue);
            Update(ddlPageTypes.SelectedValue);
        }

        protected void LinkButton_Click(object sender, EventArgs e)
        {
            var linkButton = (LinkButton)sender;

            if (linkButton == null) return;

            ResetView(linkButton.CommandArgument);
            Update(linkButton.CommandArgument);
        }

        protected void OnChange_BaseTypeUpdated(object sender, EventArgs e)
        {
            ResetView(ddlBaseTypes.SelectedValue);
            contentTypeLabel.Text = ddlBaseTypes.SelectedItem.Text;
            contentTypeList.Visible = true;
            var contentList = ContentTypeRepository.List();
            var typeList = new List<TypeViewModel>();
            var baseType = Type.GetType(ddlBaseTypes.SelectedValue);

            if (baseType == null) return;

            foreach (var content in contentList)
            {
                if (content.ModelType == null) continue;

                if (content.ModelType.IsSubclassOf(baseType))
                    typeList.Add(new TypeViewModel
                    {
                        DisplayName = content.DisplayName ?? content.Name,
                        Id = content.ID.ToString()
                    });
            }

            contentTypeRepeater.DataSource = typeList.OrderBy(x => x.DisplayName);
            contentTypeRepeater.DataBind();
        }

        private void ResetView(string selectedValue)
        {
            DropDownList[] dropdowns = new DropDownList[] {
                ddlPageTypes, ddlBaseTypes, ddlBlockTypes
            };

            lblHierarchy.Text = "";
            th.Visible = false;
            contentTypeList.Visible = false;
            contentTypeRepeater.DataSource = null;
            contentTypeRepeater.DataBind();

            foreach(var dropdown in dropdowns)
            {
                if (dropdown.SelectedValue == selectedValue) continue;

                dropdown.SelectedValue = null;
            }
        }
        #endregion

        private void Update(string selectedValue)
        {
            if (string.IsNullOrEmpty(selectedValue)) return;

            SetHierarchyOfInheritageText(selectedValue);

            UpdateTypeView(selectedValue);
        }

        private void UpdateTypeView(string selectedValue)
        {
            List<TypeViewModel> sortedProperties = GetSortedProperties(selectedValue);
            th.Visible = true;

            if (sortedProperties.Count > 0)
            {
                var allPagePropertyLists = SplitListIntoGroups(sortedProperties);

                foreach (var list in allPagePropertyLists)
                {
                    AddTypeViewUserControl(list);
                }
            }
        }

        private List<TypeViewModel> GetSortedProperties(string selectedValue)
        {
            var contentList = ContentTypeRepository.List();
            var contentPropertyList = new List<TypeViewModel>();

            foreach (var content in contentList)
            {
                if (content.ID.ToString() != selectedValue) continue;

                var modelType = content.ModelType;

                PropertyInfo[] propertyList = GetProperties(modelType);
                var inheritedPropertyList = modelType.BaseType.GetProperties();

                PopulatePropertyList(propertyList, content, contentPropertyList);
                PopulatePropertyList(inheritedPropertyList, content, contentPropertyList);
            }

            var pagePropertyListSorted = contentPropertyList.OrderBy(x => x.GroupSortIndex)
                              .ThenBy(x => x.SortIndex).ToList();

            return pagePropertyListSorted;
        }

        private void PopulatePropertyList(PropertyInfo[] propertyList, ContentType content, List<TypeViewModel> pagePropertyList)
        {
            foreach (var property in propertyList)
            {
                if (property.DeclaringType == typeof(PageData)) continue;
                if (property.DeclaringType == typeof(BlockData)) continue;
                if (pagePropertyList.Any(x => x.PropertyName == property.Name)) continue;

                var displayAttribute = property.GetCustomAttribute<DisplayAttribute>();
                if (displayAttribute == null) continue;

                var propertyDefinition = content.PropertyDefinitions.FirstOrDefault(x => property.Name == x.Name);
                if (propertyDefinition == null) continue;

                bool createReadMoreLink = false;
                var attributeList = GetAttributesForAllLanguages(propertyDefinition);
                var description = GetDivWithTranslatedValues(property, attributeList, TranslatedPropertyType.Description, ref createReadMoreLink);
                var displayName = GetDivWithTranslatedValues(property, attributeList, TranslatedPropertyType.DisplayName, ref createReadMoreLink);
                var (cultureSpecific, required) = CheckAttributes(property);

                pagePropertyList.Add(new TypeViewModel()
                {
                    Name = CreatePropertyName(property),
                    InheritedFrom = CreateInherited(property, content),
                    CultureSpecific = cultureSpecific ? "Ja" : "Nei",
                    IsRequired = required ? "Ja" : "Nei", 
                    PropertyName = property.Name,
                    Type = property.PropertyType.Name ?? "",
                    DisplayName = displayName,
                    Description = description,
                    DescriptionButton = createReadMoreLink ? CreateReadMoreLink(property) : "",
                    GroupName = displayAttribute.GroupName ?? "Content",
                    SortIndex = displayAttribute.GetOrder(),
                    GroupSortIndex = GetGroupSortIndex(displayAttribute.GroupName)
                });
            }
        }

        private void SetHierarchyOfInheritageText(string selectedValue)
        {
            var contentList = ContentTypeRepository.List();

            foreach (var content in contentList)
            {
                if (content.ID.ToString() != selectedValue) continue;

                lblHierarchy.Text = "Arvehierarki : " + GetHierarchyOfContentType(content.ModelType);
                break;
            }
        }

        private string GetHierarchyOfContentType(Type type)
        {
            var isBaseType = type == (typeof(BlockData)) || type == (typeof(PageData));

            if (isBaseType) return type.Name;

            return GetHierarchyOfContentType(type.BaseType) + " > " + type.Name;
        }

        private string CreateReadMoreLink(PropertyInfo property)
        {
            var id = property.Name;
            var link = "<span style=\"cursor: pointer; font-weight: bold; color: crimson;\"";

            link += "onclick=\"descOnClick('" + id + "')\">Les mer</span>";

            return link;
        }

        private string GetDivWithTranslatedValues(PropertyInfo property, List<PropertyTranslation> attributeList, TranslatedPropertyType translatedPropertyType, ref bool readmore)
        {
            var id = property.Name + translatedPropertyType;
            var div = "<div id='" + id + "' style='height: 15px; overflow:hidden;' > ";
            var divString = new StringBuilder(div);
            var numberOfTranslations = 0;
            var divStringLength = 0;
            var tempString = "";

            foreach (var attribute in attributeList)
            {
                if (translatedPropertyType == TranslatedPropertyType.Description)
                {
                    tempString = attribute.Description;
                }
                else if(translatedPropertyType == TranslatedPropertyType.DisplayName)
                {
                    tempString = attribute.DisplayName;
                }

                if (string.IsNullOrEmpty(tempString)) continue;

                var flagUrl = "<img src=\"" + attribute.Language.ResolvedIconPath + "\" style=\"margin-bottom:-4px;\" />";
            
                divString.Append(flagUrl + " <span>" + tempString + "</span><br/></br>");

                numberOfTranslations++;
                divStringLength += tempString.Length;
            }

            if (numberOfTranslations > 1 || divStringLength > 30)
                readmore = true;

            divString.Append("</div>");

            return divString.ToString();
        }

        private string CreatePropertyName(PropertyInfo propertyInfo)
        {
            var propertyName = "<b>" + propertyInfo.Name + "</b>";

            return propertyName;
        }

        private string CreateInherited(PropertyInfo propertyInfo, ContentType page)
        {
            var details = "";
            if (propertyInfo.DeclaringType != null && propertyInfo.DeclaringType != page.ModelType)
            {
                details = propertyInfo.DeclaringType.Name;
            }

            return details;
        }

        private (bool, bool) CheckAttributes(PropertyInfo propertyInfo)
        {
            var propertyIsRequired = propertyInfo.IsDefined(typeof(RequiredAttribute));
            var propertyIsCultureSpecific = propertyInfo.IsDefined(typeof(CultureSpecificAttribute));

            return (propertyIsCultureSpecific, propertyIsRequired);
        }

        private int GetGroupSortIndex(string groupName)
        {
            var tabRepo = ServiceLocator.Current.GetInstance<ITabDefinitionRepository>();
            var groupDefinition = tabRepo.Load(groupName);
            return groupDefinition?.SortIndex ?? -1;
        }

        private List<PropertyTranslation> GetAttributesForAllLanguages(PropertyDefinition propertyDefinition)
        {
            var attributeList = new List<PropertyTranslation>();
            var languages = ServiceLocator.Current.GetInstance<ILanguageBranchRepository>().ListEnabled();
            var thread = System.Threading.Thread.CurrentThread;
            var currentCulture = thread.CurrentCulture;
            var currentUICulture = thread.CurrentUICulture;;

            foreach (var language in languages)
            {
                thread.CurrentCulture = language.Culture;
                thread.CurrentUICulture = language.Culture;

                var description = propertyDefinition.TranslateDescription();
                var displayName = propertyDefinition.TranslateDisplayName();

                var containsAttribute = attributeList.Any(x => x.Description == description && x.DisplayName == displayName);

                if (containsAttribute) continue;

                attributeList.Add(new PropertyTranslation
                {
                    Description = description,
                    DisplayName = displayName,
                    Language = language,
                });
            }

            thread.CurrentCulture = currentCulture;
            thread.CurrentUICulture = currentUICulture;

            return attributeList;
        }

        private List<List<TypeViewModel>> SplitListIntoGroups(List<TypeViewModel> list)
        {
            var returnList = new List<List<TypeViewModel>>();
            var currentList = new List<TypeViewModel>();

            var currentGroupName = list[0].GroupName;

            foreach (var item in list)
            {
                if (currentGroupName != item.GroupName)
                {
                    returnList.Add(currentList);
                    currentGroupName = item.GroupName;
                    currentList = new List<TypeViewModel>();
                    continue;
                }

                currentList.Add(item);
            }

            if (returnList.Count == 0)
            {
                returnList.Add(currentList);
            }

            return returnList;
        }

        private void AddTypeViewUserControl(List<TypeViewModel> result)
        {
            if (result == null || result.Count <= 0) return;

            var userControl = Page.LoadControl("~/Plugins/Admin/ContentTypeOverview/TypeViewUserControl.ascx");
            var groupLabel = userControl.FindControl("tblGroupName") as Label;
            var repeater = (Repeater)userControl.FindControl("rptTypeView");

            groupLabel.Text = result[0].GroupName;
            repeater.DataSource = result;
            repeater.DataBind();
            ph.Controls.Add(userControl);
        }

        private static PropertyInfo[] GetProperties(Type modelType) => modelType.GetProperties(BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Public);
    }
}