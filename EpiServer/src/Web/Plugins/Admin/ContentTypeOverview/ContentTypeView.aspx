﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ContentTypeView.aspx.cs" Inherits="Norgesportalen.Plugins.Admin.ContentTypeOverview.ContentTypeView" EnableViewState="true" ViewStateMode="Enabled" %>
<%@ Import Namespace="Norgesportalen.Plugins.Admin.ContentTypeOverview" %>

<asp:content id="Content2" contentplaceholderid="FullRegion" runat="server">

    <script type="text/javascript">
        function descOnClick(id) {

            var id1 = id + '<%= TranslatedPropertyType.DisplayName %>';
            var id2 = id + '<%= TranslatedPropertyType.Description %>';

            var ele1 = document.getElementById(id1);
            var ele2 = document.getElementById(id2);

            var style1 = window.getComputedStyle(ele1).overflow;
            var style2 = window.getComputedStyle(ele2).overflow;

            ele1.removeAttribute("style")
            ele2.removeAttribute("style")

            if (style1.toString() === 'hidden') {
                ele1.setAttribute("style", " overflow:");
            } else {
                ele1.setAttribute("style", "height: 15px; overflow:hidden;");
            }

            if (style2.toString() === 'hidden') {
                ele2.setAttribute("style", " overflow:");
            } else {
                ele2.setAttribute("style", "height: 15px; overflow:hidden;");
            }
        }
    </script>

    <div class="epi-padding epi-contentArea">
        <div>
            <h1>Informasjon om blokk- og sidetyper</h1>
            <p>
                Dette verktøyet lister ut feltnavn, ledetekst og tooltip per blokk- eller sidetype. <br />
                Det er i tillegg mulig å velge en basetype for å en liste av blokker og sider som arver fra denne typen.
                Denne listen inneholder lenker videre til mer detaljert informasjon om valgt innholdstype.<br />
                <br />
                Merk: I tilfeller med lang tooltip, samt oversettelser av ledetekst og tooltip til flere språk kan raden
                utvides ved å trykke les mer.
            </p>
            <br />
            Sidetype: <br />
            <asp:DropDownList ID="ddlPageTypes" runat="server" AutoPostBack="true"
                OnSelectedIndexChanged="OnChange_PageTypeUpdated" />
            <br />
            <br />
            Blokktype: <br />
            <asp:DropDownList ID="ddlBlockTypes" runat="server" AutoPostBack="true"
                OnSelectedIndexChanged="OnChange_BlockTypeUpdated" />
            <br />
            <br />
            Basetype: <br />
            <asp:DropDownList ID="ddlBaseTypes" runat="server" AutoPostBack="true"
                OnSelectedIndexChanged="OnChange_BaseTypeUpdated" />
        </div>
        <br />
        <h2>
            <asp:Label runat="server" ID="lblHierarchy" />
        </h2>
        <div>
            <asp:Panel ID="th" runat="server">
                <table>
                    <tr>
                        <th style="padding-left: 10px; padding-top: 5px;">Navn</th>
                        <th style="padding-left: 10px; padding-top: 5px;">Type</th>
                        <th style="padding-left: 10px; padding-top: 5px;">Arvet fra</th>
                        <th style="padding-left: 10px; padding-top: 5px;">Språkspesifikk</th>
                        <th style="padding-left: 10px; padding-top: 5px;">Påkrevd</th>
                        <th style="padding-left: 10px; padding-top: 5px;">Ledetekst</th>
                        <th style="padding-left: 10px; padding-top: 5px;" colspan="2">Tooltip</th>
                    </tr>
                    <asp:PlaceHolder runat="server" ID="ph" />
                </table>
            </asp:Panel>
        </div>
        <br />
        <asp:Panel ID="contentTypeList" runat="server">
            <h3>Liste av typer som arver fra <asp:Label id="contentTypeLabel" runat="server"></asp:Label>:</h3>
            <br />
            <asp:Repeater ID="contentTypeRepeater" runat="server">
                <ItemTemplate>
                    <h4>
                        <asp:LinkButton Text='<%# Eval("DisplayName")%>' OnClick="LinkButton_Click" runat="server" CommandArgument='<%# Eval("Id")%>'/>
                    </h4>
                    <br />
                </ItemTemplate>
            </asp:Repeater>
        </asp:Panel>
    </div>
</asp:content>