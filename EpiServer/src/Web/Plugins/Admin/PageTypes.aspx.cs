﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Filters;
using EPiServer.PlugIn;
using EPiServer.Security;
using EPiServer.ServiceLocation;
using EPiServer.Shell.WebForms;
using Settings = EPiServer.Configuration.Settings;

namespace Norgesportalen.Plugins.Admin
{

    [GuiPlugIn(DisplayName = "[Admin] Sidetyper",
               Area = PlugInArea.AdminMenu,
               Url = "~/Plugins/Admin/PageTypes.aspx",
               SortIndex = 4000,
               Category = "Tools")]
    public partial class PageTypes : ContentWebFormsBase
    {
        private readonly IPageCriteriaQueryService _pageCriteriaQueryService = ServiceLocator.Current.GetInstance<IPageCriteriaQueryService>();
        private readonly IContentTypeRepository _contentTypeRepository = ServiceLocator.Current.GetInstance<IContentTypeRepository>();
        private readonly ILanguageBranchRepository _languageBranchRepository = ServiceLocator.Current.GetInstance<ILanguageBranchRepository>();

        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);

            MasterPageFile = Settings.Instance.UIUrl + "MasterPages/EPiServerUI.master";

            // Set timeout:
            Server.ScriptTimeout = 1800;
        }

        protected override void OnInit(EventArgs e)
        {
            if (PrincipalInfo.HasAdminAccess == false)
                AccessDenied();

            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack) return;
            PopulatePageTypeDropDown();
            PopulateLanguageDropDown();
        }

        private void PopulatePageTypeDropDown()
        {
            ddlPageTypes.Items.Add("");
            foreach (var pageType in _contentTypeRepository.List().Where(i => typeof(PageData).IsAssignableFrom(i.ModelType)).Select(x => new { Name = x.DisplayName ?? x.Name + " (slettet)", Id = x.ID }).OrderBy(x => x.Name))
            {
                ddlPageTypes.Items.Add(new ListItem(pageType.Name, pageType.Id.ToString()));
            }
        }

        private void PopulateLanguageDropDown()
        {
            foreach (var languageBranch in _languageBranchRepository.ListEnabled())
            {
                ddlLanguage.Items.Add(new ListItem(languageBranch.Name, languageBranch.LanguageID));
            }
        }

        protected void OnChange_LanguageUpdated(object sender, EventArgs e)
        {
            UpdateResultList();
        }

        protected void OnChange_PageTypeUpdated(object sender, EventArgs e)
        {
            UpdateResultList();
            UpdatePropertyDropDownList();
        }

        protected void OnChange_RootPageUpdated(object sender, EventArgs e)
        {
            UpdateResultList();
        }

        protected void OnChange_PropertyUpdated(object sender, EventArgs e)
        {
            UpdateResultList();
        }

        private void UpdateResultList()
        {
            phPages.Visible = true;

            if (string.IsNullOrWhiteSpace(ddlPageTypes.SelectedValue))
                return;

            var pages = GetPages();

            litCount.Text = pages.Count.ToString();
            phPagesMoreThan1000.Visible = pages.Count > 1000;

            var sortFilter = new FilterSort(FilterSortOrder.ChangedDescending);
            sortFilter.Sort(pages);

            var countFilter = new FilterCount(1000);
            countFilter.Filter(pages);

            plPages.DataSource = pages;
            plPages.DataBind();
        }

        private void UpdatePropertyDropDownList()
        {
            ddlProperty.Items.Clear();
            var propertyList = new List<ListItem>() { new ListItem("") };

            if (!int.TryParse(ddlPageTypes.SelectedValue, out var pageTypeId))
            {
                return;
            }

            var contentType = _contentTypeRepository.Load(pageTypeId);
            if (contentType != null)
            {
                foreach (var propertyDefinition in contentType.PropertyDefinitions)
                {
                    propertyList.Add(new ListItem($"{propertyDefinition.EditCaption} [{propertyDefinition.Name}]", propertyDefinition.Name));
                }
            }

            var sortedPropertyList = propertyList.OrderBy(x => x.Text);
            foreach (var listItem in sortedPropertyList)
            {
                ddlProperty.Items.Add(listItem);
            }
        }

        protected void ExportAllPages(object sender, EventArgs e)
        {
            Response.ContentType = "text/csv";
            Response.AppendHeader("Content-Disposition", "attachment; filename=export.csv");
            Response.Write('\uFEFF');
            Response.Write("\"ID\";");
            Response.Write("\"Name\";");
            Response.Write("\"URL name\";");
            Response.Write("\"Changed\";");
            Response.Write("\"Saved\";");
            Response.Write("\"Start publish\";");
            Response.Write("\n");

            var pages = GetPages();
            foreach (var page in pages)
            {
                Response.Write(page.PageLink.ID + ";");
                Response.Write("\"" + page.Name.Replace("\"", "\"\"") + "\";");
                Response.Write("\"" + page.URLSegment + "\";");
                Response.Write(page.Changed + ";");
                Response.Write(page.Saved + ";");
                Response.Write(page.StartPublish + ";");
                Response.Write("\n");
            }

            Response.End();
        }

        private PageDataCollection GetPages()
        {
            var rootReference = ContentReference.StartPage;
            if (iprRootPage.PageLink != null)
            {
                rootReference = iprRootPage.PageLink;
            }

            var criteria = new PropertyCriteriaCollection
            {
                new PropertyCriteria()
                {
                    Name = "PageTypeID",
                    Condition = CompareCondition.Equal,
                    Required = true,
                    Type = PropertyDataType.PageType,
                    Value = ddlPageTypes.SelectedValue
                }
            };

            if (ddlProperty.SelectedIndex > 0)
            {
                criteria.Add(
                    new PropertyCriteria()
                    {
                        Name = ddlProperty.SelectedValue,
                        Required = true,
                        IsNull = true
                    }
                );
            }
            var lang = ddlLanguage.SelectedValue;
            return _pageCriteriaQueryService.FindAllPagesWithCriteria(rootReference, criteria, lang, new LanguageSelector(lang));
        }
    }
}