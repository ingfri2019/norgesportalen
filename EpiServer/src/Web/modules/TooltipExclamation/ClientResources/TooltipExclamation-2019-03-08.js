﻿/* In CMS 'all properties' view adds an exclamation mark icon at fields where a tooltip (title attribute) is given. */

require([
    "dojo/_base/lang",
    "dojo/dom-construct",
    "dojo/dom-style",

    "epi/shell/form/Field"

], function (
    lang,
    domConstruct,
    domStyle,

    Field
) {
        lang.extend(Field,
            {
                postCreate: function () {

                    if (this.tooltip && this.tooltip != this.label) { // The property has a description, i.e. tooltip

                        // In this code, locate the readonly-icon in EPiServer (always present in the html-code), and attach an info-icon after it.

                        // We have two cases for icon-pair placement:
                        // ------------------------------------------

                        // Create an HTML element for the new info-icon
                        var icon = domConstruct.toDom('<span class="dijitInline dijitReset dijitIcon epi-iconInfo" data-dojo-attach-point="descriptionIcon"></span>');

                        // Add the icon after the pre-existing icon for read-only properties
                        domConstruct.place(icon, this.readonlyIcon, "after");

                        if (this.labelTarget && this.labelTarget.startsWith("dijit_form_CheckBox")) {
                            // 1) We are adding the icon(s) to a checkbox. In this case, the existing icon comes after the checkbox itself. This is 
                            //    EPiServer-standard, so we don't mess with this. Instead, add the new icon after the existing one

                            // Position the info-icon:
                            domStyle.set(icon, "position", "relative");
                            domStyle.set(icon, "right", "-5px");
                            domStyle.set(icon, "top", "0");

                            // Avoid icons ending up on top of each other, by nudging the original read-only icon to the side and up
                            domStyle.set(this.readonlyIcon, "position", "relative");
                            domStyle.set(this.readonlyIcon, "right", "-3px");
                            domStyle.set(this.readonlyIcon, "top", "0");
                        }
                        else {
                            // 2) We are adding the icon(s) to a any other web-control. In this case, the existing icon comes before the control. 
                            //    In this case we want to shift the read-only icon to the left, and inject the new one in its former location.

                            // Avoid icons ending up on top of each other, by nudging the original read-only icon to the left
                            domStyle.set(this.readonlyIcon, "right", "17px");
                        }

                    }

                }
            });
    });