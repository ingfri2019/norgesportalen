﻿/* Dojo-code to fire every time we open an edit page in EPiServer where the "fetch content" functionality is turned on and used. 
 * It's fired in ModifyMetadata looping through all properties, using ClientLayoutClass = "default-tab-selector", which is defined
 * in module,config. It then performs an artificial "click" on the tab we want to change to, using Javascript. */

define([
    "dojo/_base/declare",
    "epi/shell/layout/SimpleContainer"
    ],

    function (
        declare,
        SimpleContainer
    )
    {
        return declare([SimpleContainer], {
            //constructor: function () {},

            postCreate: function () { /* PostCreate fires too soon, and the tab strip is not completely rendered */ },

            startup: function () {
                // Use Jquery to select the tab we manually want to change to, and click it:
                var tabElement = $("div.dijitContentPane span.tabLabel:contains('Additional content')");
                if ($(tabElement).length) {
                    $(tabElement).trigger("click");
                }
            }

        });
    }
);

