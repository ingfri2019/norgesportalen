﻿// The purpose of this code is to enable the programmers to display an extra block of information in edit (forms-) mode.
// If such a text block is going to be inserted, first add a text property to the page type. Secondly, add a display name 
// and a description. Third, decorate it with a [UIHint("ExtraEditorInformation")] attribute. The rest will be handled by Dojo.
//
// Technical reference:
// https://www.gulla.net/episerver-help-texts-improved/

define([
    "dojo/_base/array",
    "dojo/_base/connect",
    "dojo/_base/declare",
    "dojo/_base/lang",

    "dijit/_CssStateMixin",
    "dijit/_Widget",
    "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin",

    "epi/epi"
],
function (
    array,
    connect,
    declare,
    lang,

    _CssStateMixin,
    _Widget,
    _TemplatedMixin,
    _WidgetsInTemplateMixin,

    epi
) {

    // The path/namespace in the declare statement below will generate underscore-delimited classnames on the Dojo tags accordingly (also see the CSS)
    return declare("regjeringen.editors.ExtraEditorInformationProperty",
        [_Widget,
        _TemplatedMixin,
        _WidgetsInTemplateMixin,
        _CssStateMixin], {

        templateString:
            "<div class=\"ExtraEditorInformationProperty\">\
                    <strong data-dojo-attach-point=\"heading\"></strong>\
                    <div data-dojo-attach-point=\"description\"></div>\
            </div>",

        postCreate: function () {
            this.inherited(arguments);
            this.heading.innerHTML = this.title;
            this.description.innerHTML = this.tooltip;
        }
    });
}); 