﻿define([
    "dojo/_base/array",
    "dojo/_base/connect",
    "dojo/_base/declare",
    "dojo/_base/lang",

    "dijit/_CssStateMixin",
    "dijit/_Widget",
    "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin",

    "epi/epi"
],
    function (
        array,
        connect,
        declare,
        lang,

        _CssStateMixin,
        _Widget,
        _TemplatedMixin,
        _WidgetsInTemplateMixin,

        epi
    ) {

        return declare("norgesportalen.editors.HelpImage", [_Widget, _TemplatedMixin, _WidgetsInTemplateMixin, _CssStateMixin], {

            templateString:
                "<div>\
                    <div class=\"helpimage\" data-dojo-attach-point=\"imageContainer\"></div>\
                </div>",

            postCreate: function () {
                this.inherited(arguments);
                this.imageContainer.style = "background-image: url('" + this.imgUrl + "');";
                this.imageContainer.style.backgroundImage = "url('" + this.imgUrl + "');";
            }
        });
    });
