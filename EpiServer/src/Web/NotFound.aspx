<%@ Page Language="c#" AutoEventWireup="false" Inherits="BVNetwork.NotFound.Core.NotFoundPage.NotFoundBase" %>
<%@ Import Namespace="EPiServer.Core" %>

<%-- 
    Note! This file has no code-behind. It inherits from the NotFoundBase class. You can 
    make a copy of this file into your own project, change the design and keep the inheritance 
    WITHOUT having to reference the BVNetwork.EPi404.dll assembly.
    
    If you want to use your own Master Page, inherit from SimplePageNotFoundBase instead of
    NotFoundBase, as that will bring in what is needed by EPiServer. Note! you do not need to
    create a page type for this 404 page. If you use the EPiServer API, and inherit from  
    SimplePageNotFoundBase, this page will run in the context of the site start page.
    
    Be very careful with the code you write here. If you reference resources that cannot be found
    you could end up in an infinite loop.
    
    The code behind file might do a redirect to a new page based on the redirect configuration if
    it matches the url not found. The Error event (where the rest of the redirection is done)
    might not run for .aspx files that are not found, instead it redirects here with the url it
    could not find in the query string.
    
    Available properties:
        Content (BVNetwork.FileNotFound.Content.PageContent)
            // Labels you can use - fetched from the language file
            Content.BottomText
            Content.CameFrom
            Content.LookingFor
            Content.TopText
            Content.Title
            
        UrlNotFound (string)
            The url that cannot be found
        
        Referer (string)
            The url that brought the user here
            It no referer, the string is empty (not null)
            
    If you are using a master page, you should add this:
        <meta content="noindex, nofollow" name="ROBOTS">
    to your head tag for this page (NOT all pages)
 --%>

<script runat="server" type="text/C#">
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        // Add your own logic (like databinding) here
    }
</script>

<%@ Register TagPrefix="EPiServer" Namespace="EPiServer.WebControls" Assembly="EPiServer" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta content="noindex, nofollow" name="ROBOTS" />
    <title><%= Content.Title %></title>
    <link rel="stylesheet" href="/Frontend/dist-web/Static/css/main.css">
</head>
<body class="error-page not-found-error">
    <header>
        <div class="header-content">
            <a href="/" class="brand">
                <span class="logo"></span>
                <span class="logo-title">Norway</span>
                <span class="logo-subtitle">Norwegian Ministry of Foreign Affairs</span>
            </a>
        </div>
    </header>
    <main class="main-content">
        <div class="container">
            <article>
                <div class="text-center">
                    <h1>Sorry, the page you are looking for was not found.</h1>
                    <p class="ingress">Go to the front page to find what you are looking for.</p>
                    <a href="/" class="btn">GO TO FRONT PAGE</a>
                </div>
            </article>
        </div>
    </main>
</body>
</html>
