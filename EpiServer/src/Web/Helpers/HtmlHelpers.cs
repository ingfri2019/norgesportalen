using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.WebPages;
using EPiServer;
using EPiServer.Core;
using EPiServer.ServiceLocation;
using EPiServer.Web.Mvc.Html;

using EPiServer.Web.Routing;
using Norgesportalen.Models.Pages.BaseClasses;
using Norgesportalen.Business.Extensions;

namespace Norgesportalen.Business.Helpers
{

    public static class HtmlHelpers
    {
        public static HtmlString Partial(this HtmlHelper htmlHelper, string partialViewName, object model, object viewData)
        {
            var viewDataDictionary = new ViewDataDictionary(model);
            foreach (var property in viewData.GetType().GetProperties())
            {
                viewDataDictionary[property.Name] = property.GetValue(viewData);
            }

            return htmlHelper.Partial(partialViewName, viewDataDictionary);
        }

        /// <summary>
        /// Returns an element for each child page of the rootLink using the itemTemplate.
        /// </summary>
        /// <param name="helper">The html helper in whose context the list should be created</param>
        /// <param name="rootLink">A reference to the root whose children should be listed</param>
        /// <param name="itemTemplate">A template for each page which will be used to produce the return value. Can be either a delegate or a Razor helper.</param>
        /// <param name="includeRoot">Wether an element for the root page should be returned</param>
        /// <param name="requireVisibleInMenu">Wether pages that do not have the "Display in navigation" checkbox checked should be excluded</param>
        /// <param name="requirePageTemplate">Wether page that do not have a template (i.e. container pages) should be excluded</param>
        /// <remarks>
        /// Filter by access rights and publication status.
        /// </remarks>
        public static IHtmlString MenuList(
            this HtmlHelper helper, 
            ContentReference rootLink, 
            Func<MenuItem, HelperResult> itemTemplate = null, 
            bool includeRoot = false, 
            bool requireVisibleInMenu = true, 
            bool requirePageTemplate = true)
        {
            itemTemplate = itemTemplate ?? GetDefaultItemTemplate(helper);
            var currentContentLink = helper.ViewContext.RequestContext.GetContentLink();
            var contentLoader = ServiceLocator.Current.GetInstance<IContentLoader>();

            Func<IEnumerable<PageData>, IEnumerable<PageData>> filter = 
                pages => pages.FilterForDisplay(requirePageTemplate, requireVisibleInMenu);
            
            var pagePath = contentLoader.GetAncestors(currentContentLink)
                .Reverse()
                .Select(x => x.ContentLink)
                .SkipWhile(x => !x.CompareToIgnoreWorkID(rootLink))
                .ToList();

            var menuItems = contentLoader.GetChildren<PageData>(rootLink)
                .FilterForDisplay(requirePageTemplate, requireVisibleInMenu)
                .Select(x => CreateMenuItem(x, currentContentLink, pagePath, contentLoader, filter))
                .ToList();

            if(includeRoot)
            {
                menuItems.Insert(0, CreateMenuItem(contentLoader.Get<PageData>(rootLink), currentContentLink, pagePath, contentLoader, filter));
            }

            var buffer = new StringBuilder();
            var writer = new StringWriter(buffer);
            foreach (var menuItem in menuItems)
            {
                itemTemplate(menuItem).WriteTo(writer);
            }

            return new MvcHtmlString(buffer.ToString());
        }

        private static MenuItem CreateMenuItem(PageData page, ContentReference currentContentLink, List<ContentReference> pagePath, IContentLoader contentLoader, Func<IEnumerable<PageData>, IEnumerable<PageData>> filter)
        {
            var menuItem = new MenuItem(page)
                {
                    Selected = page.ContentLink.CompareToIgnoreWorkID(currentContentLink) ||
                               pagePath.Contains(page.ContentLink),
                    HasChildren =
                        new Lazy<bool>(() => filter(contentLoader.GetChildren<PageData>(page.ContentLink)).Any())
                };
            return menuItem;
        }

        private static Func<MenuItem, HelperResult> GetDefaultItemTemplate(HtmlHelper helper)
        {
            return x => new HelperResult(writer => writer.Write(helper.PageLink(x.Page)));
        }

        public static List<Crumb> CreateBreadcrumb(this HtmlHelper htmlHelper, SitePageData currentPage)
        {
            return CreateBreadcrumb(currentPage,true,true);
        }

        public class Crumb {

            public string Text;
            public string Title;
            public string LinkUrl;
        }

        public static List<Crumb> CreateBreadcrumb(SitePageData currentPage, bool requirePageTemplate, bool requireVisibleInMenu, bool requireAccess = true, bool requirePublished = true)
        {

            var breadCrumb = new List<Crumb>();

            if (currentPage == null) return breadCrumb;

            var _repository = new ServiceLocationHelper(ServiceLocator.Current).ContentRepository();

            var ancestors =
                _repository.GetAncestors(currentPage.ContentLink)
                .OfType<SitePageData>()
                .Where(p => (p.ContentLink != PageReference.RootPage))
                .FilterForDisplay(requirePageTemplate, requireVisibleInMenu, requireAccess, requirePublished)
                .Reverse()
                .Select(x => _repository.Get<SitePageData>(x.PageLink));


            // skip World page from breadcrumb
            ancestors = ancestors.Skip(1);

            // Hide breadcrumb if no start page
            SitePageData startPage = ancestors.FirstOrDefault();
            if (startPage == null)
            {
                return breadCrumb;
            }

            breadCrumb = ancestors.Select(ancestor => new Crumb() { Text = ancestor.PageName, Title = ancestor.PageName, LinkUrl = ancestor.LinkURL }).ToList();
            breadCrumb.Add(new Crumb() { Text = currentPage.PageName });

            // If more that 4 levels, text in second and futher are replaced with ...
            for (int i = 0; i < breadCrumb.Count() - 4; i++)
            {
                breadCrumb.ToArray()[i + 1].Text = "..";
            }


            return breadCrumb;
        }

        public static string FormatDateTimeForEventCard(DateTime eventStartDateTime, DateTime eventEndDateTime)
        {
            var returnString = string.Empty;
            var monthStart = eventStartDateTime.ToString("MMM");
            var monthEnd = eventEndDateTime.ToString("MMM");
            var dateStart = eventStartDateTime.ToString("dd");
            var dateEnd = eventEndDateTime.ToString("dd");

            if (eventStartDateTime.Date == eventEndDateTime.Date)
            {
                returnString = $"{monthStart} <strong>{dateStart}</strong>";
                return returnString;
            }

            returnString = $"{monthStart} <strong>{dateStart}</strong> - {monthEnd} <strong>{dateEnd}</strong>";
            return returnString;
        }

        public static IHtmlString RenderMobileBreadcrumb(this HtmlHelper htmlHelper, SitePageData currentPage)
        {
            if (currentPage == null) return htmlHelper.Raw(string.Empty);

            var _repository = new ServiceLocationHelper(ServiceLocator.Current).ContentRepository();

            var ancestors = _repository.GetAncestors(currentPage.ContentLink).OfType<SitePageData>().FilterForDisplay(requirePageTemplate: true, requireVisibleInMenu: true);
            SitePageData previous = ancestors.First();

            const string itemFormat = @"<li><a href='{0}'>{1} <span class='icon-arrow-left'></span></a></li>";

            string breadCrumbString = "";
            previous = _repository.Get<SitePageData>(previous.PageLink);
            breadCrumbString = String.Format(itemFormat, previous.GetFriendlyUrl(), previous.PageName);
            return htmlHelper.Raw(breadCrumbString);
        }

        public class MenuItem
        {
            public MenuItem(PageData page)
            {
                Page = page;
            }
            public PageData Page { get; set; }
            public bool Selected { get; set; }
            public Lazy<bool> HasChildren { get; set; }
        }

        /// <summary>
        /// Writes an opening <![CDATA[ <a> ]]> tag to the response if the shouldWriteLink argument is true.
        /// Returns a ConditionalLink object which when disposed will write a closing <![CDATA[ </a> ]]> tag
        /// to the response if the shouldWriteLink argument is true.
        /// </summary>
        public static ConditionalLink BeginConditionalLink(this HtmlHelper helper, bool shouldWriteLink, IHtmlString url, string title = null, string cssClass = null)
        {
            if(shouldWriteLink)
            {
                var linkTag = new TagBuilder("a");
                linkTag.Attributes.Add("href", url.ToHtmlString());

                if(!string.IsNullOrWhiteSpace(title))
                {
                    linkTag.Attributes.Add("title", helper.Encode(title));
                }

                if (!string.IsNullOrWhiteSpace(cssClass))
                {
                    linkTag.Attributes.Add("class", cssClass);
                }

                helper.ViewContext.Writer.Write(linkTag.ToString(TagRenderMode.StartTag));
            }
            return new ConditionalLink(helper.ViewContext, shouldWriteLink);
        }

        /// <summary>
        /// Writes an opening <![CDATA[ <a> ]]> tag to the response if the shouldWriteLink argument is true.
        /// Returns a ConditionalLink object which when disposed will write a closing <![CDATA[ </a> ]]> tag
        /// to the response if the shouldWriteLink argument is true.
        /// </summary>
        /// <remarks>
        /// Overload which only executes the delegate for retrieving the URL if the link should be written.
        /// This may be used to prevent null reference exceptions by adding null checkes to the shouldWriteLink condition.
        /// </remarks>
        public static ConditionalLink BeginConditionalLink(this HtmlHelper helper, bool shouldWriteLink, Func<IHtmlString> urlGetter, string title = null, string cssClass = null)
        {
            IHtmlString url = MvcHtmlString.Empty;

            if(shouldWriteLink)
            {
                url = urlGetter();
            }

            return helper.BeginConditionalLink(shouldWriteLink, url, title, cssClass);
        }

        public class ConditionalLink : IDisposable
        {
            private readonly ViewContext _viewContext;
            private readonly bool _linked;
            private bool _disposed;

            public ConditionalLink(ViewContext viewContext, bool isLinked)
            {
                _viewContext = viewContext;
                _linked = isLinked;
            }

            public void Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);

            }

            protected virtual void Dispose(bool disposing)
            {
                if (_disposed)
                {
                    return;
                }

                _disposed = true;

                if (_linked)
                {
                    _viewContext.Writer.Write("</a>");
                }
            }
        }
    }

    public static class MvcHtmlStringExtensions
    {

        public static MvcHtmlString HiddenIfValue(this HtmlHelper htmlHelper, string name, object value)
        {
            if (value != null && !string.IsNullOrEmpty(value.ToString()))
            {
                return htmlHelper.Hidden(name, value, null);
            }
            return new MvcHtmlString(string.Empty);
        }

    }
}
