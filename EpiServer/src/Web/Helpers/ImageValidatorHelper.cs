﻿using EPiServer;
using EPiServer.Core;
using EPiServer.Core.Html.StringParsing;
using EPiServer.ServiceLocation;
using EPiServer.Validation;
using Norgesportalen.Models.Media;
using System.Collections.Generic;
using System.Web.WebPages;

namespace Norgesportalen.Helpers
{
    public class ImageValidatorHelper : BaseValidationHelper
    {
        public ImageValidatorHelper(ContentData contentData) : base(contentData)
        {
        }

        public IEnumerable<ValidationError> ValidateImage(XhtmlString field, string propName)
        {
            if (field?.Fragments != null)
            {
                foreach (var fragment in field.Fragments)
                {
                    if (fragment is ContentFragment)
                    {
                        var id = ((ContentFragment)fragment).ContentLink;
                        if (ContentRepository.TryGet(id, out ImageFile image))
                        {
                            if (image.AltText.IsEmpty())
                            {
                                var errorMessage = "Image is missing it's alternative text.";
                                yield return CreateImageValidationError(image, propName, errorMessage);
                            }
                        }
                    }
                }
            }
        }

        public IList<ValidationError> ValidateImages(IList<ContentReference> imageReferences, string propName)
        {
            var validationErrors = new List<ValidationError>();

            foreach (var imageReference in imageReferences)
            {
                var validationError = ValidateImage(imageReference, propName);
                if (validationError != null)
                {
                    validationErrors.Add(validationError);
                }
            }
            return validationErrors;
        }

        public ValidationError ValidateImage(ContentReference imageReference, string propName)
        {
            if (ServiceLocator.Current.GetInstance<IContentRepository>().TryGet(imageReference, out ImageFile image))
            {
                var errorMessage = "";

                if (image.AltText.IsEmpty())
                {
                    errorMessage += "Image is missing it's alternative text.\n";
                }

                if (!image.AltText.IsEmpty() && image.AltText.Length > Business.Constants.ImageFile.AltTextLength)
                {
                    errorMessage +=
                        $"Image exceeds the size of {Business.Constants.ImageFile.AltTextLength} characters for alternative text.\n";
                }

                if (!image.ImageText.IsEmpty() && image.ImageText.Length > Business.Constants.ImageFile.ImageTextLength)
                {
                    errorMessage +=
                        $"Image exceeds the size of {Business.Constants.ImageFile.ImageTextLength} characters for image text.\n";
                }

                if (!image.Credit.IsEmpty() && image.Credit.Length > Business.Constants.ImageFile.CreditLength)
                {
                    errorMessage +=
                        $"Image exceeds the size of {Business.Constants.ImageFile.CreditLength} characters for photo credits.\n";
                }

                if (!string.IsNullOrEmpty(errorMessage))
                    return CreateImageValidationError(image, propName, errorMessage);

            }
            return null;
        }

        private ValidationError CreateImageValidationError(ImageFile image, string propName, string errorMessage)
        {
            return new ValidationError
            {
                ErrorMessage = $"{errorMessage} Please update this by editing the image file '{image.Name}'. Tip: Search Media assets for ID {image.ContentLink.ID}",
                PropertyName = propName,
                Severity = ValidationErrorSeverity.Error,
                ValidationType = ValidationErrorType.AttributeMatched
            };
        }
    }
}
