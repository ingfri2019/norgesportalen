﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using EPiServer.Shell.ObjectEditing;

namespace Norgesportalen.Helpers
{
    public class TimezoneHelpers
    {
        public class TzSelectionFactory : ISelectionFactory
        {
            public IEnumerable<ISelectItem> GetSelections(ExtendedMetadata metadata)
            {
                ReadOnlyCollection<TimeZoneInfo> tz;
                tz = TimeZoneInfo.GetSystemTimeZones();
                var list = new List<CountryTimezone>();
                foreach (var t in tz)
                {
                    var ct = new CountryTimezone
                    {
                        TimeZoneID = t.Id,
                        DisplayName = t.DisplayName
                    };
                    list.Add(ct);
                }
                return list.ToArray();
            }
        }

        public class CountryTimezone : ISelectItem
        {
            public string TimeZoneID { get; set; }
            public string DisplayName { get; set; }

            public string Text => DisplayName;

            public object Value => TimeZoneID;
        }
    }
}