﻿using EPiServer;
using EPiServer.Core;
using EPiServer.ServiceLocation;
using Norgesportalen.Models.Pages.BaseClasses;

namespace Norgesportalen.Helpers
{
    public class BaseValidationHelper
    {
        public BasicPageBase PageObject { get; private set; }
        public IContentRepository ContentRepository { get; private set; }

        public BaseValidationHelper(ContentData baseContentData)
        {
            if (baseContentData is BasicPageBase)
                PageObject = baseContentData as BasicPageBase;

            ContentRepository = ServiceLocator.Current.GetInstance<IContentRepository>();
        }
    }
}
