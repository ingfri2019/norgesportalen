﻿using System;
using System.IdentityModel.Claims;
using System.Web.Helpers;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using SSO.Security;

namespace Norgesportalen
{
    public class EPiServerApplication : EPiServer.Global
    {
        protected void Application_Start()
        {
            AntiForgeryConfig.UniqueClaimTypeIdentifier = ClaimTypes.Name;
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            //Tip: Want to call the EPiServer API on startup? Add an initialization module instead (Add -> New Item.. -> EPiServer -> Initialization Module)

            // http://stackoverflow.com/questions/3418557/how-to-remove-asp-net-mvc-default-http-headers
            MvcHandler.DisableMvcResponseHeader = true;
        }

        protected override void RegisterRoutes(RouteCollection routes)
        {
            AuthenticationRoutesConfig.Register(routes);
            base.RegisterRoutes(routes);
            //routes.IgnoreRoute("IndexingService/IndexingService.svc/{*pathInfo}");
        }
    }
}