﻿import 'core-js/stable';
import 'regenerator-runtime/runtime';
import '../../components/helpers/no-js/no-js';
import '../../components/helpers/hash-links/hash-links';

// Global
import '../../components/global/buttons/buttons';

// Areas
import '../../components/areas/header/header';
import '../../components/areas/footer/footer';

// Components
import '../../components/components/components';

// Pages
import '../../components/pages/norway-info/norway-info';
import '../../components/pages/process-page/process-page';
import '../../components/pages/unsubscribe-page/unsubscribe-page';
