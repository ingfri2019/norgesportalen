## Description
Columns are used when in-line content needs to be responsive to surroundings.
For layout purposes, please use Panels.

### Two Columns
<div class="styleguide-container">
  <div class="columns">
    <div class="column column-50">
      <h2>Panel</h2>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
      Pellentesque fermentum est quis mauris pharetra luctus.</p>
    </div>
    <div class="column column-50">
      <h2>Panel</h2>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
      Pellentesque fermentum est quis mauris pharetra luctus.</p>
    </div>
  </div>
</div>

```html
<div class="columns">
  <div class="column column-50">...</div>
</div>
```

### Three Columns
<div class="styleguide-container">
  <div class="columns">
    <div class="column column-33">
      <h2>Panel</h2>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
      Pellentesque fermentum est quis mauris pharetra luctus.</p>
    </div>
    <div class="column column-33">
      <h2>Panel</h2>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
      Pellentesque fermentum est quis mauris pharetra luctus.</p>
    </div>
    <div class="column column-33">
      <h2>Panel</h2>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
      Pellentesque fermentum est quis mauris pharetra luctus.</p>
    </div>
  </div>
</div>

```html
<div class="columns">
  <div class="column column-33">...</div>
</div>
```
