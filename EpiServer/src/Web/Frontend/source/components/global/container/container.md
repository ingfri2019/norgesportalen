## Description
Containers are used for encapsuling content to the center of the page.

### Container default

```html
<div class="container">...</div>
```

### Container small
The small container is specifically used when the content needs to be very condensed.

```html
<div class="container-small">...</div>
```

### Container large
The small container is specifically used when the content needs to be fluid, and with minimal bleed.

```html
<div class="container-large">...</div>
```
