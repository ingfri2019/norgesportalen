## Description

### Main links

Links are underlined when inside `<p>`, `<span>` or `<li>`.

<div class="styleguide-container">
  <main>
    <p><a href="#">Lorem ipsum dolor sit amet</a></p>
  </main>
</div>

```html
<main>
  <p>
    <a href="#">...</a>
  </p>
</main>
```

### Arrow link

<div class="styleguide-container">
  <p class="arrow-link">
    <a href="#">Lorem ipsum dolor sit amet</a>
  </p>
</div>

```html
<p class="arrow-link">
  <a href="#">...</a>
</p>
```

### Button link

<div class="styleguide-container">
    <a href="#" class="view-all-btn">Lorem ipsum dolor sit amet</a>
</div>

```html
<a href="#" class="view-all-btn">...</a>
```
