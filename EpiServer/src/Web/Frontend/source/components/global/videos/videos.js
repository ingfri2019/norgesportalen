﻿class Videos {
    constructor() {
        this._listeners = [];
        this.allVideos = document.querySelectorAll('iframe[src*="vimeo.com"], iframe[src*="youtube.com"]');
        window.addEventListener('resize', this.resize.bind(this));
        window.addEventListener('orientationchange', this.resize.bind(this));
        this.resize();
    }

    resize(event) {
        for(let video of this.allVideos){
            this.setSize(video);
        }
        if(event) {
            for(let i = this._listeners.length-1; i >= 0; i--) {
                this._listeners[i](this);
            }
        }
        return this;
    }

    setSize(video) {
        let width = video.parentNode.offsetWidth;
        video.setAttribute('height', (width * 9) / 16);
        video.setAttribute('width', width);
    }

    onResize(listener) {
        this._listeners.push(listener);
    }
}

export default new Videos();
