## Description
Videos are responsive, and will use 100% of the width, of the containing element.
<div>
<iframe width="560" height="315" src="https://www.youtube.com/embed/kXjF0-yCmpk" frameborder="0" allowfullscreen></iframe>
</div>
```html
<iframe width="560" height="315" src="https://www.youtube.com/embed/kXjF0-yCmpk" frameborder="0" allowfullscreen></iframe>
```
