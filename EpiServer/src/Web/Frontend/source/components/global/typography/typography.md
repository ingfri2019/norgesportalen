﻿## Description

## Base typography
<div class="styleguide-container">
  <h1>Headline 1</h1>
  <h2>Headline 2</h2>
  <h3>Headline 3</h3>
  <h4>Headline 4</h4>
  <h5>Headline 5</h5>
  <h6>Headline 6</h6>
</div>

```html
<h1>Headline 1</h1>
<h2>Headline 2</h2>
<h3>Headline 3</h3>
<h4>Headline 4</h4>
<h5>Headline 5</h5>
<h6>Headline 6</h6>
```


## Variations
### Section typography
<div class="styleguide-container">
<section>
  <h1 class="h1">Headline 1</h1>
</section>
</div>

```html
<section>
  <h1 class="h1">Headline 1</h1>
</section>
```

### Article Typography
<div class="styleguide-container">
<article>
  <h1>Headline 1</h1>
  <p class="ingress">Ingress</p>
  <p>Paragraph</p>
</article>
</div>
```html
<article>
  <h1>Headline 1</h1>
  <p class="ingress">Ingress</p>
  <p>Paragraph</p>
</article>
```
