## Description
Figures are the preferred method of showing images.
In this example, the figure is contained in a panel(33%) for better view.
The figure is responsive, and will scale 100% of the width of the containing div.

## Example

<div class="styleguide-container">
<div class="panels">
  <div class="panel">
    <figure>
      <img src="http://placehold.it/350x150">
      <figcaption>Figure caption</figcaption>
    </figure>
  </div>
</div>
</div>

### Code
```html
<figure>
  <img src="http://placehold.it/350x150">
  <figcaption>Figure caption</figcaption>
</figure>
```
