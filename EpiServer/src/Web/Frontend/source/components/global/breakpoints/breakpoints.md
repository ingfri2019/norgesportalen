## Description
Breakpoints are used directly assigned to selectors, meaning that there are no helper classes to break the layout.
The definitions below are

## Definition
**mini** is used for dimentions between 0 and 375px width

**mobile** is used for dimentions between 376px and 768px width

**tablet** is used for dimentions between 769px and 1024px width

Sizes beyond 1025px is defined as desktop, and is default.

### Variables

```scss
$mini: 375px;
$mobile: 768px;
$tablet: 1024px;
```

### Mixins

```scss
// Explicit
@mixin larger-than($breakpoint) {...}

@mixin smaller-than($breakpoint) {...}

@mixin between($small, $big) {...}

// Implicit
@mixin desktop() {...}

@mixin tablet() {...}

@mixin mobile() {...}

@mixin mini() {...}
```
