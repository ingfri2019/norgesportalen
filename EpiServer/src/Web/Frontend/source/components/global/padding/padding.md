## Description
The `.padded` and `.padded--*` only affects padding on top and bottom.

## Sizes
<section class="padding-example padded light">
  Default
</section>

<section class="padding-example padded--xsmall light">
  X-Small padding
</section>

<section class="padding-example padded--small light">
  Small
</section>

<section class="padding-example padded--medium light">
  Medium padding
</section>

<section class="padding-example padded--large light">
  Large padding
</section>

<section class="padding-example padded--xlarge light">
  X-Large padding
</section>

### Code

```html
<div class="padded"></div>//Default, same as padded--medium
<div class="padded--xsmall"></div>
<div class="padded--small"></div>
<div class="padded--medium"></div>
<div class="padded--large"></div>
<div class="padded--xlarge"></div>
```
