## Description


### Default
By default, the panel is not fluid, but responsive.

<div class="styleguide-container">
<div class="panels">
  <div class="panel">
      <h2>Panel</h2>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
      Pellentesque fermentum est quis mauris pharetra luctus.</p>
  </div>
  <div class="panel">
      <h2>Panel</h2>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
      Pellentesque fermentum est quis mauris pharetra luctus.</p>
  </div>
</div>
</div>

```html
<div class="panels">
  <div class="panel">
      ...
  </div>
</div>
```

### Large panel
By default, the panel is not fluid, but responsive.

<div class="styleguide-container">
<div class="panels">
  <div class="panel panel--large">
      <h2>Panel</h2>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
      Pellentesque fermentum est quis mauris pharetra luctus.</p>
  </div>
  <div class="panel panel--large">
      <h2>Panel</h2>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
      Pellentesque fermentum est quis mauris pharetra luctus.</p>
  </div>
</div>
</div>

```html
<div class="panels">
  <div class="panel panel--large">
    ...
  </div>
</div>
```

### Fluid panel
By default, the panel is not fluid, but responsive.

<div class="styleguide-container">
<div class="panels panels--fluid">
  <div class="panel">
      <h2>Panel</h2>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
      Pellentesque fermentum est quis mauris pharetra luctus.</p>
  </div>
  <div class="panel">
      <h2>Panel</h2>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
      Pellentesque fermentum est quis mauris pharetra luctus.</p>
  </div>
</div>
</div>

```html
<div class="panels panels--fluid">
  <div class="panel">
    ...
  </div>
</div>
```
