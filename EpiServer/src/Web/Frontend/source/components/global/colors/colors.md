## Primary

<div class="panels color-palette">
  <div class="panel">
    <div class="color passion-red-light">
      Passion Red Light<br>
      #ee3c30
    </div>
    <div class="color passion-red">
      Passion Red<br>
      #c61c08
    </div>
    <div class="color passion-red-dark">
      Passion Red Dark<br>
      #960004
    </div>
  </div>
  <div class="panel">
    <div class="color arctic-blue-light">
      Arctic Blue Light<br>
      #56adb8
    </div>
    <div class="color arctic-blue">
      Arctic Blue<br>
      #20788c
    </div>
    <div class="color arctic-blue-dark">
      Arctic Blue Dark<br>
      #0e465e
    </div>
  </div>
</div>

## Supporting

<div class="panels color-palette">
<div class="panel">
  <div class="color inverted alternate-background">
    Alternate Background<br>
    #f7f7f7
  </div>
</div>
  <div class="panel">
    <div class="color inverted seperator-light">
      Seperator Light<br>
      #c6c6c6
    </div>
  </div>
  <div class="panel">
    <div class="color seperator">
      Seperator<br>
      #656565
    </div>
  </div>

  <div class="panel">
    <div class="color inverted arctic-blue-lighter">
      Arctic Blue Lighter<br>
      #a2ccd2
    </div>
  </div>
</div>
