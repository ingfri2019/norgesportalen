import SCROLLTO from '../../helpers/scroll-to/scroll-to';

const anchorLinks = document.querySelectorAll('a.anchor');
const header = document.querySelector('body > header');

for(let anchorLink of anchorLinks){
    let href = anchorLink.getAttribute('href');
    let target = document.getElementById(href.substr(1));

    anchorLink.addEventListener('click', () => {
        SCROLLTO.y((target.offsetTop - (header.offsetHeight+15)), 500, 'easeInOutQuint');
    });
}
