## Basic

<div class="styleguide-container">
<a href="#" class="btn">Button</a>
<button class="btn">Button</button>
<button type="button" class="btn" disabled>Button</button>
<input type="button" class="btn" value="Button"></input>
</div>

```html
<a href="#" class="btn">Button</a>
<button class="btn">Button</button>
<button type="button" class="btn" disabled>Button</button>
<input type="button" class="btn" value="Button"></input>
```


## Anchor

<div class="styleguide-container">
<a href="#test" class="btn anchor">Button</a>
<button class="btn anchor">Button</button>
<button type="button" class="btn anchor" disabled>Button</button>
<input type="button" class="btn anchor" value="Button"></input>
</div>

```html
<a href="#" class="btn anchor">Button</a>
<button class="btn anchor">Button</button>
<button type="button" class="btn anchor" disabled>Button</button>
<input type="button" class="btn anchor" value="Button"></input>
```
