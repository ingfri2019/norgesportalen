## Description

## Section variations

### White / Default
<section class="padded--small white">
  The white and default section is unstyled, but the selector `.white` gives the section an white background.
</section>

```html
<section class="padded--small white">
  ...
</section>
```

### Light
<section class="padded--small light">
  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec at magna at urna hendrerit blandit vel sed leo.
</section>

```html
<section class="padded--small light">
  ...
</section>
```

### Gray
<section class="padded--small gray">
  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec at magna at urna hendrerit blandit vel sed leo.
</section>

```html
<section class="padded--small gray">
  ...
</section>
```

### Dark
<section class="padded--small dark">
  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec at magna at urna hendrerit blandit vel sed leo.
</section>

```html
<section class="padded--small dark">
  ...
</section>
```
