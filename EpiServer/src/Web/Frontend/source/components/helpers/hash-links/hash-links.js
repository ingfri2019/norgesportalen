import SCROLLTO from '../scroll-to/scroll-to';

if (window.top === window.self) {
	const header = document.querySelector('body > header');
	const articleHashLinks = document.querySelectorAll('#main-content a[href^="#"]');

	for (let i = 0; i < articleHashLinks.length; i++) {
	    articleHashLinks[i].addEventListener('click', (e) => {
	        e.preventDefault();
	        let hash = articleHashLinks[i].hash;
	        let target = document.getElementById(hash.substr(1));
	        SCROLLTO.y((target.offsetTop - (header.offsetHeight + 15)), 500, 'easeInOutQuint')
	        setTimeout(() => {
	            history.pushState(null, null, hash);
	        }, 510);
	    });
	};
}