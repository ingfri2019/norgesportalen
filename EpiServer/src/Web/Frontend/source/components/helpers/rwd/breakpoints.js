﻿export default {
    mini: 375,
    mobile: 768,
    tablet: 1024
};