﻿import breakpoints from './breakpoints';

class RWD {
    constructor() {
        this._listeners = [];
        window.addEventListener('resize', this.resize.bind(this));
        window.addEventListener('orientationchange', this.resize.bind(this));
        this.resize();
    }

    resize(event) {
        this.width = window.innerWidth;

        this.mini = (this.width < breakpoints.mini);
        this.mobile = (!this.mini && this.width < breakpoints.mobile);
        this.tablet = (!this.mobile && this.width < breakpoints.tablet);
        this.desktop = (this.width >= breakpoints.tablet);

        if(event) {
            for(let i = this._listeners.length-1; i >= 0; i--) {
                this._listeners[i](this);
            }
        }
        return this;
    }

    onResize(listener) {
        this._listeners.push(listener);
    }
}

export default new RWD();