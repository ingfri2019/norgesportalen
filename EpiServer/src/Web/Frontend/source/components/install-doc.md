
# Norgesportalen Frontend Build

## Install and Build instructions
Norgesportalens frontend require several packages to be installed. For installation of these packages, you need a local version of [NodeJS](https://nodejs.org/en/download/) on your machine.

After installing NodeJS, you need to make sure you have the latest version of npm installed by running the following:

```sh
npm install npm -g
```

You can verify the versions on your machine by typing `node -v` and `npm -v`.
The supported version for this project is:
* Above Node v5.0.0
* Above Npm v3.3.6

### Install dependencies
before we can run our project, we need to make sure all dependencies are installed.
The dependencies are specified in the package.json file in the root of the project.
Run the following command at the root of the project:
```sh
npm install
```

### Build
To build the project, run the following command:
```sh
npm run build
```
or just
```sh
gulp
```

### Tasks and watchers
In addition to the build script, the project has several defined tasks:

`npm run dev`
Starts the development task, which runs a build and watches for changes across the entire prosject.

`npm run dev:js` Starts the JavaScript development task, runs and watches for changes on .js-files.

`npm run dev:scss` Starts the CSS development task, runs and watches for changes on .scss-files.

`npm run dev:docs` Starts the docs development task, runs and watches for changes inside /designmanual/ folder and .md-files.

`npm run build:docs` Starts the docs build task.

`npm run build` Starts all tasks above and copies images and fonts.

`npm run lint` Starts the lint task, which checks code standard for all .js-files.

\* Excluding /node_modules/

### Deployment
When running the build script on a server, you can use the BuildFrontend.cmd file as a build-script for frontend.
