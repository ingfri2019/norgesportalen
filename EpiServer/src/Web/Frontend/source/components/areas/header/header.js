import { GLOBAL_TEXTS } from '../../helpers/global-text/global-text';

const navigationBtn = document.getElementById('navigation-btn');
const navigation = document.getElementById('navigation');
const mainContent = document.getElementById('main-content');
const body = document.body;
const languageDropdownBtns = document.querySelectorAll('.language-dropdown-btn');

if (navigation && navigationBtn) {
    navigationBtn.querySelector('.text').innerHTML = GLOBAL_TEXTS.menu.open;
    navigationBtn.addEventListener('click', () => {
        navigationBtn.classList.toggle('open');
        navigation.classList.toggle('open');
        body.classList.toggle('locked');
        if(navigation.classList.contains('open')){
            navigationBtn.querySelector('.text').innerHTML = GLOBAL_TEXTS.menu.close;
            navigationBtn.setAttribute('aria-expanded', true);
            navigation.setAttribute('aria-hidden', false);
            mainContent.setAttribute('aria-hidden', true);
        } else {
            navigationBtn.querySelector('.text').innerHTML = GLOBAL_TEXTS.menu.open;
            navigationBtn.setAttribute('aria-expanded', false);
            navigation.setAttribute('aria-hidden', true);
            mainContent.setAttribute('aria-hidden', false);
        }

    });
}

for(let languageDropdownBtn of languageDropdownBtns){
    languageDropdownBtn.addEventListener('click', () => {
        if(languageDropdownBtn.parentElement.querySelector('.language-dropdown').classList.contains('open')) {
            for(let element of languageDropdownBtns) {
                element.parentElement.querySelector('.language-dropdown').classList.remove('open');
            }
        } else {
            for(let element of languageDropdownBtns) {
                element.parentElement.querySelector('.language-dropdown').classList.remove('open');
                languageDropdownBtn.parentElement.querySelector('.language-dropdown').classList.add('open');
            }
        }

    });

    languageDropdownBtn.addEventListener('keyup', (e) => {
        if (e.which === 13 || e.which === 32) {
            languageDropdownBtn.click();
        }
    });
}
