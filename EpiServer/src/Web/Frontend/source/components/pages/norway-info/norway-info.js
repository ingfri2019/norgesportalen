﻿if (document.querySelector('.norway-info')) {
    const searchField = document.getElementById("search-field");

    /* RENDER CONTENT TO VIEW */
    function renderColumns(input, output) {
        let target = document.querySelector('.embassies');
        let srTarget = document.querySelector('.result-text');
        let content = JSON.parse(input);

        target.innerHTML = ''; // Clear old content

        let size = content.length;

        let chunkSize = 1;
        if (size > 5) {
            //chunkSize = Math.ceil((size / 5) / 5) * 5;
            chunkSize = size / 4;
        }

        var i, j, temparray, chunk = chunkSize, nrOfResults = 0;
        for (i = 0, j = content.length; i < j; i += chunk) {
            let panel = document.createElement("div");
            panel.className = "panel";
            let inner = document.createElement("div");
            inner.className = "inner";

            // Iterate and populate
            for (let embassys of content.slice(i, i + chunk)) {
                
                let header = document.createElement("h2");
                header.innerHTML = embassys.Letter;
                inner.appendChild(header);
                nrOfResults += embassys.Embassies.length;
                let list = document.createElement("ul");

                for (let embassy of embassys.Embassies) {
                    let item = document.createElement("li");
                    let link = document.createElement("a");
                    link.setAttribute("href", embassy.Website);
                    link.textContent = embassy.Name;
                    item.appendChild(link);
                    list.appendChild(item);
                }

                inner.appendChild(list);
            }
            panel.appendChild(inner);
            target.appendChild(panel);
        }
        if (output != "") {
            srTarget.innerHTML = 'Found ' + nrOfResults + ' results for "' + output + '"';
        } else {
            srTarget.innerHTML = 'Found ' + nrOfResults;
        }

        
    }

    /* AJAX */
    function getEmbassies(searchString) {
        let pageId = document.getElementById("page-id").value;
        let xhr = new XMLHttpRequest();
        xhr.open('GET', '/api/embassies/SortedByCategory?id=' + pageId + '&name=' + searchString);
        // xhr.open('GET', 'https://www.norway.no/api/embassies/SortedByCategory?id=' + pageId + '&name=' + searchString);
        xhr.send(null);

        xhr.onreadystatechange = function () {
            let DONE = 4;
            let OK = 200;
            if (xhr.readyState === DONE) {
                if (xhr.status === OK) {
                    renderColumns(xhr.responseText, searchString);
                } else {
                    console.log('Error: ' + xhr.status);
                }
            }
        };
    }

    /* ACTIVATE ON KEYUP LISTENER */
    searchField.addEventListener('keyup', function (e) {
        getEmbassies(searchField.value);
    });

    /* INITIAL POPULATION OF DATA */
    getEmbassies("");
}