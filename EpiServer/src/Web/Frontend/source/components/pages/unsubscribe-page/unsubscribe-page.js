﻿const unsubscribePage = document.querySelector('.unsubscribe-page');

function unsubscribe(email, id) {
    let xhr = new XMLHttpRequest();
    xhr.open('POST', '/api/newsletter/Unsubscribe?email=' + email + '&list=' + id);
    xhr.send(null);

    xhr.onreadystatechange = function () {
        let DONE = 4;
        if (xhr.readyState === DONE) {
            displayNotification(status)
        }
    };
}

function displayNotification(xhr) {
    let OK = 200;
    if (xhr.status === OK) {
        alert("YES");
    } else {
        alert('Error: ' + xhr.status);
    }
    
}

if (unsubscribePage) {
    let form = unsubscribePage.querySelector('.unsubscribe-form');
    form.addEventListener('submit', (event) => {
        event.preventDefault();
        let email = form.querySelector(".unsubscribe-email").value;
        let id = form.querySelector(".unsubscribe-id").value;
        unsubscribe(email, id);
    })
}