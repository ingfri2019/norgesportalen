﻿if (document.querySelector('.processpage')) {
    let hash = window.location.hash;
    let target = document.getElementById(hash.substr(1));
    if (target) {
        target.querySelector('.process-step-header').click();
    }
}