// const thumbs = document.querySelector('.carousel__thumbnail');
const carouselSlidesWrapper = document.querySelector("[data-carousel-slides]");
const carouselButtons = document.querySelectorAll("[data-carousel-button]");

const carouselSlides = document.querySelectorAll("[data-carousel-slide]");

carouselButtons.forEach((carouselButton, index) => {
    // Set the first item in the list to be active with correct aria attr and classes.
    carouselButtons[0].classList.add("carousel__button--active");
    carouselSlides[0].setAttribute("aria-hidden", `false`);
    
    carouselButtons.forEach(carouselButton => {
        carouselButton.setAttribute("aria-selected", "false");
    });
    carouselButtons[0].setAttribute("aria-selected", `true`);

    // Listen for the click event
    carouselButton.addEventListener("click", () => {
        /* SLIDES */
        // Reset classes and attributes
        carouselSlides.forEach(slide => {
            slide.setAttribute("aria-hidden", `true`);
        });

        // Add attributes and classes to active slide.
        carouselSlides[index].setAttribute("aria-hidden", `false`);

        /* THUMBNAILS */
        // Reset classes and attributes
        carouselButtons.forEach(carouselButton => {
            carouselButton.classList.remove("carousel__button--active");
            carouselButton.setAttribute("aria-selected", "false");
        });

        // Add attributes and classes to active thumb.
        carouselButton.classList.add("carousel__button--active");
        carouselButton.setAttribute("aria-selected", "true");

        carouselSlidesWrapper.setAttribute("data-active-slide", `${index + 1}`);
    });
});
