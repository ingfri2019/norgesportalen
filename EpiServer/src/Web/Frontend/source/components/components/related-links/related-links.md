## Description

Links in a list.

## Usage

```html
<div class="related-links">
    <h3>Related links</h3>
    <ul class="related-links__list">
        <li class="related-links__item arrow-link">
            <a href="#">Lorem ipsum dolor sit amet consectetur adipisicing elit</a>
        </li>
        <li class="related-links__item arrow-link">
            <a href="#">Sit amet consectetur</a>
        </li>
        <li class="related-links__item arrow-link">
            <a href="#">Adipisicing elit</a>
        </li>
    </ul>
</div>
```

### Preview

<div class="related-links">
    <h3>Related links</h3>
    <ul class="related-links__list">
        <li class="related-links__item arrow-link">
            <a href="#">Lorem ipsum dolor sit amet consectetur adipisicing elit</a>
        </li>
        <li class="related-links__item arrow-link">
            <a href="#">Sit amet consectetur</a>
        </li>
        <li class="related-links__item arrow-link">
            <a href="#">Adipisicing elit</a>
        </li>
    </ul>
</div>
