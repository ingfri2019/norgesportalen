## 4+1 Top Panel

<div class="styleguide-container">
<section class="top-tasks-panel">
  <div class="container-large">
    <div class="panels five">
      <div class="panel">
        <div class="panels">
          <div class="panel panel-small">
            <a href="#" class="inner"><img src="http://placehold.it/40x35" alt="Icon">
              <span class="headline">Top Task 1</span>
            </a>
          </div>
          <div class="panel panel-small">
            <a href="#" class="inner"><img src="http://placehold.it/40x35" alt="Icon">
              <span class="headline">Top Task 2</span>
            </a>
          </div>
          <div class="panel panel-small">
            <a href="#" class="inner"><img src="http://placehold.it/40x35" alt="Icon">
              <span class="headline">Top Task 3</span>
            </a>
          </div>
          <div class="panel panel-small">
            <a href="#" class="inner"><img src="http://placehold.it/40x35" alt="Icon">
              <span class="headline">Top Task 4</span>
            </a>
          </div>
        </div>
      </div>
      <div class="panel panel-large">
        <div class="panels">
          <div class="panel">
            <div class="inner">
              <span class="headline">
                For nordmenn
              </span>
              <p class="arrow-link"><a href="#">Single line link</a><br></p>
              <p class="arrow-link"><a href="#">Link with description</a><br>Description</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
</div>

```html
<section class="top-tasks-panel">
  <div class="container-large">
    <div class="panels five">
      <div class="panel">
        <div class="panels">
          <div class="panel panel-small">
            <a href="#" class="inner"
              ><img src="http://placehold.it/40x35" alt="Icon" />
              <span class="headline">Top Task 1</span>
            </a>
          </div>
          <div class="panel panel-small">
            <a href="#" class="inner"
              ><img src="http://placehold.it/40x35" alt="Icon" />
              <span class="headline">Top Task 2</span>
            </a>
          </div>
          <div class="panel panel-small">
            <a href="#" class="inner"
              ><img src="http://placehold.it/40x35" alt="Icon" />
              <span class="headline">Top Task 3</span>
            </a>
          </div>
          <div class="panel panel-small">
            <a href="#" class="inner"
              ><img src="http://placehold.it/40x35" alt="Icon" />
              <span class="headline">Top Task 4</span>
            </a>
          </div>
        </div>
      </div>
      <div class="panel panel-large">
        <div class="panels">
          <div class="panel">
            <div class="inner">
              <span class="headline">
                For nordmenn
              </span>
              <p class="arrow-link"><a href="#">Single line link</a><br /></p>
              <p class="arrow-link">
                <a href="#">Link with description</a><br />Description
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
```

## 3+1 Top Panel

<div class="styleguide-container">
<section class="top-tasks-panel">
  <div class="container-large">
    <div class="panels">
      <div class="panel panel-small">
        <a href="#" class="inner"><img src="http://placehold.it/40x35" alt="Icon">
          <span class="headline">Top Task 1</span>
        </a>
      </div>
      <div class="panel panel-small">
        <a href="#" class="inner"><img src="http://placehold.it/40x35" alt="Icon">
          <span class="headline">Top Task 2</span>
        </a>
      </div>
      <div class="panel panel-small">
        <a href="#" class="inner"><img src="http://placehold.it/40x35" alt="Icon">
          <span class="headline">Top Task 3</span>
        </a>
      </div>
      <div class="panel panel-large">
        <div class="inner">
          <span class="headline">
            For nordmenn
          </span>
          <p class="arrow-link"><a href="#">Single line link</a><br></p>
          <p class="arrow-link"><a href="#">Link with description</a><br>Description</p>
        </div>
      </div>
    </div>
  </div>
</section>
</div>

```html
<section class="top-tasks-panel">
  <div class="container-large">
    <div class="panels">
      <div class="panel panel-small">
        <a href="#" class="inner"
          ><img src="http://placehold.it/40x35" alt="Icon" />
          <span class="headline">Top Task 1</span>
        </a>
      </div>
      <div class="panel panel-small">
        <a href="#" class="inner"
          ><img src="http://placehold.it/40x35" alt="Icon" />
          <span class="headline">Top Task 2</span>
        </a>
      </div>
      <div class="panel panel-small">
        <a href="#" class="inner"
          ><img src="http://placehold.it/40x35" alt="Icon" />
          <span class="headline">Top Task 3</span>
        </a>
      </div>
      <div class="panel panel-large">
        <div class="inner">
          <span class="headline">
            For nordmenn
          </span>
          <p class="arrow-link"><a href="#">Single line link</a><br /></p>
          <p class="arrow-link">
            <a href="#">Link with description</a><br />Description
          </p>
        </div>
      </div>
    </div>
  </div>
</section>
```

## 2+1

<div class="styleguide-container">
<section class="top-tasks-panel">
  <div class="container-large">
    <div class="panels">
      <div class="panel panel-small">
        <a href="#" class="inner"><img src="http://placehold.it/40x35" alt="Icon">
          <span class="headline">Top Task 1</span>
        </a>
      </div>
      <div class="panel panel-small">
        <a href="#" class="inner"><img src="http://placehold.it/40x35" alt="Icon">
          <span class="headline">Top Task 2</span>
        </a>
      </div>
      <div class="panel panel-large">
        <div class="inner">
          <span class="headline">
            For nordmenn
          </span>
          <p class="arrow-link"><a href="#">Single line link</a><br></p>
          <p class="arrow-link"><a href="#">Link with description</a><br>Description</p>
        </div>
      </div>
    </div>
  </div>
</section>
</div>

```html
<section class="top-tasks-panel">
  <div class="container-large">
    <div class="panels">
      <div class="panel panel-small">
        <a href="#" class="inner"
          ><img src="http://placehold.it/40x35" alt="Icon" />
          <span class="headline">Top Task 1</span>
        </a>
      </div>
      <div class="panel panel-small">
        <a href="#" class="inner"
          ><img src="http://placehold.it/40x35" alt="Icon" />
          <span class="headline">Top Task 2</span>
        </a>
      </div>
      <div class="panel panel-large">
        <div class="inner">
          <span class="headline">
            For nordmenn
          </span>
          <p class="arrow-link"><a href="#">Single line link</a><br /></p>
          <p class="arrow-link">
            <a href="#">Link with description</a><br />Description
          </p>
        </div>
      </div>
    </div>
  </div>
</section>
```

## 1+1

<div class="styleguide-container">
<section class="top-tasks-panel">
  <div class="container-large">
    <div class="panels">
      <div class="panel panel-small">
        <a href="#" class="inner"><img src="http://placehold.it/40x35" alt="Icon">
          <span class="headline">Top Task 1</span>
        </a>
      </div>
      <div class="panel panel-large">
        <div class="inner">
          <span class="headline">
            For nordmenn
          </span>
          <p class="arrow-link"><a href="#">Single line link</a><br></p>
          <p class="arrow-link"><a href="#">Link with description</a><br>Description</p>
        </div>
      </div>
    </div>
  </div>
</section>
</div>

```html
<section class="top-tasks-panel">
  <div class="container-large">
    <div class="panels">
      <div class="panel panel-small">
        <a href="#" class="inner"
          ><img src="http://placehold.it/40x35" alt="Icon" />
          <span class="headline">Top Task 1</span>
        </a>
      </div>
      <div class="panel panel-large">
        <div class="inner">
          <span class="headline">
            For nordmenn
          </span>
          <p class="arrow-link"><a href="#">Single line link</a><br /></p>
          <p class="arrow-link">
            <a href="#">Link with description</a><br />Description
          </p>
        </div>
      </div>
    </div>
  </div>
</section>
```
