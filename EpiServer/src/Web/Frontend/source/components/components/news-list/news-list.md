## Description


## Usage

```
<section class="news-list">
  <div class="container-large">
    <a href="{LinkUrl0}">
      <h1 class="h1 text-center">News List</h1>
    </a>
    <div class="panels">
      <div class="panel">
        <a href="{LinkUrl1}" class="inner">
          <small>{Date1} | {Location1}</small>
          <h2>{Headline1}</h2>
          <p>{Ingress1}</p>
        </a>
      </div>
      <div class="panel">
        <a href="{LinkUrl2}" class="inner">
          <small>{Date2} | {Location2}</small>
          <h2>{Headline2}</h2>
          <p>{Ingress2}</p>
        </a>
      </div>
      <div class="panel">
        <a href="{LinkUrl3}" class="inner">
          <small>{Date3} | {Location3}</small>
          <h2>{Headline3}</h2>
          <p>{Ingress3}</p>
        </a>
      </div>
    </div>
    <span class="text-center">
      <a href="{LinkUrl0}" class="view-all-btn">View all</a>
    </span>
  </div>
</section>
```

### Preview

<section class="news-list">
  <div class="container-large">
    <a href="{LinkUrl0}">
      <h1 class="h1 text-center">News List</h1>
    </a>
    <div class="panels">
      <div class="panel">
        <a href="{LinkUrl1}" class="inner">
          <small>{Date1} | {Location1}</small>
          <h2>{Headline1}</h2>
          <p>{Ingress1}</p>
        </a>
      </div>
      <div class="panel">
        <a href="{LinkUrl2}" class="inner">
          <small>{Date2} | {Location2}</small>
          <h2>{Headline2}</h2>
          <p>{Ingress2}</p>
        </a>
      </div>
      <div class="panel">
        <a href="{LinkUrl3}" class="inner">
          <small>{Date3} | {Location3}</small>
          <h2>{Headline3}</h2>
          <p>{Ingress3}</p>
        </a>
      </div>
    </div>
    <span class="text-center">
      <a href="{LinkUrl0}" class="view-all-btn">View all</a>
    </span>
  </div>
</section>
