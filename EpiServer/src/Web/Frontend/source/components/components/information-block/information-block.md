## Description


## Usage

```
<section class="information-block padded--large">
  <div class="container">
    <h2>Information heading</h2>
    <p>Information Main body</p>
    <p>Information Main body 2</p>
  </div>
</section>
```

### Preview

<section class="information-block padded--large">
  <div class="container">
    <h2>Information heading</h2>
    <p>Information Main body</p>
    <p>Information Main body 2</p>
  </div>
</section>
