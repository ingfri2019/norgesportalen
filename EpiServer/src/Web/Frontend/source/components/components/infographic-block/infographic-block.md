## Description


## Usage

```
<div class="panels">
  <div class="panel infographic">
    <div class="inner">
      <img src="http://placehold.it/150x150" alt="{Alt1}">
      <h3>Headline 1</h3>
      <p>Description 1</p>
    </div>
  </div>
</div>
```

### Preview

<div class="panels">
  <div class="panel infographic">
    <div class="inner">
      <img src="http://placehold.it/150x150" alt="{Alt1}">
      <h3>Headline 1</h3>
      <p>Description 1</p>
    </div>
  </div>
</div>
