﻿import './accordion/accordion';
import './gdpr/gdpr';
import './process/process';
// import './scroll-hint/scroll-hint';
import './services-panel/services-panel';
import './subscription-block/subscription-block';
import './card/card';
//import './page-survey/page-survey';
import './carousel/carousel';
