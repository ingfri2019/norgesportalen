<div class="styleguide-container">
  <div class="panels service-panel">
    <div class="panel">
      <a href="#">
        <h2>Link 1</h2>
        <p>Description of link 1</p>
      </a>
    </div>
    <div class="panel">
      <a href="#">
        <h2>Link 2</h2>
        <p>Description of link 2</p>
      </a>
    </div>
    <div class="panel">
      <a href="//via.placeholder.com/800x450?text=Landscape" target="_blank">
        <h2>Link 3: Landscape image</h2>
        <img alt="Landscape image" src="//via.placeholder.com/800x450?text=Landscape">
      </a>
    </div>
    <div class="panel">
      <a href="//via.placeholder.com/450x800?text=Portrait" target="_blank">
        <h2>Link 4: Portrait image</h2>
        <img alt="Portrait image" src="//via.placeholder.com/450x800?text=Portrait">
      </a>
    </div>
    <div class="panel">
      <a href="//via.placeholder.com/600x600?text=Square" target="_blank">
        <h2>Link 5: Square image</h2>
        <img alt="Square image" src="//via.placeholder.com/600x600?text=Square">
      </a>
    </div>
  </div>
</div>

```html
<div class="panels service-panel">
  <div class="panel">
    <a href="#">
      <h2>Link 1</h2>
      <p>Description of link 1</p>
    </a>
  </div>
  <div class="panel">
    <a href="#">
      <h2>Link 2</h2>
      <p>Description of link 2</p>
    </a>
  </div>
  <div class="panel">
    <a href="//via.placeholder.com/800x450?text=Landscape">
      <h2>Link 3: Landscape image</h2>
      <img alt="Landscape image" src="//via.placeholder.com/800x450?text=Landscape">
    </a>
  </div>
  <div class="panel">
    <a href="//via.placeholder.com/450x800?text=Portrait">
      <h2>Link 4: Portrait image</h2>
      <img alt="Portrait image" src="//via.placeholder.com/450x800?text=Portrait">
    </a>
  </div>
  <div class="panel">
    <a href="//via.placeholder.com/600x600?text=Square">
      <h2>Link 5: Square image</h2>
      <img alt="Square image" src="//via.placeholder.com/600x600?text=Square">
    </a>
  </div>
</div>
```
