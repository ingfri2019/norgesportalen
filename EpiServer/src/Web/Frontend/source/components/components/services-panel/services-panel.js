(function (arr) {
    arr.forEach(function (item) {
        if (item.hasOwnProperty('remove')) {
            return;
        }
        Object.defineProperty(item, 'remove', {
            configurable: true,
            enumerable: true,
            writable: true,
            value: function remove() {
                this.parentNode.removeChild(this);
            }
        });
    });
})([Element.prototype, CharacterData.prototype, DocumentType.prototype]);

const servicePanel = document.querySelector('.service-panel');

if (servicePanel) {
    let panelImages = servicePanel.querySelectorAll('img');

    for (let i = 0; i <= panelImages.length - 1; i++) {
        let img = panelImages[i];
        let src = img.src;
        let wrapper = document.createElement('div');
        wrapper.className = 'image-wrapper';
        wrapper.setAttribute('style', 'background-image: url(' + src + ');');
        img.parentNode.insertBefore(wrapper, img);
        wrapper.appendChild(img);
        img.remove();
    }
}