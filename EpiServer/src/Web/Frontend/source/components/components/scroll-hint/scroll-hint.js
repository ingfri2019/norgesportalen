function elemOffset(el) {
    var rect = el.getBoundingClientRect(),
    scrollTop = window.pageYOffset || document.documentElement.scrollTop;
    return { top: rect.top + scrollTop }
}

const scrollHint = document.querySelector('[data-scroll-hint]');

if (scrollHint !== null) {
    let children = scrollHint.parentNode.children;
    let ind = [].indexOf.call(children, scrollHint);
    let prevElem = children[ind === 0 ? 0 : ind - 1];

    let prevElemOffset = elemOffset(prevElem);
    let prevElemOffsetTop = prevElemOffset.top;
    let scrollHintOffsetTop = prevElemOffsetTop + prevElem.offsetHeight;

    window.addEventListener('load', function() {
        if (window.innerWidth > 767 && (scrollHintOffsetTop + 300) > window.innerHeight) {
            scrollHint.classList.add('scroll-hint-shown');
        }
    });

    window.addEventListener('scroll', function() {
        if (window.scrollY > 100) {
            scrollHint.classList.add('hidden');
        }
    });
}