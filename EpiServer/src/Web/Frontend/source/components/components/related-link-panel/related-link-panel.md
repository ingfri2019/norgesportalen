## Description

Links can be used with and without description.

## Usage

```html
<div class="related-link-panel">
  <h2>Links</h2>
  <div class="panels">
    <div class="panel">
      <div class="inner related-link-block-element">
        <p>{LinkDescription1}</p>
        <p class="arrow-link">
          <a title="{LinkTitle1}" href="{LinkUrl1}" target="_blank"
            >{LinkTitle1}</a
          >
        </p>
      </div>
    </div>
    <div class="panel">
      <div class="inner related-link-block-element">
        <p>{LinkDescription2}</p>
        <p class="arrow-link">
          <a title="{LinkTitle2}" href="{LinkUrl2}" target="_blank"
            >{LinkTitle2}</a
          >
        </p>
      </div>
    </div>
  </div>
</div>
```

### Preview

<div class="related-link-panel">
  <h2>Related links</h2>
  <div class="panels">
    <div class="panel">
      <div class="inner related-link-block-element">
        <p>{LinkDescription1}</p>
        <p class="arrow-link">
          <a title="{LinkTitle1}" href="{LinkUrl1}" target="_blank">{LinkTitle1}</a>
        </p>
      </div>
    </div>
    <div class="panel">
      <div class="inner related-link-block-element">
        <p>{LinkDescription2}</p>
        <p class="arrow-link">
          <a title="{LinkTitle2}" href="{LinkUrl2}" target="_blank">{LinkTitle2}</a>
        </p>
      </div>
    </div>
  </div>
</div>
