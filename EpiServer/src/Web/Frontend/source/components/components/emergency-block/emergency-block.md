## Description


## Usage

```
<section class="emergency-block padded--large">
    <div class="container">
        <h2>Emergency Heading</h2>
        <p>Main Emergency body.</p>
        <p><a href="{Link1}">{Link1}</a></p>
    </div>
</section>
```

### Preview

<section class="emergency-block padded--large">
    <div class="container">
        <h2>Emergency Heading</h2>
        <p>Main Emergency body.</p>
        <p><a href="{Link1}">{Link1}</a></p>
    </div>
</section>
