function getCookie(name) {
    var v = document.cookie.match('(^|;) ?' + name + '=([^;]*)(;|$)');
    return v ? v[2] : null;
}

function setCookie(name, value, days) {
    var d = new Date;
    d.setTime(d.getTime() + 24*60*60*1000*days);
    document.cookie = name + '=' + value + ';path=/;expires=' + d.toGMTString();
}

const gdpr = document.querySelector('[data-gdpr]');
const accept = document.querySelector('[data-accept-gdpr]');
const cookie = getCookie('gdpr');

if (gdpr && cookie !== 'accepted') {
    gdpr.classList.remove('hidden');

	accept.onclick = function() {
	    gdpr.classList.add('hidden');
	    setCookie('gdpr', 'accepted', 365);
	};
}