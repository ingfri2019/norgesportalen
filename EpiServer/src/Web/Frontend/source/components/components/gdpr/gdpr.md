<div class="styleguide-container">
  <div class="gdpr-wrapper hidden" data-gdpr>
    <div class="container-large">
      <div class="gdpr">
        <div class="gdpr-col-logo">
          <span class="logo"></span>
          Norwegian Ministry of foreign affairs
        </div>
        <div class="gdpr-col-content">
          <h2>Use of cookies</h2>
          <p>We do not use cookies to personalize content or ads, we use them to analyse out traffic. We do not share any information about your use of our site.</p>
        </div>
        <div class="gdpr-col-accept">
          <button type="button" class="btn-link" data-accept-gdpr>I accept</button>
        </div>
      </div>
    </div>
  </div>
</div>

```html
<div class="gdpr-wrapper hidden" data-gdpr>
  <div class="container-large">
    <div class="gdpr">
      <div class="gdpr-col-logo">
        <span class="logo"></span>
        Norwegian Ministry of foreign affairs
      </div>
      <div class="gdpr-col-content">
        <h2>Use of cookies</h2>
        <p>We do not use cookies to personalize content or ads, we use them to analyse out traffic. We do not share any information about your use of our site.</p>
      </div>
      <div class="gdpr-col-accept">
        <button type="button" class="btn-link" data-accept-gdpr>I accept</button>
      </div>
    </div>
  </div>
</div>
```
