## Description

Accordion is used in the following cases:

- When information is not critical
- When information is supplementary on the page
- When information contains second-relative links

## Usage

```html
<div class="accordion" role="tablist">
  <div>
    <div class="accordion-item">
      <div
        class="accordion-title"
        tabindex="0"
        role="tab"
        aria-expanded="false"
        id="Staff"
      >
        Accordion Headline
        <span class="accordion-icon">
          <span class="horizontal"></span>
          <span class="vertical"></span>
        </span>
      </div>
      <div class="accordion-content" role="tabpanel" aria-hidden="true">
        <p>Content for the accordion</p>
      </div>
    </div>
  </div>
</div>
```

### Preview

<div class="accordion" role="tablist">
    <div>
        <div class="accordion-item">
            <div class="accordion-title" tabindex="0" role="tab" aria-expanded="false" id="Staff">
                Accordion Headline
                <span class="accordion-icon">
                    <span class="horizontal"></span>
                    <span class="vertical"></span>
                </span>
            </div>
            <div class="accordion-content" role="tabpanel" aria-hidden="true">
				<p>Content for the accordion</p>
            </div>
        </div>
    </div>
</div>

## Variations

Accordions can be used in the following variations:

### Link list

```html
<div class="accordion" role="tablist">
  ...
  <div class="accordion-content link-list" role="tabpanel" aria-hidden="false">
    <div class="panels">
      <div class="panel">
        <div class="inner">
          <p class="arrow-link">
            <a href="{YourURL-1}">
              Link 1
            </a>
          </p>
        </div>
      </div>
      <div class="panel">
        <div class="inner">
          <p class="arrow-link">
            <a href="{YourURL-2}">
              Link 2
            </a>
          </p>
        </div>
      </div>
    </div>
  </div>
</div>
```

<div class="accordion" role="tablist">
    <div>
        <div class="accordion-item open">
			<div class="accordion-title" tabindex="0" role="tab" aria-expanded="true" id="Links-headline">Links headline
			<span class="accordion-icon">
				<span class="horizontal"></span>
				<span class="vertical"></span>
			</span>
		</div>
		<div class="accordion-content link-list" role="tabpanel" aria-hidden="false">
			<div class="panels">
				<div class="panel">
					<div class="inner">
						<p class="arrow-link">
							<a href="{YourURL-1}">
								Link 1
							</a>
						</p>
					</div>
				</div>
			<div class="panel">
				<div class="inner">
					<p class="arrow-link">
						<a href="{YourURL-2}">
							Link 2
						</a>
					</p>
				</div>
			</div>
		</div>
	</div>
</div>

### People list

### Single column text

### Double column text
