﻿import SCROLLTO from '../../helpers/scroll-to/scroll-to';

const accordionTitleItems = document.querySelectorAll('.accordion-title');
const header = document.querySelector('body > header');

// Click Handler
for (let i = 0; i < accordionTitleItems.length; i++) {
    accordionTitleItems[i].addEventListener('click', (e) => {
        e.preventDefault();
        // history.replaceState({}, '', '#' + accordionTitleItems[i].getAttribute('id'));
        // accordionTitleItems[i].parentElement.classList.toggle('open');
        if (accordionTitleItems[i].parentElement.classList.contains('open')) {
            accordionTitleItems[i].parentElement.classList.remove('open');
            accordionTitleItems[i].setAttribute('aria-expanded', false);
            accordionTitleItems[i].parentElement.querySelector('.accordion-content').setAttribute('aria-hidden', true);
            history.replaceState({}, '', window.location.pathname);
        } else {
            accordionTitleItems[i].parentElement.classList.add('open');
            accordionTitleItems[i].setAttribute('aria-expanded', true);
            accordionTitleItems[i].parentElement.querySelector('.accordion-content').setAttribute('aria-hidden', false);
            history.replaceState({}, '', '#' + accordionTitleItems[i].getAttribute('id'));
        }
    });

    // accordionTitleItems[i].addEventListener('keyup', (e) => {
    //     if (e.which === 13 || e.which === 32) {
    //         accordionTitleItems[i].click();
    //     }
    // });
}

// Router
if (document.querySelector('.accordion')) {
    let hash = window.location.hash;
    let target = document.getElementById(hash.substr(1));
    if (target) {
        target.click();
        SCROLLTO.y((target.offsetTop - (header.offsetHeight + 15)), 500, 'easeInOutQuint')
        target.focus();
    }
}
