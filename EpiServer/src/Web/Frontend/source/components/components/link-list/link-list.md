## Description

## Usage

```html
<div class="link-list">
  <h2>Links</h2>
  <div class="panels">
    <div class="panel">
      <div class="inner">
        <p class="arrow-link">
          <a title="{LinkTitle1}" href="{LinkUrl1}">
            {LinkTitle1}
          </a>
        </p>
      </div>
    </div>
    <div class="panel">
      <div class="inner">
        <p class="arrow-link">
          <a title="{LinkTitle2}" href="{LinkUrl2}">
            {LinkTitle2}
          </a>
        </p>
      </div>
    </div>
  </div>
</div>
```

### Preview

<div class="link-list">
  <h2>Links</h2>
  <div class="panels">
    <div class="panel">
      <div class="inner">
        <p class="arrow-link">
          <a title="{LinkTitle1}" href="{LinkUrl1}">
            {LinkTitle1}
          </a>
        </p>
      </div>
    </div>
    <div class="panel">
      <div class="inner">
        <p class="arrow-link">
          <a title="{LinkTitle2}" href="{LinkUrl2}">
            {LinkTitle2}
          </a>
        </p>
      </div>
    </div>
  </div>
</div>
