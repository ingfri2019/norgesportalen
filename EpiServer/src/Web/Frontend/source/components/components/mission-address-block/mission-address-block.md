## Description

This component have 3 variations:

- Address and map - half width each
- Address only - full width
- Map only - full width

## Usage

```
<div class="panels mission-address-block">
    <div class="panel">
        <p class="panel-info">
            <strong>Adresse</strong>
            <p>Dampfærgevej 10, 4. sal<br />2100 København Ø<br />Danmark</p>
        </p>
        <p class="panel-info">
            <strong>Kontaktinformasjon</strong>
            <p>Phone: +1(646)430-7510<br/>
            Fax: +1(646)430-7511<br/>
            Email: delun@mfa.no</p>
        </p>
    </div>
    <div class="panel" id="gmap">
        <script>
            function initMap() {
                var address = {lat: 55.697286, lng: 12.594880};
                var map = new google.maps.Map(document.getElementById('gmap'), {
                zoom: 18,
                center: address
                });
                var marker = new google.maps.Marker({
                position: address,
                map: map
                });
            }
            </script>
            <script async defer
                src="//maps.googleapis.com/maps/api/js?key=AIzaSyDAt0PjFnYWOtK5SXiu-Q0iclktdmTTRBo&callback=initMap">
            </script>
    </div>
</div>
```

### Preview

<div class="panels mission-address-block">
    <div class="panel">
        <p class="panel-info">
            <strong>Adresse</strong>
            <p>Dampfærgevej 10, 4. sal<br />2100 København Ø<br />Danmark</p>
        </p>
        <p class="panel-info">
            <strong>Kontaktinformasjon</strong>
            <p>Phone: +1(646)430-7510<br/>
            Fax: +1(646)430-7511<br/>
            Email: delun@mfa.no</p>
        </p>
    </div>
    <div class="panel" id="gmap">
        Map will render here
        <script>
            function initMap() {
                var address = {lat: 55.697286, lng: 12.594880};
                var map = new google.maps.Map(document.getElementById('gmap'), {
                zoom: 18,
                center: address
                });
                var marker = new google.maps.Marker({
                position: address,
                map: map
                });
            }
        </script>
        <script async defer
            src="//maps.googleapis.com/maps/api/js?key=AIzaSyDAt0PjFnYWOtK5SXiu-Q0iclktdmTTRBo&callback=initMap">
        </script>
    </div>
</div>
