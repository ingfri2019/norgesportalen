const pageSurvey = document.querySelector('[data-page-survey]');

if (pageSurvey) {
    let surveyId = pageSurvey.dataset.pageSurvey;
    let surveyApi = pageSurvey.dataset.pageSurveyApi;
    let str_legend = pageSurvey.dataset.textHiddenTitle;
    let str_question = pageSurvey.dataset.textQuestion;
    let arr_options = JSON.parse(pageSurvey.dataset.textOptions);
    let str_comment = pageSurvey.dataset.textComment;
    let str_send = pageSurvey.dataset.textSend;
    let str_thanks = pageSurvey.dataset.textThanks;
    let str_failed = pageSurvey.dataset.textFailed;
    let err_illegal = pageSurvey.dataset.textIllegalCharacters;

    // fn to set multiple attributes on an element
    function setAttributes(el, attrs) {
        for (var key in attrs) {
            el.setAttribute(key, attrs[key]);
        }
    }

    // fn to insert element after another element
    function insertAfter(el, referenceNode) {
        referenceNode.parentNode.insertBefore(el, referenceNode.nextSibling);
    }

    // fieldset
    let fieldset = document.createElement('fieldset');
    fieldset.setAttribute('data-page-survey-fieldset','');

    // legend
    let legend = document.createElement('legend');
    legend.className = 'visuallyhidden';
    legend.innerHTML = str_legend;

    // heading
    let heading = document.createElement('h2');
    heading.innerHTML = str_question;

    // options
    let optionsWrapper = document.createElement('div');
    optionsWrapper.className = 'options';

    // comments field
    let commentsWrapper = document.createElement('div');
    commentsWrapper.className = 'comments';
    commentsWrapper.setAttribute('data-page-survey-comments', '');

    // comments label
    let commentsLabel = document.createElement('label');
    commentsLabel.setAttribute('for', 'PageSurveyAnswerComment' + surveyId);
    commentsLabel.innerHTML = str_comment;
    let commentsField = document.createElement('textarea');
    setAttributes(commentsField, {
        'id': 'PageSurveyAnswerComment_' + surveyId,
        'name': 'PageSurveyAnswerComment_' + surveyId,
        'maxlength': '255',
        'cols': '24',
        'rows': '4',
        'placeholder': ''
    });

    // loop through options array
    for (let i in arr_options) {
        let index = i;
        let humanId = Number(index) + 1;

        // create option elements
        let option = document.createElement('input');
        setAttributes(option, {
            'type': 'radio',
            'name': 'pageSurveyQuestion_' + surveyId,
            'id': 'PageSurveyOption_' + surveyId + '_' + humanId,
            'value': arr_options[i].response,
            'data-text': arr_options[i].feedback
        });

        // assign click fn to option elements
        option.addEventListener('click', () => {
            commentsField.setAttribute('placeholder', arr_options[index].feedback);
            commentsWrapper.classList.add('shown');
            submitWrapper.classList.add('shown');
        });

        // option label
        let optionLabel = document.createElement('label');
        optionLabel.setAttribute('for', 'PageSurveyOption_' + surveyId + '_' + humanId);
        optionLabel.innerHTML = arr_options[i].response;

        let optionWrapper = document.createElement('div');
        optionWrapper.className = 'option';

        optionWrapper.appendChild(option);
        optionWrapper.appendChild(optionLabel);

        optionsWrapper.appendChild(optionWrapper);
    }

    // submit
    let submitWrapper = document.createElement('div');
    submitWrapper.className = 'submit';
    let submitButton = document.createElement('button');
    setAttributes(submitButton, {
        'type': 'submit',
        'id': 'PageSurveyAnswerSubmitButton_' + surveyId,
        'value': str_send
    })
    submitButton.innerHTML = str_send;
    submitButton.addEventListener('click', () => {
        let element = document.createElement('div');
        let comment = commentsField.value;
        if (comment && typeof comment === 'string') {
            // strip script/html tags
            comment = comment.replace(/<script[^>]*>([\S\s]*?)<\/script>/gmi, '');
            comment = comment.replace(/<\/?\w(?:[^"'>]|"[^"]*"|'[^']*')*>/gmi, '');
            comment = comment.replace(/<\/?\w(?:[^"'>]|"[^"]*"|'[^']*')*>/g, '');
            element.innerHTML = comment;
            comment = element.textContent;
            element.textContent = '';
        }
        else {
            comment = '';
        }
        let commentEncoded = encodeURI(comment);

        let pathname = document.location.href;
        let optionName = 'pageSurveyQuestion_' + surveyId;
        let optionSelectedId = Array.from(document.getElementsByName(optionName)).find(r => r.checked).id;
        let optionSelectedIndex = optionSelectedId.substring(optionSelectedId.length - 1, optionSelectedId.length);
        let optionSelectedText = Array.from(document.getElementsByName(optionName)).find(r => r.checked).value;

        // callback handlers
        let callBackFailed = function () {
            fieldset.style.display = 'none';
            failed.classList.add('shown');
        };

        let callBackSuccess = function () {
            fieldset.style.display = 'none';
            thanks.classList.add('shown');
            if (typeof(dataLayer) !== 'undefined') {
                dataLayer.push({
                    'feedback_text': comment,
                    'event': 'page_feedback_text'
                });
            }
        };

        // let pageSurveyInfo = { PageSurveyId: surveyId, Answer: answer }
        let pageSurveyInfo = 'PageSurveyId=' + surveyId + '&Option=' + optionSelectedIndex + '&OptionText=' + optionSelectedText + '&Comment=' + commentEncoded + '&Pathname=' + pathname;
        // console.log(pageSurveyInfo);
        post(pageSurveyInfo, callBackSuccess, callBackFailed);
        submitButton.setAttribute('disabled', 'disabled');
    });
    submitWrapper.appendChild(submitButton);

    fieldset.appendChild(legend);
    fieldset.appendChild(heading);
    fieldset.appendChild(optionsWrapper);
    fieldset.appendChild(commentsWrapper);
    fieldset.appendChild(submitWrapper);

    pageSurvey.appendChild(fieldset);

    let thanks = document.createElement('div');
    thanks.className = 'thankyounote';
    thanks.innerHTML = str_thanks;
    thanks.setAttribute('data-thank-you-note','');

    let failed = document.createElement('div');
    failed.className = 'failednote';
    failed.innerHTML = str_failed;
    failed.setAttribute('data-failed-note','');

    insertAfter(failed, fieldset);
    insertAfter(thanks, fieldset);

    // validate comments
    commentsField.addEventListener('keyup', function() {
        validateComment(commentsField, submitButton);
    });

    // error message
    let errorMessageWrapper = document.createElement('div');
    errorMessageWrapper.className = 'error-message';

    commentsWrapper.appendChild(commentsLabel);
    commentsWrapper.appendChild(commentsField);
    commentsWrapper.appendChild(errorMessageWrapper);

    // fn to validate comment
    const validateComment = function (textarea) {
        let commentsRegex = /^[a-zA-Z0-9-+æøåÆØÅČčdĐđÁáŊŋŠšŦŧŽž .,!?()"':;\r\n]+$/;
        let comment = textarea.value;
        let errorMessage = errorMessageWrapper;

        if (comment === '') {
            return true;
        }
        else {
            if (!commentsRegex.test(comment) || comment.length > 256) {
                errorMessage.innerHTML = err_illegal;
                errorMessage.classList.add('shown');
                submitButton.setAttribute('disabled', 'disabled');
                return false;
            }
            else {
                errorMessage.innerHTML = '';
                errorMessage.className = 'error-message';
                submitButton.removeAttribute('disabled');
                return true;
            }
        }
    };

    // fn to asynchronously post survey answer using
    function post(data, success, failed) {
        let xhr = new XMLHttpRequest();
        let url = surveyApi;
        xhr.open('POST', url, true);
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.send(data);
        xhr.onreadystatechange = function() {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    if (xhr.responseText === '"Success"') {
                        success();
                    }
                    else {
                        failed();
                    }
                }
                else {
                    failed();
                }
            }
        };
        return xhr;
    };

    // fn post example request:
    //   post('http://foo.bar/', 'p1=1&p2=Hello+World', function(data){ console.log(data); });
    //   post('p1=1&p2=Hello+World', function(data){ console.log(data); });

    // fn post example request with data object:
    //   post('http://foo.bar/', { p1: 1, p2: 'Hello World' }, function(data){ console.log(data); });
    //   post({ p1: 1, p2: 'Hello World' }, function(data){ console.log(data); });
}