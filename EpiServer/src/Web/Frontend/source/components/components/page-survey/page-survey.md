<div class="styleguide-container">
  <div class="page-survey-container">
    <div class="page-survey" 
      data-page-survey="123" 
      data-page-survey-api="https://dev.regjeringen.no/api/survey/SubmitPageSurveyAnswer" 
      data-text-hidden-title="Tilbakemeldingsskjema" 
      data-text-question="Fant du det du lette etter?" 
      data-text-options='[{"response": "Ja", "feedback": "Så bra! Hva lette du etter? Din tilbakemelding hjelper oss å lage bedre nettsider."},{"response": "Nei", "feedback": "Har du forslag til forbedringer? Din tilbakemelding hjelper oss å lage bedre nettsider."}]' 
      data-text-comment="Kommentar" 
      data-text-illegal-characters="Kommentaren inneholder ulovlige tegn! Prøv å fjerne spesialtegn." 
      data-text-send="Send" 
      data-text-thanks="<h2>Tusen takk for ditt svar!</h2><p>Alle tilbakemeldinger blir lest, men vi kan ikke svare deg.</p>">
    </div>
  </div>
</div>

```html
<div class="page-survey-container">
  <div class="page-survey" 
    data-page-survey="123" 
    data-page-survey-api="https://dev.regjeringen.no/api/survey/SubmitPageSurveyAnswer" 
    data-text-hidden-title="Tilbakemeldingsskjema" 
    data-text-question="Fant du det du lette etter?" 
    data-text-options='[{"response": "Ja", "feedback": "Så bra! Hva lette du etter? Din tilbakemelding hjelper oss å lage bedre nettsider."},{"response": "Nei", "feedback": "Har du forslag til forbedringer? Din tilbakemelding hjelper oss å lage bedre nettsider."}]' 
    data-text-comment="Kommentar" 
    data-text-illegal-characters="Kommentaren inneholder ulovlige tegn! Prøv å fjerne spesialtegn." 
    data-text-send="Send" 
    data-text-thanks="<h2>Tusen takk for ditt svar!</h2><p>Alle tilbakemeldinger blir lest, men vi kan ikke svare deg.</p>">
  </div>
</div>
```
