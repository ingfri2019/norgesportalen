## Large Priority Panels
<div class="styleguide-container">
  <div class="priority-panel">
    <div class="panels">
      <div class="panel-large panel">
        <a href="/en/croatia/values-priorities/arts-culture/" style="background-image: url('http://placehold.it/400x350');" class="inner">
          <div class="overlay">
            <h2>Arts and culture</h2>
            <p>Norway has a vibrant arts and culture sector that has deep historical roots. Today, as in the past, many Norwegian artists and other cultural professionals find inspiration beyond Norway’s national borders.</p>
          </div>
        </a>
      </div>
      <div class="panel-large panel">
        <a href="/en/croatia/values-priorities/high-north/" style="background-image: url('http://placehold.it/400x350');" class="inner">
          <div class="overlay">
            <h2>The High North</h2>
            <p>The Arctic is changing. Climate change and melting sea ice are creating challenges but also opportunities in the north. International cooperation is more important than ever.</p>
          </div>
        </a>
      </div>
    </div>
  </div>
</div>

```html
<div class="priority-panel">
  <div class="panels">
    <div class="panel-large panel">
      <a href="/en/croatia/values-priorities/arts-culture/" style="background-image: url('http://placehold.it/400x350');" class="inner">
        <div class="overlay">
          <h2>Arts and culture</h2>
          <p>Norway has a vibrant arts and culture sector that has deep historical roots. Today, as in the past, many Norwegian artists and other cultural professionals find inspiration beyond Norway’s national borders.</p>
        </div>
      </a>
    </div>
    <div class="panel-large panel">
      <a href="/en/croatia/values-priorities/high-north/" style="background-image: url('http://placehold.it/400x350');" class="inner">
        <div class="overlay">
          <h2>The High North</h2>
          <p>The Arctic is changing. Climate change and melting sea ice are creating challenges but also opportunities in the north. International cooperation is more important than ever.</p>
        </div>
      </a>
    </div>
  </div>
</div>
```

## Small Priority Panels
<div class="styleguide-container">
  <div class="priority-panel">
    <div class="panels">
      <div class="panel-medium panel">
        <a href="/en/croatia/values-priorities/energy-marine-res/" style="background-image: url('http://placehold.it/300x250');" class="inner">
          <div class="overlay">
            <h2>Energy and marine resources</h2>
          </div>
        </a>
      </div>
      <div class="panel-medium panel">
        <a href="/en/croatia/values-priorities/peace-stability-sec/" style="background-image: url('http://placehold.it/300x250');" class="inner">
          <div class="overlay">
            <h2>Peace, stability and security</h2>
          </div>
        </a>
      </div>
      <div class="panel-medium panel">
        <a href="/en/croatia/values-priorities/climate-env/" style="background-image: url('http://placehold.it/300x250');" class="inner">
          <div class="overlay">
            <h2>Climate change and the environment</h2>
          </div>
        </a>
      </div>
    </div>
  </div>
</div>

```html
<div class="priority-panel">
  <div class="panels">
    <div class="panel-medium panel">
      <a href="/en/croatia/values-priorities/energy-marine-res/" style="background-image: url('http://placehold.it/300x250');" class="inner">
        <div class="overlay">
          <h2>Energy and marine resources</h2>
        </div>
      </a>
    </div>
    <div class="panel-medium panel">
      <a href="/en/croatia/values-priorities/peace-stability-sec/" style="background-image: url('http://placehold.it/300x250');" class="inner">
        <div class="overlay">
          <h2>Peace, stability and security</h2>
        </div>
      </a>
    </div>
    <div class="panel-medium panel">
      <a href="/en/croatia/values-priorities/climate-env/" style="background-image: url('http://placehold.it/300x250');" class="inner">
        <div class="overlay">
          <h2>Climate change and the environment</h2>
        </div>
      </a>
    </div>
  </div>
</div>
```
