﻿## Description
The Subscription Block should be used with a number of settings.

<div class="styleguide-container">
<div class="subscription-block">
    <h2>Subscribe to our weekly newsletter</h2>
    <span class="information">
      Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
    </span>
    <form class="newsletter-subscription" data-subscribeid="" data-thankyou="You have successfully been added to our newsletter list" data-error="Error" data-tryagain="Try again">
        <div class="input-group">
            <input type="email" name="email" class="subscribe-email" placeholder="Your email">
            <div class="input-group-btn">
                <input type="submit" value="Send">
            </div>
        </div>
    </form>
    <div class="response hidden">
    </div>
</div>
</div>

```html
<div class="subscription-block">
  <h2>Subscribe to our weekly newsletter</h2>
  <span class="information">...</span>
    <form class="newsletter-subscription" data-subscribeid="" data-thankyou="You have successfully been added to our newsletter list" data-error="Error" data-tryagain="Try again">
        <div class="input-group">
            <input type="email" name="email" class="subscribe-email" placeholder="Your email">
            <div class="input-group-btn">
                <input type="submit" value="Send">
            </div>
        </div>
    </form>
    <div class="response hidden">...</div>
</div>
```
