﻿const subscriptionBlocks = document.querySelectorAll('.subscription-block');

function subscribe(email, id, subscriptionBlock, thankyou, error, tryagain) {
    let xhr = new XMLHttpRequest();
    xhr.open('POST', '/api/newsletter/Subscribe?email=' + email + '&list=' + id);
    xhr.send(null);

    xhr.onreadystatechange = function () {
        let DONE = 4;
        if (xhr.readyState === DONE) {
            setResponse(xhr.status, subscriptionBlock, thankyou, error, tryagain);
        }
    };
}

function reset(subscriptionBlock) {
    let information = subscriptionBlock.querySelector('.information');
    let response = subscriptionBlock.querySelector('.response');
    let form = subscriptionBlock.querySelector('.newsletter-subscription');

    form.querySelector('.input-group-btn input').removeAttribute('disabled');
    form.classList.remove('hidden');
    information.classList.remove('hidden');
    response.classList.add('hidden');
}

function setResponse(status, subscriptionBlock, thankyou, error, tryagain) {
    let OK = 200;
    let information = subscriptionBlock.querySelector('.information');
    let response = subscriptionBlock.querySelector('.response');
    let form = subscriptionBlock.querySelector('.newsletter-subscription');

    form.classList.add('hidden');
    information.classList.add('hidden');
    response.classList.remove('hidden');

    if (status === OK) {
        response.innerHTML = '<p>' + thankyou + '</p>';
    } else {
        response.innerHTML = '<p>' + error + ': ' + status + '</p><button class="btn">' + tryagain + '</button>';
    }

    response.querySelector('.btn').addEventListener('click', () => {
        reset(subscriptionBlock);
    });
}

if (subscriptionBlocks.length) {
    for(let subscriptionBlock of subscriptionBlocks) {
        let form = subscriptionBlock.querySelector('.newsletter-subscription');
        form.addEventListener('submit', (event) => {
            event.preventDefault();
            let email = form.querySelector('.subscribe-email').value;
            let id = form.dataset.subscribeid;
            let thankyou = form.dataset.thankyou;
            let error = form.dataset.error;
            let tryagain = form.dataset.tryagain;
            form.querySelector('.input-group-btn input').setAttribute('disabled', true);
            subscribe(email, id, subscriptionBlock, thankyou, error, tryagain);
        });
    }
}
