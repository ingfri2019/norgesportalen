## Search

### Large search container
<div class="styleguide-container">
<div class="search-container search--large">
  <form role="search" aria-describedby="search-description">
      <p id="search-description" class="visually-hidden">The search updates results while you type</p>
      <input id="page-id" type="hidden" value="6">
      <label for="search-field" class="visually-hidden">Search</label>
      <input id="search-field" aria-label="Search input" type="text" placeholder="Type here">
      <button type="submit" title="Search button" aria-label="Submit"><span role="presentation"></span></button>
  </form>
</div>
</div>

```html
<div class="search-container search--large">
  <form role="search" aria-describedby="search-description">
      <p id="search-description" class="visually-hidden">The search updates results while you type</p>
      <input id="page-id" type="hidden" value="6">
      <label for="search-field" class="visually-hidden">Search</label>
      <input id="search-field" aria-label="Search input" type="text" placeholder="Type here">
      <button type="submit" title="Search button" aria-label="Submit"><span role="presentation"></span></button>
  </form>
</div>
```

### Small search container
<div class="styleguide-container">
<div class="search-container">
  <form action="/en/laos/search-page/" method="get">
    <div id="search-area" role="search">
      <label for="search-field" class="visually-hidden">Search</label>
      <input id="search-field" title="Enter your search criteria" placeholder="Search here..." type="text" name="q">
      <button title="Search button" type="submit" class="searchButton" id="SearchButton" aria-label="Submit"><span role="presentation"></span></button>
    </div>
  </form>
</div>
</div>

```html
<div class="search-container">
  <form action="/en/laos/search-page/" method="get">
    <div id="search-area" role="search">
      <label for="search-field" class="visually-hidden">Search</label>
      <input id="search-field" title="Enter your search criteria" placeholder="Search here..." type="text" name="q">
      <button title="Search button" type="submit" class="searchButton" id="SearchButton" aria-label="Submit"><span role="presentation"></span></button>
    </div>
  </form>
</div>
```
