## Description


## Usage

```
<div class="hero">
    <div class="hero__poster">
        <img class="ratio__content hero__img" src="https://placeimg.com/1440/720/any" alt="Hero Image" loading="auto">
    </div>
    <h1 class="hero__title">Hero Title</h1>
</div>
```

### Preview

<div class="hero">
    <div class="hero__poster">
        <img class="ratio__content hero__img" src="https://placeimg.com/1440/720/any" alt="Hero Image" loading="auto">
    </div>
    <h1 class="hero__title">Hero Title</h1>
</div>
