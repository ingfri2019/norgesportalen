## Description


## Usage

```html
<div class="event-details">
    <div class="event-details__content">
        <dl class="event-details__list">
            <dt>Where</dt>
            <dd>
                <address>
                    The National Gallery of Art<br />
                    Adressline 123,<br />
                    890 New York
                </address>
            </dd>
            <dt>When</dt>
            <dd>
                <time datetime="2019-04-16T09:00:00">16. Apr 2017, 09:00</time> - 
                <time datetime="2019-04-16T18:00:00">18:00</time>
            </dd>
            <dt>Ticket</dt>
            <dd>Entrance is free of charge</dd>
        </dl>
        <a href="#" class="btn">More info</a>
    </div>
</div>
```

### Preview

<div class="event-details">
    <div class="event-details__content">
        <dl class="event-details__list">
            <dt>Where</dt>
            <dd>
                <address>
                    The National Gallery of Art<br />
                    Adressline 123,<br />
                    890 New York
                </address>
            </dd>
            <dt>When</dt>
            <dd>
                <time datetime="2019-04-16T09:00:00">16. Apr 2017, 09:00</time> - 
                <time datetime="2019-04-16T18:00:00">18:00</time>
            </dd>
            <dt>Ticket</dt>
            <dd>Entrance is free of charge</dd>
        </dl>
        <a href="#" class="btn">More info</a>
    </div>
</div>

<div class="event-details event-details--maps">
    <div class="event-details__content">
        <dl class="event-details__list">
            <dt>Where</dt>
            <dd>
                <address>
                    The National Gallery of Art<br />
                    Adressline 123,<br />
                    890 New York
                </address>
            </dd>
            <dt>When</dt>
            <dd>
                <time datetime="2019-04-16T09:00:00">16. Apr 2017, 09:00</time> - 
                <time datetime="2019-04-16T18:00:00">18:00</time>
            </dd>
            <dt>Ticket</dt>
            <dd>Entrance is free of charge</dd>
        </dl>
        <a href="#" class="btn">More info</a>
    </div>
    <div class="event-details__map">
        <iframe class="ratio__content" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d64041.69142983672!2d10.715077470885314!3d59.89392248120755!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46416e61f267f039%3A0x7e92605fd3231e9a!2sOslo!5e0!3m2!1sen!2sno!4v1575287109855!5m2!1sen!2sno" width="400" height="300" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
    </div>
</div>