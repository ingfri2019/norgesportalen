## Description


## Usage

```html
<div class="exclamation">
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vitae egestas est. Donec non varius eros. Suspendisse vehicula tristique lorem ac aliquet. Nulla ac velit in eros volutpat pulvinar quis quis dolor.</p>
</div>
```

### Preview

<div class="exclamation">
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vitae egestas est. Donec non varius eros. Suspendisse vehicula tristique lorem ac aliquet. Nulla ac velit in eros volutpat pulvinar quis quis dolor.</p>
</div>
