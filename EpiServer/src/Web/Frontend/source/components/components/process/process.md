## Process Steps
<div class="styleguide-container">
<div class="container-large">
  <div class="process">
    <div class="process-steps" role="tablist">
      <div class="process-step" id="before-apply">
        <a href="" class="process-step-header" tabindex="0" role="tab" aria-expanded="true">
          <span class="step-number">1</span>
          <h2>Process Step</h2>
        </a>
        <div class="process-step-content" role="tabpanel" aria-hidden="true">
          <p>Process content</p>
        </div>
      </div>
    </div>
  </div>
</div>
</div>

```html
<div class="container-large">
  <div class="process">
    <div class="process-steps" role="tablist">
      <div class="process-step" id="{UNIQUE-ID}">
        <a href="#{UNIQUE-ID}" class="process-step-header" tabindex="0" role="tab" aria-expanded="true">
          <span class="step-number">1</span>
          <h2>Process Step</h2>
        </a>
        <div class="process-step-content" role="tabpanel" aria-hidden="true">
          <p>Process content</p>
        </div>
      </div>
    </div>
  </div>
</div>
```
