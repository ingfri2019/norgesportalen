﻿import SCROLLTO from '../../helpers/scroll-to/scroll-to';

const processItems = document.querySelectorAll('.process-step');
const header = document.querySelector('body > header');

for(let processItem of processItems) {
    let processItemHeader = processItem.querySelector('.process-step-header');
    let processItemContent = processItem.querySelector('.process-step-content');
    processItemHeader.addEventListener('click', (e) => {
        e.preventDefault();
        history.replaceState({}, '', '#' + processItem.getAttribute('id'));
        processItem.classList.toggle('open');
        if(processItem.classList.contains('open')) {
            SCROLLTO.y((processItem.offsetTop - (header.offsetHeight + 15)), 500, 'easeInOutQuint')
            processItemHeader.setAttribute('aria-expanded', true);
            processItemContent.setAttribute('aria-hidden', false);
        } else {
            processItemHeader.setAttribute('aria-expanded', false);
            processItemContent.setAttribute('aria-hidden', true);
        }
    });
}
