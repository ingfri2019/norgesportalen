## Description
Usage for People Panel is preferred, but not restricted, to the Accrodion.

## Examples

### Ambassador
<div class="styleguide-container">
<div class="people-panel">
  <div class="panels">
    <div class="panel ambassador">
      <div class="inner">
        <div class="portrait">
          <img src="http://placehold.it/75x85">
        </div>
        <div class="information">
          <h2>Firstname Lastname</h2>
          <p class="ingress">Ambassador</p>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua..</p>
        </div>
      </div>
    </div>
  </div>
</div>
</div>

```html
<div class="people-panel">
  <div class="panels">
    <div class="panel ambassador">
      <div class="inner">
        <div class="portrait">
          <img>
        </div>
        <div class="information">
          <h2>...</h2>
          <p>...</p>
        </div>
      </div>
    </div>
  </div>
</div>
```

### Staff

<div class="styleguide-container">
<div class="people-panel">
  <div class="panels">
    <div class="panel">
      <div class="inner">
        <h3>Firstname Lastname</h3>
        <p>Title</p>
      </div>
    </div>
    <div class="panel">
      <div class="inner">
        <h3>Firstname Lastname</h3>
        <p>Title</p>
      </div>
    </div>
  </div>
</div>
</div>

```html
<div class="people-panel">
  <div class="panels">
    <div class="panel">
      <div class="inner">
        <h3>...</h3>
        <p>...</p>
      </div>
    </div>
  </div>
</div>
```
