## Description


### Example
<div class="styleguide-container">
<div class="pagination">
<ul>
  <li class="arrow-prev"><a href="/en/philippinene/news-events-and-statements/?page=1">#</a></li>
  <li class="current"><a href="#">1</a></li>
  <li><a href="/en/philippinene/news-events-and-statements/?page=2">2</a></li>
  <li class="arrow-next"><a href="/en/philippinene/news-events-and-statements/?page=2">#</a></li>
</ul>
</div>
</div>
```html
<div class="pagination">
  <ul>
    <li class="arrow-prev"><a href="?page=1">#</a></li>
    <li class="current"><a href="?page=1">1</a></li>
    <li><a href="?page=2">2</a></li>
    <li class="arrow-next"><a href="?page=2">#</a></li>
  </ul>
</div>
```
