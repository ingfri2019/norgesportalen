## Icons
<div class="styleguide-container">
  <span class="socialmedia-icon facebook">Facebook</span>
  <span class="socialmedia-icon twitter">Twitter</span>
  <span class="socialmedia-icon vimeo">Vimeo</span>
  <span class="socialmedia-icon youtube">YouTube</span>
  <span class="socialmedia-icon instagram">Instagram</span>
  <span class="socialmedia-icon linkedin">LinkedIn</span>
  <span class="socialmedia-icon vkontakte">VKontakte</span>
  <span class="socialmedia-icon weibo">Weibo</span>
  <span class="socialmedia-icon snapchat">Snapchat</span>
  <span class="socialmedia-icon flickr">Flickr</span>
</div>

```html
<span class="socialmedia-icon facebook">Facebook</span>
<span class="socialmedia-icon twitter">Twitter</span>
<span class="socialmedia-icon vimeo">Vimeo</span>
<span class="socialmedia-icon youtube">YouTube</span>
<span class="socialmedia-icon instagram">Instagram</span>
<span class="socialmedia-icon linkedin">LinkedIn</span>
<span class="socialmedia-icon vkontakte">VKontakte</span>
<span class="socialmedia-icon weibo">Weibo</span>
<span class="socialmedia-icon snapchat">Snapchat</span>
<span class="socialmedia-icon flickr">Flickr</span>
```

## Panel
<section class="social-panel padded--xsmall">
  <div class="container-large">
    <div class="panels">
      <div class="panel">
        <a href="#" class="inner">
          <span class="socialmedia-icon facebook"></span>
          The Embassy on Facebook
        </a>
      </div>
      <div class="panel">
        <a href="#" class="inner">
          <span class="socialmedia-icon twitter"></span>
          The embassy on Twitter
        </a>
      </div>
      <div class="panel">
        <a href="#" class="inner">
          <span class="socialmedia-icon linkedin"></span>
          The embassy on LinkedIn
        </a>
      </div>
      <div class="panel">
        <a href="#" class="inner">
          <img src="/Frontend/dist-web/Static/images/lenkePil.svg" class="icon">
          <span>See all social media channels</span>
        </a>
      </div>
    </div>
  </div>
</section>

```html 
<section class="social-panel padded--xsmall">
  <div class="container-large">
    <div class="panels">
      <div class="panel">
        <a href="#" class="inner">
          <span class="socialmedia-icon facebook"></span>
          The Embassy on Facebook
        </a>
      </div>
      <div class="panel">
        <a href="#" class="inner">
          <span class="socialmedia-icon twitter"></span>
          The embassy on Twitter
        </a>
      </div>
      <div class="panel">
        <a href="#" class="inner">
          <span class="socialmedia-icon linkedin"></span>
          The embassy on LinkedIn
        </a>
      </div>
      <div class="panel">
        <a href="#" class="inner">
          <img src="/Frontend/dist-web/Static/images/lenkePil.svg" class="icon">
          <span>See all social media channels</span>
        </a>
      </div>
    </div>
  </div>
</section>
```

## Footer
When social media icons are used in the footer, they get an override class using the white logo instead.
