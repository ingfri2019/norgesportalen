## Description


## Usage

```
<section class="filter-block padded--small">
  <div class="container">
    <div class="text-center">
      <ul class="filter-items" role="group">
        <li class="filter-item" role="checkbox" aria-describedby="{UniqueID1}">
          <a class="checkbox unchecked" href="{Link1}">{Link1}</a>
          <span class="visually-hidden" id="{UniqueID1}">{DescriptiveText1}</span>
        </li>
        <li class="filter-item" role="checkbox" aria-describedby="{UniqueID2}">
          <a class="checkbox unchecked" href="{Link2}">{Link2}</a>
          <span class="visually-hidden" id="{UniqueID2}">{DescriptiveText2}</span>
        </li>
      </ul>
    </div>
  </div>
</section>
```

### Preview

<section class="filter-block padded--small">
  <div class="container">
    <div class="text-center">
      <ul class="filter-items" role="group">
        <li class="filter-item" role="checkbox" aria-describedby="{UniqueID1}">
          <a class="checkbox unchecked" href="{Link1}">{Link1}</a>
          <span class="visually-hidden" id="{UniqueID1}">{DescriptiveText1}</span>
        </li>
        <li class="filter-item" role="checkbox" aria-describedby="{UniqueID2}">
          <a class="checkbox unchecked" href="{Link2}">{Link2}</a>
          <span class="visually-hidden" id="{UniqueID2}">{DescriptiveText2}</span>
        </li>
      </ul>
    </div>
  </div>
</section>
