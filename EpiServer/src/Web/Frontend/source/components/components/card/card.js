const cards = document.querySelectorAll('.card');



Array.prototype.forEach.call(cards, function(card) {
    card.style.cursor = 'pointer';

    let down, up, link = card.querySelectorAll('.js-card__title')[0];
    card.onmousedown = function() { down = +new Date() };
    card.onmouseup = function() {
        up = +new Date();
        if ((up - down) < 200) {
            link.click();
        }
    }

    link.addEventListener('focus', function() {
        console.log(event);
        card.classList.add("has-focus");
    })

    link.addEventListener('blur', function() {
        card.classList.remove("has-focus");
    })
});