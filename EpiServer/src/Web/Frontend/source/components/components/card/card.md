## Description

## Usage

```html
<div class="card">
  <p>Card</p>
</div>
```

### Preview

<article class="card">
  <div class="card__img">
    <img src="http://placehold.it/360x160" alt="{Alt1}" />
  </div>
  <time datetime="" class="card__date">Apr <strong>23</strong></time>
  <h2 class="card__title">Title</h2>
  <p class="card__excerpt">
    Lorem ipsum, dolor sit amet consectetur adipisicing elit. Cupiditate officia
    rerum eius, ullam eligendi neque nostrum ex distinctio perspiciatis rem
    voluptate? Maiores mollitia distinctio quae. Enim dicta exercitationem nemo
    pariatur.
  </p>
</article>
