'use strict';

const gulp = require('gulp'),
    autoprefixer = require('gulp-autoprefixer'),
    browserify = require('browserify'),
    browserSync = require('browser-sync').create(),
    buffer = require('vinyl-buffer'),
    csso = require('gulp-csso'),
    del = require('del'),
    eslint = require('gulp-eslint'),
    FS = require('fs'),
    markdown = require('gulp-markdown'),
    PATH = require('path'),
    scss = require('gulp-sass'),
    source = require('vinyl-source-stream'),
    sourcemaps = require('gulp-sourcemaps'),
    stylelint = require('gulp-stylelint'),
    uglify = require('gulp-uglify'),
    wrap = require('gulp-wrap');

// ------------------------------------------------------------------------------
// Gulp Task: config
// ------------------------------------------------------------------------------

const config = {
    paths: {
        src: './source/',
        destWeb: './dist-web/',
        fonts: './source/Static/fonts/**/*.{eot,svg,ttf,woff,woff2}',
        images: ['./source/Static/images/**/*.{jpg,jpeg,png,gif,svg}'],
        favicons: ['./source/Static/favicons/*.{jpg,jpeg,png,gif,svg}'],
        scripts: ['./**/*.js', '!gulpfile*'],
        scss: ['./source/Static/sass/'],
        build: ['./dist-web/'],
        dmbuild: './dist-web/designmanual/',
        dm: [
            './source/components/areas/**/*.md',
            './source/components/components/**/*.md',
            './source/Static/fonts/**/*.md'
        ]
    }
};

// ------------------------------------------------------------------------------
// Gulp Task: cleanWeb
// Description: Deletes /dist-web
// ------------------------------------------------------------------------------

const cleanWeb = done => {
    return del(config.paths.destWeb, done());
};

exports.cleanWeb = cleanWeb;

// ------------------------------------------------------------------------------
// Gulp Task: html
// Description: Copies .html files from ./source/html to /dist-web
// ------------------------------------------------------------------------------

const html = () =>
    gulp.src(config.paths.src + '**/*.html').pipe(gulp.dest(config.paths.destWeb + '/'));

exports.html = html;

// ------------------------------------------------------------------------------
// Gulp Task: lintStyles
// Description: Uses stylelint to lint all .scss files in /source
// ------------------------------------------------------------------------------

const lintStyles = () =>
    gulp
        .src([
            config.paths.src + '**/*.scss',
            '!' + config.paths.src + '**/tinymce.scss'
        ])
        .pipe(
            stylelint({
                failAfterError: false,
                reportOutputDir: 'reports/lint',
                reporters: [{ formatter: 'verbose', console: true }],
                debug: true
            })
        );

exports.lintStyles = lintStyles;

// ------------------------------------------------------------------------------
// Gulp Task: styles
// Description: Compiles .scss files in /source to .css in /dist-bak
// -----------------------------------------------------------------------------

const styles = () =>
    gulp
        .src(config.paths.scss + '**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(scss())
        .on('error', function onError(err) {
            console.log(err);
            this.emit('end');
        })
        .pipe(sourcemaps.write({ sourceRoot: '.' }))
        .pipe(sourcemaps.init({ loadMaps: true }))
        .pipe(autoprefixer({ grid: true }))
        .pipe(csso({
            restructure: false
        })) 
        .pipe(sourcemaps.write('.', { sourceRoot: '.' }))
        .pipe(gulp.dest(config.paths.destWeb + 'Static/css'));

exports.styles = styles;

// ------------------------------------------------------------------------------
// Gulp Task: lintScripts
// Description: Uses eslint to lint all .js files in /source
// ------------------------------------------------------------------------------

const lintScripts = () =>
    gulp
        .src(config.paths.src + '**/*.js')
        .pipe(eslint())
        .pipe(eslint.formatEach())
        .pipe(
            eslint.results(results => {
                // Called once for all ESLint results.
                console.log(`Total Results: ${results.length}`);
                console.log(`Total Warnings: ${results.warningCount}`);
                console.log(`Total Errors: ${results.errorCount}`);
            })
        );

exports.lintScripts = lintScripts;

// ------------------------------------------------------------------------------
// Gulp Task: Browserify
// Description: Bundles JavaScript
// ------------------------------------------------------------------------------

const bundleJs = () => {
    return (
        browserify({
            entries: [config.paths.src + 'Static/js/main.js'],
            extensions: ['.js'],
            debug: true
        })
            .transform('babelify', { presets: ['@babel/preset-env'] }, 'uglifyify')
            .bundle()
            .pipe(source('main.js'))
            .pipe(buffer())
            .pipe(sourcemaps.init())
            .pipe(uglify())
            .pipe(sourcemaps.write('./'))
            .pipe(gulp.dest(config.paths.destWeb + 'Static/js'))
    );
};

exports.bundleJs = bundleJs;

// ------------------------------------------------------------------------------
// Gulp Task: copyImages
// Description: Copies images to /dist-web
// ------------------------------------------------------------------------------

const copyImages = () =>
    gulp
        .src(config.paths.src + 'Static/images/**/*.{jpg,jpeg,png,gif,svg}')        
        .pipe(gulp.dest(config.paths.destWeb + 'Static/images'));

exports.copyImages = copyImages;

// ------------------------------------------------------------------------------
// Gulp Task: copyFavicons
// Description: Copies favicons to /dist-web
// ------------------------------------------------------------------------------

const copyFavicons = () =>
    gulp
        .src(config.paths.src + 'Static/favicons/*.{jpg,jpeg,png,gif,svg}')        
        .pipe(gulp.dest(config.paths.destWeb + 'favicons'));

exports.copyFavicons = copyFavicons;

// ------------------------------------------------------------------------------
// Gulp Task: buildImages
// Description: Combinator task for images and favicons
// ------------------------------------------------------------------------------

const buildImages = gulp.parallel(copyImages, copyFavicons);

exports.buildImages = buildImages;

// ------------------------------------------------------------------------------
// Gulp Task: fonts
// Description: Copies fonts for EpiServer
// ------------------------------------------------------------------------------

const fonts = () =>
    gulp
        .src(config.paths.fonts)
        .pipe(gulp.dest(config.paths.destWeb + 'Static/fonts'));

exports.fonts = fonts;

// ------------------------------------------------------------------------------
// Gulp Task: fontsDM
// Description: Copies fonts for Designmanual
// ------------------------------------------------------------------------------

const fontsDM = () =>
    gulp
        .src(config.paths.fonts)
        .pipe(gulp.dest(config.paths.destWeb + 'designmanual/fonts'));

exports.fontsDM = fontsDM;

// ------------------------------------------------------------------------------
// Gulp Task: testing
// Description: Seems to check if something is a file or directory. Used to build DM
// ------------------------------------------------------------------------------

const testing = (path, extensions) => {
    var name = PATH.basename(path);
    var item = { path, name };
    var stats;

    try {
        stats = FS.statSync(path);
    } catch (e) {
        return null;
    }

    if (stats.isFile()) {
        var ext = PATH.extname(path).toLowerCase();
        if (
            (extensions &&
                extensions.length &&
                extensions.indexOf(ext) === -1) ||
            name.charAt(0) === '.'
        ) {
            return null;
        }
        item.extension = ext;
        item.isFile = true;
    } else if (stats.isDirectory()) {
        try {
            item.isFile = false;
            item.children = FS.readdirSync(path)
                .map(child => testing(PATH.join(path, child), extensions))
                .filter(e => !!e);
        } catch (ex) {
            if (ex.code === 'EACCES') {
                return null;
            }
        }
    } else {
        return null;
    }
    return item;
};

exports.testing = testing;

// ------------------------------------------------------------------------------
// Gulp Task: buildDocs
// Description: Builds the designmanual
// ------------------------------------------------------------------------------

let collection = [];

const buildDocs = async () => {
    await Promise.all([
        new Promise(function (resolve, reject) {
            gulp.src(['./source/designmanual/main.scss'])
                .pipe(scss())
                .pipe(autoprefixer({ grid: true }))
                .pipe(
                    csso({
                        restructure: false
                    })
                )
                .pipe(gulp.dest(config.paths.dmbuild + '/css'))
                .on('end', resolve);
        }),
        new Promise(function (resolve, reject) {
            gulp.src([
                './source/designmanual/index.html',
                './source/designmanual/404.html'
            ])
                .pipe(gulp.dest(config.paths.dmbuild))
                .on('end', resolve);
        }),
        new Promise(function (resolve, reject) {
            gulp.src(['./source/components/**/*.md', '!node_modules/**'])
                .pipe(markdown({
                    langPrefix: 'hljs ',
                    highlight: function (code_1) {
                        return require('highlight.js').highlightAuto(code_1)
                            .value;
                    }
                }))
                .pipe(wrap({ src: './source/designmanual/template.tpl' }))
                .pipe(gulp.dest(config.paths.dmbuild + '/src'))
                .on('end', resolve);
        })
    ]);
    collection.push(testing(config.paths.dmbuild + '/src/'));
    FS.writeFileSync(config.paths.dmbuild + '/collection.json', JSON.stringify(collection));
};

exports.buildDocs = buildDocs;

// ------------------------------------------------------------------------------
// Gulp Task: watch
// Description: Watches for changes and reloads browsersync
// ------------------------------------------------------------------------------

const watch = done => {
    // gulp.watch(['./source/**/*.js'], gulp.series(lintScripts, browserify));
    gulp.watch(['./source/**/*.js'], gulp.series(bundleJs));

    gulp.watch(['./source/**/*.scss'], styles);

    gulp.watch(
        ['./source/designmanual/*.*', '**/*.md'],
        gulp.series(buildDocs, styles)
    );

    gulp.watch([
        config.paths.dest + '**/*.css',
        config.paths.dest + '**/*.html',
        config.paths.dest + '**/*.js'
    ]).on('change', browserSync.reload);

    done();
};

exports.watch = watch;

// ------------------------------------------------------------------------------
// Gulp Task: server
// Description: Starts browserSync web server for local development
// ------------------------------------------------------------------------------

const server = done => {
    browserSync.init({
        server: {
            baseDir: config.paths.destWeb
        },
        ui: false,
        open: false
    });

    done();
};

exports.server = server;

// ------------------------------------------------------------------------------
// Gulp Task: build
// Description: Builds everything to /dist-web
// ------------------------------------------------------------------------------

const build = gulp.parallel(
    styles,
    bundleJs,
    buildImages,
    buildDocs,
    fonts,
    fontsDM,
    html
);

exports.build = build;

// ------------------------------------------------------------------------------
// Gulp Task: buildWeb
// Description: Builds /dist-web files for EpiServer, but not /dist-web/designmanual
// ------------------------------------------------------------------------------

const buildWeb = gulp.parallel(buildImages, styles, fonts, bundleJs);

exports.buildWeb = buildWeb;

// ------------------------------------------------------------------------------
// Gulp Task: dev
// Description: Builds /dist-web, watches for changes and serves /dist-web
// ------------------------------------------------------------------------------

const dev = gulp.series(build, watch, server);

exports.dev = dev;

// ------------------------------------------------------------------------------
// Gulp Task: default
// Description: The default task when just running `gulp`
// ------------------------------------------------------------------------------

exports.default = dev;