# Norway.no Frontend 

## Developing & Install docs

For legacy reasons, there is a filed called install-doc.md located in source/components
This file is also used to build install-doc.html which is used for the designmanual.

- Run `npm install` to install dependencies
- Run `npm start` for developing (starts server)
- Run `npm run build` for production build

All source code exists in the `./source` directory.

## File structure

source/html contains all html code except index.html which is directly inside source.
source/components/ contains all 'components'. A component consists of a .scss file and an .md file. This .md filed is used to generate a .html file used in the design guide.

## Tracked Files

Only the source files should be tracked. This means that you should never commit or push built files to the repository (the files inside `./dist-web`). Each developer will build the project on their own machines while developing, and the build server will build the project for deployment.
