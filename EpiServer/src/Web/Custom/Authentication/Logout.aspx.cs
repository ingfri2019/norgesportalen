﻿using System;
using System.Web.Security;
using System.IdentityModel;
using System.IdentityModel.Services;

namespace Mfa.Norgesportalen.Site.Custom.Templates.Authentication
{
    public partial class Logout : System.Web.UI.Page
    {
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            FormsAuthentication.SignOut();
            Session.Abandon();
            SessionAuthenticationModule sessionAuthenticationModule = FederatedAuthentication.SessionAuthenticationModule;
            sessionAuthenticationModule.SignOut();

            Response.Redirect("/");
        }
    }
}