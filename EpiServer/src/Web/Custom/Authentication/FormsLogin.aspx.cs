﻿using System;
using System.Linq;
using System.Net;
using EPiServer.ServiceLocation;
using log4net;
using SSO.Security;
using SSO.Security.Contracts;

namespace Norgesportalen.Custom.Authentication
{
    public partial class FormsLogin : EPiServer.UI.Util.Login
    {
        private IIpAddressResolver _ipAddressResolver;
        private static readonly ILog Log = LogManager.GetLogger(typeof(FormsLogin));

        protected void Page_Load(object sender, EventArgs e)
        {
            _ipAddressResolver = ServiceLocator.Current.GetInstance<IIpAddressResolver>();
            HandleLogin();
            
        }

        private void HandleLogin()
        {
            var ip = _ipAddressResolver.GetClientIpAddressFor(Request);
            string redirectUrl = Context.Request.QueryString[SSO.Security.Constants.ReturnUrl];

            Log.DebugFormat($"Incomming IP address: {ip}");

            if (ip != null)
            {
                var trustedHosts = Configuration.Current.TrustedHosts.Cast<TrustedHostElement>().ToList();
                var isTrustedHost = trustedHosts.Any(th => new IPAddressRange(IPAddress.Parse(th.Lower), IPAddress.Parse(th.Upper)).IsInRange(ip));

                Log.DebugFormat($"IP address: {ip}. Is trusted host?: {isTrustedHost}");

                if (isTrustedHost)
                {
                    Log.DebugFormat($"Transfering to claims based authentication for trusted host: {ip}");
                    Response.Redirect($"~/{SSO.Security.Constants.ClaimBasedLoginUrl}?{SSO.Security.Constants.ReturnUrl}={redirectUrl}");
                }
            }
            else
            {
                Log.DebugFormat("Incomming IP address is null");
            }

            if (string.IsNullOrEmpty(ApplicationSettings.Instance.FormsLoginUrl))
                throw new Exception("Forms login URL is missing from application settings.");

            var url = $"{EPiServer.Configuration.Settings.Instance.SiteUrl}{ApplicationSettings.Instance.FormsLoginUrl}?{Constants.ReturnUrl}={redirectUrl}";
            Log.DebugFormat($"Redirecting to {url}");
            Response.Redirect(url);
        }

         
    }
}