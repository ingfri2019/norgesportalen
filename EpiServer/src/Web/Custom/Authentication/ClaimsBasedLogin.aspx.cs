﻿using System;
using System.IdentityModel.Services;
using System.Web.UI;
using EPiServer.Web;
using SSO.Security;

namespace Norgesportalen.Custom.Authentication
{
    public partial class ClaimsBasedLogin : Page
    {
        protected override void OnLoad(EventArgs e)
        {
            FederatedAuthentication.WSFederationAuthenticationModule.Realm = SiteDefinition.Current.SiteUrl.ToString();
            FederatedAuthentication.WSFederationAuthenticationModule.Freshness = "10";
            FederatedAuthentication.WSFederationAuthenticationModule.Issuer = "https://sso.mfa.no/adfs/ls";

            string redirectUrl = string.IsNullOrEmpty(Context.Request.QueryString[Constants.ReturnUrl]) ? ApplicationSettings.Instance.FormsLoginUrl : Context.Request.QueryString[Constants.ReturnUrl];
            FederatedAuthentication.WSFederationAuthenticationModule.RedirectToIdentityProvider(SiteDefinition.Current.SiteUrl.ToString(), redirectUrl, true);
        }
    }
}