﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeBehind="FormsLogin.aspx.cs" Inherits="Norgesportalen.Custom.Authentication.FormsLogin" %>
<%@ Register TagPrefix="EPiServerUI" Namespace="EPiServer.UI.WebControls" Assembly="EPiServer.UI, Version=9.9.2.0, Culture=neutral, PublicKeyToken=8fe83dea738b45b7" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

</head>
<body>
    <form id="form1" runat="server">
      <div class="epi-loginContainer">
        <EPiServerUI:Login ID="LoginControl" runat="server" FailureText="<%$ Resources: EPiServer, login.loginfailed %>">
            <LayoutTemplate>
                <div class="epi-loginTop">
                </div>
                <div class="epi-loginMiddle">
                    <div class="epi-loginContent">
                        <div class="epi-loginLogo">EPiServer CMS</div>
                        <div class="epi-loginForm">
                            <h1><EPiServer:Translate runat="server" Text="/login/loginheading" /> <span style="color:Red;"><asp:Literal runat="server" ID="FailureText" /></span></h1>
                           
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" DisplayMode="List" EnableClientScript="true" />
                            <div class="epi-credentialsContainer">

                                <div class="epi-float-left">
                                    <asp:Label ID="Label133" AssociatedControlID="UserName" Text="<%$ Resources: EPiServer, login.username %>"
                                        CssClass="episize80" runat="server" /><br />
                                    <asp:TextBox  CssClass="epi-inputText" ID="UserName" runat="server" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="<%$ Resources: EPiServer, login.usernamerequired %>"
                                        Text="&#173;" ControlToValidate="UserName" Display="Dynamic" runat="server" />
                                </div>
                                <div class="epi-float-left">
                                    <asp:Label ID="Label2" AssociatedControlID="Password" Text="<%$ Resources: EPiServer, login.password %>"
                                        CssClass="episize80" runat="server" /><br />
                                    <asp:TextBox  CssClass="epi-inputText" ID="Password" Text="password"
                                        runat="server" TextMode="Password" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="<%$ Resources: EPiServer, login.missingpassword %>"
                                        Text="&#173;" ControlToValidate="Password" Display="Dynamic" runat="server" />                                    
                                </div>
                                <div class="epi-button-container epi-float-left">
                                    <span class="epi-button">
                                        <span class="epi-button-child">
                                            <asp:Button ID="Button1" CssClass="epi-button-child-item" CommandName="Login" Text="<%$ Resources: EPiServer, button.login %>"
                                                runat="server" />
                                        </span>
                                    </span>
                                </div>
                                <div class="epi-checkbox-container">
                                    <asp:CheckBox ID="RememberMe" CssClass="epi-checkbox" runat="server" />
                                    <asp:Label ID="Label1" AssociatedControlID="RememberMe" Text="<%$ Resources: EPiServer, login.persistentlogin %>"
                                        runat="server" />
                                </div>
                            </div>
                            <p>
                                <a href="#" onclick="toggleCookieText(); return false;">
                                    <asp:Literal ID="Literal1" Text="<%$ Resources: EPiServer, cookie.logincaption %>"
                                        runat="server" />
                                </a>
                                <div id="cookieInfoPanel" style="display: none; text-align: left;">
                                    <br />
                                    <asp:Literal ID="Literal2" Text="<%$ Resources: EPiServer, cookie.logininfo %>" runat="server" />
                                </div>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="epi-loginBottom">
                </div>
            </LayoutTemplate>
        </EPiServerUI:Login>
    </div>
    </form>
</body>
</html>
