﻿using EPiServer.Core;

namespace Norgesportalen.Business.Menu
{
    public class MenuSubItem
    {
        public MenuSubItem()
        {
            Text = string.Empty;
            Link = string.Empty;
            ContentLink = ContentReference.EmptyReference;
        }

        public string Text { get; set; }
        public string Link { get; set; }
        public ContentReference ContentLink { get; set; }
    }
}