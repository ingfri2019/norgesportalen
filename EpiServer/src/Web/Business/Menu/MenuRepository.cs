﻿using EPiServer;
using EPiServer.Core;
using EPiServer.ServiceLocation;
using Norgesportalen.Business.Extensions;
using Norgesportalen.Models.Pages;
using System.Collections.Generic;
using System.Linq;
using PageBase = Norgesportalen.Models.Pages.BaseClasses.PageBase;

namespace Norgesportalen.Business.Menu
{
    [ServiceConfiguration(typeof(IMenuRepository))]
    public class MenuRepository : IMenuRepository
    {
        private readonly IContentRepository _contentRepository;

        public MenuRepository(IContentRepository contentRepository)
        {
            _contentRepository = contentRepository;
        }

        public Menu GetMenu(HomePage homePage)
        {
            Menu menuData = new Menu();

            menuData.TopMenu = new MenuData { Items = GetTopMenuItems(homePage.ContentLink, homePage.MaximumNumberOfFirstLevelMenuItems, homePage.MaximumNumberOfSecondLevelMenuItems) };
            menuData.HorizontalMenu = GetHorizontalMenuItem(homePage.HorizontalMenuPage,
                homePage.MaximumNumberOfHorizontalMenuItems);

            return menuData;
        }

        private MenuItem GetHorizontalMenuItem(PageReference horizontalMenuPage, int maximumMenuItems)
        {
            if (PageReference.IsNullOrEmpty(horizontalMenuPage))
            {
                return new MenuItem();
            }

            var page = _contentRepository.Get<PageBase>(horizontalMenuPage);
            if (page == null)
            {
                return new MenuItem();
            }

            var menuItem = new MenuItem
            {
                Title = page.HeadingOrPageName,
                Link = page.GetFriendlyUrl(),
                ContentLink = page.ContentLink,
                Items = GetAllHorizontalMenuSubItems(page)
            };
            menuItem.HasMoreItems = menuItem.Items.Count > maximumMenuItems;
            menuItem.Items = menuItem.Items.Take(maximumMenuItems).ToList();

            return menuItem;
        }

        private List<MenuSubItem> GetAllHorizontalMenuSubItems(PageData page)
        {
            if (page == null)
            {
                return new List<MenuSubItem>();
            }

            var children = _contentRepository.GetChildren<PageBase>(page.ContentLink).FilterForDisplay(true, true);

            List<MenuSubItem> subItems = CreateMenuSubItems(children);

            return subItems;
        }

        private List<MenuItem> GetTopMenuItems(ContentReference homePage, int topLevelMaxNumber, int secondLevelMaxNumber)
        {
            if (ContentReference.IsNullOrEmpty(homePage))
            {
                return new List<MenuItem>();
            }

            var homePageChildPages = _contentRepository.GetChildren<PageBase>(homePage).FilterForDisplay(true, true);

            var filteredChildPages = FilterPageTypes(homePageChildPages);

            return CreateMenuItems(filteredChildPages.Take(topLevelMaxNumber), secondLevelMaxNumber);
        }

        private IEnumerable<PageBase> FilterPageTypes(IEnumerable<PageBase> pages)
        {
            if (pages == null)
            {
                return new List<PageBase>();
            }

            if (!pages.Any())
            {
                return new List<PageBase>();
            }

            List<PageBase> filteredList = new List<PageBase>();

            foreach (PageBase page in pages)
            {
                if (page is SectionListPage)
                    filteredList.Add(page);
                if (page is SectionsPage)
                    filteredList.Add(page);
                if (page is CoreArticlePage)
                    filteredList.Add(page);
                if (page is CooperationPage)
                    filteredList.Add(page);
                if (page is NordmennPage)
                    filteredList.Add(page);
                if (page is ProfilingPage)
                    filteredList.Add(page);
                if (page is ServicesPage)
                    filteredList.Add(page);
            }

            return filteredList;
        }


        private List<MenuItem> CreateMenuItems(IEnumerable<PageBase> pages, int maximumNumber)
        {
            if (pages == null)
            {
                return new List<MenuItem>();
            }

            if (!pages.Any())
            {
                return new List<MenuItem>();
            }

            var menuItems = new List<MenuItem>();

            foreach (var page in pages)
            {
                var item = new MenuItem
                {
                    Title = page.HeadingOrPageName,
                    Link = page.GetFriendlyUrl(),
                    ContentLink = page.ContentLink
                };

                var childPages = _contentRepository.GetChildren<PageBase>(page.ContentLink).FilterForDisplay(true, true).ToList();
                item.Items = !childPages.Any() ? new List<MenuSubItem>() : CreateMenuSubItems(childPages.Take(maximumNumber));
                item.HasMoreItems = childPages.Count > maximumNumber;
                menuItems.Add(item);
            }

            return menuItems;
        }

        private static List<MenuSubItem> CreateMenuSubItems(IEnumerable<PageBase> pages)
        {
            return pages.Select(page => new MenuSubItem
            {
                Text = page.HeadingOrPageName,
                Link = page.GetFriendlyUrl(),
                ContentLink = page.ContentLink
            }).ToList();
        }
    }
}