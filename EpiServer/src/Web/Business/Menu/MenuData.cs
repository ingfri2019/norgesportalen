using System.Collections.Generic;

namespace Norgesportalen.Business.Menu
{
    public class MenuData
    {
        public MenuData()
        {
            Items = new List<MenuItem>();
        }
        public List<MenuItem> Items { get; set; }
    }
}