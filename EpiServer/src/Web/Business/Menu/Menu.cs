namespace Norgesportalen.Business.Menu
{
    public class Menu
    {
        public Menu()
        {
            TopMenu = new MenuData();
            HorizontalMenu = new MenuItem();
        }

        public MenuData TopMenu { get; set; }
        public MenuItem HorizontalMenu { get; set; }
    }
}