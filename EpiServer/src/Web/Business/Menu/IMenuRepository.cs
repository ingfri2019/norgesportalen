﻿using Norgesportalen.Models.Pages;

namespace Norgesportalen.Business.Menu
{
    public interface IMenuRepository
    {
        Menu GetMenu(HomePage homePage);
    }
}