using System.Collections.Generic;
using EPiServer.Core;

namespace Norgesportalen.Business.Menu
{
    public class MenuItem
    {
        public MenuItem()
        {
            Title = string.Empty;
            Link = string.Empty;
            Items = new List<MenuSubItem>();
            HasMoreItems = false;
        }

        public string Title { get; set; }
        public string Link { get; set; }
        public ContentReference ContentLink { get; set; }
        public List<MenuSubItem> Items { get; set; }
        public bool HasMoreItems { get; set; }
    }
}