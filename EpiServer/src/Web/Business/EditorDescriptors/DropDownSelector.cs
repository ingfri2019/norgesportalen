﻿using EPiServer.Shell.ObjectEditing;
using EPiServer.Shell.ObjectEditing.EditorDescriptors;
using System;
using System.Collections.Generic;

namespace Norgesportalen.Business.EditorDescriptors
{
    public class DropDownSelectorFactory<TEnum> : ISelectionFactory
    {
        public IEnumerable<ISelectItem> GetSelections(
            ExtendedMetadata metadata)
        {
            var values = Enum.GetValues(typeof(TEnum));
            foreach (var value in values)
            {
                yield return new SelectItem
                {
                    Text = GetValueName(value),
                    Value = value
                };
            }
        }

        private string GetValueName(object value)
        {
            var staticName = Enum.GetName(typeof(TEnum), value);
            return staticName;
        }
    }

    public class DropDownEditorDescriptor<TEnum> : EditorDescriptor
    {
        public override void ModifyMetadata(
            ExtendedMetadata metadata,
            IEnumerable<Attribute> attributes)
        {
            SelectionFactoryType = typeof(DropDownSelectorFactory<TEnum>);
            ClientEditingClass = "epi-cms/contentediting/editors/SelectionEditor";
            base.ModifyMetadata(metadata, attributes);
        }
    }

    public enum SocialmediaIcons
    {
        Facebook = 1,
        Twitter = 2,
        Instagram = 3,
        YouTube = 4,
        Vimeo = 5,
        LinkedIn = 6,
        Weibo = 7,
        Vkontakte = 8,
        Snapchat = 9,
        Flickr = 10
    }


    public static class SocialmediaIconHelper
    {
        public static string GetSocialMediaIconCssClass(int icon)
        {
            return Enum.IsDefined(typeof(SocialmediaIcons), icon) ? ((SocialmediaIcons)icon).ToString().ToLower() : " ";
        }
    }
}