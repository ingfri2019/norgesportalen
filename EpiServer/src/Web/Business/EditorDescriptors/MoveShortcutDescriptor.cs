﻿using EPiServer.Core;
using EPiServer.Security;
using EPiServer.Shell.ObjectEditing;
using EPiServer.Shell.ObjectEditing.EditorDescriptors;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Norgesportalen.Business.EditorDescriptors
{
    [EditorDescriptorRegistration(TargetType = typeof(ContentData))]
    public class MoveShortcutDescriptor : EditorDescriptor
    {
        public override void ModifyMetadata(ExtendedMetadata metadata, IEnumerable<Attribute> attributes)
        {
            foreach (var modelMetadata in metadata.Properties)
            {
                var property = (ExtendedMetadata)modelMetadata;
                if (property.PropertyName == "iversionable_shortcut")
                {
                    property.GroupName = Global.GroupNames.Config;
                    property.GroupSettings = CreateGroupSettingsIfConfigTabIsMissing(metadata);
                }
            }
        }

        private static GroupSettings CreateGroupSettingsIfConfigTabIsMissing(ExtendedMetadata metadata)
        {
            if (metadata.Properties.Cast<ExtendedMetadata>().Any(property => property.GroupName == Global.GroupNames.Config && property.GroupSettings != null))
            {
                return null;
            }

            var ownerContent = metadata.Model as PageData;
            var isAdmin = ownerContent.QueryDistinctAccess(AccessLevel.Administer);

            // Hvis denne sidetypen ikke har noen egenskaper på fanen «Config» fra før, må vi opprette fanen først.
            // Hvis vi ikke oppretter fanen slik, vil den ikke sorteres riktig, og tittelen vil også vises to ganger.
            // Dersom det legges til egenskaper på Config-fanen på en baseklasse, slik at fanen finnes for alle sidetyper,
            // kan hele metoden CreateGroupSettingsIfConfigTabIsMissing fjernes.
            return new GroupSettings(Global.GroupNames.Config, isAdmin, "epi/shell/layout/SimpleContainer",
                new Dictionary<string, object>())
                {
                    Title = Global.GroupNames.Config,
                    DisplayOrder = 1000
                };
        }
    }
}