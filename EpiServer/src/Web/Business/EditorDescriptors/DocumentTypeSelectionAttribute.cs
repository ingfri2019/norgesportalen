﻿using System;
using EPiServer.Shell.ObjectEditing;

namespace Norgesportalen.Business.EditorDescriptors
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class DocumentTypeSelectionAttribute : SelectManyAttribute
    {
        public override Type SelectionFactoryType
        {
            get { return typeof(DocumentTypeSelectionFactory); }
            set { base.SelectionFactoryType = value; }
        }
    }
}
