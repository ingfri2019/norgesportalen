﻿using DSS.PageSurvey.Models.Pages;
using EPiServer.Shell;
using Norgesportalen.Models.Blocks;

namespace Norgesportalen.Business.EditorDescriptors
{
    [UIDescriptorRegistration]
    public class EmergencyBlockUIDescriptor : UIDescriptor<EmergencyBlock>
    {
        public EmergencyBlockUIDescriptor()
        {            
            DefaultView = CmsViewNames.AllPropertiesView;
        }       
    }

    [UIDescriptorRegistration]
    public class SurveyPageUIDescriptor : UIDescriptor<SurveyPage>
    {
        public SurveyPageUIDescriptor()
        {
            DefaultView = CmsViewNames.AllPropertiesView;
        }
    }
}