﻿using System;
using System.Collections.Generic;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using EPiServer.Forms.Implementation.Elements;
using EPiServer.ServiceLocation;
using EPiServer.Shell.ObjectEditing;
using EPiServer.Web.Routing;

namespace Norgesportalen.Business.EditorDescriptors
{
    public class GlobalFormsSelectionFactory : ISelectionFactory
    {
        public IEnumerable<ISelectItem> GetSelections(ExtendedMetadata metadata)
        {
            try
            {
                var pageRouteHelper = ServiceLocator.Current.GetInstance<IPageRouteHelper>();
                var homePageLocator = ServiceLocator.Current.GetInstance<IHomePageLocator>();
                var contentRepository = ServiceLocator.Current.GetInstance<IContentRepository>();

                var homePage = homePageLocator.TryFindCountriesOverviewPage(pageRouteHelper.PageLink);

                if (homePage != null)
                {
                    // get global forms configuration set by editor
                    var formsReference = homePage.GlobalFormsLocation;

                    if (formsReference == null)
                    {
                        // get default location for forms
                        var formsRootContentFolder = contentRepository.GetChildren<IContent>(ContentReference.GlobalBlockFolder).FirstOrDefault(x => x.Name == EPiServer.Forms.Configuration.Settings.RootFolderName);
                        formsReference = formsRootContentFolder?.ContentLink;
                    }

                    if (formsReference != null)
                    {
                        var formsBlocks = contentRepository.GetChildren<FormContainerBlock>(formsReference);

                        var formsList = new List<SelectItem>(formsBlocks.Select(c => new SelectItem
                        {
                            Value = c.Content.ContentGuid.ToString(), 
                            Text = c.Form.Name
                        }));

                        formsList.Insert(0, new SelectItem() { Text = string.Empty, Value = string.Empty });
                        return formsList;
                    }
                }
            }
            catch (Exception exp)
            {
                return new List<ISelectItem>();
            }

            return new List<ISelectItem>();
        }
    }
}