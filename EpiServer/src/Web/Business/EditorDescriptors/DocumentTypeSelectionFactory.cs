﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using EPiServer.DataAbstraction;
using EPiServer.ServiceLocation;
using EPiServer.Shell.ObjectEditing;

namespace Norgesportalen.Business.EditorDescriptors
{
    public class DocumentTypeSelectionFactory : ISelectionFactory
    {
        public IEnumerable<ISelectItem> GetSelections(ExtendedMetadata metadata)
        {
            var repository = ServiceLocator.Current.GetInstance<IContentTypeRepository>();
            return repository.List()
                .Where(x => !string.IsNullOrEmpty(x.DisplayName))
                .OrderBy(x => x.DisplayName)
                .Select(x => new SelectItem { Text = x.DisplayName, Value = x.ID.ToString(CultureInfo.InvariantCulture) });
        }
    }
}
