﻿using System.Collections.Generic;
using EPiServer.Shell.ObjectEditing;

namespace Norgesportalen.Business.EditorDescriptors
{
    public class ListAlternativesSelectionFactory : ISelectionFactory
    {

        public IEnumerable<ISelectItem> GetSelections(ExtendedMetadata extendedMetadata)
        {
            return new List<SelectItem>
            {
                new SelectItem
                {
                    Text = "Grid view",
                    Value = Constants.ListAlternatives.GridView
                },
                new SelectItem
                {
                    Text = "List view",
                    Value = Constants.ListAlternatives.ListView
                }
            };
        }
    }
}
