using EPiServer;
using EPiServer.Core;
using EPiServer.ServiceLocation;
using EPiServer.Shell.ObjectEditing;
using Norgesportalen.Models.Media;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Norgesportalen.Business.EditorDescriptors
{
    /// <summary>
    /// Provides a list of options corresponding to ContactPage pages on the site
    /// </summary>
    /// <seealso cref="ContactPageSelector"/>
    public class ImageSelectionFactory<TImageSelectorSettings> : ISelectionFactory
    {
        IContentRepository _contentRepository = ServiceLocator.Current.GetInstance<IContentRepository>();

        public IEnumerable<ISelectItem> GetSelections(ExtendedMetadata metadata)
        {
            var settings = Activator.CreateInstance<TImageSelectorSettings>() as IImageSelectorSettings;
            var folderImages = GetContentFromAssetsMediaFolder(settings.AssetFolderName());
            var selectableImages = new List<SelectItem>();
            
            // Default option
            selectableImages.Add(
                        new SelectItem()
                        {
                            Text = "None",
                            Value = string.Empty
                        });

            if (folderImages != null)
            {
                foreach (var image in folderImages)
                {
                    selectableImages.Add(
                        new SelectItem()
                        {
                            Text = image.Name,
                            Value = image.ContentLink.ID.ToString()
                        });
                }
            }

            return selectableImages;
        }

        private IEnumerable<ImageFile> GetContentFromAssetsMediaFolder(string assetFolderName )
        {
            
            var descendents = _contentRepository.GetDescendents(ContentReference.RootPage);

            IContent folder = null;
            foreach (ContentReference des in descendents)
            {
                var content = _contentRepository.Get<IContent>(des);
                if (content.Name.Equals(assetFolderName))
                {
                    folder = content;
                    break;
                }
            }

            //var folder = descendents
            //                .Select(d => d.Get<IContent>())
            //                .Where(c => c.Name == assetFolderName)
            //                .First();

            if (folder == null) return null;

            return _contentRepository.GetChildren<IContent>(folder.ContentLink).OfType<ImageFile>();
        }
    }
}