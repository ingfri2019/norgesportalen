﻿using EPiServer.Shell.ObjectEditing;
using System.Collections.Generic;


namespace Norgesportalen.Business.EditorDescriptors
{
    public class EmergencyBannerColorSelectionFactory : ISelectionFactory
    {
        public IEnumerable<ISelectItem> GetSelections(ExtendedMetadata extendedMetadata)
        {
            IList<(string ColorName, string ColorRef)> defaultColorTuples = GetDefaultColors();
            foreach (var colorTuple in defaultColorTuples)
            {
                yield return new SelectItem
                {
                    Value = colorTuple.ColorRef,
                    Text = colorTuple.ColorName
                };
            }
        }

        public IList<(string, string)> GetDefaultColors()
        {
            return new List<(string, string)>
            {
                ("Black", "emergency-block--black"),
                ("Grey light",  "emergency-block--grey"),
                ("Arctic Blue Light",  "emergency-block--blue"),
                ("Passion Red Dark",  "emergency-block--red")
            };
        }
    }
}
