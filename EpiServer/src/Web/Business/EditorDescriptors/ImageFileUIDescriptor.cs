﻿using EPiServer.Shell;
using Norgesportalen.Models.Media;

namespace Norgesportalen.Business.EditorDescriptors
{
    [UIDescriptorRegistration]
    public class ImageFileUIDescriptor : UIDescriptor<ImageFile>, IEditorDropBehavior
    {
        public ImageFileUIDescriptor()
        {
            EditorDropBehaviour = EditorDropBehavior.CreateContentBlock;
            DefaultView = CmsViewNames.AllPropertiesView;
        }

        public EditorDropBehavior EditorDropBehaviour { get; set; }
    }
}