using EPiServer.Shell.ObjectEditing;
using EPiServer.Shell.ObjectEditing.EditorDescriptors;
using System;
using System.Collections.Generic;

namespace Norgesportalen.Business.EditorDescriptors
{
    /// <summary>
    /// Registers an editor to select a predefined image using a drop down
    /// </summary>
    public class ImageSelectorEditorDescriptor<ImageSelectorSettings> : EditorDescriptor
    {

        public override void ModifyMetadata(ExtendedMetadata metadata, IEnumerable<Attribute> attributes)
        {
            SelectionFactoryType = typeof(ImageSelectionFactory<ImageSelectorSettings>);

            ClientEditingClass = "epi-cms/contentediting/editors/SelectionEditor";

            base.ModifyMetadata(metadata, attributes);
        }

        

    }
}
