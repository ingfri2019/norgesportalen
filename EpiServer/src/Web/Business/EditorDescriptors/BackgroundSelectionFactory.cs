﻿using EPiServer.Shell.ObjectEditing;
using Norgesportalen.Business.BackgroundAvailability;
using System.Collections.Generic;


namespace Norgesportalen.Business.EditorDescriptors
{
    public class BackgroundSelectionFactory : ISelectionFactory
    {
        public IEnumerable<ISelectItem> GetSelections(ExtendedMetadata extendedMetadata)
        {
            var service = new BackgroundAvailabilityService();

            yield return new SelectItem
            {
                Value = null,
                Text = "[NONE]"
            };

            var imageFiles = service.GetBackgroundList();
            foreach (var imageFile in imageFiles)
            {
                yield return new SelectItem
                {
                    Value = imageFile.ContentLink.ID.ToString(),
                    Text = imageFile.Name
                };
            }
        }
    }
}
