﻿using EPiServer.Shell.ObjectEditing;
using EPiServer.Shell.ObjectEditing.EditorDescriptors;
using System;
using System.Collections.Generic;

namespace Norgesportalen.Business.EditorDescriptors
{

    /// <summary>
    /// Simple editor descriptor for label property in the forms-mode. It will display a label and a heading instead of the text property itself.
    /// Note that this property is modified in SiteFetchContentFromExtender.cs, to decide whether to show or hide it based on whether a tab is 
    /// marked as readonly or not.
    /// </summary>
    [EditorDescriptorRegistration(TargetType = typeof(string), UIHint = "ExtraEditorInformationProperty")]
    public class ExtraEditorInformationEditorDescriptior : EditorDescriptor
    {
        public override void ModifyMetadata(ExtendedMetadata metadata, IEnumerable<Attribute> attributes)
        {
            ClientEditingClass = "extra-editor-information-property";
            metadata.EditorConfiguration.Add("title", metadata.DisplayName); // This is the "header" in the component, marked in <strong> 
            metadata.DisplayName = ""; // This is the label-tag, which we subsequently hide in CSS anyway
            base.ModifyMetadata(metadata, attributes);

        }
    }

}