﻿using System;
using System.Collections.Generic;
using EPiServer.Cms.Shell.UI.ObjectEditing;
using EPiServer.ServiceLocation;
using EPiServer.Shell.ObjectEditing;
using EPiServer.Shell.ObjectEditing.EditorDescriptors;
using EPiServer.Web.Routing;
using Norgesportalen.Models.Media;

namespace Norgesportalen.Business.EditorDescriptors
{
    [EditorDescriptorRegistration(TargetType = typeof(string), UIHint = "HelpImage")]
    public class HelpImageEditorDescriptor : EditorDescriptor
    {
        public override void ModifyMetadata(ExtendedMetadata metadata, IEnumerable<Attribute> attributes)
        {
            ContentDataMetadata contentDataMetadata = metadata as ContentDataMetadata;
            var imageFile = contentDataMetadata?.OwnerContent as ImageFile;
            if (imageFile == null)
            {
                base.ModifyMetadata(metadata, attributes);
                return;
            }
            var urlResolver = ServiceLocator.Current.GetInstance<IUrlResolver>();
            var url = urlResolver.GetUrl(imageFile.ContentLink);
            ClientEditingClass = "editor-assets-help-image/EditorAssetsHelpImage";
            metadata.EditorConfiguration.Add("imgUrl", url);
            metadata.DisplayName = "";
            base.ModifyMetadata(metadata, attributes);
        }
    }
}
