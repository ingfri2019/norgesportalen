﻿using EPiServer.Core;
using EPiServer.Shell.ObjectEditing;
using EPiServer.Shell.ObjectEditing.EditorDescriptors;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Norgesportalen.Business.EditorDescriptors
{

    [EditorDescriptorRegistration(TargetType = typeof(ContentData))]
    public class SiteFetchContentFromExtender : EditorDescriptor
    {

        public override void ModifyMetadata(ExtendedMetadata metadata, IEnumerable<Attribute> attributes)
        {
            var owner = metadata.Model as PageData;

            // Correct type?
            if (owner == null)
                return;

            // This code mainly does two things:
            // ---------------------------------
            // 1a) If the page has a fetch-data functionality (property) and is using fetched data from another page, render all properties on certain 
            //     tabs as disabled, and properties on other tabs as enabled. 
            // 1b) ...and vice versa if the functionality exists, but data is NOT fetched.
            // 2)  If the Content-tab is disabled (meaning, the properties are readonly), automatically set the "alternative content" tab as selected.

            if (owner?.LinkType == PageShortcutType.FetchData)
            {
                // We are currently on pages that can fetch data from another page, and that do. Check the properties:

                foreach (var modelMetadata in metadata.Properties)
                {
                    var property = (ExtendedMetadata)modelMetadata;

                    // Disable all properties on tabs with properties that won't affect the front end when content is fetched from another source (i.e. the "content"-tab):
                    if (ShouldHide(property.GroupName, true))
                    {
                        //property.GroupSettings.DisplayUI = false; // This hides the property tab, but we are currently not doing that.
                        property.IsReadOnly = true;

                        HidePropertyBasedOnMetadataConditions(property, true);
                    }
                    else
                    {
                        HidePropertyBasedOnMetadataConditions(property, false);
                    }

                    // When fetch content is used, locate the tab where alternative properties are turned on, and attach Dojo-code there to run a script
                    // which manually clicks and activates this tab (instead of the default "content"-tab, which now has all properties as ReadOnly.
                    // This can not be done in EPiServer-code.
                    if (ShouldHide(property.GroupName, false))
                    {
                        if (property.GroupSettings != null)
                        {
                            //property.GroupSettings.ClientLayoutClass = "/modules/CMSDefaultTabSelector/ClientResources/CMSDefaultTabSelector.js"; // This works, but is ugly
                            property.GroupSettings.ClientLayoutClass = "default-tab-selector";
                        }
                    }

                    // We don't need an "else" clause where the IsReadOnly is set to false, because this could potentially re-enable properties that were 
                    // set to readonly on purpose in other places in the code/system.
                }
            }
            else
            {
                // The page is not currently fetching data from another page, but is potentially able to:
                foreach (var modelMetadata in metadata.Properties)
                {
                    var property = (ExtendedMetadata)modelMetadata;

                    // Disable all properties on tabs with properties that won't affect the front end when content is NOT fetched (i.e. "alternative content"):
                    if (ShouldHide(property.GroupName, false))
                    {
                        property.IsReadOnly = true;
  
                        HidePropertyBasedOnMetadataConditions(property, true);
                    }
                    else
                    {
                        HidePropertyBasedOnMetadataConditions(property, false);
                    }

                    // Specifically hide the FetchDataFromHelper property since it's only used in the front end editor, and doesn't work properly in the edit view:
                    if (string.Equals(property.PropertyName, "FetchDataFromHelper", StringComparison.OrdinalIgnoreCase))
                    {
                        property.GroupName = "TabWithHiddenProperties";
                        property.GroupSettings = new GroupSettings("TabWithHiddenProperties", false, "", null);
                    }
                }
            }

        }

        private bool ShouldHide(string tabName, bool pageFetchesContent)
        {
            // Some properties don't belong to any tab. Ignore these. 
            if (string.IsNullOrWhiteSpace(tabName))
            {
                return false;
            }

            // Determine if the property should be hidden or not, depending on the scenario (fetching/not fetching data) and the tab name:
            if(pageFetchesContent) { 
                return Global.DisableTabContentWhenFetchContent.Any(a => string.Equals(a, tabName, StringComparison.OrdinalIgnoreCase));
            }

            return Global.DisableTabContentWhenNotFetchContent.Any(a => string.Equals(a, tabName, StringComparison.OrdinalIgnoreCase));
        }


        private void HidePropertyBasedOnMetadataConditions(ExtendedMetadata property, bool readonlyTab)
        {
            UIHintExtendedAttribute.DisplayConditions showOnReadOnlyTab    = UIHintExtendedAttribute.DisplayConditions.ShowOnReadonlyTab; // Included for readability
            UIHintExtendedAttribute.DisplayConditions showOnNotReadOnlyTab = UIHintExtendedAttribute.DisplayConditions.ShowOnNotReadonlyTab; 

            // Check if the property is decorated with the UIHintExtendedAttribute, and if it should be displayed. 
            // This is typically only the case for non-content properties used only for info-texts in the editor UI.
            // If it is, do some magic:
            UIHintExtendedAttribute UIHintExtendedAttr = property.Attributes.OfType<UIHintExtendedAttribute>().FirstOrDefault();
            if (UIHintExtendedAttr != null)
            {
                if (readonlyTab && (UIHintExtendedAttr.DisplayCondition & showOnReadOnlyTab) == showOnReadOnlyTab)
                {
                    property.IsReadOnly = false; // Enables proper styling so we don't have to override Dojo readonly classes in the CSS
                }
                else if (!readonlyTab && (UIHintExtendedAttr.DisplayCondition & showOnNotReadOnlyTab) == showOnNotReadOnlyTab)
                {
                    property.IsReadOnly = false; // Enables proper styling so we don't have to override Dojo readonly classes in the CSS
                }
                else
                {
                    property.ShowForEdit = false;
                }
            }

        }

    }


}