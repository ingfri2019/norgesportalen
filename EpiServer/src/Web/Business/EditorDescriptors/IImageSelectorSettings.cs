﻿namespace Norgesportalen.Business.EditorDescriptors
{
    public interface IImageSelectorSettings
    {
        string AssetFolderName();
    }
}