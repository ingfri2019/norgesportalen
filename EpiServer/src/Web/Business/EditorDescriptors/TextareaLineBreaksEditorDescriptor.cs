﻿using System;
using System.Collections.Generic;
using EPiServer.Shell.ObjectEditing;
using EPiServer.Shell.ObjectEditing.EditorDescriptors;

namespace Norgesportalen.Business.EditorDescriptors
{
    [EditorDescriptorRegistration(TargetType = typeof(string), UIHint = "LineBreakString")]
    public class TextareaLineBreaksEditorDescriptor : EditorDescriptor
    {
        public override void ModifyMetadata(ExtendedMetadata metadata, IEnumerable<Attribute> attributes)
        {
            ClientEditingClass = "dijit/form/Textarea";

            // open the floating editor and not the inline editing (content editable)
            metadata.CustomEditorSettings["uiWrapperType"] = EPiServer.Shell.UiWrapperType.Floating;

            // set width of the editor
            metadata.EditorConfiguration.Add("style", "width: 300px");

            base.ModifyMetadata(metadata, attributes);
        }
    }
}