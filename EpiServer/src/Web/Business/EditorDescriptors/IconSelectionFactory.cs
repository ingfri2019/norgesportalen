﻿using EPiServer.Cms.Shell.UI.ObjectEditing;
using EPiServer.DataAbstraction;
using EPiServer.ServiceLocation;
using EPiServer.Shell.ObjectEditing;
using Norgesportalen.Business.IconAvailability;
using System.Collections.Generic;


namespace Norgesportalen.Business.EditorDescriptors
{
    public class IconSelectionFactory : ISelectionFactory
    {
        public IEnumerable<ISelectItem> GetSelections(ExtendedMetadata extendedMetadata)
        {
            var service = new IconAvailabilityService();
            var metaData = extendedMetadata as ContentDataMetadata;
            var contentData = metaData?.OwnerContent;
            if (contentData == null)
                yield break;

            var contentTypeRepository = ServiceLocator.Current.GetInstance<IContentTypeRepository>();
            var pageType = contentData.GetType();
            var contentType = contentTypeRepository.Load(pageType.BaseType);
            if (contentType == null)
                yield break;

            yield return new SelectItem
            {
                Value = null,
                Text = "[NONE]"
            };

            var imageFiles = service.GetIconListForPageTypeId(contentType.ID);
            foreach (var imageFile in imageFiles)
            {
                yield return new SelectItem
                {
                    Value = imageFile.ContentLink.ID.ToString(),
                    Text = imageFile.Name
                };
            }
        }
    }
}
