﻿using System.Collections.Generic;
using EPiServer.ServiceLocation;
using EPiServer.Shell.ObjectEditing;
using Norgesportalen.Business.Newsletter;

namespace Norgesportalen.Business.EditorDescriptors
{
    public class NewsletterListSelectionFactory : ISelectionFactory
    {
        private readonly INewsLetterService _newsLetterService;

        public NewsletterListSelectionFactory()
        {
            _newsLetterService = ServiceLocator.Current.GetInstance<INewsLetterService>();
        }
        public IEnumerable<ISelectItem> GetSelections(ExtendedMetadata metadata)
        {
            var newslettersList = _newsLetterService.GetNewsletterLists();
            var selectableNewsletters = new List<SelectItem>();

            foreach (var list in newslettersList)
            {
                var item = new SelectItem();
                item.Text = list.Name;
                item.Value = list.Id.ToString();
                selectableNewsletters.Add(item);
            }
            return selectableNewsletters;
        }
    }
}