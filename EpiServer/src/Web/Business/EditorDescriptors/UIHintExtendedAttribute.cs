﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Norgesportalen.Business.EditorDescriptors
{
    /// <summary>
    /// The UIHintExtended attribute is an extention to the regular UIHint attribute class. It enables certain types of metadata to be passed to the 
    /// rendering process and manipulate the objects further there, typically in ModifyMetadata.
    /// </summary>
    public class UIHintExtendedAttribute : UIHintAttribute
    {
        /// <summary>
        /// Sets the conditions for when the underlying property will be displayed in the EPiServer forms interface.
        /// </summary>
        public enum DisplayConditions
        {
            /// <summary>
            /// If the tab is set to readonly-mode, the info message will display.
            /// </summary>
            ShowOnReadonlyTab = 1,
            /// <summary>
            /// If the tab is not set to readonly-mode, the info message will display.
            /// </summary>
            ShowOnNotReadonlyTab = 2,
            /// <summary>
            /// The info message will display regardless of whether the tab is set to readonly mode or not.
            /// </summary>
            ShowAlways = 3
        }

        public DisplayConditions DisplayCondition { get; set; }

        public UIHintExtendedAttribute(string propertyName) : base(propertyName)
        {
            DisplayCondition = DisplayConditions.ShowAlways;
        }

        public UIHintExtendedAttribute(string propertyName, DisplayConditions displayStyle) : base(propertyName)
        {
            DisplayCondition = displayStyle;
        }

    }

}