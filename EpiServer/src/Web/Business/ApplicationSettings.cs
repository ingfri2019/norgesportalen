﻿using System;
using System.Web.UI.WebControls;
using EPiServer.Events;
using EPiServer.Events.Clients;
using EPiServer.PlugIn;
using EPiServer.Security;

namespace Norgesportalen.Business
{
    [GuiPlugIn(Area = PlugInArea.None, DisplayName = "Application Settings", RequiredAccess = AccessLevel.Administer, RequireLicenseForLoad = false)]
    public class ApplicationSettings
    {
        private static ApplicationSettings _instance;

        [PlugInProperty(Description = "GDPR footer Page ID", AdminControl = typeof(TextBox), AdminControlValue = "Text")]
        public int GdprFooterPageId { get; set; }

        public static ApplicationSettings Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ApplicationSettings();
                    PlugInSettings.AutoPopulate(_instance);
                }
                return _instance;
            }
        }
    }
}