﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;

namespace Norgesportalen.Business.Search
{
    public class CustomLink
    {
        public bool IsActivePage { get; set; }

        public string LinkText { get; set; }

        public string Url { get; set; }

        public RouteValueDictionary Route { get; set; }
    }
}