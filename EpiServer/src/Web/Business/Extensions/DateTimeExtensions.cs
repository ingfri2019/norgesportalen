﻿using System;

namespace Norgesportalen.Business.Extensions
{
    public static class DateTimeExtensions
    {
        public static DateTime GetTimeZoneConvertedDateTime(this DateTime currentDateTime, string timeZoneId)
        {
            var timeZoneConvertedDateTime = currentDateTime;

            if (string.IsNullOrEmpty(timeZoneId))
            {
                return timeZoneConvertedDateTime;
            }

            var tz = TimeZoneInfo.FindSystemTimeZoneById(timeZoneId);

            var dateTimeToConvertToUnspecified = new DateTime(currentDateTime.Ticks, DateTimeKind.Unspecified);
            timeZoneConvertedDateTime = TimeZoneInfo.ConvertTimeFromUtc(dateTimeToConvertToUnspecified, tz);

            return timeZoneConvertedDateTime;
        }
    }
}