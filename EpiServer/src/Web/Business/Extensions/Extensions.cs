﻿using EPiServer;
using EPiServer.Core;
using EPiServer.Globalization;
using EPiServer.ServiceLocation;
using EPiServer.Web;
using EPiServer.Web.Routing;
using Norgesportalen.Models.Media;
using Norgesportalen.Models.Pages.BaseClasses;
using System;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Routing;
using PageBase = Norgesportalen.Models.Pages.BaseClasses.PageBase;

namespace Norgesportalen.Business.Extensions
{

    public static class Extensions
    {
        public static string FirstCharToUpper(this string input)
        {
            switch (input)
            {
                case null: throw new ArgumentNullException(nameof(input));
                case "": throw new ArgumentException($"{nameof(input)} cannot be empty", nameof(input));
                default: return input.First().ToString().ToUpper() + input.Substring(1);
            }
        }

        public static string GetHeadingFromPageref(this PageReference pageRef)
        {
            var repository = ServiceLocator.Current.GetInstance<IContentRepository>();
            if (pageRef != null && pageRef.ID != 0)
            {
                PageBase page;
                if (repository.TryGet(pageRef, out page))
                {
                    return page.HeadingOrPageName;
                }
            }
            return string.Empty;
        }

        public static string GetFriendlyUrl(this SitePageData page)
        {
            try
            {
                if (page != null)
                {
                    return ServiceLocator.Current.GetInstance<UrlResolver>()
                        .GetUrl(page.ContentLink, page.Language.Name,
                            new VirtualPathArguments { ContextMode = ContextMode.Default });
                }
                return null;
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        public static string GetImageUrl(this string imageId, string imageSize)
        {
            int imageContentId;
            if (!int.TryParse(imageId, out imageContentId))
                return string.Empty;
            ContentReference image = new ContentReference(imageContentId);
            return image.GetImageUrl(imageSize);
        }

        public static string GetImageUrl(this ContentReference image, string imageSize)
        {
            try
            {
                if (ContentReference.IsNullOrEmpty(image)) return null;

                var url = UrlResolver.Current.GetUrl(image, null, new VirtualPathArguments { ContextMode = ContextMode.Default });

                // Adding imagesize
                if (!string.IsNullOrWhiteSpace(imageSize))
                    url = url.AddUrlParameter("preset", imageSize);

                // Adding changed timestamp, so we can increase max-age caching
                var imageCacheId = GetAssetCacheId(image);
                if (imageCacheId != null)
                    url = url.AddUrlParameter("v", imageCacheId);

                return url;
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        public static string GetImageName(this string imageId)
        {
            int imageContentId;
            if (!int.TryParse(imageId, out imageContentId))
                return string.Empty;
            ContentReference image = new ContentReference(imageContentId);
            return image.GetImageName();
        }

        public static string GetImageName(this ContentReference image)
        {
            ImageFile imageFile;
            if (ServiceLocator.Current.GetInstance<IContentRepository>().TryGet(image, out imageFile))
            {
                return imageFile.Name;
            }
            return string.Empty;
        }

        public static string GetImageAltText(this ContentReference image)
        {
            ImageFile imageFile;
            if (ServiceLocator.Current.GetInstance<IContentRepository>().TryGet(image, out imageFile))
            {
                return imageFile.AltText;
            }
            return string.Empty;
        }
        public static string GetImageCredit(this ContentReference image)
        {
            ImageFile imageFile;
            if (ServiceLocator.Current.GetInstance<IContentRepository>().TryGet(image, out imageFile))
            {
                return imageFile.Credit;
            }
            return string.Empty;
        }

        public static string GetImageText(this ContentReference image)
        {
            if (ServiceLocator.Current.GetInstance<IContentRepository>().TryGet(image, out ImageFile imageFile))
            {
                return imageFile.ImageText;
            }
            return string.Empty;
        }

        public static string AddUrlParameter(this string url, string name, string value)
        {
            return string.Format(url.Contains("?") ? "{0}&{1}={2}" : "{0}?{1}={2}", url, name, value);
        }

        public static string PageUrl(this UrlHelper urlHelper, PageReference pageLink)
        {
            return urlHelper.PageUrl(pageLink, new RouteValueDictionary(), null);
        }

        public static string PageUrl(this UrlHelper urlHelper, PageReference pageLink, RouteValueDictionary routeValueDictionary)
        {
            return urlHelper.PageUrl(pageLink, routeValueDictionary, null);
        }

        private static string PageUrl(this UrlHelper urlHelper, PageReference pageLink, RouteValueDictionary routeValues, IContentRepository contentRepository)
        {
            if (contentRepository == null)
                contentRepository = ServiceLocator.Current.GetInstance<IContentRepository>();
            if (PageReference.IsNullOrEmpty(pageLink))
                return string.Empty;
            var page = contentRepository.Get<PageData>(pageLink);
            return urlHelper.PageUrl(page, routeValues);
        }

        public static string PageUrl(this UrlHelper urlHelper, PageData page)
        {
            return PageUrl(urlHelper, page, (object)null);
        }

        public static string PageUrl(this UrlHelper urlHelper, PageData page, object routeValues)
        {
            var routeValueDictionary = new RouteValueDictionary(routeValues);
            return urlHelper.PageUrl(page, routeValueDictionary);
        }

        public static string PageUrl(this UrlHelper urlHelper, PageData page, RouteValueDictionary routeValueDictionary)
        {
            if (page == null || !page.HasTemplate())
                return string.Empty;

            switch (page.LinkType)
            {
                case PageShortcutType.Normal:
                case PageShortcutType.Shortcut:
                case PageShortcutType.FetchData:
                    return PageUrl(urlHelper, page.PageLink, routeValueDictionary, null, null);

                case PageShortcutType.External:
                    return page.LinkURL;

                case PageShortcutType.Inactive:
                    return string.Empty;

                default:
                    return string.Empty;
            }
        }

        private static string PageUrl(this UrlHelper urlHelper, PageReference pageLink, RouteValueDictionary routeValueDictionary, IContentLoader contentQueryable, LanguageSelectorFactory languageSelectorFactory)
        {
            if (!routeValueDictionary.ContainsKey(RoutingConstants.LanguageKey))
                routeValueDictionary[RoutingConstants.LanguageKey] = ContentLanguage.PreferredCulture.Name;
            if (!routeValueDictionary.ContainsKey(RoutingConstants.ActionKey))
                routeValueDictionary[RoutingConstants.ActionKey] = "index";
            routeValueDictionary[RoutingConstants.NodeKey] = pageLink;
            return urlHelper.Action(null, routeValueDictionary);
        }

        private static string GetAssetCacheId(this ContentReference fileRef)
        {
            if (ContentReference.IsNullOrEmpty(fileRef)) return null;

            var asset = DataFactory.Instance.Get<MediaData>(fileRef);
            return asset != null ? asset.Saved.Ticks.ToString(CultureInfo.InvariantCulture).GetHashCode().ToString() : null;
        }


        /// <summary>
        /// Usage:
        /// @Html.DisplayFor(m => m.PropertyName)
        /// 
        /// supply cssclass name, and override span with div tag
        /// @Html.DisplayFor(m => m.PropertyName, "desc", "div")
        /// 
        /// using the named param
        /// @Html.DisplayFor(m => m.PropertyName, tagName: "div")
        /// </summary>
        public static MvcHtmlString DescriptionFor<TModel, TValue>(this HtmlHelper<TModel> self, Expression<Func<TModel, TValue>> expression, string cssClassName = "", string tagName = "span")
        {
            var metadata = ModelMetadata.FromLambdaExpression(expression, self.ViewData);
            var description = metadata.Description;

            if (!string.IsNullOrEmpty(description))
            {
                var tag = new TagBuilder(tagName) { InnerHtml = description };
                if (!string.IsNullOrEmpty(cssClassName))
                    tag.AddCssClass(cssClassName);

                return new MvcHtmlString(tag.ToString());
            }

            return MvcHtmlString.Empty;
        }
    }
}