﻿using EPiServer.Core;
using EPiServer.Globalization;
using EPiServer.ServiceLocation;
using EPiServer.Web;
using EPiServer.Web.Internal;
using EPiServer.Web.Routing;
using System.Web.Mvc;
using System.Web.Routing;

namespace Norgesportalen.Business.Extensions
{
    /// <summary>
    /// Just a slightly modified version of EPiServer.Web.Mvc.Html.CanonicalLinkExtensions that outputs absolute URLs.
    /// </summary>
    public static class CanonicalLinkExtensions
    {
        /// <summary>
        /// Renders a canonical link element for the provided parameters.
        /// </summary>
        /// <param name="html">The HTML.</param>
        /// <param name="contentLink">The content link of the content. If null the current content will be used.</param>
        /// <param name="language">The content language. If null the current content language or preferred language will be used.</param>
        /// <param name="action">The controller action that should be appended to the url. If null the current action will be used. Use an empty string to remove the current action.</param>
        /// <returns>A canonical link element with the canonical url for the provided parameters.</returns>
        public static MvcHtmlString AbsoluteCanonicalLink(this HtmlHelper html, ContentReference contentLink, string language, string action)
        {
            var requestContext = html.ViewContext.RequestContext;
            if ((object) contentLink == null)
            {
                contentLink = requestContext.GetContentLink();
            }

            return new MvcHtmlString(Render(contentLink, language ?? GetLang(requestContext), new VirtualPathArguments()
            {
                ForceCanonical = true,
                Action = action ?? GetAction(requestContext)
            }));
        }

        /// <summary>
        /// Renders a canonical link element for the current context.
        /// </summary>
        /// <param name="html">The HTML.</param>
        /// <returns>A canonical link element with the canonical url for the current context.</returns>
        public static MvcHtmlString AbsoluteCanonicalLink(this HtmlHelper html)
        {
            return html.AbsoluteCanonicalLink((ContentReference)null, (string)null, (string)null);
        }

        private static string GetLang(RequestContext requestContext)
        {
            return requestContext.GetLanguage() ?? ContentLanguage.PreferredCulture.Name;
        }

        private static string GetAction(RequestContext requestContext)
        {
            return requestContext.GetRouteValue(RoutingConstants.ActionKey, (RouteValueDictionary)null) as string;
        }
        private static string Render(ContentReference contentLink, string language = null, VirtualPathArguments virtualPathArguments = null)
        {
            if (ContentReference.IsNullOrEmpty(contentLink))
            {
                return "";
            }

            var urlResolver= ServiceLocator.Current.GetInstance<UrlResolver>();
            var url = urlResolver.GetUrl(contentLink, language, virtualPathArguments);
            if (string.IsNullOrEmpty(url))
            {
                return "";
            }

            return new TagBuilder("link")
            {
                Attributes = {
                    {
                        "rel",
                        "canonical"
                    },
                    {
                        "href",
                        SiteDefinition.Current.SiteUrl.ToString().TrimEnd('/') + UrlEncoder.Encode(url)
                    }
                }
            }.ToString(TagRenderMode.SelfClosing);
        }
    }
}