﻿using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.ServiceLocation;
using EPiServer.Web;
using EPiServer.Web.Internal;
using EPiServer.Web.Routing;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.Routing;

namespace Norgesportalen.Business.Extensions
{
    /// <summary>
    /// Just a slightly modified version of EPiServer.Web.Mvc.Html.AlternateLinksExtensions that outputs absolute URLs.
    /// </summary>
    public static class AlternateLinksExtensions
    {
        /// <summary>
        /// Renders a alternative link element for the provided parameters.
        /// </summary>
        /// <param name="html">The HTML.</param>
        /// <param name="contentLink">The content for which alternate links should be rendered. If null, the currently routed content will be used.</param>
        /// <param name="action">The controller action that should be appended to the url. If null the current action will be used. Use an empty string to remove the current action.</param>
        /// <returns>
        /// A canonical link element with the canonical url for the provided parameters.
        /// </returns>
        public static MvcHtmlString AbsoluteAlternateLinks(this HtmlHelper html, ContentReference contentLink, string action)
        {
            var requestContext = html.ViewContext.RequestContext;
            if ((object) contentLink == null)
            {
                contentLink = requestContext.GetContentLink();
            }

            return new MvcHtmlString(Render(contentLink, action ?? GetAction(requestContext)));
        }

        /// <summary>
        /// Renders a alternative link element for the current context.
        /// </summary>
        /// <param name="html">The HTML.</param>
        /// <returns>A alternative link element with the canonical url for the current context.</returns>
        public static MvcHtmlString AbsoluteAlternateLinks(this HtmlHelper html)
        {
            return html.AbsoluteAlternateLinks(null, null);
        }

        private static string GetAction(RequestContext requestContext)
        {
            return requestContext.GetRouteValue(RoutingConstants.ActionKey) as string;
        }

        private static string Render(ContentReference contentLink, string action = null)
        {
            List<string> alternateLanguages = GetAlternateLanguages(contentLink);
            if (alternateLanguages.Count < 2)
                return "";
            VirtualPathArguments virtualPathArguments = new VirtualPathArguments()
            {
                ForceCanonical = true,
                Action = action
            };
            StringBuilder stringBuilder = new StringBuilder();
            foreach (string language in alternateLanguages)
            {
                var urlResolver = ServiceLocator.Current.GetInstance<UrlResolver>();
                string url = urlResolver.GetUrl(contentLink, language, virtualPathArguments);
                if (!string.IsNullOrEmpty(url))
                    stringBuilder.Append(new TagBuilder("link")
                    {
                        Attributes = {
                            {
                                "rel",
                                "alternate"
                            },
                            {
                                "href",
                                SiteDefinition.Current.SiteUrl.ToString().TrimEnd('/') + UrlEncoder.Encode(url)
                            },
                            {
                                "hreflang",
                                language
                            }
                        }
                    }.ToString(TagRenderMode.SelfClosing));
            }
            return stringBuilder.ToString();
        }

        private static List<string> GetAlternateLanguages(ContentReference contentLink)
        {
            var languageBranchRepository = ServiceLocator.Current.GetInstance<ILanguageBranchRepository>();
            var contentRepository = ServiceLocator.Current.GetInstance<IContentRepository>();
            var publishedStateAssessor = ServiceLocator.Current.GetInstance<IPublishedStateAssessor>();
            var contentLanguageSettingsHandler = ServiceLocator.Current.GetInstance<IContentLanguageSettingsHandler>();

            var enabledLanguages = new HashSet<CultureInfo>(languageBranchRepository.ListEnabled().Select(x => x.Culture));
            var publishedLanguages = contentRepository.GetLanguageBranches<IContent>(contentLink).Where(x =>
            {
                if (publishedStateAssessor.IsPublished(x))
                    return HasValidLinkType(x);
                return false;
            }).OfType<ILocalizable>().Where(x =>
            {
                if (x.Language != null)
                    return enabledLanguages.Contains(x.Language);
                return false;
            }).Select(x => x.Language.Name).ToList();

            var second = contentLanguageSettingsHandler.Get(contentLink).Where(x =>
            {
                if (x.IsActive && !publishedLanguages.Contains(x.LanguageBranch) && x.LanguageBranchFallback != null)
                    return x.LanguageBranchFallback.Any(y => publishedLanguages.Contains(y));
                return false;
            }).Select(x => x.LanguageBranch);
            return publishedLanguages.Concat(second).ToList();
        }

        private static bool HasValidLinkType(IContent content)
        {
            if (content is PageData pageData && pageData.LinkType != PageShortcutType.Normal)
            {
                return pageData.LinkType == PageShortcutType.FetchData;
            }

            return true;
        }
    }
}