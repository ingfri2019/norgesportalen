using EPiServer.ServiceLocation;
using EPiServer.Web;
using EPiServer.Web.Mvc;
using Norgesportalen.Models.Blocks;
using Norgesportalen.Models.Pages;
using Norgesportalen.Models.Pages.BaseClasses;

namespace Norgesportalen.Business.Rendering
{
    [ServiceConfiguration(typeof(IViewTemplateModelRegistrator))]
    public class TemplateCoordinator : IViewTemplateModelRegistrator
    {
        public const string SharedBlocksFolder = "~/Views/Shared/Blocks/";
        public const string SharedPagePartialsFolder = "~/Views/Shared/PagePartials/";

        public static void OnTemplateResolved(object sender, TemplateResolverEventArgs args)
        {
            //Disable DefaultPageController for page types that shouldn't have any renderer as pages
            // ......................................................................................................................DefaultPageController in Alloy
            if (args.ItemToRender is IContainerPage && args.SelectedTemplate != null /*&& args.SelectedTemplate.TemplateType == typeof(Controllers.PageControllerBase)*/)
            {
                args.SelectedTemplate = null;
            }
        }

        /// <summary>
        /// Registers renderers/templates which are not automatically discovered,
        /// i.e. partial views whose names does not match a content type's name.
        /// </summary>
        /// <remarks>
        /// Using only partial views instead of controllers for blocks and page partials
        /// has performance benefits as they will only require calls to RenderPartial instead of
        /// RenderAction for controllers.
        /// Registering partial views as templates this way also enables specifying tags and
        /// that a template supports all types inheriting from the content type/model type.
        /// </remarks>
        public void Register(TemplateModelCollection templates)
        {
            templates.Add(typeof(PageBase),
                new EPiServer.DataAbstraction.TemplateModel()
                {
                    Name = "Related links",
                    Tags = new string[] { Global.ContentAreaTags.RelatedLinks },
                    Path = "~/Views/Shared/DisplayTemplates/PageBase.cshtml",
                    AvailableWithoutTag = true,
                    Inherit = true
                });

           // Top tasks Partials for Country Start Page
            templates.Add(typeof(SectionsPage),
                new EPiServer.DataAbstraction.TemplateModel()
                {
                    Tags = new string[] { Global.ContentAreaTags.HomePageTopTasks },
                    Name = "Service front page as top task",
                    Path = "~/Views/HomePage/TopTasks/TopTask.cshtml",
                    AvailableWithoutTag = false
                });

            templates.Add(typeof(SectionListPage),
                new EPiServer.DataAbstraction.TemplateModel()
                {
                    Tags = new string[] { Global.ContentAreaTags.HomePageTopTasks },
                    Name = "Section page as top task",
                    Path = "~/Views/HomePage/TopTasks/TopTask.cshtml",
                    AvailableWithoutTag = false
                });

            templates.Add(typeof(TopTaskLinkListBlock),
                    new EPiServer.DataAbstraction.TemplateModel()
                    {
                        Tags = new string[] { Global.ContentAreaTags.HomePageTopTasks },
                        Name = "Link list as top task",
                        Path = "~/Views/HomePage/TopTasks/TopTaskLinkListBlock.cshtml",
                        AvailableWithoutTag = false
                    });

            templates.Add(typeof(InformationArticlePage),
                new EPiServer.DataAbstraction.TemplateModel()
                {
                    Tags = new string[] { Global.ContentAreaTags.HomePageTopTasks },
                    Name = "Information page  as top task",
                    Path = "~/Views/HomePage/TopTasks/TopTask.cshtml",
                    AvailableWithoutTag = false
                });

            templates.Add(typeof(CoreArticlePage),
                new EPiServer.DataAbstraction.TemplateModel()
                {
                    Tags = new string[] { Global.ContentAreaTags.HomePageTopTasks },
                    Name = "Core article as top task",
                    Path = "~/Views/HomePage/TopTasks/TopTask.cshtml",
                    AvailableWithoutTag = false
                });

            // Partials for themes area in country start page - IS THIS USED??
            templates.Add(typeof(ThemePage),
                new EPiServer.DataAbstraction.TemplateModel()
                {
                    Tags = new string[] { Global.ContentAreaTags.HomePageThemes },
                    Name = "Theme page on theme area",
                    Path = "~/Views/HomePage/Themes/ThemePage.cshtml",
                    AvailableWithoutTag = false,
                });

            templates.Add(typeof(CoreArticlePage),
                new EPiServer.DataAbstraction.TemplateModel()
                {
                    Tags = new string[] { Global.ContentAreaTags.HomePageThemes },
                    Name = "Theme page on theme area",
                    Path = "~/Views/HomePage/Themes/CoreArticleTheme.cshtml",
                    AvailableWithoutTag = false,
                });

            // Partials for news area in country start page
            templates.Add(typeof(NewsPage),
                   new EPiServer.DataAbstraction.TemplateModel()
                   {
                       Tags = new string[] { Global.ContentAreaTags.HomePageManualNews },
                       Name = "News page on manual news area",
                       Path = "~/Views/HomePage/News/HomePageManualNews.cshtml",
                       AvailableWithoutTag = false
                   });

            templates.Add(typeof(EventPage),
                new EPiServer.DataAbstraction.TemplateModel()
                {
                    Tags = new string[] { Global.ContentAreaTags.HomePageManualNews },
                    Name = "Basic page on manual news area",
                    Path = "~/Views/HomePage/News/HomePageManualNews.cshtml",
                    AvailableWithoutTag = false
                });

            templates.Add(typeof(CoreArticlePage),
                new EPiServer.DataAbstraction.TemplateModel()
                {
                    Tags = new string[] { Global.ContentAreaTags.HomePageManualNews },
                    Name = "Basic page on manual news area",
                    Path = "~/Views/HomePage/News/HomePageManualNews.cshtml",
                    AvailableWithoutTag = false
                });

            templates.Add(typeof(CoreArticlePage),
                new EPiServer.DataAbstraction.TemplateModel()
                {
                    Tags = new string[] {Global.ContentAreaTags.HomePageAutomaticNews},
                    Name = "News page on auto news area",
                    Path = "~/Views/HomePage/News/HomePageAutomaticNews.cshtml",
                    AvailableWithoutTag = false
                });

            templates.Add(typeof(NewsPage),
                new EPiServer.DataAbstraction.TemplateModel()
                {
                    Tags = new string[] { Global.ContentAreaTags.HomePageAutomaticNews },
                    Name = "News page on auto news area",
                    Path = "~/Views/HomePage/News/HomePageAutomaticNews.cshtml",
                    AvailableWithoutTag = false
                });

            templates.Add(typeof(EventPage),
                new EPiServer.DataAbstraction.TemplateModel()
                {
                    Tags = new string[] { Global.ContentAreaTags.HomePageAutomaticNews },
                    Name = "Basic page on auto news area",
                    Path = "~/Views/HomePage/News/HomePageAutomaticNews.cshtml",
                    AvailableWithoutTag = false
                });

            templates.Add(typeof(EventPage),
                new EPiServer.DataAbstraction.TemplateModel()
                {
                    Tags = new string[] { Global.ContentAreaTags.FocusedEvent },
                    Name = "Show focused EventPage on EventListPage",
                    Path = "~/Views/EventPage/FocusedOnEventListPage.cshtml",
                    AvailableWithoutTag = false
                });

            templates.Add(typeof(EventPage),
                new EPiServer.DataAbstraction.TemplateModel()
                {
                    Tags = new string[] { Global.ContentAreaTags.RelatedEvents },
                    Name = "Event page as related event on another EventPage",
                    Path = "~/Views/EventPage/RelatedEvent.cshtml",
                    AvailableWithoutTag = false
                });

            //NewsList
            templates.Add(typeof(BasicPageBase),
                new EPiServer.DataAbstraction.TemplateModel()
                {
                    Inherit = true,
                    Tags = new string[] { Global.ContentAreaTags.NewsList },
                    Name = "News page on manual news area",
                    Path = "~/Views/Shared/PagePartials/NewsList.cshtml",
                    AvailableWithoutTag = false
                });

            //Contactslisting in accordianblock on contactpage

            templates.Add(typeof(ContactPersonBlockForAccordion),
               new EPiServer.DataAbstraction.TemplateModel()
               {
                   Tags = new string[] { Global.ContentAreaTags.ContactAccordion },
                   Name = "Contacts in listing",
                   Path = "~/Views/Shared/PagePartials/ContactAccordion.cshtml",
                   AvailableWithoutTag = false
               });

            templates.Add(typeof(ContactAmbassadorBlockForAccordion),
               new EPiServer.DataAbstraction.TemplateModel()
               {
                   Tags = new string[] { Global.ContentAreaTags.TopContactAccordion },
                   Name = "Top contact in listing",
                   Path = "~/Views/Shared/PagePartials/TopContactAccordion.cshtml",
                   AvailableWithoutTag = false
               });



            /*
             * SOCIAL MEDIA TAGS
            */
            templates.Add(typeof(SocialMediaBlock),
              new EPiServer.DataAbstraction.TemplateModel()
              {
                  Tags = new string[] { Global.ContentAreaTags.SocialMedia.Footer },
                  Name = "SocialMedia view for footer",
                  Path = "~/Views/Shared/PagePartials/SocialMedia/SocialMediaFooter.cshtml",
                  AvailableWithoutTag = false
              });

            templates.Add(typeof(SocialMediaBlock),
              new EPiServer.DataAbstraction.TemplateModel()
              {
                  Tags = new string[] { Global.ContentAreaTags.SocialMedia.Panel },
                  Name = "Panel of social media in footer",
                  Path = "~/Views/Shared/PagePartials/SocialMedia/SocialMediaPanel.cshtml",
                  AvailableWithoutTag = false
              });

            templates.Add(typeof(SocialMediaBlock),
              new EPiServer.DataAbstraction.TemplateModel()
              {
                  Tags = new string[] { Global.ContentAreaTags.SocialMedia.List },
                  Name = "List of social media",
                  Path = "~/Views/Shared/PagePartials/SocialMedia/SocialMediaList.cshtml",
                  AvailableWithoutTag = false
              });

            templates.Add(typeof(SocialMediaBlock),
              new EPiServer.DataAbstraction.TemplateModel()
              {
                  Tags = new string[] { Global.ContentAreaTags.SocialMedia.Inline },
                  Name = "Social media inline",
                  Path = "~/Views/Shared/PagePartials/SocialMedia/SocialMediaInline.cshtml",
                  AvailableWithoutTag = true
              });

            templates.Add(typeof(SocialMediaBlock),
              new EPiServer.DataAbstraction.TemplateModel()
              {
                  Tags = new string[] { Global.ContentAreaTags.SocialMedia.Newsletter },
                  Name = "Social media newsletter",
                  Path = "~/Views/Shared/PagePartials/SocialMedia/SocialMediaNewsletter.cshtml",
                  AvailableWithoutTag = true
              });

            templates.Add(typeof(NewsPage),
              new EPiServer.DataAbstraction.TemplateModel()
              {
                  Tags = new string[] { Global.ContentAreaTags.Newsletter },
                  Name = "Mewsletter",
                  Path = "~/Views/Shared/PagePartials/Newsletter.cshtml",
                  AvailableWithoutTag = false
              });
        }

        private static string BlockPath(string fileName)
        {
            return string.Format("{0}{1}", SharedBlocksFolder, fileName);
        }

        private static string PagePartialPath(string fileName)
        {
            return string.Format("{0}{1}", SharedPagePartialsFolder, fileName);
        }
    }
}