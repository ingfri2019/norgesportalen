﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EPiServer.Shell;

namespace Norgesportalen.Business.Rendering
{
    [UIDescriptorRegistration]
    public class ContainerPageUIDescriptor : UIDescriptor<IContainerPage>
    {
        public ContainerPageUIDescriptor()
            : base(ContentTypeCssClassNames.Container)
        {
            DefaultView = CmsViewNames.AllPropertiesView;
        }
    }
}