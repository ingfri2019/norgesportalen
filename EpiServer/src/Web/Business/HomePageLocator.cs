﻿using EPiServer;
using EPiServer.Core;
using EPiServer.ServiceLocation;
using Norgesportalen.Models.Pages;

namespace Norgesportalen.Business
{
    [ServiceConfiguration(typeof(IHomePageLocator))]
    public class HomePageLocator : IHomePageLocator
    {
        //One public method that takes currentPage as param, and returns CountryPage, as CountryPage is assumed base for all homepages at present time.
        private readonly IContentRepository _contentRepository;

        public HomePageLocator()
        {
            _contentRepository = ServiceLocator.Current.GetInstance<IContentRepository>();
        }

        public HomePageLocator(IContentRepository contentRepository)
        {
            _contentRepository = contentRepository;
        }

        public HomePage TryFindHomePage(ContentReference currentContentLink)
        {
            HomePage homePage;
            if (_contentRepository.TryGet<HomePage>(currentContentLink, out homePage))
            {
                return homePage;
            }
            
            PageData currentPage;
            _contentRepository.TryGet<PageData>(currentContentLink, out currentPage);

            if (currentPage is FolderPage)
                return null;

            if (currentPage.ParentLink == ContentReference.RootPage)
                return null;
        
            return TryFindHomePage(currentPage.ParentLink);
        }

        public CountriesOverviewPage TryFindCountriesOverviewPage(ContentReference currentContentLink)
        {
            CountriesOverviewPage countriesOverviewPage;
            if (_contentRepository.TryGet<CountriesOverviewPage>(currentContentLink, out countriesOverviewPage))
            {
                return countriesOverviewPage;
            }

            PageData currentPage;
            _contentRepository.TryGet<PageData>(currentContentLink, out currentPage);           

            if (currentPage.ParentLink == null || currentPage.ParentLink == ContentReference.RootPage)
                return null;

            return TryFindCountriesOverviewPage(currentPage.ParentLink);
        }
    }
}

