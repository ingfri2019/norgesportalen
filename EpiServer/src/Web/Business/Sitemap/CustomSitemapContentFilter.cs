﻿using EPiServer.Core;
using Norgesportalen.Models.Pages;

namespace Norgesportalen.Business.Sitemap
{
    public class CustomSitemapContentFilter : Geta.SEO.Sitemaps.Utils.ContentFilter
    {
        /// <summary>
        /// Makes it possible to exclude content from Geta.SEO.Sitemaps.
        /// Call method in base class to exclude the obvious, blocks, deleted pages etc.
        /// https://github.com/Geta/SEO.Sitemaps
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        public override bool ShouldExcludeContent(IContent content)
        {
            if (content is SearchPage)
            {
                return true;
            }

            return base.ShouldExcludeContent(content);
        }
    }
}