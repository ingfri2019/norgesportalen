﻿using EPiServer.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Norgesportalen.Business.Publishing
{
    public interface IAbleToFetchFromCentral
    {
        ContentReference FetchDataFromHelper { get; set; }

        // The following properties are not used for content puposes, but are used to display an info text in the edit UI regarding the fetched 
        // content and why properties are set to readonly or not.
        string TabInfo_Content_FetchedContentTabReadonly { get; set; }
        string TabInfo_AdditionalContent_NotFetchedContentTabReadonly { get; set; }
        string TabInfo_AdditionalContent_NotFetchedContentTabNotReadonly { get; set; }
        string TabInfo_SoMeTab_FetchedContentTabReadonly { get; set; }
    }
}
