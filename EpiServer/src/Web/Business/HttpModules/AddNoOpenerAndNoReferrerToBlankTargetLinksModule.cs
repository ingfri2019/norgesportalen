﻿using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace Norgesportalen.Business.HttpModules
{
    public class AddNoOpenerAndNoReferrerToBlankTargetLinksModule : IHttpModule
    {
        public void Init(HttpApplication httpApplication)
        {
            httpApplication.ReleaseRequestState += HttpApplication_OnReleaseRequestState;
        }

        private void HttpApplication_OnReleaseRequestState(object sender, EventArgs e)
        {
            var httpResponse = HttpContext.Current.Response;
            if (httpResponse.ContentType == "text/html")
                httpResponse.Filter = new AddNoOpenerAndNoReferrerToBlankTargetLinksResponseStream(httpResponse.Filter);
        }

        public void Dispose() { }
    }

    public class AddNoOpenerAndNoReferrerToBlankTargetLinksResponseStream : MemoryStream
    {
        private const string AttrPattern = @"\b([a-zA-Z-]*)\b(=""(.*?)(?<!\\)""|=\b(\w*)\b)?";  // Matches 'key=value' or 'value'
        private readonly Regex _linkWithAttributesRegex = new Regex(@"<a(\s*" + AttrPattern + @")*\s*>", RegexOptions.Compiled);
        private readonly Regex _attributesRegex = new Regex(AttrPattern, RegexOptions.Compiled);
        private readonly Stream _responseStream;

        public AddNoOpenerAndNoReferrerToBlankTargetLinksResponseStream(Stream inputStream)
        {
            _responseStream = inputStream;
        }

        public override void Close()
        {
            const string noreferrer = "noreferrer";
            const string noopener = "noopener";

            var response = Encoding.UTF8.GetString(ToArray());
            response = _linkWithAttributesRegex.Replace(response, _ =>
            {
                if (_.Value.IndexOf("target=\"_blank\"", StringComparison.Ordinal) == -1)
                    return _.Value;
                bool relExists = false;
                var attr = _attributesRegex.Replace(_.Value.Substring(2, _.Value.Length - 3), __ =>
                {
                    var name = __.Groups[1].Value;
                    var value = string.IsNullOrWhiteSpace(__.Groups[3].Value) ? __.Groups[4].Value : __.Groups[3].Value;
                    if (name.Equals("rel") != true)
                        return __.Value;
                    var hasNoreferrer = value.IndexOf(noreferrer, StringComparison.Ordinal) != -1;
                    var hasNoopener = value.IndexOf(noopener, StringComparison.Ordinal) != -1;
                    relExists = true;
                    if (hasNoopener && hasNoreferrer)
                        return __.Value;
                    if (!hasNoreferrer)
                        value += $" {noreferrer}";
                    if (!hasNoopener)
                        value += $" {noopener}";
                    return $"{name}=\"{value}\"";
                });
                if (!relExists)
                    attr += $" rel=\"{noreferrer} {noopener}\"";
                return $"<a{attr}>";
            });

            var bytes = Encoding.UTF8.GetBytes(response);
            _responseStream.Write(bytes, 0, bytes.Length);
            _responseStream.Flush();

            base.Close();
        }
    }
}