﻿using System.Collections.Generic;
using EPiServer.Cms.TinyMce.Core;
using EPiServer.Framework;
using EPiServer.Framework.Initialization;
using EPiServer.ServiceLocation;
using Norgesportalen.Models.Blocks;
using Norgesportalen.Models.Pages;

namespace Regjeringen.Models.Business.Initialization
{
    [ModuleDependency(typeof(TinyMceInitialization))]
    public class TinyMceSettingsInitialization : IConfigurableModule
    {
        public void Initialize(InitializationEngine context)
        {
        }

        public void Uninitialize(InitializationEngine context)
        {
        }

        // Oppsett av EPiServer.CMS.TinyMCE i Episerver:
        // https://world.episerver.com/documentation/developer-guides/CMS/add-ons/customizing-the-tinymce-editor-v2/
        // 
        // Hvilke plugin som skjuler hvilke funksjoner/knapper:
        // https://www.tinymce.com/docs/advanced/editor-control-identifiers/#toolbarcontrols
        public void ConfigureContainer(ServiceConfigurationContext context)
        {
            context.Services.Configure<TinyMceConfiguration>(config =>
            {
                config.Default()
                    .Width(650)
                    .Height(300)
                    .AddPlugin("epi-link link code image media paste fullscreen searchreplace code anchor")
                    .Toolbar("code styleselect image media | epi-link unlink anchor | cut pastetext fullscreen | bold italic blockquote | bullist numlist undo redo searchreplace")
                    .StyleFormats(
                        new { title = "Paragraph", block = "p" },
                        new { title = "Paragraph centered", block = "p", classes = "text-center" },
                        new
                        {
                            title = "Healine styles",
                            items = new[] {
                                new { title = "Headline", block = "h2" },
                                new { title = "Headline 2", block = "h3" },
                            }
                        },
                        new
                        {
                            title = "Button style",
                            items = new[] {
                                new { title = "Button style", selector = "a", classes = "btn" }
                            }
                        }
                    )
                    .ContentCss("/Frontend/dist-web/static/css/tinymce.css");

                var simple = config.Default().Clone()
                    .AddPlugin("epi-link")
                    .Toolbar("styleselect epi-link bold italic")
                    .StyleFormats(
                        new { title = "Paragraph", block = "p" },
                        new
                        {
                            title = "Headline styles",
                            items = new[] {
                                    new { title = "Headline", block = "h2" },
                            }
                        }
                    );

                config.For<HomePage>(t => t.FooterLeft, simple);
                config.For<HomePage>(t => t.FooterCenter, simple);

                config.For<CountriesOverviewPage>(t => t.FooterLeft, simple);
                config.For<CountriesOverviewPage>(t => t.FooterCenter, simple);

                // Create a TinyMCE xhtml-setting that only contains bold and italic
                var boldItalic = config.Default().Clone()
                    .Toolbar("bold italic")
                    .Height(150);

                // Set up xhtml configuration for the mission address field property on the contact page type
                config.For<ContactPage>(t => t.MissionAddressField, boldItalic);
            });
        }
    }
}