﻿using System;
using System.Collections.Generic;
using System.Web.Optimization;
using EPiServer.Cms.Shell.UI.Rest;
using EPiServer;
using EPiServer.Core;
using EPiServer.Framework;
using EPiServer.Framework.Initialization;
using np = Norgesportalen.Models.Pages.BaseClasses;
using System.Linq;

namespace Norgesportalen.Business.Initialization
{
    public class ContentTypeAdvisorModule : IContentTypeAdvisor
    {
        private readonly IContentRepository _contentRepository;

        public ContentTypeAdvisorModule(IContentRepository contentRepository)
        {
            _contentRepository = contentRepository;
        }
        public IEnumerable<int> GetSuggestions(IContent parent, bool contentFolder, IEnumerable<string> requestedTypes)
        {
            if (parent is np.PageBase)
            {
                var pb = (np.PageBase)parent;
                if (pb.SuggestedPageTypes != null)
                {
                    return pb.SuggestedPageTypes.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(x => int.Parse(x));
                }
            }
            return new int[] { };
        }

    }
}