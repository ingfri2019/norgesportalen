﻿using System.Web.Mvc;
using EPiServer.Cms.Shell.UI.Rest;
using EPiServer.Framework;
using EPiServer.Framework.Initialization;
using EPiServer.ServiceLocation;
using Geta.SEO.Sitemaps.Utils;
using Norgesportalen.Business.Sitemap;
using StructureMap;


namespace Norgesportalen.Business.Initialization
{
    [InitializableModule]
    public class DependancyResolverInitianlization : IConfigurableModule
    {
        public void ConfigureContainer(ServiceConfigurationContext context)
        {
            var container = context.StructureMap();
            container.EjectAllInstancesOf<IContentTypeAdvisor>();
            container.Configure(ConfigureContainer);
            DependencyResolver.SetResolver(new StructureMapDependencyResolver(container));
        }

        private static void ConfigureContainer(ConfigurationExpression container)
        {
            container.For<IContentTypeAdvisor>().Use<ContentTypeAdvisorModule>();
            container.For<IContentFilter>().Use<CustomSitemapContentFilter>();
        }

        public void Initialize(InitializationEngine context)
        {
        }

        public void Uninitialize(InitializationEngine context)
        {
        }
    }
}