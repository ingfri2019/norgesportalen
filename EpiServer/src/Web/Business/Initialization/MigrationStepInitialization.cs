﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EPiServer.DataAbstraction.Migration;

namespace Norgesportalen.Business.Initialization
{
    public class MigrationStepInitialization : MigrationStep
    {
        public override void AddChanges()
        {
            RenameContentType();
        }

        private void RenameContentType()
        {
            //The content type formerly known as "Velocipede" should hereby be known as "Bicycle".           
            //ContentType("SectionListPage").UsedToBeNamed("SectionsPage");
            //ContentType("SectionsPage").UsedToBeNamed("ServiceFrontPage");            
        }
    }
}