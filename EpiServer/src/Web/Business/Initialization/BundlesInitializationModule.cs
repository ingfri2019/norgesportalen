﻿using EPiServer.Framework;
using EPiServer.Framework.Initialization;
using System.Web.Optimization;

namespace Norgesportalen.Business.Initialization
{
    [InitializableModule]
    public class BundlesInitializationModule : IInitializableModule
    {
        public void Initialize(InitializationEngine context)
        {
            BundleTable.Bundles.Add(new ScriptBundle("~/Frontend/dist-web/Static/js/mainjs")
                .Include("~/Frontend/dist-web/Static/js/main.js")
            );

            BundleTable.Bundles.Add(
                new ScriptBundle("~/Static/Survey/page-survey")
                    .Include("~/Static/Survey/page-survey.js")
            );

            BundleTable.Bundles.Add(
                new StyleBundle("~/Frontend/dist-web/Static/css/maincss")
                    .Include("~/Frontend/dist-web/Static/css/main.css")
            );

            BundleTable.Bundles.Add(
                new StyleBundle("~/Frontend/dist-web/Static/css/editmode")
                    .Include("~/Frontend/dist-web/Static/css/edit.css")
            );

            BundleTable.EnableOptimizations = true;
        }

        public void Uninitialize(InitializationEngine context)
        {
        }
    }
}