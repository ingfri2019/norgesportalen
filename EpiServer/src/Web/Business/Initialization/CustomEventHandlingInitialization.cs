﻿using System.Linq;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAccess;
using EPiServer.Framework;
using EPiServer.Framework.Initialization;
using EPiServer.Security;
using EPiServer.ServiceLocation;
//using EPiServer.Shell.ObjectEditing;
using Norgesportalen.Business.BackgroundAvailability;
using Norgesportalen.Business.IconAvailability;
using Norgesportalen.Models.Media;
using Norgesportalen.Models.Pages;

namespace Norgesportalen.Business.Initialization
{
    [InitializableModule]
    [ModuleDependency(typeof(EPiServer.Web.InitializationModule))]
    public class CustomEventHandlingInitialization : IInitializableModule
    {
        public void Initialize(InitializationEngine context)
        {
            var contentEvents = ServiceLocator.Current.GetInstance<IContentEvents>();

            contentEvents.SavingContent += SavingContent;
            contentEvents.PublishingContent += PublishingContent;
            PageTypeConverter.PagesConverted += PageTypeConverter_PagesConverted;

            //var registry = context.Locate.Advanced.GetInstance<MetadataHandlerRegistry>();
            //registry.RegisterMetadataHandler(typeof(ContentData), new AdminPropertyManager());
        }

        private void PageTypeConverter_PagesConverted(object sender, ConvertedPageEventArgs e)
        {
            var contentRepository = ServiceLocator.Current.GetInstance<IContentRepository>();
            var contentTypeRepository = ServiceLocator.Current.GetInstance<IContentTypeRepository>();
            if (contentRepository.TryGet(e.PageLink, out CoreArticlePage page))
            {
                var writableCoreArticle = (CoreArticlePage)page.CreateWritableClone();
                var contentType = contentTypeRepository.Load(writableCoreArticle.ContentTypeID);
                writableCoreArticle.SetDefaultValues(contentType);
                contentRepository.Save(writableCoreArticle, SaveAction.Patch, AccessLevel.NoAccess);
            }
        }

        private void PublishingContent(object sender, ContentEventArgs e)
        {
            var contentRepository = ServiceLocator.Current.GetInstance<IContentRepository>();
            var versionRepository = ServiceLocator.Current.GetInstance<IContentVersionRepository>();

            if (contentRepository.TryGet(e.ContentLink, out ImageFile image))
            {
                var versionsList = versionRepository.List(image.ContentLink);
                bool imageHasBeenIcon = false;
                bool imageHasBeenBackground = false;
                foreach (var version in versionsList)
                {
                    if (contentRepository.TryGet(version.ContentLink, out ImageFile versionImage))
                    {
                        if (versionImage.IsIcon)
                        {
                            imageHasBeenIcon = true;
                        }
                            
                        if (versionImage.IsBackground)
                        {
                            imageHasBeenBackground = true;
                        }                            
                    }
                }
                if (imageHasBeenIcon)
                {
                    var service = new IconAvailabilityService();
                    service.AddOrUpdateIconToStore(image);
                }
                if (imageHasBeenBackground)
                {
                    var service = new BackgroundAvailabilityService();
                    service.AddOrUpdateIconToStore(image);
                }
            }
        }

        public void Uninitialize(InitializationEngine context)
        {
            var contentEvents = ServiceLocator.Current.GetInstance<IContentEvents>();
        }

        private void SavingContent(object sender, ContentEventArgs e)
        {
            var fetchDataFromHelper = e.Content.Property["FetchDataFromHelper"]?.Value as PageReference;

            if (fetchDataFromHelper != null && e.Content is PageData)
            {
                var pg = (PageData) e.Content;
                var repository = ServiceLocator.Current.GetInstance<IContentRepository>();
                var fetcedhPg = repository.Get<PageData>(fetchDataFromHelper);               

                var listProps = pg.Property.Where(m => m.OwnerTab == 0 && m.Value != null); // get props under 'Content - tab 0'

                foreach (var prop in listProps)
                {
                    if (prop.Name != "PageCategory" && prop.Name != "FetchDataFromHelper" &&
                        prop.Name != "EventStartDate" && prop.Name != "EventStopDate" && prop.Name != "EventTimezoneId")
                    {
                        prop.Value = null;
                    }
                }
                pg.PageName = fetcedhPg.PageName;
                pg.URLSegment = fetcedhPg.URLSegment;
            }
        }
    }
}