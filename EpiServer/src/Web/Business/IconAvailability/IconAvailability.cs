﻿using System.Collections.Generic;
using EPiServer.Data;
using EPiServer.Data.Dynamic;

namespace Norgesportalen.Business.IconAvailability
{
    [EPiServerDataStore(AutomaticallyRemapStore = true)]
    public class IconAvailability
    {
        public Identity Id { get; set; }
        public List<PageWithIcons> PagesWithIcons { get; set; }
    }
}
