﻿using System;
using System.Collections.Generic;
using System.Linq;
using Castle.Core.Internal;
using EPiServer;
using EPiServer.Core;
using EPiServer.Logging.Compatibility;
using EPiServer.ServiceLocation;
using Norgesportalen.Models.Media;

namespace Norgesportalen.Business.IconAvailability
{
    public class IconAvailabilityService
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(IconAvailabilityService));
        private readonly IconStoreService _store;
        private readonly IContentRepository _iContentRepository;
        private IconAvailability _iconAvailability;

        public IconAvailabilityService()
        {
            _store = IconStoreService.Instance;
            _iContentRepository = ServiceLocator.Current.GetInstance<IContentRepository>();
            _iconAvailability = GetOrCreateIconAvailability();
        }

        public IEnumerable<ImageFile> GetIconListForPageTypeId(int id)
        {
            if (_iconAvailability.PagesWithIcons == null || !_iconAvailability.PagesWithIcons.Any())
            {
                Log.WarnFormat($"No icons for pageId {id}");
                return new List<ImageFile>();
            }

            var pageIdWithIcons = _iconAvailability.PagesWithIcons.FirstOrDefault(x => x.PageId == id);

            if (pageIdWithIcons == null || !pageIdWithIcons.PageId.Equals(id))
            {
                Log.WarnFormat($"No icons for pageId {id}");
                return new List<ImageFile>();
            }

            var iconList = new List<ImageFile>();

            foreach (var iconId in pageIdWithIcons.IconIdList)
            {
                ImageFile image;

                if (_iContentRepository.TryGet(new ContentReference(iconId), out image))
                {
                    iconList.Add(image);
                }
            }
            return iconList;
        }

        public void AddOrUpdateIconToStore(ImageFile imageFile)
        {
            var pageTypes = GetPageTypeIds(imageFile.FilterIconPages);

            RemoveIconsFromPages(imageFile.ContentLink.ID, pageTypes);
            AddIconsToPages(imageFile.ContentLink.ID, pageTypes);
            _store.AddOrUpdate(_iconAvailability);

        }

        private void RemoveIconsFromPages(int imageFileId, List<int> pageTypes)
        {
            if (_iconAvailability.PagesWithIcons != null && _iconAvailability.PagesWithIcons.Any())
            {
                var removeIconFromPages = _iconAvailability.PagesWithIcons.Where(z => z.IconIdList.Contains(imageFileId) && !pageTypes.Contains(z.PageId)).ToList();
                foreach (var pageWithIcons in removeIconFromPages)
                {
                    _iconAvailability.PagesWithIcons.FirstOrDefault(x => x.PageId == pageWithIcons.PageId)
                        ?.IconIdList
                        .Remove(imageFileId);
                }
            }
        }

        private void AddIconsToPages(int imageFileId, List<int> pageTypes)
        {
            foreach (var pageTypeId in pageTypes)
            {
                if (_iconAvailability.PagesWithIcons == null || !_iconAvailability.PagesWithIcons.Any())
                {
                    _iconAvailability.PagesWithIcons = new List<PageWithIcons>
                    {
                        new PageWithIcons
                        {
                            PageId = pageTypeId,IconIdList = new List<int> {imageFileId }
                        }
                    };
                }

                var pageWithIcons = _iconAvailability.PagesWithIcons.FirstOrDefault(x => x.PageId.Equals(pageTypeId));

                if (pageWithIcons != null && !pageWithIcons.IconIdList.Contains(imageFileId))
                {
                    pageWithIcons.IconIdList.Add(imageFileId);
                }
                else if (pageWithIcons == null)
                {
                    _iconAvailability.PagesWithIcons.Add(new PageWithIcons
                    {
                        PageId = pageTypeId,
                        IconIdList = new List<int> { imageFileId }
                    });
                }
            }
        }

        private List<int> GetPageTypeIds(string pageTypeIdsString)
        {
            if (pageTypeIdsString.IsNullOrEmpty())
                return new List<int>();

            var intList = new List<int>();
            foreach (var s in pageTypeIdsString.Split(','))
            {
                int pageTypeId;
                if (int.TryParse(s, out pageTypeId))
                    intList.Add(pageTypeId);
            }
            return intList;
        }

        private IconAvailability GetOrCreateIconAvailability()
        {
            var iconAvailability = _store.Get(new Guid(IconAvailabilityConstants.Id));
            if (iconAvailability != null)
                return iconAvailability;


            var newiconAvailability = new IconAvailability
            {
                Id = new Guid(IconAvailabilityConstants.Id),
                PagesWithIcons = new List<PageWithIcons>()
            };

            _store.AddOrUpdate(newiconAvailability);
            return newiconAvailability;
        }

    }
}