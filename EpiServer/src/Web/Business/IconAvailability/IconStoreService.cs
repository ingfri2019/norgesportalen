﻿using System;
using System.Collections.Generic;
using EPiServer.Data.Dynamic;
using EPiServer.Logging.Compatibility;

namespace Norgesportalen.Business.IconAvailability
{
    public class IconStoreService
    {
  
        public static IconStoreService Instance
        {
            get
            {
                return new IconStoreService();
            }
        }

        private static DynamicDataStore IconStore
        {
            get
            {
                return DynamicDataStoreFactory.Instance.CreateStore(typeof(IconAvailability));

            }
        }

        public IconAvailability Get(Guid id)
        {
            return IconStore.Load<IconAvailability>(id);
        }

        public IEnumerable<IconAvailability> GetAll()
        {

            return IconStore.LoadAll<IconAvailability>();

        }

        public void AddOrUpdate(IconAvailability iconAvailability)
        {
            IconStore.Save(iconAvailability);
        }

        public void Delete(Guid id)
        {
            IconStore.Delete(id);
        }

        public void DeleteAll()
        {
            IconStore.DeleteAll();
        }

        public void DeleteStore()
        {
            DynamicDataStoreFactory.Instance.DeleteStore(typeof(IconAvailability), true);
        }
    }
}
