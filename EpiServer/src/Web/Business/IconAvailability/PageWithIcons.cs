﻿using System.Collections.Generic;

namespace Norgesportalen.Business.IconAvailability
{
    public class PageWithIcons
    {
        public int PageId { get; set; }
        public List<int> IconIdList { get; set; }
    }
}