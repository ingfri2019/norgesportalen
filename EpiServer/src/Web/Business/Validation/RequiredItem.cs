﻿using EPiServer.Core;
using EPiServer.SpecializedProperties;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

// http://henrikm.com/custom-episerver-property-validation/

namespace Norgesportalen.Business.Validation
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field , AllowMultiple = false)]
    public sealed class RequiredItem : ValidationAttribute
    {
        public override bool IsValid(object value)
        {

            if (value==null)
                return false;

            if (value as ContentReference != null)
            {
                return true;
            }

            if (value as DateTime? != null)
            {
                DateTime dt = (DateTime)value;
                if (dt.Year>2000)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            string typeName = value.GetType().ToString();
            this.ErrorMessage = String.Format("Validator not implemented for {0}.", typeName);

            return false;
        }


    }
}