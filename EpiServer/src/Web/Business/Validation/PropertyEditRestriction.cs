﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using EPiServer.Core;

namespace Norgesportalen.Business.Validation
{
    public class PropertyEditRestriction : ValidationAttribute, IMetadataAware
    {
        public PropertyEditRestriction(string[] allowedRoles)
        {
            AllowedRoles = allowedRoles;
        }

        public string[] AllowedRoles { get; set; }

        public void OnMetadataCreated(ModelMetadata metadata)
        {
            foreach (string role in AllowedRoles)
            {
                if (EPiServer.Security.PrincipalInfo.CurrentPrincipal.IsInRole(role))
                {
                    return;
                }
            }
            metadata.IsReadOnly = true;
        }

        public override string FormatErrorMessage(string name)
        {
            return "You do not have access to change " + name;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var contentData = validationContext.ObjectInstance as IContentData;
            if (contentData == null)
            {
                //attribute only handles instances of IContentData. return ValidationResult.Success;
            }
            if (!contentData.Property[validationContext.MemberName].IsModified)
            {
                return ValidationResult.Success;
            }
            if (Validate())
            {
                return ValidationResult.Success;
            }
            else { return new ValidationResult("You do not have access"); }
        }

        public override bool RequiresValidationContext => true;

        public bool Validate()
        {
            foreach (string role in AllowedRoles)
            {
                if (EPiServer.Security.PrincipalInfo.CurrentPrincipal.IsInRole(role))
                {
                    return true;
                }
            }
            return false;
        }
    }
}