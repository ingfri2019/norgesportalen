﻿using EPiServer.Core;
using EPiServer.SpecializedProperties;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

// http://henrikm.com/custom-episerver-property-validation/

namespace Norgesportalen.Business.Validation
{
    /// <summary>    
    /// Limit numbers of items in ContentArea
    /// Add [MaxItemCount(2)] to a prop-definition limits number of items;
    ///   [Display(Name = "Articles")]
    ///   [MaxItemCount(2)]
    ///   public virtual ContentArea ArticlesContentArea { get; set; }
    /// </summary>  
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
    public sealed class MaxItemCount : ValidationAttribute
    {
        private readonly int _limit;
        public int Limit
        {
            get { return _limit; }
        }

        public MaxItemCount(int limit)
        {
            _limit = limit;
        }

        public MaxItemCount(int limit, string customErrorMessage) : this(limit)
        {
            base.ErrorMessage = customErrorMessage;
        }

        public override bool IsValid(object value)
        {
            if (value as ContentArea != null)
                return ValidateContentArea(value as ContentArea);

            if (value as LinkItemCollection != null)
                return ValidateLinkItemCollection(value as LinkItemCollection);

            return true;
        }

        private bool ValidateContentArea(ContentArea contentArea)
        {
            if (contentArea == null || contentArea.Items == null || !contentArea.Items.Any())
                return true;

            return contentArea.Items.Count <= Limit;
        }

        private bool ValidateLinkItemCollection(LinkItemCollection collection)
        {         
            if (collection == null || !collection.Any())
                return true;

            return collection.Count <= Limit;
        }

    }
}