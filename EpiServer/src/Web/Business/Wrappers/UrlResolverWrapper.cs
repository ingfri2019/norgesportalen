﻿using EPiServer.Core;
using EPiServer.ServiceLocation;
using EPiServer.Web.Routing;

namespace Norgesportalen.Business.Wrappers
{
    [ServiceConfiguration(typeof(IUrlResolverWrapper))]
    public class UrlResolverWrapper : IUrlResolverWrapper

    {
        private readonly UrlResolver _urlResolver;

        public UrlResolverWrapper()
        {
            _urlResolver = ServiceLocator.Current.GetInstance<UrlResolver>();
        }

        public string GetVirtualPath(ContentReference contentLink, string language)
        {
            return _urlResolver.GetVirtualPath(contentLink, language).VirtualPath;
        }

        public string GetCurrentSiteUrl()
        {
            return EPiServer.Web.SiteDefinition.Current.SiteUrl.ToString();
        }
    }

    public interface IUrlResolverWrapper
    {
        string GetVirtualPath(ContentReference contentLink, string language);
        string GetCurrentSiteUrl();
    }
}