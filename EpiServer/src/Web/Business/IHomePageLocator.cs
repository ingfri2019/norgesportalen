﻿using EPiServer.Core;
using Norgesportalen.Models.Pages;

namespace Norgesportalen.Business
{
    public interface IHomePageLocator
    {
        HomePage TryFindHomePage(ContentReference currentContentLink);
        CountriesOverviewPage TryFindCountriesOverviewPage(ContentReference currentContentLink);
    }
}
