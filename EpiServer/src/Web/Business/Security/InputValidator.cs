﻿using System;
using System.Text.RegularExpressions;
using EPiServer.ServiceLocation;

namespace Norgesportalen.Business.Security
{
    [ServiceConfiguration(typeof(IInputValidator))]
    public class InputValidator : IInputValidator
    {
        public bool IsValidEmail(string email)
        {
            if (!string.IsNullOrEmpty(email))
            {

                //string simplevalidation = @"/^.+@.+\..+$/";

                string emailvalidation = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                                         @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                                            @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
                Regex re = new Regex(emailvalidation);
                if (re.IsMatch(email))
                {
                    return true;
                }
            }
            return false;
        }

        public bool IsValidGuid(string guid)
        {
            if (!string.IsNullOrEmpty(guid))
            {
                Guid convertedGuid = Guid.Empty;
                bool isValidGuid = Guid.TryParse(guid, out convertedGuid);

                return isValidGuid;
            }
            return false;
        }
    }

    public interface IInputValidator
    {
        bool IsValidEmail(string email);
        bool IsValidGuid(string guid);
    }
}