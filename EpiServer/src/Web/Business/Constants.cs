﻿using EPiServer.Shell.ObjectEditing.EditorDescriptors;

namespace Norgesportalen.Business
{
    public static class Constants
    {
        public static class ImageSize
        {
            public const string Newsletter = "newsletter";
            public const string Tiny = "tiny";
            public const string Small = "small";
            public const string Medium = "medium";
            public const string Large = "large";
            public const string Xlarge = "xlarge";
            public const string Icon = "icon";
            public const string HeroPoster = "HeroPoster";
            public const string MainImageMiddle = "MainImageMiddle";
            public const string CarouselMain = "carouselmain";
            public const string CarouselThumbnail = "carouselthumbnail";
            public const string MainImageTop = "mainimagetop";
        }
        public static class ListAlternatives
        {
            public const string ListView = "ListView";
            public const string GridView = "GridView";
        }

        public static class Roles
        {            
            public const string CmsAdmins = "CmsAdmins";
        }

        public static class UIHint
        {
            public const string FloatingString = "FloatingString";
            public const string LineBreakString = "LineBreakString";
        }

        public static class ImageFile
        {
            public const int AltTextLength = 100;
            public const int ImageTextLength = 300;
            public const int CreditLength = 100;
        }
    }
}