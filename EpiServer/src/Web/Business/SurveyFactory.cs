﻿using DSS.PageSurvey.Models.Pages;
using DSS.PageSurvey.Models.ViewModels;
using DSS.PageSurvey.Services;
using EPiServer.Core;
using Norgesportalen.Models.Pages.BaseClasses;


namespace Norgesportalen.Business
{
    public class SurveyFactory
    {
        public SurveyViewModel CreateSurveyViewModel(PageData page)
        {
            var surveyPage = page as SurveyPage;
            if (surveyPage != null)
            {
                return new SurveyViewModel(surveyPage, "Kommentaren inneholder ulovlige tegn! Prøv å fjerne spesialtegn.", "Send", "Det er ikke deg, det er oss.", "Det oppsto en uventet feil med serveren. Prøv igjen senere.");
            }

            var currentSurvey = SurveyService.Instance.GetActivePageSurvey(page);
            if (currentSurvey == null)
            {
                return null;
            }

            var model = new SurveyViewModel(currentSurvey, "Kommentaren inneholder ulovlige tegn! Prøv å fjerne spesialtegn.", "Send", "Det er ikke deg, det er oss.", "Det oppsto en uventet feil med serveren. Prøv igjen senere.");
            return model;
        }
    }
}