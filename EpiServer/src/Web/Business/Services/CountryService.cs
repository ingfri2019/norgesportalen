﻿using EPiServer;
using EPiServer.Core;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using EPiServer.SpecializedProperties;
using EPiServer.ServiceLocation;
using EPiServer.Web.Routing;
using Norgesportalen.Models.Pages;
using System.Runtime.Caching;
using Norgesportalen.Models.ViewModels;

namespace Norgesportalen.Business.Services
{
    [ServiceConfiguration(typeof(ICountryService))]
    public class CountryService : ICountryService
    {
        private readonly IContentRepository _contentRepository;
        private readonly UrlResolver _urlResolver;

        private ObjectCache _cache = MemoryCache.Default;
        private object _lock = new object();

        public CountryService()
        {
            _contentRepository = ServiceLocator.Current.GetInstance<IContentRepository>();
            _urlResolver = ServiceLocator.Current.GetInstance<UrlResolver>();
        }

        public CountryService(IContentRepository contentRepository)
        {
            _contentRepository = contentRepository;
            _urlResolver = ServiceLocator.Current.GetInstance<UrlResolver>();
        }

        public List<CountriesOverviewViewModel.SimplePageInfo> GetFilteredCountryList(ContentReference startPage)
        {
            var unfilteredEmbassies = GetHomePagesUnderStartPage(startPage);
            var countriesFilteredForDisplay = FilterForDisplay(unfilteredEmbassies);

            var list = new List<CountriesOverviewViewModel.SimplePageInfo>();
            foreach (var page in countriesFilteredForDisplay)
            {
                var spi = new CountriesOverviewViewModel.SimplePageInfo();
                spi.Name = page.Name;
                spi.ShortText = string.Empty;
                spi.Website = UrlResolver.Current == null ? string.Empty : UrlResolver.Current.GetUrl(page.ContentLink);
                list.Add(spi);
            }

            return list;
        }

        public List<string> GetFilteredCountryNames(ContentReference startPage)
        {
            var unfilteredCountries = GetHomePagesUnderStartPage(startPage);
            var embassiesFilteredForDisplay = FilterForDisplay(unfilteredCountries);

            var nameList = new List<string>();
            foreach (var page in embassiesFilteredForDisplay)
            {
                nameList.Add(page.Name);
            }
            return nameList;
        }

        public List<CountriesOverviewViewModel.CountrySiteCategory> GetFilteredAlphaSortedCountrySiteList(ContentReference startPage)
        {
            var unfilteredEmbassies = GetHomePagesUnderStartPage(startPage);
            var embassiesFilteredForDisplay = FilterForDisplay(unfilteredEmbassies);
            var list = CreateEmbassySiteCategoryList(embassiesFilteredForDisplay);

            return AlphaSort(list);
        }

        public List<CountriesOverviewViewModel.CountrySiteCategory> GetEmbassiesThatContain(ContentReference startPage, string name)
        {
            var countries = GetFilteredAlphaSortedCountrySiteList(startPage);
            if (countries == null)
            {
                return new List<CountriesOverviewViewModel.CountrySiteCategory>();
            }

            var embassiesThatContain = new List<CountriesOverviewViewModel.CountrySiteCategory>();

            foreach (CountriesOverviewViewModel.CountrySiteCategory cat in countries)
            {
                var catThatContains = new CountriesOverviewViewModel.CountrySiteCategory { Letter = cat.Letter };
                catThatContains.Embassies = new List<CountriesOverviewViewModel.CountrySiteCategory.CountrySite>();

                var embList = new List<CountriesOverviewViewModel.CountrySiteCategory.CountrySite>();

                embList.AddRange(cat.Embassies.Where(x => x.Name.ToLower().Contains(name.ToLower())).ToList());

                if (embList.Count <= 0) continue;

                catThatContains.Embassies.AddRange(embList);
                embassiesThatContain.Add(catThatContains);
            }

            return embassiesThatContain;
        }

        private List<HomePage> GetHomePagesUnderStartPage(ContentReference startPage)
        {

            var key = "CachedHomePagesUnderStartPage";

            var homePagesUnderStartPage = _cache[key] as List<HomePage>;

            // Check whether the value exists
            if (homePagesUnderStartPage == null)
            {
                lock (_lock)
                {
                    // Try to get the object from the cache again
                    homePagesUnderStartPage = _cache[key] as List<HomePage>;

                    // Double-check that another thread did 
                    // not call the DB already and load the cache
                    if (homePagesUnderStartPage == null)
                    {
                        // Get the list from the DB
                        // http://world.episerver.com/blogs/Johan-Bjornfot/Dates1/2015/2/changes-regarding-language-handling-during-content-loading-in-cms8/
                        homePagesUnderStartPage = _contentRepository.GetChildren<HomePage>(startPage, CultureInfo.InvariantCulture).ToList();

                        // Add the list to the cache
                        _cache.Set(key, homePagesUnderStartPage, DateTimeOffset.Now.AddMinutes(2));
                    }
                }
            }

            return homePagesUnderStartPage;
        }

        public List<CountriesOverviewViewModel.SimplePageInfo> GetFilteredConsulateGeneralList(ContentArea consulateGeneralList)
        {
            if (consulateGeneralList == null || !consulateGeneralList.FilteredItems.Any())
            {
                return new List<CountriesOverviewViewModel.SimplePageInfo>();
            }

            List<PageData> pageList = new List<PageData>();

            // Try to resolve the link list back to pages in EPiServer:
            foreach (ContentAreaItem contentAreaItem in consulateGeneralList.FilteredItems)
            {
                if (!_contentRepository.TryGet(contentAreaItem.ContentLink, out IContentData item))
                {
                    continue;
                }
                if (item is PageData page)
                {
                    pageList.Add(page);
                }
            }

            return CreateSimpleConsulateGeneralInfo(pageList);
        }

        private List<HomePage> FilterForDisplay(List<HomePage> pages)
        {
            List<HomePage> filteredPages = pages.FilterForDisplay(true, true).ToList();
            return filteredPages;
        }


        public List<CountriesOverviewViewModel.CountrySiteCategory> AlphaSort(IEnumerable<CountriesOverviewViewModel.CountrySiteCategory.CountrySite> pages)
        {
            if (pages == null)
            {
                return new List<CountriesOverviewViewModel.CountrySiteCategory>();
            }

            var sortedList = pages.OrderBy(x => x.Name).ToList();
            var categorized = new List<CountriesOverviewViewModel.CountrySiteCategory>();

            foreach (CountriesOverviewViewModel.CountrySiteCategory.CountrySite embassySite in sortedList)
            {
                char categoryName = embassySite.Name.ToUpper()[0];
                var category = categorized.Where(c => c.Letter == categoryName);

                if (!category.Any())
                {
                    var newCategory = new CountriesOverviewViewModel.CountrySiteCategory
                    {
                        Letter = categoryName,
                        Embassies = new List<CountriesOverviewViewModel.CountrySiteCategory.CountrySite> { embassySite }
                    };
                    categorized.Add(newCategory);
                }
                else
                {
                    category.First().Embassies.Add(embassySite);
                }
            }

            return categorized.ToList();
        }


        public IEnumerable<CountriesOverviewViewModel.CountrySiteCategory.CountrySite> CreateEmbassySiteCategoryList(List<HomePage> pages)
        {
            if (pages == null)
            {
                return new List<CountriesOverviewViewModel.CountrySiteCategory.CountrySite>();
            }

            var unsortedList = new List<CountriesOverviewViewModel.CountrySiteCategory.CountrySite>();

            foreach (HomePage page in pages)
            {

                string name = page.Name;
                if (page.NameInCountriesOverview != null && page.NameInCountriesOverview != string.Empty)
                {
                    name = page.NameInCountriesOverview;
                }

                // fix test, todo: mock page.masterlangugage
                string masterLang = "en";
                if (page.MasterLanguage != null)
                    masterLang = page.MasterLanguage.Name;
                // ------------------------------------------

                unsortedList.Add(new CountriesOverviewViewModel.CountrySiteCategory.CountrySite()
                {
                    Name = name,
                    Website = _urlResolver.GetUrl(page.ContentLink, masterLang)

            });
            }

            return unsortedList;
        }

        public List<CountriesOverviewViewModel.SimplePageInfo> CreateSimpleConsulateGeneralInfo(List<PageData> pages)
        {
            if (pages == null)
            {
                return new List<CountriesOverviewViewModel.SimplePageInfo>();
            }

            var simplePageInfoList = new List<CountriesOverviewViewModel.SimplePageInfo>();

            foreach (var consulateGeneralPage in pages)
            {
                var spi = new CountriesOverviewViewModel.SimplePageInfo();
                spi.Name = consulateGeneralPage.Name;
                spi.Website = UrlResolver.Current == null
                    ? string.Empty
                    : UrlResolver.Current.GetUrl(consulateGeneralPage.ContentLink);

                if (consulateGeneralPage is ContactPage)
                {
                    spi.ShortText = ((ContactPage)consulateGeneralPage).TeaserText;
                }
                else if (consulateGeneralPage is Models.Pages.BaseClasses.PageBase)
                {
                    spi.ShortText = ((Models.Pages.BaseClasses.PageBase)consulateGeneralPage).ShortIntro;
                }

                simplePageInfoList.Add(spi);
            }

            return simplePageInfoList.ToList();
        }


    }
}