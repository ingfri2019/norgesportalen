﻿using EPiServer.Core;
using System.Collections.Generic;
using EPiServer.SpecializedProperties;
using Norgesportalen.Models.Pages;
using Norgesportalen.Models.ViewModels;

namespace Norgesportalen.Business.Services
{
    public interface IDelegationsService
    {
        List<CountriesOverviewViewModel.SimplePageInfo> GetFilteredDelegationsList(ContentArea delegationList);
        List<CountriesOverviewViewModel.SimplePageInfo> CreateSimpleDelegationInfo(List<PageData> pages);
    }
}