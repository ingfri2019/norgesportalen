﻿using EPiServer;
using EPiServer.Core;
using EPiServer.ServiceLocation;
using EPiServer.SpecializedProperties;
using EPiServer.Web.Routing;
using Norgesportalen.Models.Pages;
using Norgesportalen.Models.ViewModels;
using System.Collections.Generic;
using System.Linq;

namespace Norgesportalen.Business.Services
{
    [ServiceConfiguration(typeof(IDelegationsService))]
    public class DelegationsService : IDelegationsService
    {
        private readonly IContentRepository _contentRepository;

        public DelegationsService()
        {
            _contentRepository = ServiceLocator.Current.GetInstance<IContentRepository>();
        }

        public DelegationsService(IContentRepository contentRepository)
        {
            _contentRepository = contentRepository;
        }

        public List<CountriesOverviewViewModel.SimplePageInfo> GetFilteredDelegationsList(ContentArea DelegationsLinkList)
        {
            if (DelegationsLinkList == null || !DelegationsLinkList.FilteredItems.Any())
            {
                return new List<CountriesOverviewViewModel.SimplePageInfo>();
            }

            List<PageData> pageList = new List<PageData>();

            // Try to resolve the link list back to pages in EPiServer:
            foreach (ContentAreaItem contentAreaItem in DelegationsLinkList.FilteredItems)
            {
                if (!_contentRepository.TryGet(contentAreaItem.ContentLink, out IContentData item))
                {
                    continue;
                }
                if (item is PageData page)
                {
                    pageList.Add(page);
                }
            }

            return CreateSimpleDelegationInfo(pageList);
        }

        private List<HomePage> FilterForDisplay(List<HomePage> pages)
        {
            List<HomePage> filteredPages = pages.FilterForDisplay(true,true).ToList();
            return filteredPages;
        }

        public List<CountriesOverviewViewModel.SimplePageInfo> CreateSimpleDelegationInfo(List<PageData> pages)
        {
            if (pages == null)
            {
                return new List<CountriesOverviewViewModel.SimplePageInfo>();
            }

            var simplePageInfoList = new List<CountriesOverviewViewModel.SimplePageInfo>();

            foreach (var delegationPage in pages)
            {
                var spi = new CountriesOverviewViewModel.SimplePageInfo();
                spi.Name = delegationPage.Name;
                spi.Website = UrlResolver.Current == null
                    ? string.Empty
                    : UrlResolver.Current.GetUrl(delegationPage.ContentLink);
                if (delegationPage is HomePage)
                {
                    spi.ShortText = ((HomePage)delegationPage).ShortText;
                }
                else if (delegationPage is Models.Pages.BaseClasses.PageBase)
                {
                    spi.ShortText = ((Models.Pages.BaseClasses.PageBase)delegationPage).ShortIntro;
                }

                simplePageInfoList.Add(spi);
            }

            return simplePageInfoList.ToList();
        }

    }
}