﻿using EPiServer.Core;
using EPiServer.SpecializedProperties;
using Norgesportalen.Models.ViewModels;
using System.Collections.Generic;

namespace Norgesportalen.Business.Services
{
    public interface ICountryService
    {

        List<CountriesOverviewViewModel.SimplePageInfo> GetFilteredCountryList(ContentReference startPage);
        List<CountriesOverviewViewModel.SimplePageInfo> GetFilteredConsulateGeneralList(ContentArea delegationList);
        List<string> GetFilteredCountryNames(ContentReference startPage);
        List<CountriesOverviewViewModel.CountrySiteCategory> GetFilteredAlphaSortedCountrySiteList(ContentReference startPage);
        List<CountriesOverviewViewModel.CountrySiteCategory> GetEmbassiesThatContain(ContentReference startPage, string name);
        List<CountriesOverviewViewModel.CountrySiteCategory> AlphaSort(IEnumerable<CountriesOverviewViewModel.CountrySiteCategory.CountrySite> pages);
    }
}