using System.Collections.Generic;
using System.Globalization;
using EPiServer.Core;
using Norgesportalen.Models.Pages;

namespace Norgesportalen.Business.Language
{
    public interface ILanguageChoicesRepository
    {
        //LanguageChoices GetLanguageChoices(PageData page);
        //LanguageChoices GetLanguageChoices(PageData page, HomePage homePage);
        //LanguageChoices GetLanguageChoices(ContentReference page);
        //LanguageChoices GetLanguageChoices(ContentReference page, HomePage homePage);

        LanguageOptions GetLanguageOptions(ContentReference contentLink);

        LanguageOptions CreateLanguageOptions(PageData page, HomePage homePage,
            IEnumerable<CultureInfo> pageLanguages,
            IEnumerable<CultureInfo> homePageLanguages);
    }
}