﻿using EPiServer;
using EPiServer.Core;
using EPiServer.Framework.Localization;
using EPiServer.ServiceLocation;
using Norgesportalen.Business.Wrappers;
using Norgesportalen.Models.Pages;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Norgesportalen.Business.Language
{
    [ServiceConfiguration(typeof(ILanguageChoicesRepository))]
    public class LanguageChoicesRepository : ILanguageChoicesRepository
    {
        private readonly IContentRepository _contentRepository;
        private readonly IHomePageLocator _homePageLocator;
        private readonly IUrlResolverWrapper _urlResolverWrapper;

        public LanguageChoicesRepository()
        {
            _contentRepository = ServiceLocator.Current.GetInstance<IContentRepository>();
            _homePageLocator = ServiceLocator.Current.GetInstance<IHomePageLocator>();
            _urlResolverWrapper = ServiceLocator.Current.GetInstance<IUrlResolverWrapper>();
        }

        public LanguageChoicesRepository(IUrlResolverWrapper urlResolverWrapper)
        {
            _contentRepository = ServiceLocator.Current.GetInstance<IContentRepository>();
            _homePageLocator = ServiceLocator.Current.GetInstance<IHomePageLocator>();
            _urlResolverWrapper = urlResolverWrapper;
        }

        public LanguageOptions GetLanguageOptions(ContentReference contentLink)
        {
            var page = _contentRepository.Get<PageData>(contentLink);
            HomePage homePage = _homePageLocator.TryFindHomePage(page.ContentLink);

            if (homePage == null)
            {
                return new LanguageOptions();
            }

            IEnumerable<CultureInfo> pageLanguages = page.ExistingLanguages;

            //Need to get the pagedata of every languageversion in order to run FilterForDisplay, so there are no links to homepages that should not be available to the end user (unpusblished versions, access rights..)
            var homePageCulturesAvailable = homePage.ExistingLanguages
                                            .Select(lang => homePage.GetPageLanguage(lang.ToString()))
                                            .FilterForDisplay()
                                            .Select(p => p.Language); 

            return CreateLanguageOptions(page, homePage, pageLanguages, homePageCulturesAvailable);
        }

        public LanguageOptions CreateLanguageOptions(PageData page, 
                                                    HomePage homePage,
                                                    IEnumerable<CultureInfo> pageLanguages,
                                                    IEnumerable<CultureInfo> homePageLanguages)
        {
            LanguageOptions options = new LanguageOptions();

            options.Options = CreateListOfLanguageOption(page, homePage, pageLanguages, homePageLanguages);
            options.MoreThanOneOption = options.Options.Count > 1;

            return options;
        }

        public List<LanguageOption> CreateListOfLanguageOption(PageData page, HomePage homePage, IEnumerable<CultureInfo> pageLanguages, IEnumerable<CultureInfo> homePageLanguages)
        {
            List<LanguageOption> options = new List<LanguageOption>();

            foreach (var lang in homePageLanguages)
            {
                LanguageOption option = new LanguageOption();

                option.DisplayText = lang.NativeName;
                option.HomePageUrlForLanguageCode = GetFullUrl(homePage, lang.Name); ;
                option.LanguageCode = lang.Name.ToLower();

                option.CurrentPageIsPublishedInThisLanguageCode = pageLanguages
                    .Any(x => string.Equals(x.Name, lang.Name, StringComparison.OrdinalIgnoreCase));

                if (option.CurrentPageIsPublishedInThisLanguageCode)
                {
                    option.DisplayTextPageMissingInLangCode = string.Empty;
                    option.PageUrlForLanguageCode = GetFullUrl(page, lang.Name); ;
                }
                else
                {
                    option.DisplayTextPageMissingInLangCode = LocalizationService.Current.GetString("/Menu/pageIsNotPublished") + lang.NativeName;
                    option.PageUrlForLanguageCode = string.Empty;
                }
                options.Add(option);
            }

            return options;
        }

        private string GetFullUrl(PageData page, string lang)
        {
            var siteBaseUrl = _urlResolverWrapper.GetCurrentSiteUrl();
            return siteBaseUrl + _urlResolverWrapper.GetVirtualPath(page.ContentLink, lang);
        }

     
    }

    public class LanguageChoices
    {
        public bool MoreThanOneLanguage { get; set; }
        public List<LanguageChoiceItem> Languages { get; set; }
        public List<HomePageChoiceItem> HomePageLanguages { get; set; }

        public LanguageChoices()
        {
            MoreThanOneLanguage = false;
            Languages = new List<LanguageChoiceItem>();
            HomePageLanguages = new List<HomePageChoiceItem>();
        }
    }

    public class LanguageChoiceItem
    {
        public string Name { get; set; }  //English
        public string Code { get; set; } //en
        public string DisplayText { get; set; } //English version of page || This page does not exist in english  (in english for french, german, etc too???)
        public bool IsPublished { get; set; } //page exists in this language (en for this example)
    }

    public class HomePageChoiceItem
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string DisplayText { get; set; }
    }

    public class LanguageOptions
    {
        public bool MoreThanOneOption { get; set; }
        public List<LanguageOption> Options { get; set; }

        public LanguageOptions()
        {
            MoreThanOneOption = false;
            Options = new List<LanguageOption>();
        }
    }

    public class LanguageOption
    {
        public string LanguageCode { get; set; }
        public string DisplayText { get; set; }
        public bool CurrentPageIsPublishedInThisLanguageCode { get; set; }
        public string DisplayTextPageMissingInLangCode { get; set; }
        public string PageUrlForLanguageCode { get; set; }
        public string HomePageUrlForLanguageCode { get; set; }



    }


}