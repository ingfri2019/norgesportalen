﻿using EPiServer;
using EPiServer.Core;
using EPiServer.Web;
using EPiServer.Web.Routing;
using Norgesportalen.Business.Language;
using Norgesportalen.Business.Menu;
using Norgesportalen.Helpers;
using Norgesportalen.Models.Pages;
using Norgesportalen.Models.ViewModels;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;
using Norgesportalen.Business.Initialization;
using Norgesportalen.Models.Pages.BaseClasses;

namespace Norgesportalen.Business
{
    public class PageViewContextFactory
    {
        private readonly IContentLoader _contentLoader;
        private readonly UrlResolver _urlResolver;
        private readonly IHomePageLocator _homePageLocator;
        private readonly IMenuRepository _menuRepository;
        private readonly ILanguageChoicesRepository _langChoiceRepository;
        private readonly SurveyFactory _surveyFactory;


        public PageViewContextFactory(IContentLoader contentLoader, UrlResolver urlResolver, IHomePageLocator homePageLocator, IMenuRepository menuRepository, ILanguageChoicesRepository languageChoicesRepository)
        {
            _contentLoader = contentLoader;
            _urlResolver = urlResolver;
            _homePageLocator = homePageLocator;
            _menuRepository = menuRepository;
            _langChoiceRepository = languageChoicesRepository;
            _surveyFactory = new SurveyFactory();
        }

        public virtual LayoutModel CreateLayoutModel(ContentReference currentContentLink, RequestContext requestContext, IPageViewModel<SitePageData> model)
        {
            HomePage homePage = _homePageLocator.TryFindHomePage(currentContentLink);
            CountriesOverviewPage overviewPage = _homePageLocator.TryFindCountriesOverviewPage(currentContentLink);
            
            LayoutModel layoutModel;

            if (homePage != null)
            {
                layoutModel = new LayoutModel
                {
                    LogotypeLinkUrl = new MvcHtmlString(_urlResolver.GetUrl(homePage.ContentLink)),
                    Logo = homePage.HeaderLogo,
                    Menu = _menuRepository.GetMenu(homePage),
                    HomePage = homePage,
                    HomePageTitle = homePage.HeaderLogo.LogoTitle,
                    CurrentPageIsHomePage = homePage.ContentLink.Equals(currentContentLink),
                    LanguageOptions = _langChoiceRepository.GetLanguageOptions(currentContentLink),
                    SearchPageRouteValues = requestContext.GetPageRoute(homePage.SearchPageLink)
                };
            }
            else
            {
                layoutModel = new LayoutModel
                {
                    LogotypeLinkUrl = new MvcHtmlString(string.Empty),
                    Menu = new Menu.Menu(),
                    HomePage = null,
                    HomePageTitle = string.Empty,
                    LanguageOptions = new LanguageOptions()
                };               
            }

            layoutModel.SurveyViewModel = _surveyFactory.CreateSurveyViewModel(model.CurrentPage);
            layoutModel.GdprPopupPage = GetGdprPopupPage();

            if (overviewPage != null)
            {
                layoutModel.CurrentPageIsCountriesOverviewPage = overviewPage.ContentLink.Equals(currentContentLink);

                if (layoutModel.CurrentPageIsHomePage)
                {
                    layoutModel.NationalEmergencyArea = overviewPage.NatEmergencyContentArea;
                }
                else
                {
                    if (overviewPage.ShowEmergencyOnSubpages && !layoutModel.CurrentPageIsCountriesOverviewPage)
                    {
                        layoutModel.NationalEmergencyArea = overviewPage.NatEmergencyContentArea;
                    }
                    else
                    {
                        layoutModel.NationalEmergencyArea = null;
                    }
                }
            }
            return layoutModel;
        }      

        public virtual IContent GetSection(ContentReference contentLink)
        {
            var currentContent = _contentLoader.Get<IContent>(contentLink);
            if (currentContent.ParentLink != null && currentContent.ParentLink.CompareToIgnoreWorkID(SiteDefinition.Current.StartPage))
            {
                return currentContent;
            }

            return _contentLoader.GetAncestors(contentLink)
                .OfType<PageData>()
                .SkipWhile(x => x.ParentLink == null || !x.ParentLink.CompareToIgnoreWorkID(SiteDefinition.Current.StartPage))
                .FirstOrDefault();
        }

        private GdprPopupPage GetGdprPopupPage()
        {
            var pageRef = new PageReference(ApplicationSettings.Instance.GdprFooterPageId);
            if (pageRef != null)
            {
                GdprPopupPage page;
                _contentLoader.TryGet(pageRef, out page);
                if (page != null)
                {
                    return page;
                }
            }
            return null;
        }
    }
}
