﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using StructureMap;

namespace Norgesportalen.Business
{
    public class StructureMapDependencyResolver : IDependencyResolver
    {
        public IContainer Container { get; set; }

        public StructureMapDependencyResolver(IContainer container)
        {
            Container = container;
        }

        public object GetService(Type serviceType)
        {
            if (serviceType.IsInterface || serviceType.IsAbstract)
            {
                return GetInterfaceService(serviceType);
            }
            return GetConcreteService(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return Container.GetAllInstances(serviceType).Cast<object>();
        }

        private object GetInterfaceService(Type serviceType)
        {
            return Container.TryGetInstance(serviceType);

        }

        private object GetConcreteService(Type serviceType)
        {
            try
            {
                // Can't use TryGetInstance here because it won’t create concrete types
                return Container.GetInstance(serviceType);
            }
            catch (StructureMapException)
            {
                return null;
            }

        }
    }
}