﻿using Norgesportalen.Models.ViewModels;

namespace Norgesportalen.Business
{
    interface IModifyLayout
    {
        void ModifyLayout(LayoutModel layoutModel);
    }
}
