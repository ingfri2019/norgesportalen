﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EPiServer;
using EPiServer.Cms.Shell.UI.Rest.ContentQuery;
using EPiServer.Core;
using EPiServer.Filters;
using EPiServer.Security;
using EPiServer.ServiceLocation;
using EPiServer.Shell.ContentQuery;
using Norgesportalen.Models.Pages;

namespace Norgesportalen.Business
{

    [ServiceConfiguration(typeof(IContentQuery))]
    public class ChildrenByUserPermissionsQuery : GetChildrenQuery
    {
        public override int Rank { get { return 100; } } //The default queries have 0 as rank

        public ChildrenByUserPermissionsQuery(IContentQueryHelper queryHelper, IContentRepository contentRepository, LanguageSelectorFactory languageSelectorFactory) : base(queryHelper, contentRepository, languageSelectorFactory)
        {
        }

        protected override IEnumerable<IContent> GetContent(ContentQueryParameters parameters)
        {
            //TODO: If user is administrator, skip filtering?
            //TODO: If central content, skip filtering? Need own pagetype..


            var filteredContents = base.GetContent(parameters) 
                .Where(x => (x is FolderPage) || x is CountriesOverviewPage || FilterAccess.QueryDistinctAccessEdit(x, AccessLevel.Publish));

            return filteredContents;
        }
    }
}

