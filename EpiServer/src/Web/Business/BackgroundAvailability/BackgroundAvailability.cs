﻿using System.Collections.Generic;
using EPiServer.Data;
using EPiServer.Data.Dynamic;

namespace Norgesportalen.Business.BackgroundAvailability
{
    [EPiServerDataStore(AutomaticallyRemapStore = true)]
    public class BackgroundAvailability
    {
        public Identity Id { get; set; }
        /// <summary>
        /// List of ContentReference IDs to background images.
        /// </summary>
        public List<int> Backgrounds { get; set; }
    }
}
