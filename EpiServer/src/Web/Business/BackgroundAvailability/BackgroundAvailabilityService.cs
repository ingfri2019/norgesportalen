﻿using EPiServer;
using EPiServer.Core;
using EPiServer.Logging.Compatibility;
using EPiServer.ServiceLocation;
using Norgesportalen.Models.Media;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Norgesportalen.Business.BackgroundAvailability
{
    public class BackgroundAvailabilityService
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(BackgroundAvailabilityService));
        private readonly BackgroundStoreService _store;
        private readonly IContentRepository _iContentRepository;
        private BackgroundAvailability _backgroundAvailability;

        public BackgroundAvailabilityService()
        {
            _store = BackgroundStoreService.Instance;
            _iContentRepository = ServiceLocator.Current.GetInstance<IContentRepository>();
            _backgroundAvailability = GetOrCreateBackgroundAvailability();
        }

        public IEnumerable<ImageFile> GetBackgroundList()
        {
            if (_backgroundAvailability.Backgrounds == null || !_backgroundAvailability.Backgrounds.Any())
            {
                Log.WarnFormat($"No backgrounds");
                return new List<ImageFile>();
            }

            var backgroundList = new List<ImageFile>();

            foreach (var backgroundId in _backgroundAvailability.Backgrounds)
            {
                if (_iContentRepository.TryGet(new ContentReference(backgroundId), out ImageFile image))
                {
                    backgroundList.Add(image);
                }
            }
            return backgroundList;
        }

        public void AddOrUpdateIconToStore(ImageFile imageFile)
        {
            if (imageFile.IsBackground)
            {
                AddBackground(imageFile.ContentLink.ID);
            }
            else
            {
                RemoveBackground(imageFile.ContentLink.ID);
            }            
            
            _store.AddOrUpdate(_backgroundAvailability);
        }

        public void RemoveBackground(int imageFileId)
        {
            if (_backgroundAvailability.Backgrounds != null && _backgroundAvailability.Backgrounds.Contains(imageFileId))
            {
                _backgroundAvailability.Backgrounds.Remove(imageFileId);
            }
        }

        public void AddBackground(int imageFileId)
        {
            if (_backgroundAvailability.Backgrounds == null)
            {
                _backgroundAvailability.Backgrounds = new List<int>();
            }

            if  (!_backgroundAvailability.Backgrounds.Contains(imageFileId))
            {
                _backgroundAvailability.Backgrounds.Add(imageFileId);
            }
        }

        private BackgroundAvailability GetOrCreateBackgroundAvailability()
        {
            var backgroundAvailability = _store.Get(new Guid(BackgroundAvailabilityConstants.Id));
            if (backgroundAvailability != null)
                return backgroundAvailability;

            var newBackgroundAvailability = new BackgroundAvailability
            {
                Id = new Guid(BackgroundAvailabilityConstants.Id),
                Backgrounds = new List<int>()
            };

            _store.AddOrUpdate(newBackgroundAvailability);
            return newBackgroundAvailability;
        }
    }
}