﻿using EPiServer.Data.Dynamic;
using System;
using System.Collections.Generic;

namespace Norgesportalen.Business.BackgroundAvailability
{
    public class BackgroundStoreService
    {
  
        public static BackgroundStoreService Instance
        {
            get
            {
                return new BackgroundStoreService();
            }
        }

        private static DynamicDataStore BackgroundStore
        {
            get
            {
                return DynamicDataStoreFactory.Instance.CreateStore(typeof(BackgroundAvailability));

            }
        }

        public BackgroundAvailability Get(Guid id)
        {
            return BackgroundStore.Load<BackgroundAvailability>(id);
        }

        public IEnumerable<BackgroundAvailability> GetAll()
        {

            return BackgroundStore.LoadAll<BackgroundAvailability>();

        }

        public void AddOrUpdate(BackgroundAvailability backgroundAvailability)
        {
            BackgroundStore.Save(backgroundAvailability);
        }

        public void Delete(Guid id)
        {
            BackgroundStore.Delete(id);
        }

        public void DeleteAll()
        {
            BackgroundStore.DeleteAll();
        }

        public void DeleteStore()
        {
            DynamicDataStoreFactory.Instance.DeleteStore(typeof(BackgroundAvailability), true);
        }
    }
}
