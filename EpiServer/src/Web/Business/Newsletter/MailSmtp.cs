﻿using System;
using System.Web;
using System.Net.Mail;
using System.Net;

namespace Norgesportalen.Business.Newsletter
{
    public static class MailSmtp
    {       
        public static bool SendMail(MailMessage mail)
        {
            try
            {
                mail.BodyEncoding = System.Text.Encoding.UTF8;
                SmtpClient client = new SmtpClient { Timeout = 5 * 1000 };
                client.Send(mail);

                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("EXCEPTION:" + ex);                
            }
            return false;
        }
    }
}
