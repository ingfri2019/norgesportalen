﻿using System;
using System.Collections.Generic;
using System.Linq;
using EPiServer.Data.Dynamic;
using EPiServer.Logging.Compatibility;

namespace Norgesportalen.Business.Newsletter
{
    public class ConfirmEmailStoreService
    {
        private Guid StoreId = new Guid("a3bdf437-29fd-47ba-ab64-c375526b1fee");
        private static ConfirmEmailStoreService _instance;
        private static readonly ILog Log = LogManager.GetLogger(typeof(ConfirmEmailStoreService));

        public static ConfirmEmailStoreService Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ConfirmEmailStoreService();                    
                }
                return _instance;
            }
        }

        public ConfirmEmailStoreService()
        {
            if (Get()==null)
            {
                AddOrUpdate(new ConfirmStoreData { Id = StoreId, EmailList = new List<ConfData>() });
            }
        }

        private DynamicDataStore GetEmailStore() => DynamicDataStoreFactory.Instance.CreateStore(typeof(ConfirmStoreData));

        public ConfirmStoreData Get() => GetEmailStore().Load<ConfirmStoreData>(StoreId);

        public ConfirmStoreData GetFiltered()
        {
            var emailData = Get();

            if (emailData != null)
            {
                int count = emailData.EmailList.Count;
                emailData.EmailList = emailData.EmailList.Where(x => x.Timestamp.AddDays(1) > DateTime.UtcNow).ToList();
                if (emailData.EmailList.Count < count)
                {
                    // save filtered list
                    AddOrUpdate(emailData);
                }
            } else
            {
                Log.Error("Failed to get Confirm Email DynamicDataStoreFactory.");
            }
            return emailData;
        }

        public void AddOrUpdate(ConfirmStoreData confirmEmailData)
        {
            GetEmailStore().Save(confirmEmailData);
        }

        public void Delete()
        {
            GetEmailStore().Delete(StoreId);
        }       

        public void DeleteStore()
        {
            DynamicDataStoreFactory.Instance.DeleteStore(typeof(ConfirmStoreData), true);
        }
    }
}