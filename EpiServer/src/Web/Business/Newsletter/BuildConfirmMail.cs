﻿using System.IO;
using System.Net.Mail;
using System.Web.Hosting;
using PreMailer.Net;
using Norgesportalen.Business.Wrappers;
using Norgesportalen.Models.Pages;


namespace Norgesportalen.Business.Newsletter
{
    public class BuildConfirmMail
    {
        private readonly IUrlResolverWrapper _urlResolverWrapper;

        public BuildConfirmMail(UrlResolverWrapper urlResolver)
        {
            _urlResolverWrapper = urlResolver;
        }

        public MailMessage GetMail(NewsletterConfirmPage confPage, string toAdr, string url, string customerRef)
        {                                
            string subject = string.IsNullOrEmpty(confPage.MailSubject) ? "Confirm your subscription" : confPage.MailSubject;

            string htmlText = ReadHtmlFile("ConfirmLetter.html");
            htmlText = htmlText.Replace("%header%", confPage.MailHeader);
            htmlText = htmlText.Replace("%headersub%", string.IsNullOrEmpty(confPage.MailSubHeader)? "" : confPage.MailSubHeader);            
            htmlText = htmlText.Replace("%siteurl%", _urlResolverWrapper.GetCurrentSiteUrl());            
            htmlText = htmlText.Replace("%title%", string.IsNullOrEmpty(confPage.MailTitle) ? "" : confPage.MailTitle );
            htmlText = htmlText.Replace("%subtitle%", string.IsNullOrEmpty(confPage.MailSubHeader) ? "" : confPage.MailSubHeader);
            htmlText = htmlText.Replace("%confirmationbody-paragraph1%", confPage.MailPara1);// LocalizationService.Current.GetString("/NewsLetter/confirmationbody-paragraph1"));
            htmlText = htmlText.Replace("%confirmationbody-paragraph2%", string.IsNullOrEmpty(confPage.MailPara2) ? "" : confPage.MailPara2 ); //LocalizationService.Current.GetString("/NewsLetter/confirmationbody-paragraph2"));
            htmlText = htmlText.Replace("%buttoninfo%", string.IsNullOrEmpty(confPage.MailButtonIntro) ? "" : confPage.MailButtonIntro);
            htmlText = htmlText.Replace("%buttonlink%", $"{url}?reference={customerRef}");
            htmlText = htmlText.Replace("%buttontext%", confPage.MailButton);

            var message = new MailMessage(confPage.MailSender, toAdr);
            message.Subject = subject;
            message.Body = htmlText;
            message.IsBodyHtml = true;

            // Inline all css
            InlineResult cssInline = PreMailer.Net.PreMailer.MoveCssInline(message.Body);
            message.Body = cssInline.Html;

            return message;
        }

        private string ReadHtmlFile(string htmlFileName) => File.ReadAllText(HostingEnvironment.MapPath($"~/Business/Newsletter/{htmlFileName}"));
    }
}