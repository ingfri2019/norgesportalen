﻿using System;
using System.Net;
using SendGrid;
using System.Net.Mail;
using System.Web.Configuration;

namespace Norgesportalen.Business.Newsletter
{
    public static class MailSendgrid
    {
        public static bool SendMail(MailMessage mail)
        {
            try
            {
                var username = WebConfigurationManager.AppSettings["Newsletter.SendGrid.Username"];
                var password = WebConfigurationManager.AppSettings["Newsletter.SendGrid.Password"];
                var client = new Web(new NetworkCredential(username, password));

                var msg = new SendGridMessage()
                {
                    From = new MailAddress(mail.From.Address, mail.From.DisplayName),
                    Subject = mail.Subject
                };
                if (mail.IsBodyHtml)
                {
                    msg.Html = mail.Body;
                }
                else
                {
                    msg.Text = mail.Body;
                }
                foreach (var adr in mail.To)
                {
                    msg.AddTo(adr.Address);
                }

                client.Deliver(msg);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
           
        }
    }
}