﻿using System;
using System.Collections.Generic;
using EPiServer.Data;
using EPiServer.Data.Dynamic;

namespace Norgesportalen.Business.Newsletter
{
    [EPiServerDataStore(AutomaticallyRemapStore = true)]
    public class ConfirmStoreData
    {
        public Identity Id { get; set; }

        public List<ConfData> EmailList { get; set; }        
    }

    public class ConfData
    {
        public Guid Id { get; set; }

        public string Email { get; set; }

        public int NewsletterListId { get; set; }

        public DateTime Timestamp { get; set; }        
    }
}