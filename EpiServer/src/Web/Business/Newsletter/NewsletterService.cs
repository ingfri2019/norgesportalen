﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using BVNetwork.EPiSendMail.Api;
using BVNetwork.EPiSendMail.DataAccess;
using EPiServer;
using EPiServer.Core;
using EPiServer.Logging.Compatibility;
using EPiServer.ServiceLocation;
using EPiServer.Web.Routing;
using Norgesportalen.Business.Security;
using Norgesportalen.Business.Wrappers;
using Norgesportalen.Models.Pages;

namespace Norgesportalen.Business.Newsletter
{
    [ServiceConfiguration(typeof(INewsLetterService))]
    public class NewsletterService : INewsLetterService
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(NewsletterService));

        private readonly IInputValidator _inputValidator;
        private readonly IContentRepository _contentRepository;
        private UrlResolverWrapper _urlResolverWrapper;

        public NewsletterService(IInputValidator inputValidator, IContentRepository contentRepository, UrlResolverWrapper urlResolverWrapper)
        {
            _inputValidator = inputValidator;
            _contentRepository = contentRepository;
            _urlResolverWrapper = urlResolverWrapper;
        }

        public List<RecipientList> GetNewsletterLists(string input = "")
        {
            var list = RecipientLists.ListAll().ToList();

            if (string.IsNullOrEmpty(input))
                return list;

            var filteredList = list.Where(x => x.Name.ToLower().Contains(input.ToLower())).ToList();

            return filteredList;
        }

        public SubscriptionResult SubscribeToNewsletter(string email, string list)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(email) || !_inputValidator.IsValidEmail(email))
                    return SubscriptionResult.EmailNotValid;

                if (!string.IsNullOrWhiteSpace(list))
                {
                    var splitList = list.Split(',');

                    int NewsletterListId;
                    var isInt = Int32.TryParse(splitList[0], out NewsletterListId);

                    if (isInt)
                    {
                        if (splitList.Count() == 1)
                        {
                            // no Confirm page id, confirm email directly                
                            return new SubscriptionApi().Subscribe(email, NewsletterListId);
                        }
                        return RequestEmailConfirmation(email, splitList[1], NewsletterListId);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("SubscribeToNewsletter error. ", ex);
                return SubscriptionResult.EmailNotValid;
            }
            return SubscriptionResult.RecipientListNotValid;
        }

        private SubscriptionResult RequestEmailConfirmation(string emailAdr, string confPageId, int listIdAsNumber)
        {
            string confPageUrl;
            NewsletterConfirmPage confPage = GetConfPage(confPageId, out confPageUrl);

            if (confPage != null && !string.IsNullOrEmpty(confPageUrl))
            {
                Guid customerRef;
                var confirmEmailStore = ConfirmEmailStoreService.Instance;

                var emailData = confirmEmailStore.GetFiltered();

                if (emailData != null)
                {
                    var em = emailData.EmailList.FirstOrDefault(x => x.Email == emailAdr && x.NewsletterListId == listIdAsNumber);

                    if (em == null)
                    {
                        customerRef = Guid.NewGuid();
                        emailData.EmailList.Add(new ConfData { Id = customerRef, Email = emailAdr.ToLower(), Timestamp = DateTime.UtcNow, NewsletterListId = listIdAsNumber });
                    }
                    else
                    {
#if DEBUG
                        int TimeLimit = 1;
#else
                        int TimeLimit = 5;
#endif
                        if (em.Timestamp.AddMinutes(TimeLimit) > DateTime.UtcNow)
                        {
                            // less than 5 minutes since prev email, just report 'Success'
                            return SubscriptionResult.Success;
                        }
                        else
                        {
                            customerRef = em.Id;
                            em.Timestamp = DateTime.UtcNow;
                        }
                    }

                    var buildConfirmMail = new BuildConfirmMail(_urlResolverWrapper);

                    var confEmail = buildConfirmMail.GetMail(confPage, emailAdr, confPageUrl, customerRef.ToString());

                    if (MailSendgrid.SendMail(confEmail))
                    {  
                        // store the updated email confirmation list
                        confirmEmailStore.AddOrUpdate(emailData);
                        return SubscriptionResult.Success;
                    }
                }
            }             
            return SubscriptionResult.EmailNotValid;            
        }

        private  NewsletterConfirmPage GetConfPage(string confPageId, out string urlString)
        {
            if (!string.IsNullOrEmpty(confPageId))
            {
                ContentReference contentRef = new ContentReference(confPageId);
                if (contentRef != null)
                {
                    urlString = GetExternalUrl(contentRef);
                    return _contentRepository.Get<NewsletterConfirmPage>(contentRef);                    
                }
            }
            urlString = null;
            return null;
        }

        public SubscriptionResult ConfirmNewsletter(string reference)
        {
            if (!string.IsNullOrWhiteSpace(reference))
            {                
                var confirmEmailService = ConfirmEmailStoreService.Instance;
                var emailData = confirmEmailService.GetFiltered();

                var em = emailData.EmailList.FirstOrDefault(x => x.Id.ToString() == reference);

                if (em != null)
                {
                    emailData.EmailList.Remove(em);
                    confirmEmailService.AddOrUpdate(emailData);
                    return new SubscriptionApi().Subscribe(em.Email, em.NewsletterListId);                                        
                }               
            }
            return SubscriptionResult.EmailNotValid;
        }

        public void Unsubscribe(string email, string list)
        {
            if (!_inputValidator.IsValidEmail(email))
                return;

            int NewsletterListId;
            var isInt = Int32.TryParse(list, out NewsletterListId);            

            if (isInt)
            {                
                new SubscriptionApi().Unsubscribe(email, NewsletterListId);
            }
            else
            {
                Log.Warn($"Failed unsubscribe to newsletter list: {list} for email: {email}");
            }
        }

        private static string GetExternalUrl(ContentReference contentReference)
        {
            var internalUrl = UrlResolver.Current.GetUrl(contentReference);
            var url = new UrlBuilder(internalUrl);
            EPiServer.Global.UrlRewriteProvider.ConvertToExternal(url, null, Encoding.UTF8);

            string externalUrl = HttpContext.Current == null
                ? UriSupport.AbsoluteUrlBySettings(url.ToString())
                : HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + url;

            return externalUrl;
        }
    }

    public interface INewsLetterService
    {
        List<RecipientList> GetNewsletterLists(string input = "");
        SubscriptionResult SubscribeToNewsletter(string email, string list);
        SubscriptionResult ConfirmNewsletter(string reference);
        void Unsubscribe(string email, string list);
    }
}