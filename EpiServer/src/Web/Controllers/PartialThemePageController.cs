﻿using EPiServer.Core;
using EPiServer.Framework.DataAnnotations;
using EPiServer.Framework.Web;
using EPiServer.Web.Mvc;
using Norgesportalen.Models.Pages;
using System.Web.Mvc;
using System.Web.SessionState;

namespace Norgesportalen.Controllers
{
    [SessionState(SessionStateBehavior.ReadOnly)]
    [TemplateDescriptor(TemplateTypeCategory = TemplateTypeCategories.MvcPartialController, AvailableWithoutTag = false, Tags = new [] { "List" })]
    public class PartialThemPageController : PageController<ThemePage>
    {
        [ContentOutputCache(Duration = 60)]
        public ActionResult Index(ThemePage currentPage)
        {
            string themePageSmallClass = "panel panel-medium";
            string themePageLargeClass = "panel panel-large";
            string themePageClass = themePageSmallClass;
            string themePageSmallView = "/Views/HomePage/Themes/ThemepageSmall.cshtml";
            string themePageLargeView = "/Views/HomePage/Themes/ThemepageLarge.cshtml";
            string themePageView = themePageSmallView;

            // get the current content area        
            var currentContentArea = ControllerContext.ParentActionViewContext.ViewData.Model as ContentArea;
            if (currentContentArea == null)
            {
                ControllerContext.ParentActionViewContext.ViewData["ChildrenCssClass"] = themePageClass;
                return PartialView(themePageView); //return null or something that indicates there is nothing to return... THIS IS A DUMMY VALUE, THE READER CAN CHANGE IT IF YOU KNOW A BETTER WAY ;)
            }

            int index = 0;
            for (int i = 0; i < currentContentArea.Count; i++)
            {
                if (currentContentArea.Items[i].ContentGuid == (currentPage as IContent).ContentGuid)
                {
                    index = i + 1;
                    break;
                }
            }
            
            int total = currentContentArea.Count;
            

            if (total == 1 || total == 2 || total == 4)
            {
                themePageView = themePageLargeView;
                themePageClass = themePageLargeClass;
            }

            else if (total == 3)
            {
                themePageView = themePageSmallView;
                themePageClass = themePageSmallClass;
            }

            else if (total >= 5)
            {
                switch (index+2)
                {
                    case 1:
                        themePageView = themePageLargeView;
                        themePageClass = themePageLargeClass;
                        break;
                    case 2:
                        themePageView = themePageLargeView;
                        themePageClass = themePageLargeClass;
                        break;
                    case 3:
                        themePageView = themePageSmallView;
                        themePageClass = themePageSmallClass;
                        break;
                    case 4:
                        themePageView = themePageSmallView;
                        themePageClass = themePageSmallClass;
                        break;
                    case 5:
                        themePageView = themePageSmallView;
                        themePageClass = themePageSmallClass;
                        break;
                    default:
                        themePageView = themePageSmallView;
                        themePageClass = themePageSmallClass;
                        break;
                }
            }
            //currentPage.Index = index;

            /* BLIR IKKE SATT PÅ INDEX 0. WHY ? */
            ControllerContext.ParentActionViewContext.ViewData["ChildrenCssClass"] = themePageClass;
            ControllerContext.ParentActionViewContext.ViewData["CssClass"] = themePageClass;
            return PartialView(themePageView,currentPage);
    
        }
    }
}