﻿using EPiServer.Core;
using EPiServer.Framework.Web;
using EPiServer.Search;
using EPiServer.ServiceLocation;
using EPiServer.Web;
using EPiServer.Web.Mvc;
using EPiServer.Web.Routing;
using Norgesportalen.Business;
using Norgesportalen.Business.Search;
using Norgesportalen.Helpers;
using Norgesportalen.Models.Pages;
using Norgesportalen.Models.Pages.BaseClasses;
using Norgesportalen.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.SessionState;

namespace Norgesportalen.Controllers
{
    [SessionState(SessionStateBehavior.ReadOnly)]
    public class SearchPageController : PageControllerBase<SearchPage>
    {
        private const int MaxResults = 150;
        private readonly SearchService _searchService;
        private readonly ContentSearchHandler _contentSearchHandler;
        private readonly UrlResolver _urlResolver;
        private readonly TemplateResolver _templateResolver;
        private readonly IHomePageLocator _homePageLocator;

        public SearchPageController()
        {
            _searchService = ServiceLocator.Current.GetInstance<SearchService>();
            _contentSearchHandler = ServiceLocator.Current.GetInstance<ContentSearchHandler>();
            _templateResolver = ServiceLocator.Current.GetInstance<TemplateResolver>();
            _urlResolver = ServiceLocator.Current.GetInstance<UrlResolver>();
            _homePageLocator = ServiceLocator.Current.GetInstance<IHomePageLocator>();
        }

        private string SearchTerm
        {
            get { return Request.QueryString["q"]; }
        }

        [ValidateInput(false)]
        [ContentOutputCache(Duration = 60)]
        public ViewResult Index(SearchPage currentPage, string q, int page = 1)
        {
            var model = new SearchContentModel(currentPage)
            {
                SearchServiceDisabled = !_searchService.IsActive,
                SearchedQuery = q
            };

            int pageSize = 5;
            var skipNumber = 0;
            
            model.CurrentPageNo = page;

            var homePage = _homePageLocator.TryFindHomePage(currentPage.ContentLink);

            if (!string.IsNullOrWhiteSpace(q) && _searchService.IsActive)
            {
                skipNumber = (page - 1) * pageSize;

                var hits = Search(q.Trim(),
                    new[] { homePage.PageLink },
                    ControllerContext.HttpContext,
                    currentPage.Language.Name).ToList();

                model.Hits = hits.Skip(skipNumber).Take(pageSize);
                model.NumberOfHits = hits.Count;
            }

            if (model.NumberOfHits > 0)
            {
                model.TotalPages = (int)Math.Ceiling((double)model.NumberOfHits / pageSize);

                if (model.TotalPages > 1)
                {
                    for (int i = 1; i < model.TotalPages + 1; i++)
                    {
                        var pagerLink = new CustomLink();

                        pagerLink.LinkText = (i).ToString();
                        if (page == i)
                        {
                            pagerLink.IsActivePage = true;
                        }
                        pagerLink.Route = CreatePagingLink(currentPage, i);

                        model.PagerLinks.Add(pagerLink);
                    }
                    model.PreviousPage = new CustomLink();
                    model.NextPage = new CustomLink();

                    model.PreviousPage.Route = CreatePreviousPagingLinks(currentPage);
                    model.PreviousPage.LinkText = "Forrige";

                    model.NextPage.Route = CreateNextPagingLinks(currentPage);
                    model.NextPage.LinkText = "Neste";


                    if (model.TotalPages >= 5)
                    {
                        model.Items = model.PagerLinks.Skip(model.CurrentPageNo - 1).Take(pageSize);
                    }
                    else
                    {
                        model.Items = model.PagerLinks;
                    }


                }
            }

            return View(model);
        }

        /// <summary>
        /// Performs a search for pages and media and maps each result to the view model class SearchHit.
        /// </summary>
        /// <remarks>
        /// The search functionality is handled by the injected SearchService in order to keep the controller simple.
        /// Uses EPiServer Search. For more advanced search functionality such as keyword highlighting,
        /// facets and search statistics consider using EPiServer Find.
        /// </remarks>
        private IEnumerable<SearchContentModel.SearchHit> Search(string searchText, IEnumerable<ContentReference> searchRoots, HttpContextBase context, string languageBranch)
        {
            var searchResults = _searchService.Search(searchText, searchRoots, context, languageBranch, MaxResults);

            return searchResults.IndexResponseItems.SelectMany(CreateHitModel);
        }

        private IEnumerable<SearchContentModel.SearchHit> CreateHitModel(IndexResponseItem responseItem)
        {
            var content = _contentSearchHandler.GetContent<IContent>(responseItem);
            if (content != null && HasTemplate(content) && IsPublished(content as IVersionable))
            {
                yield return CreatePageHit(content);
            }
        }

        private bool HasTemplate(IContent content)
        {
            return _templateResolver.HasTemplate(content, TemplateTypeCategories.Page);
        }

        private bool IsPublished(IVersionable content)
        {
            if (content == null)
                return true;
            return content.Status.HasFlag(VersionStatus.Published);
        }

        private SearchContentModel.SearchHit CreatePageHit(IContent content)
        {
            return new SearchContentModel.SearchHit
            {
                Title = content.Name,
                Url = _urlResolver.GetUrl(content.ContentLink),
                Excerpt = content is PageBase ? ((PageBase)content).MainIntro : string.Empty
            };
        }
        public RouteValueDictionary CreatePreviousPagingLinks(SearchPage currentPage)
        {
            return CreatePagingLink(currentPage, PagingPage - 1);
        }

        private RouteValueDictionary CreatePagingLink(SearchPage currentPage, int pageIndex)
        {
            return Url.ContentRoute(currentPage.PageLink, new { q = SearchTerm, page = pageIndex });
        }

        public RouteValueDictionary CreateNextPagingLinks(SearchPage currentPage)
        {
            return CreatePagingLink(currentPage, PagingPage + 1);
        }

        private int PagingPage
        {
            get
            {
                if (!int.TryParse(Request.QueryString["page"], out var pagingPage))
                {
                    pagingPage = 1;
                }
                return pagingPage;
            }
        }
    }
}
