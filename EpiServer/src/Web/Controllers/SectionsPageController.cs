﻿using EPiServer;
using EPiServer.Web.Mvc;
using Norgesportalen.Business;
using Norgesportalen.Models.Pages;
using Norgesportalen.Models.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.SessionState;
using npBaseClasses = Norgesportalen.Models.Pages.BaseClasses;

namespace Norgesportalen.Controllers
{
    [SessionState(SessionStateBehavior.ReadOnly)]
    public class SectionsPageController : PageControllerBase<SectionsPage>
    {
        private readonly IContentLoader _contentLoader;

        public SectionsPageController(IContentLoader contentLoader)
        {
            _contentLoader = contentLoader;
        }

        // GET: SectionPage
        [ContentOutputCache(Duration = 60)]
        public ActionResult Index(SectionsPage currentPage)
        {
            if (SetFetchFrom(currentPage))
            {
                return Redirect(this.Request.RawUrl + "&RefreshParent=True");
            }

            currentPage.ServicePages = GetServices(currentPage);

            var model = PageViewModel.Create(currentPage);
            
            return View(model);
        }

        /// <summary>
        /// This function looks for subpages under the current page and will return them if they are of a given type. The current allowed types are
        /// subclasses of PageBase (most content pages).
        /// </summary>
        private List<npBaseClasses.PageBase> GetServices(SectionsPage currentPage)
        {
            List<npBaseClasses.PageBase> resultList = _contentLoader.GetChildren<npBaseClasses.PageBase>(currentPage.ContentLink).FilterForDisplay(true, true).ToList();

            return resultList;
        }

        [Authorize(Roles = "WebAdmins, Administrators")]
        public ActionResult RemoveFetch(SectionsPage currentPage)
        {
            return RemoveFetchFrom(currentPage.ContentLink);
        }
    }
}