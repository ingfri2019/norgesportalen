﻿using EPiServer.Web.Mvc;
using Norgesportalen.Models.Pages.BaseClasses;
using Norgesportalen.Models.ViewModels;
using System.Web.Mvc;
using System.Web.SessionState;

namespace Norgesportalen.Controllers
{
    [SessionState(SessionStateBehavior.ReadOnly)]
    public class BasicPageBaseController : PageControllerBase<BasicPageBase>
    {
        [ContentOutputCache(Duration = 60)]
        public ActionResult Index(BasicPageBase currentPage)
        {
            if (SetFetchFrom(currentPage))
            {
                return Redirect(this.Request.RawUrl + "&RefreshParent=True");
            }

            return View(PageViewModel.Create(currentPage));
        }

        [Authorize(Roles = "WebAdmins, Administrators")]
        public ActionResult RemoveFetch(BasicPageBase currentPage)
        {
            return RemoveFetchFrom(currentPage.ContentLink);
        }
    }
}
