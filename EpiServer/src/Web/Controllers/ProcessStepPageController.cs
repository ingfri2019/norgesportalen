﻿using EPiServer.Web.Mvc;
using Norgesportalen.Models.Pages;
using Norgesportalen.Models.ViewModels;
using System.Web.Mvc;
using System.Web.SessionState;

namespace Norgesportalen.Controllers
{
    [SessionState(SessionStateBehavior.ReadOnly)]
    public class ProcessStepPageController : PageControllerBase<ProcessStepPage>
    {
        [ContentOutputCache(Duration = 60)]
        public ActionResult Index(ProcessStepPage currentPage)
        {

            if (SetFetchFrom(currentPage))
            {
                return Redirect(Request.RawUrl + "&RefreshParent=True");
            }

            var model = PageViewModel.Create(currentPage);
            return View(model);
        }


        [Authorize(Roles = "WebAdmins, Administrators")]
        public ActionResult RemoveFetch(ProcessStepPage currentPage)
        {
            return base.RemoveFetchFrom(currentPage.ContentLink);
        }
    }
}