﻿using EPiServer;
using EPiServer.Web.Mvc;
using Norgesportalen.Business;
using Norgesportalen.Models.Pages;
using Norgesportalen.Models.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.SessionState;

namespace Norgesportalen.Controllers
{
    [SessionState(SessionStateBehavior.ReadOnly)]
    public class NordmennPageController : PageControllerBase<NordmennPage>
    {
        private readonly IContentLoader _contentLoader;

        public NordmennPageController(IContentLoader contentLoader)
        {
            _contentLoader = contentLoader;
        }

        [ContentOutputCache(Duration = 60)]
        public ActionResult Index(NordmennPage currentPage)
        {
            if (SetFetchFrom(currentPage))
            {
                return Redirect(Request.RawUrl + "&RefreshParent=True");
            }
            currentPage.Pages = GetChildrenPages(currentPage);

            var model = PageViewModel.Create(currentPage);

            return View(model);
        }

        [Authorize(Roles = "WebAdmins, Administrators")]
        public ActionResult RemoveFetch(NordmennPage currentPage)
        {
            return RemoveFetchFrom(currentPage.ContentLink);
        }

        private List<Models.Pages.BaseClasses.PageBase> GetChildrenPages(NordmennPage currentPage)
        {
            return _contentLoader.GetChildren<Models.Pages.BaseClasses.PageBase>(currentPage.ContentLink).FilterForDisplay(true, true).ToList();
        }
    }
}