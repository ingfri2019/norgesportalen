﻿using EPiServer.ServiceLocation;
using EPiServer.Web.Mvc;
using Norgesportalen.Business.Services;
using Norgesportalen.Models.Pages;
using Norgesportalen.Models.ViewModels;
using System.Web.Mvc;
using System.Web.SessionState;

namespace Norgesportalen.Controllers
{
    [SessionState(SessionStateBehavior.ReadOnly)]
    public class CountriesOverviewPageController : PageControllerBase<CountriesOverviewPage>
    {
        private readonly ICountryService _embassiesService;
        private readonly IDelegationsService _delegationsService;

        public CountriesOverviewPageController()
        {
            _embassiesService = ServiceLocator.Current.GetInstance<ICountryService>();
            _delegationsService = ServiceLocator.Current.GetInstance<IDelegationsService>();
        }

        public CountriesOverviewPageController(ICountryService embassiesService, IDelegationsService delegationsService)
        {
            _embassiesService = embassiesService;
            _delegationsService = delegationsService;
        }

        [ContentOutputCache(Duration = 60)]
        public ViewResult Index(CountriesOverviewPage currentPage)
        {
            var model = new CountriesOverviewViewModel(currentPage);
            model.AlphaCategoriesEmbassyList = _embassiesService.GetFilteredAlphaSortedCountrySiteList(currentPage.ContentLink);
            model.ConsulateGeneralsList = _embassiesService.GetFilteredConsulateGeneralList(currentPage.ConsulatesLinkList);
            model.DelegationList = _delegationsService.GetFilteredDelegationsList(currentPage.DelegationsLinkList);

            return View(model);
        }

    }
}