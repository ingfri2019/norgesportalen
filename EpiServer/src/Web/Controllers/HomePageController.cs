﻿using CsQuery.ExtensionMethods.Internal;
using EPiServer;
using EPiServer.Core;
using EPiServer.Web.Mvc;
using Norgesportalen.Business;
using Norgesportalen.Models.Interfaces;
using Norgesportalen.Models.Pages;
using Norgesportalen.Models.Pages.BaseClasses;
using Norgesportalen.Models.ViewModels;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.SessionState;

namespace Norgesportalen.Controllers
{
    [SessionState(SessionStateBehavior.ReadOnly)]
    public class HomePageController : PageControllerBase<HomePage>
    {
        private readonly IContentLoader _contentLoader;
        public HomePageController(IContentLoader contentLoader)
        {
            _contentLoader = contentLoader;
        }

        [ContentOutputCache(Duration = 60)]
        public ActionResult Index(HomePage currentPage)
        {
            var model = new HomePageViewModel(currentPage);

            if (currentPage.NewsListPageRef != null && currentPage.NewsListPageRef != ContentReference.EmptyReference)
            {
                model.AutomaticNewsList = GetAutomaticNews(currentPage.NewsListPageRef, currentPage.NewsContentArea);
            }

            //if (SiteDefinition.Current.StartPage.CompareToIgnoreWorkID(currentPage.ContentLink)) // Check if it is the StartPage or just a page of the StartPage type.
            //Connect the view models logotype property to the start page's to make it editable
            var editHints = ViewData.GetEditHints<PageViewModel<HomePage>, HomePage>();
            editHints.AddConnection(m => m.Layout.Logo, p => p.HeaderLogo);
            // Footer
            editHints.AddConnection(m => m.Layout.HomePage.FooterTitle, p => p.FooterTitle);
            editHints.AddConnection(m => m.Layout.HomePage.FooterLeft, p => p.FooterLeft);
            editHints.AddConnection(m => m.Layout.HomePage.FooterCenter, p => p.FooterCenter);
            editHints.AddConnection(m => m.Layout.HomePage.Footer3, p => p.Footer3);

            return View(model);
        }

        private ContentArea GetAutomaticNews(ContentReference contentReference, ContentArea newsContentArea)
        {
            var newsList = _contentLoader.GetDescendents(contentReference).ToList();
            if (!newsList.Any())
                return new ContentArea();

            if (newsContentArea != null && !newsContentArea.IsEmpty)
            {
                newsList = newsList.Except(newsContentArea.Items.Select(i => i.ContentLink)).ToList();
            }

            var newsListFiltered = newsList.Select(c => _contentLoader.Get<PageData>(c))
                .FilterForDisplay()
                .Where(p => p is ICanBeInNewsList && p.StartPublish > DateTime.Now.AddYears(-1))
                .Select(p => p as BasicPageBase)
                .OrderByDescending(p => p?.Changed)
                .Take(6);
                

            var ca = new ContentArea();
            ca.Items.AddRange(newsListFiltered.Select(x => new ContentAreaItem
            {
                ContentLink = x.ContentLink,
                ContentGuid = x.ContentGuid
            }));
            return ca;
        }
    }
}