﻿using EPiServer;
using EPiServer.Web.Mvc;
using Norgesportalen.Business;
using Norgesportalen.Models.Pages;
using Norgesportalen.Models.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.SessionState;

namespace Norgesportalen.Controllers
{
    [SessionState(SessionStateBehavior.ReadOnly)]
    public class ProcessPageController : PageControllerBase<ProcessPage>
    {
        [ContentOutputCache(Duration = 60)]
        public ActionResult Index(ProcessPage currentPage)
        {

            if (SetFetchFrom(currentPage))
            {
                return Redirect(Request.RawUrl + "&RefreshParent=True");
            }

            currentPage.Pages = GetChildrenPages(currentPage);

            var model = PageViewModel.Create(currentPage);

            return View(model);
        }

        [Authorize(Roles = "WebAdmins, Administrators")]
        public ActionResult RemoveFetch(ProcessPage currentPage)
        {
            return RemoveFetchFrom(currentPage.ContentLink);
        }

        private List<ProcessStepPage> GetChildrenPages(ProcessPage currentPage)
        {
            return DataFactory.Instance.GetChildren<ProcessStepPage>(currentPage.ContentLink).FilterForDisplay(true, true).ToList();
        }
    }
}