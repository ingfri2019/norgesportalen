﻿using BVNetwork.EPiSendMail.Api;
using EPiServer.ServiceLocation;
using Norgesportalen.Business.Newsletter;
using System.Web.Http;

namespace Norgesportalen.Controllers.API
{
    [RoutePrefix("api/newsletter")]
    public class NewsletterController : ApiController
    {
        private readonly INewsLetterService _newsLetterService;
        public NewsletterController()
        {
            _newsLetterService = ServiceLocator.Current.GetInstance<INewsLetterService>();
        }

        [Route("Subscribe", Name = "PostNewSubscriber")]
        [HttpPost]
        public SubscriptionResult SubscribeToNewsletter(string email, string list)
        {
            var result = _newsLetterService.SubscribeToNewsletter(email, list);

            return result;
        }

        [Route("Unsubscribe", Name = "UnsubscribeExistingSubscriber")]
        [HttpPost]
        public void Unsubscribe(string email, string list)
        {            
            _newsLetterService.Unsubscribe(email, list);
        }

      }
}