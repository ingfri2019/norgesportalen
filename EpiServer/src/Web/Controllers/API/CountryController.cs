﻿using EPiServer.Core;
using EPiServer.ServiceLocation;
using Norgesportalen.Business.Services;
using Norgesportalen.Models.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace Norgesportalen.Controllers.API
{
    [RoutePrefix("api/embassies")]
    public class CountryController : ApiController
    {

        private readonly ICountryService _embassiesService;

        public CountryController(ICountryService embassiesService)
        {
            _embassiesService = embassiesService;
        }

        public CountryController()
        {
            _embassiesService = ServiceLocator.Current.GetInstance<ICountryService>();
        }


        [Route("ByName", Name = "GetEmbassiesNames")]
        [HttpGet]
        public List<string> Get(string id, string name)
        {

            if (string.IsNullOrEmpty(id))
            {
                return new List<string>();
            }
            ContentReference parentPage;
            if (!ContentReference.TryParse(id, out parentPage))
            {
                return new List<string>();
            }

            return _embassiesService.GetFilteredCountryNames(parentPage).Where(x=> x.ToLower().Contains(name.ToLower())).ToList();
        }

        [Route("SortedByCategory", Name = "GetEmbassySiteCategoryByName")]
        [HttpGet]
        public List<CountriesOverviewViewModel.CountrySiteCategory> GetAll(string id, string name)
        {

            if (string.IsNullOrEmpty(id))
            {
                return new List<CountriesOverviewViewModel.CountrySiteCategory>();
            }
            ContentReference parentPage;
            if (!ContentReference.TryParse(id, out parentPage))
            {
                return new List<CountriesOverviewViewModel.CountrySiteCategory>();
            }


            if (string.IsNullOrEmpty(name))
                return _embassiesService.GetFilteredAlphaSortedCountrySiteList(parentPage);

            return _embassiesService.GetEmbassiesThatContain(parentPage,name);
        }

        [Route("SortedByCategory", Name = "GetAllEmbassySiteCategory")]
        [HttpGet]
        public List<CountriesOverviewViewModel.CountrySiteCategory> GetAll(string id)
        {

            if (string.IsNullOrEmpty(id))
            {
                return new List<CountriesOverviewViewModel.CountrySiteCategory>();
            }
            ContentReference parentPage;
            if (!ContentReference.TryParse(id, out parentPage))
            {
                return new List<CountriesOverviewViewModel.CountrySiteCategory>();
            }


            return _embassiesService.GetFilteredAlphaSortedCountrySiteList(parentPage);
        }


    }
}