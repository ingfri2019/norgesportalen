﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI;
using EPiServer;
using EPiServer.Core;
using EPiServer.Forms.Implementation.Elements;
using Norgesportalen.Business;
using Norgesportalen.Models.Pages.BaseClasses;
using Norgesportalen.Models.ViewModels;

namespace Norgesportalen.Controllers
{
    public class BaseFormsPageController: PageControllerBase<BaseFormsPage>
    {
        private readonly IContentLoader _contentLoader;
        public BaseFormsPageController(IContentLoader contentLoader)
        {
            _contentLoader = contentLoader;
        }

        [OutputCache(Duration = 0, Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult Index(BaseFormsPage currentPage)
        {
            if (SetFetchFrom(currentPage))
            {
                return Redirect(Request.RawUrl + "&RefreshParent=True");
            }

            var model = GetModel(currentPage);
            return View(model);
        }

        [Authorize(Roles = "WebAdmins, Administrators")]
        public ActionResult RemoveFetch(BasicPageBase currentPage)
        {
            return RemoveFetchFrom(currentPage.ContentLink);
        }

        private BaseFormsViewModel GetModel(BaseFormsPage currentPage)
        {
            var model = new BaseFormsViewModel(currentPage);
            model.Children = _contentLoader.GetChildren<PageData>(currentPage.ContentLink).FilterForDisplay();
            model.ShowChildrenList = model.Children.Any();
            model.UseListViewForChildren = currentPage.ListAlternatives == Constants.ListAlternatives.ListView;
            MapFormToModel(currentPage, model);
            return model;
        }

        private void MapFormToModel(BaseFormsPage currentPage, BaseFormsViewModel model)
        {
            if (!string.IsNullOrEmpty(currentPage.FormSelection))
            {
                var formGuid = new Guid(currentPage.FormSelection);
                var formReference = _contentLoader.Get<FormContainerBlock>(formGuid);

                if (formReference != null)
                {
                    if (model.FormsArea == null)
                    {
                        model.FormsArea = new ContentArea();
                    }
                    model.FormsArea.Items.Add(new ContentAreaItem() {ContentLink = formReference.Content.ContentLink});
                }
            }
        }
    }
}