﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Caching;
using System.Web.Mvc;
using System.Web.SessionState;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Editor;
using EPiServer.Filters;
using EPiServer.Web.Mvc;
using Norgesportalen.Business;
using Norgesportalen.Models.Pages;
using Norgesportalen.Models.Pages.BaseClasses;
using Norgesportalen.Models.ViewModels;

namespace Norgesportalen.Controllers
{
    [SessionState(SessionStateBehavior.ReadOnly)]
    public class FilteredListPageController : PageControllerBase<FilteredListPage>
    {
        private readonly IContentLoader _contentLoader;
        private ObjectCache _cache = MemoryCache.Default;
        private object _lock = new object();

        public FilteredListPageController(IContentLoader contentLoader)
        {
            _contentLoader = contentLoader;
        }

        [ContentOutputCache(Duration = 60)]
        public ViewResult Index(FilteredListPage currentPage, int page = 1)
        {
            var model = new FilteredListViewModel(currentPage);

            var allArticles = GetArticlesFilteredCached(currentPage, currentPage.Language);

            model.ArticlesOnPage = 
                            allArticles
                            .Skip(model.CurrentPage.PageSize * (page - 1))
                            .Take(model.CurrentPage.PageSize);
            model.Paging = new PagingModel(allArticles.Count(), model.CurrentPage.PageSize, PagingPage);
            return View(model);
        }

        private IEnumerable<FilteredListViewModel.ListedArticle> GetArticlesFilteredCached(FilteredListPage currentPage, CultureInfo language)
        {
            var key = $"Cache{currentPage.ContentGuid}{language.Name}";

            IEnumerable<FilteredListViewModel.ListedArticle> filteredResult = null;
            if (!PageEditing.PageIsInEditMode)
            {
                filteredResult = _cache[key] as IEnumerable<FilteredListViewModel.ListedArticle>;
            }

            // Check whether the value exists
            if (filteredResult == null)
            {
                lock (_lock)
                {
                    // Try to get the object from the cache again
                    if (!PageEditing.PageIsInEditMode)
                        filteredResult = _cache[key] as IEnumerable<FilteredListViewModel.ListedArticle>;

                    // Double-check that another thread did 
                    // not call the DB already and load the cache
                    if (filteredResult == null)
                    {
                        var descendentPages = _contentLoader
                                            .GetDescendents(currentPage.ContentLink);

                        var descendentPageForCurrentLanguage = new List<PageData>();

                        foreach (var page in descendentPages)
                        {
                            var pageData = _contentLoader.Get<PageData>(page, language);

                            if(pageData == null)
                                continue;

                            descendentPageForCurrentLanguage.Add(pageData);
                        }
                        var sortOrder = (FilterSortOrder)currentPage[MetaDataProperties.PageChildOrderRule];

                        var unsortedList = descendentPageForCurrentLanguage.FilterForDisplay(true, true);

                        descendentPageForCurrentLanguage = Sort(unsortedList, sortOrder).ToList();                        
                       
                        filteredResult = FilterDescendents(descendentPageForCurrentLanguage);

                        // Add the list to the cache
                        _cache.Set(key, filteredResult, DateTimeOffset.Now.AddMinutes(3));
                    }
                }
            }
            return filteredResult;
        }

        // From https://github.com/episerver/AlloyDemoKit/blob/master/src/AlloyDemoKit/Controllers/PageListBlockController.cs
        private IEnumerable<PageData> Sort(IEnumerable<PageData> pages, FilterSortOrder sortOrder)
        {
            var asCollection = new PageDataCollection(pages);
            var sortFilter = new FilterSort(sortOrder);
            sortFilter.Sort(asCollection);
            return asCollection;
        }

        public static IEnumerable<FilteredListViewModel.ListedArticle> FilterDescendents(IEnumerable<PageData> descendentPages)
        {
            var filteredList = new List<FilteredListViewModel.ListedArticle>();
            foreach (var content in descendentPages)
            {
                if (content is BasicPageBase)
                    filteredList.Add(new FilteredListViewModel.ListedArticle(content as BasicPageBase, content.GetType()));                
            }

            return filteredList;
        }

        private int PagingPage
        {
            get
            {
                if (!int.TryParse(Request.QueryString["page"], out var pagingPage))
                {
                    pagingPage = 1;
                }
                return pagingPage;
            }
        }
    }
}