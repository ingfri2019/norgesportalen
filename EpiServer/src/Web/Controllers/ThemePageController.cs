﻿using EPiServer.Web.Mvc;
using Norgesportalen.Models.Pages;
using Norgesportalen.Models.ViewModels;
using System.Web.Mvc;
using System.Web.SessionState;

namespace Norgesportalen.Controllers
{
    [SessionState(SessionStateBehavior.ReadOnly)]
    public class ThemePageController : PageControllerBase<ThemePage>
    {
        // TODO: Themepage burde kunne bruke basicpagebasecontroller?!
        [ContentOutputCache(Duration = 60)]
        public ActionResult Index(ThemePage currentPage)
        {
            if (SetFetchFrom(currentPage))
            {
                return Redirect(Request.RawUrl + "&RefreshParent=True");
            }

            return View(PageViewModel.Create(currentPage));
        }

        [Authorize(Roles = "WebAdmins, Administrators")]
        public ActionResult RemoveFetch(ThemePage currentPage)
        {
            return base.RemoveFetchFrom(currentPage.ContentLink);
        }
    }
}