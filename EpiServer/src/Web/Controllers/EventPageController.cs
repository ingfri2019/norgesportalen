﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.SessionState;
using EPiServer;
using EPiServer.Web.Mvc;
using Norgesportalen.Models.Media;
using Norgesportalen.Models.Pages;
using Norgesportalen.Models.ViewModels;
using SitePageData = Norgesportalen.Models.Pages.BaseClasses.SitePageData;

namespace Norgesportalen.Controllers
{
    [SessionState(SessionStateBehavior.ReadOnly)]
    public class EventPageController : PageControllerBase<EventPage>
    {
        private readonly IContentLoader _contentLoader;
        public EventPageController(IContentLoader contentLoader)
        {
            _contentLoader = contentLoader;
        }

        [ContentOutputCache(Duration = 60)]
        public ActionResult Index(EventPage currentPage)
        {
            EventPageViewModel model = new EventPageViewModel(currentPage);

            var timeZoneId = string.Empty;

            if (string.IsNullOrEmpty(currentPage.EventTimezoneId))
            {
                // look for parent value
                var homePg = SitePageData.GetHomePage(currentPage.ParentLink);
                if (homePg != null)
                {
                    timeZoneId = homePg.DeaultTimezoneId;
                }
            }
            else
            {
                timeZoneId = currentPage.EventTimezoneId;
            }
            if (!string.IsNullOrEmpty(timeZoneId))
            {
                try
                {
                    var tz = TimeZoneInfo.FindSystemTimeZoneById(timeZoneId);

                    currentPage.StartTimeConverted = TimeZoneInfo.ConvertTimeFromUtc(currentPage.EventStartDate.ToUniversalTime(), tz);
                    currentPage.EndTimeConverted = TimeZoneInfo.ConvertTimeFromUtc(currentPage.EventStopDate.ToUniversalTime(), tz);
                }
                catch (Exception)
                {
                    // do nothing
                }
            }
            else
            {
                // just set the input time without modification if timeZoneId is not set.
                currentPage.StartTimeConverted = currentPage.EventStartDate;
                currentPage.EndTimeConverted = currentPage.EventStopDate;
            }

            model.CarouselImages = GetCarouselImages(currentPage);
            model.RelatedEvents = GetRelatedEvents(currentPage);

            return View("~/Views/EventPage/Index.cshtml", model);
        }

        private IEnumerable<ImageFile> GetCarouselImages(EventPage currentPage)
        {
            var images = currentPage.ImageCarouselArea?.FilteredItems?
                .Select(item => _contentLoader.Get<ImageFile>(item.ContentLink))
                .Where(image => image != null);

            return images ?? new List<ImageFile>();
        }

        private IEnumerable<EventPage> GetRelatedEvents(EventPage currentPage)
        {
            var relatedEvents = currentPage.RelatedEventPages?.FilteredItems?
                .Select(item => _contentLoader.Get<EventPage>(item.ContentLink))
                .Where(relatedEvent => relatedEvent != null);

            return relatedEvents ?? new List<EventPage>();
        }
    }
}