﻿using BVNetwork.EPiSendMail.Api;
using EPiServer.Editor;
using EPiServer.ServiceLocation;
using Norgesportalen.Business.Newsletter;
using Norgesportalen.Models.Pages;
using Norgesportalen.Models.ViewModels;
using System.Web.Mvc;
using System.Web.SessionState;

namespace Norgesportalen.Controllers
{
    [SessionState(SessionStateBehavior.ReadOnly)]

    public class NewsletterConfirmPageController : PageControllerBase<NewsletterConfirmPage>
    {
        // GET: NewsletterConfirmPage
        public ActionResult Index(NewsletterConfirmPage currentPage, string reference)
        {
            if (SetFetchFrom(currentPage))
            {
                return Redirect(this.Request.RawUrl + "&RefreshParent=True");
            }
            var newsLetterService = ServiceLocator.Current.GetInstance<INewsLetterService>();
            var subscriptionResult = newsLetterService.ConfirmNewsletter(reference);

            currentPage.ShowErrormsg = false;
            if (subscriptionResult != SubscriptionResult.Success && !PageEditing.PageIsInEditMode)
            {
                currentPage.ShowErrormsg = true;
            }
            var model = PageViewModel.Create(currentPage);
            return View(model);
        }
    }
}