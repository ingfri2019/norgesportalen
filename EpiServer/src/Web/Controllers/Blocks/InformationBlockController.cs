﻿using EPiServer.Core;
using EPiServer.Web.Mvc;
using EPiServer.Web.Routing;
using Norgesportalen.Business;
using Norgesportalen.Models.Blocks;
using Norgesportalen.Models.Pages;
using System.Web.Mvc;

namespace Norgesportalen.Controllers.Blocks
{
    public class InformationBlockController : BlockController<InformationBlock>
    {
        private PageData _currentPage;
        private IHomePageLocator _homePageLocator;

        public InformationBlockController(IPageRouteHelper pageRouteHelper, IHomePageLocator homePageLocator)
        {
            _currentPage = pageRouteHelper.Page;
            _homePageLocator = homePageLocator;
        }

        public override ActionResult Index(InformationBlock currentBlock)
        {
            if (_currentPage != null) {
                HomePage frontPage = _homePageLocator.TryFindHomePage(_currentPage.PageLink);
                currentBlock.OwnerCountry = frontPage?.Name;
            }

            return View("~/Views/Shared/Blocks/InformationBlock.cshtml", currentBlock);
        }
    }
}