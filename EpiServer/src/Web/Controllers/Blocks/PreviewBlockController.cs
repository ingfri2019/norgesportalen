﻿using EPiServer.Core;
using EPiServer.Framework.DataAnnotations;
using EPiServer.Framework.Web;
using EPiServer.Web;
using System.Web.Mvc;

namespace Norgesportalen.Controllers.Blocks
{
    [TemplateDescriptor(Inherited = true,
        Tags = new[] { RenderingTags.Preview },
        TemplateTypeCategory = TemplateTypeCategories.MvcController)]
    public class PreviewBlockController : Controller, IRenderTemplate<BlockData>
    {
        public ActionResult Index(BlockData currentBlock)
        {
         return View(currentBlock);
        }
    }
}