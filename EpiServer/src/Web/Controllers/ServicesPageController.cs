﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.SessionState;
using EPiServer;
using EPiServer.Web.Mvc;
using Norgesportalen.Business;
using Norgesportalen.Models.Pages;
using Norgesportalen.Models.ViewModels;

namespace Norgesportalen.Controllers
{
    [SessionState(SessionStateBehavior.ReadOnly)]
    public class ServicesPageController : PageControllerBase<ServicesPage>
    {
        private readonly IContentLoader _contentLoader;
        public ServicesPageController(IContentLoader contentLoader)
        {
            _contentLoader = contentLoader;
        }

        [ContentOutputCache(Duration = 60)]
        public ActionResult Index(ServicesPage currentPage)
        {
            if (SetFetchFrom(currentPage))
            {
                return Redirect(Request.RawUrl + "&RefreshParent=True");
            }
            currentPage.Pages = GetChildrenPages(currentPage);

            var model = PageViewModel.Create(currentPage);

            return View(model);
        }

        [Authorize(Roles = "WebAdmins, Administrators")]
        public ActionResult RemoveFetch(ServicesPage currentPage)
        {
            return RemoveFetchFrom(currentPage.ContentLink);
        }

        private List<Models.Pages.BaseClasses.PageBase> GetChildrenPages(ServicesPage currentPage)
        {
            return _contentLoader.GetChildren<Models.Pages.BaseClasses.PageBase>(currentPage.ContentLink).FilterForDisplay(true, true).ToList();
        }
    }
}