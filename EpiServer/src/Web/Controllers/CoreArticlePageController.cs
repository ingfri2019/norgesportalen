﻿using System.Linq;
using System.Web.Mvc;
using EPiServer;
using EPiServer.Core;
using Norgesportalen.Business;
using Norgesportalen.Models.Pages;
using Norgesportalen.Models.Pages.BaseClasses;
using Norgesportalen.Models.ViewModels;

namespace Norgesportalen.Controllers
{
    public class CoreArticlePageController : PageControllerBase<CoreArticlePage>
    {
        private readonly IContentLoader _contentLoader;
        public CoreArticlePageController(IContentLoader contentLoader)
        {
            _contentLoader = contentLoader;
        }

        // GET: CoreArticle
        public ActionResult Index(CoreArticlePage currentPage)
        {
            if (SetFetchFrom(currentPage))
            {
                return Redirect(Request.RawUrl + "&RefreshParent=True");
            }

            var model = GetModel(currentPage);
            return View(model);
        }

        [Authorize(Roles = "WebAdmins, Administrators")]
        public ActionResult RemoveFetch(BasicPageBase currentPage)
        {
            return RemoveFetchFrom(currentPage.ContentLink);
        }

        private CoreArticleViewModel GetModel(CoreArticlePage currentPage)
        {
            var model = new CoreArticleViewModel(currentPage);
            model.Children = _contentLoader.GetChildren<PageData>(currentPage.ContentLink).FilterForDisplay();
            model.ShowChildrenList = model.Children.Any();
            model.UseListViewForChildren = currentPage.ListAlternatives == Constants.ListAlternatives.ListView;
            return model;
        }
    }
}
