﻿using EPiServer;
using EPiServer.Editor;
using EPiServer.Web.Mvc;
using Norgesportalen.Business;
using Norgesportalen.Models.Pages;
using Norgesportalen.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Caching;
using System.Web.Mvc;
using System.Web.SessionState;
using Norgesportalen.Business.Extensions;

namespace Norgesportalen.Controllers
{
    [SessionState(SessionStateBehavior.ReadOnly)]
    public class EventListPageController : PageControllerBase<EventListPage>
    {
        private ObjectCache _cache = MemoryCache.Default;
        private object _lock = new object();
        private readonly IContentLoader _contentLoader;
        private const int PageSize = 9;

        public EventListPageController(IContentLoader contentLoader)
        {
            _contentLoader = contentLoader;
        }

        [ContentOutputCache(Duration = 60)]
        public ViewResult Index(EventListPage currentPage, int page = 1)
        {
            var model = new EventListViewModel(currentPage)
            {
                ShowFocusedEvent = false,
                SelectedPage = page
            };

            //Focused Event
            var focusedEventPage = currentPage.FocusEventContent?.FilteredItems.FirstOrDefault();
            if (focusedEventPage != null)
            {
                var pageRef = focusedEventPage.ContentLink;
                var eventPage = _contentLoader.Get<EventPage>(pageRef);
                model.ShowFocusedEvent = IsEventPageActive(eventPage);
            }

            var allEvents = GetEventsSortedCached(currentPage, currentPage.Language, model.ShowFocusedEvent).ToList();

            model.EventsOnPage = allEvents
                            .Skip(PageSize * (page - 1))
                            .Take(PageSize);

            model.Paging = new PagingModel(allEvents.Count(), PageSize, PagingPage);

            return View(model);
        }

        private IEnumerable<EventListViewModel.ListedEvent> GetEventsSortedCached(EventListPage currentPage, CultureInfo language, bool showFocusedEvent)
        {
            var key = $"Cache{currentPage.ContentGuid}{language.Name}";

            IEnumerable<EventListViewModel.ListedEvent> eventList = null;
            if (!PageEditing.PageIsInEditMode)
            {
                eventList = _cache[key] as IEnumerable<EventListViewModel.ListedEvent>;
            }

            if (eventList == null)
            {
                lock (_lock)
                {
                    if (!PageEditing.PageIsInEditMode)
                    {
                        eventList = _cache[key] as IEnumerable<EventListViewModel.ListedEvent>;
                    }
                    if (eventList != null)
                    {
                        //Cache hit
                        return eventList;
                    }

                    var descendentPages = _contentLoader.GetDescendents(currentPage.ContentLink).ToList();
                    var focusedItem = currentPage.FocusEventContent?.FilteredItems.FirstOrDefault();

                    if (focusedItem?.ContentLink?.ID != null)
                    {
                        //Remove focused event from listing
                        var index = descendentPages.FindIndex(o => o.ID == focusedItem.ContentLink.ID);
                        if (showFocusedEvent && index > -1)
                        {
                            descendentPages.RemoveAt(index);
                        }
                    }

                    var descendentPagesForCurrentLanguage = new List<EventPage>();
                    foreach (var page in descendentPages)
                    {
                        if (!_contentLoader.TryGet(page, language, out EventPage eventPage))
                        {
                            continue;
                        }
                        descendentPagesForCurrentLanguage.Add(eventPage);
                    }

                    // FilterForDisplay on list
                    descendentPagesForCurrentLanguage = descendentPagesForCurrentLanguage.FilterForDisplay(true, true).ToList();
                    eventList = SortListedEvents(descendentPagesForCurrentLanguage);

                    _cache.Set(key, eventList, DateTimeOffset.Now.AddMinutes(3));
                }
            }
            return eventList;
        }


        private static IEnumerable<EventListViewModel.ListedEvent> SortListedEvents(IEnumerable<EventPage> eventPages)
        {
            var sortedEventsList = new List<EventListViewModel.ListedEvent>();
            var expiredEventsList = new List<EventListViewModel.ListedEvent>();

            var listOfEventPages = eventPages.ToList();

            //Active events sorted ascending by EventStartDate
            sortedEventsList.AddRange(listOfEventPages.Where(IsEventPageActive).OrderBy(e => e.EventStartDate).Select(e => new EventListViewModel.ListedEvent(e)));

            //Expired events sorted descending by EventStartDate
            expiredEventsList.AddRange(listOfEventPages.Where(IsNotEventPageActive).OrderByDescending(e => e.EventStopDate).Select(e => new EventListViewModel.ListedEvent(e)));

            sortedEventsList.AddRange(expiredEventsList);
            return sortedEventsList;
        }

        private static bool IsNotEventPageActive(EventPage eventPage)
        {
            return !IsEventPageActive(eventPage);
        }

        private static bool IsEventPageActive(EventPage page)
        {
            var timeZoneId = page.EventTimezoneId;
            if (page.EventStopDate.GetTimeZoneConvertedDateTime(timeZoneId) > DateTime.UtcNow.GetTimeZoneConvertedDateTime(timeZoneId))
            {
                return true;
            }
            return false;
        }

        private int PagingPage
        {
            get
            {
                if (!int.TryParse(Request.QueryString["page"], out var pagingPage))
                {
                    pagingPage = 1;
                }
                return pagingPage;
            }
        }
    }
}