﻿using System.Web.Mvc;
using EPiServer.ServiceLocation;
using EPiServer.Web.Mvc;
using EPiServer.Web.Routing;
using Norgesportalen.Models.Media;
using Norgesportalen.Models.ViewModels;

namespace Norgesportalen.Controllers.Media
{
    public class GenericMediaController : PartialContentController<GenericMedia>
    {
        // GET: GenericMedia
        public override ActionResult Index(GenericMedia currentContent)
        {
            var urlResolver = ServiceLocator.Current.GetInstance<UrlResolver>();

            var model = new GenericMediaViewModel
            {
                Url = urlResolver.GetUrl(currentContent),
                Name = currentContent.Name,
                Intro = currentContent.Description
            };

            return PartialView(model);
        }
    }
}