﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EPiServer.ServiceLocation;
using EPiServer.Web.Mvc;
using EPiServer.Web.Mvc.Html;
using EPiServer.Web.Routing;
using Norgesportalen.Business;
using Norgesportalen.Business.Extensions;
using Norgesportalen.Models.Media;
using Norgesportalen.Models.ViewModels;

namespace Norgesportalen.Controllers.Media
{
    public class ImageFileController : PartialContentController<ImageFile>
    {
        /// <summary>
        /// The index action for the image file. Creates the view model and renders the view.
        /// </summary>
        /// <param name="currentContent">The current image file.</param>
        public override ActionResult Index(ImageFile currentContent)
        {
            var model = new ImageViewModel
            {
                Url = currentContent.ContentLink.GetImageUrl(Constants.ImageSize.Large),
                AltText = currentContent.AltText,
                Credits = currentContent.Credit,
                ImageText = currentContent.ImageText
            };

            return PartialView(model);
        }
    }
}