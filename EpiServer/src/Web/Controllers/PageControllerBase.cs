using EPiServer;
using EPiServer.Core;
using EPiServer.DataAccess;
using EPiServer.ServiceLocation;
using EPiServer.Web.Mvc;
using Norgesportalen.Models.Pages.BaseClasses;
using System.Web.Mvc;
using System.Web.Security;

namespace Norgesportalen.Controllers
{
    /// <summary>
    /// All controllers that renders pages should inherit from this class so that we can 
    /// apply action filters, such as for output caching site wide, should we want to.
    /// </summary>
    public abstract class PageControllerBase<T> : PageController<T> where T : SitePageData
    {

        IContentRepository _contentRepository = ServiceLocator.Current.GetInstance<IContentRepository>();

        /// <summary>
        /// Signs out the current user and redirects to the Index action of the same controller.
        /// </summary>
        /// <remarks>
        /// There's a log out link in the footer which should redirect the user to the same page. 
        /// As we don't have a specific user/account/login controller but rely on the login URL for 
        /// forms authentication for login functionality we add an action for logging out to all
        /// controllers inheriting from this class.
        /// </remarks>
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index");
        }

        protected bool SetFetchFrom(PageData currentPage)
        {
            var fetchDataFromHelper = currentPage.Property["FetchDataFromHelper"]?.Value as ContentReference;

            if (fetchDataFromHelper != null
                && currentPage.LinkType != PageShortcutType.FetchData
                && currentPage.LinkType == PageShortcutType.Normal)
            {
                var writable = _contentRepository.Get<PageData>(currentPage.ContentLink).CreateWritableClone();
                writable.LinkType = PageShortcutType.FetchData;
                writable.Property["PageShortcutLink"].Value = fetchDataFromHelper;

                SaveAndUpdatePage(writable);

                return true;
            }
            return false;
        }

        [Authorize(Roles = "WebAdmins, Administrators")]
        protected ActionResult RemoveFetchFrom(ContentReference contentLink)
        {
            var _contentRepository = ServiceLocator.Current.GetInstance<IContentRepository>();
            var writable = _contentRepository.Get<PageData>(contentLink).CreateWritableClone();
            writable.LinkType = PageShortcutType.Normal;
            writable.Property["PageShortcutLink"].Value = null;
            writable.Property["FetchDataFromHelper"].Value = null;

            SaveAndUpdatePage(writable);

            return RedirectToAction("Index");
        }

        private void SaveAndUpdatePage(PageData writableClone)
        {
            var newReference = _contentRepository.Save(writableClone, SaveAction.Save | SaveAction.SkipValidation);

            // N�dvendig?
            var repository = ServiceLocator.Current.GetInstance<IContentVersionRepository>();
            repository.SetCommonDraft(newReference);
        }
    }
}
