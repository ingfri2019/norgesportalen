﻿using EPiServer;
using EPiServer.Web.Mvc;
using Norgesportalen.Business;
using Norgesportalen.Business.EditorDescriptors;
using Norgesportalen.Models.Blocks;
using Norgesportalen.Models.Pages;
using Norgesportalen.Models.ViewModels;
using System.Web.Mvc;
using System.Web.SessionState;

namespace Norgesportalen.Controllers
{
    [SessionState(SessionStateBehavior.ReadOnly)]
    public class NewsletterPageController : PageController<NewsletterPage>
    {
        private readonly IHomePageLocator _homePageLocator;
        private readonly IContentLoader _contentLoader;

        // For some reason dependency injection makes one test fail (breadcrumb-test)
        public NewsletterPageController(IHomePageLocator homePageLocator, IContentLoader contentLoader)
        {
            _homePageLocator = homePageLocator;
            _contentLoader = contentLoader;
        }

        [ContentOutputCache(Duration = 60)]
        public ActionResult Index(NewsletterPage currentPage)
        {
            var model = new NewsletterPageViewModel(currentPage);

            model.HomePage = _homePageLocator.TryFindHomePage(currentPage.ContentLink);

            return View(model);
        }

        public ActionResult GetSocialMediaBlock(NewsletterPage currentPage)
        {
            var model = new NewsletterPageViewModel(currentPage);

            model.HomePage = _homePageLocator.TryFindHomePage(currentPage.ContentLink);

            string returnHtml = "";

            foreach (EPiServer.Core.ContentAreaItem socialMediaItem in model.HomePage.Footer3.Items)
            {
                SocialMediaBlock socialMediaBlock;

                if (_contentLoader.TryGet(socialMediaItem.ContentLink, out socialMediaBlock))
                {
                    returnHtml += $"<table>" +
                        $"<tr>" +
                        $"<td><a href=\"{socialMediaBlock.LinkUrl}\">{GetSocialMediaIcon(socialMediaBlock, model.SiteUrl)}</a></td>" +
                        $"<td><a href=\"{socialMediaBlock.LinkUrl}\">{socialMediaBlock.Heading}</a></td>" +
                        $"</tr>" +
                        $"</table>";
                }
            }

            return Content(returnHtml);
        }

        private string GetSocialMediaIcon(SocialMediaBlock socialMediaBlock, string baseUrl)
        {
            string imageSrc = "";
            string imageAlt = "";
            string inlineIconStyle = " style=\"width:20px; height:20px; padding-right: 8px;\"";

            switch (socialMediaBlock.Icons)
            {
                case SocialmediaIcons.Facebook:
                    imageSrc = string.Format("{0}{1}", baseUrl, "Frontend/dist-web/Static/images/facebook-logo-white.svg");
                    imageAlt = "Facebook icon";
                    break;
                case SocialmediaIcons.Flickr:
                    imageSrc = string.Format("{0}{1}", baseUrl, "Frontend/dist-web/Static/images/flickr-logo-white.svg");
                    imageAlt = "Flickr icon";
                    break;
                case SocialmediaIcons.Instagram:
                    imageSrc = string.Format("{0}{1}", baseUrl, "Frontend/dist-web/Static/images/instagram-logo-white.svg");
                    imageAlt = "Instagram icon";
                    break;
                case SocialmediaIcons.LinkedIn:
                    imageSrc = string.Format("{0}{1}", baseUrl, "Frontend/dist-web/Static/images/linkedin-logo-white.svg");
                    imageAlt = "LinkedIn icon";
                    break;
                case SocialmediaIcons.Snapchat:
                    imageSrc = string.Format("{0}{1}", baseUrl, "Frontend/dist-web/Static/images/snapchat-logo-white.svg");
                    imageAlt = "Snapchat icon";
                    break;
                case SocialmediaIcons.Twitter:
                    imageSrc = string.Format("{0}{1}", baseUrl, "Frontend/dist-web/Static/images/twitter-logo-white.svg");
                    imageAlt = "Twitter icon";
                    break;
                case SocialmediaIcons.Vimeo:
                    imageSrc = string.Format("{0}{1}", baseUrl, "Frontend/dist-web/Static/images/vimeo-logo-white.svg");
                    imageAlt = "Vimeo icon";
                    break;
                case SocialmediaIcons.Vkontakte:
                    imageSrc = string.Format("{0}{1}", baseUrl, "Frontend/dist-web/Static/images/vkontakte-logo-white.svg");
                    imageAlt = "Vkontakte icon";
                    break;
                case SocialmediaIcons.Weibo:
                    imageSrc = string.Format("{0}{1}", baseUrl, "Frontend/dist-web/Static/images/weibo-logo-white.svg");
                    imageAlt = "Weibo icon";
                    break;
                case SocialmediaIcons.YouTube:
                    imageSrc = string.Format("{0}{1}", baseUrl, "Frontend/dist-web/Static/images/youtube-play-button-white.svg");
                    imageAlt = "YouTube icon";
                    break;
                default:
                    break;
            }

            string imageAltParam = string.IsNullOrWhiteSpace(imageAlt) ? "" : $" alt=\"{imageAlt}\"";

            return $"<img src=\"{imageSrc}\"{inlineIconStyle}{imageAltParam}>";
        }
    }
}