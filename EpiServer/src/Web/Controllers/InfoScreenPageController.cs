﻿using EPiServer;
using EPiServer.Core;
using Norgesportalen.Models.Blocks;
using Norgesportalen.Models.Pages;
using Norgesportalen.Models.Pages.BaseClasses;
using Norgesportalen.Models.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Norgesportalen.Controllers
{
    public class InfoScreenPageController : PageControllerBase<InfoScreenPage>
    {
        private readonly IContentLoader _contentLoader;
        public InfoScreenPageController(IContentLoader contentLoader)
        {
            _contentLoader = contentLoader;
        }

        public ActionResult Index(InfoScreenPage currentPage)
        {
            var links = GetLinksFromPage(currentPage);
            var centralLinks = new List<string>();
            if (ContentReference.StartPage != null)
            {
                var page = _contentLoader.Get<SitePageData>(ContentReference.StartPage);
                if (page != null && page is CountriesOverviewPage startpage && startpage.CentralInfoScreenPage != null)
                {
                    page = _contentLoader.Get<SitePageData>(startpage.CentralInfoScreenPage);
                    if (page is InfoScreenPage infoscreen)
                    {
                        centralLinks = GetLinksFromPage(infoscreen);
                    }
                }
            }
            var model = new InfoScreenViewModel(currentPage);
            model.PageData = currentPage;
            model.LinksJsList = "'" + string.Join("','", links.Concat(centralLinks)) + "'";
            return View(model);
        }

        private List<string> GetLinksFromPage(InfoScreenPage page)
        {
            var links = new List<string>();
            if (page?.InternalLinks != null)
            {
                foreach (var item in page.InternalLinks.Items)
                {
                    var block = _contentLoader.Get<InfoScreenInternalLinkBlock>(item.ContentLink);
                    var p = _contentLoader.Get<SitePageData>(block.PageReference);
                    for (var i = 0; i < block.NumberOfDisplaysPerRotation; i++)
                    {
                        links.Add(p.CanonicalUrl);
                    }
                }
            }
            if (page?.ExternalLinks != null)
            {
                foreach (var item in page.ExternalLinks.Items)
                {
                    var block = _contentLoader.Get<InfoScreenExternalLinkBlock>(item.ContentLink);
                    for (var i = 0; i < block.NumberOfDisplaysPerRotation; i++)
                    {
                        links.Add(block.Url);
                    }
                }
            }
            if (page?.SocialMediaLinks != null)
            {
                foreach (var item in page.SocialMediaLinks.Items)
                {
                    var block = _contentLoader.Get<SocialMediaBlock>(item.ContentLink);
                    for (var i = 0; i < block.NumberOfDisplaysPerRotation; i++)
                    {
                        links.Add(block.LinkUrl.OriginalString);
                    }
                }
            }
            return links;
        }
    }
}
