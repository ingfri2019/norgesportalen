﻿using BVNetwork.EPiSendMail.Api;
using Norgesportalen.Models.Pages;
using Norgesportalen.Models.Pages.BaseClasses;
using Norgesportalen.Models.ViewModels;
using System.Linq;
using System.Web.Mvc;

namespace Norgesportalen.Controllers
{
    public class NewsletterUnsubscribePageController : PageControllerBase<NewsletterUnsubscribePage>
    {
        public ActionResult Index(NewsletterUnsubscribePage currentPage, string mail = "")
        {
            if (SetFetchFrom(currentPage))
            {
                return Redirect(Request.RawUrl + "&RefreshParent=True");
            }

            if (!string.IsNullOrWhiteSpace(mail) && currentPage.NewsLetterList.Any())
            {
                var api = new SubscriptionApi();

                if (int.TryParse(currentPage.NewsLetterList, out var listId))
                    api.Unsubscribe(mail, listId);
            }

            return View("~/Views/BasicPageBase/Index.cshtml", new PageViewModel<BasicPageBase>(currentPage));
        }
    }
}