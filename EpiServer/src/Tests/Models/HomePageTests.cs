﻿using EPiFakeMaker;
using EPiServer.Core;
using EPiServer.SpecializedProperties;
using EPiServer.ServiceLocation;
using Moq;
using Norgesportalen.Models.Pages;
using Norgesportalen.Models.ViewModels;
using NUnit.Framework;
using System.Collections.Generic;

namespace Norgesportalen.Tests.Models
{
    [TestFixture]
    public class HomePageTests
    {
        private FakeMaker _fake;

        [SetUp]
        public void Setup()
        {
            _fake = new FakeMaker();

            var serviceLocator = new Mock<IServiceLocator>();
            ServiceLocator.SetLocator(serviceLocator.Object);


        }


        // top task only
        [TestCase(0, false, false, 0)]
        [TestCase(1, false, false, 1)] 
        [TestCase(2, false, false, 2)] 
        [TestCase(3, false, false, 3)] 
        [TestCase(4, false, false, 4)] 
        // with list
        [TestCase(0, true, false, 1)]
        [TestCase(1, true, false, 2)]
        [TestCase(2, true, false, 3)]
        [TestCase(3, true, false, 4)]
        [TestCase(4, true, false, 5)]
        // Edit mode = 5 regardless
        [TestCase(4, false, true, 5)]
        [TestCase(4, false, true, 5)]
        [TestCase(0, true, true, 5)]
        [TestCase(1, true, true, 5)]
        [TestCase(8, true, true, 5)]
        public void TopTasksCount_Test(int publisedItemsCase, bool hasTopTaskListCase, bool isEditModeCase, int excpectedResult)
        {

            // ARRANGE

            // Mock up HomePage
            var homePageMock = new Mock<HomePage>();

            // Mock up Top Task Content area Items
            var contentAreaItems = new List<ContentAreaItem>();
            for (int i=0; i < publisedItemsCase; i++)
            {
                var fakeContentAreaItem = new Mock<ContentAreaItem>();
                contentAreaItems.Add(fakeContentAreaItem.Object);
            }
            // Mock up Top Task Content Area
            var topTasksContentAreaMock = new Mock<ContentArea>();
            homePageMock
                .Setup(x => x.TopTasksContentArea.FilteredItems)
                .Returns(contentAreaItems);
            homePageMock
                .Setup(u => u.TopTasksContentArea)
                .Returns(topTasksContentAreaMock.Object);
            homePageMock
                .Setup(u => u.TopTasksContentArea.Items)
                .Returns(contentAreaItems);

            // Mock up Top Task LIST content area
            if (hasTopTaskListCase)
            {
                var topTasksLinkListMock = new LinkItemCollection();
                topTasksLinkListMock.Add(new LinkItem());

                homePageMock
                    .Setup(h => h.TopTasksLinkList)
                     .Returns(topTasksLinkListMock);
            }


            homePageMock
                .Setup(v => v.IsInEditMode())
                .Returns(isEditModeCase);

            var homePageViewModel = new HomePageViewModel(homePageMock.Object);


            // Act
            int result = homePageViewModel.TopTasksCount();

            // Assert
            Assert.That(result, Is.EqualTo(excpectedResult));

            // to do, casting null. problemer med å mocke.
            //if (excpectedResult<5 && combined!=null)
            //    Assert.That(combined.Count, Is.EqualTo(excpectedResult));
                
        }

        [Test]
        public void TopTaskBackgroundStyle()
        {

            // Arrange
            var homePageMock = new Mock<HomePage>();

            homePageMock
                .Setup(h => h.TopTasksBackground)
                .Returns("1234");

            homePageMock
                .Setup(h => h.GetUrlForContentID(homePageMock.Object.TopTasksBackground))
                .Returns("/globalassets/homepage-backgrounds/studies-research_jumbo.jpg");


            var homePageViewModel = new HomePageViewModel(homePageMock.Object);

            // Act
            string result = homePageViewModel.TopTaskBackgroundStyle;

            // Assert
            Assert.That( result, Does.StartWith("Style=background-image:url('"));
            Assert.That( result, Does.EndWith("');"));
        }


        [TestCase(false,0, "panel-large panel")]
        [TestCase(false, 1, "panel-large panel")]
        [TestCase(false, 2, "panel-large panel")]
        [TestCase(false, 3, "panel-medium panel")]
        [TestCase(true, 0, "panel-large panel")]
        [TestCase(true, 1, "panel-large panel")]
        [TestCase(true, 2, "panel-large panel")]
        [TestCase(true, 3, "panel-medium panel")]
        public void ThemesPanelsCssLayoutTest(bool isEditModeCase, int themesContentAreaCountCase, string excepectStyle)
        {
            // Arrange
            var homePageMock = new Mock<HomePage>();

            // Mock up Top Task Content area Items
            var contentAreaItems = new List<ContentAreaItem>();
            for (int i = 0; i < themesContentAreaCountCase; i++)
            {
                var fakeContentAreaItem = new Mock<ContentAreaItem>();
                contentAreaItems.Add(fakeContentAreaItem.Object);
            }
            // Mock up Top Task Content Area
            var topTasksContentAreaMock = new Mock<ContentArea>();
            homePageMock
                .Setup(x => x.ThemesContentArea.FilteredItems)
                .Returns(contentAreaItems);
            homePageMock
                .Setup(p => p.ThemesContentArea.Items.Count)
                .Returns(themesContentAreaCountCase);

            // EditMode mock
            homePageMock
                .Setup(v => v.IsInEditMode())
                .Returns(isEditModeCase);

            var homePageViewModel = new HomePageViewModel(homePageMock.Object);

            // Act
            string result = homePageViewModel.ThemesPanelsCssLayout;

            // Assert
            Assert.That(result, Is.EqualTo(excepectStyle));

        }

    }
}