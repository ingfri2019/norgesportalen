﻿using Norgesportalen.Models.Pages.BaseClasses;
using NUnit.Framework;

namespace Norgesportalen.Tests.Models
{
    [TestFixture]
    public class BasicPageBaseTests
    {
        [TestCase("", "", "")]
        [TestCase("ABC", "DEF", "ABC")]
        [TestCase("ABC", "", "ABC")]
        [TestCase("", "DEF", "DEF")]
        [TestCase("Der Eintritt zur Veranstaltung ist frei, eine Anmeldung erforderlich.", "ABC", "Der Eintritt zur Veranstaltung ist frei, eine Anmeldung erforderlich.")]
        public void FilteredListViewModel_TeaserOrIntro_Test(string teaser, string intro, string excpeted)
        {
            // Arrange

            // Act
            string result = BasicPageBase.TeaserOrFallbackToIntro(teaser, intro);

            // Assert
            Assert.That(result, Is.EqualTo(excpeted));
            Assert.That(result.Length, Is.LessThan(250));
        }

        [Test]
        public void FilterListViewModel_TeaserOrIntro_Long_Text_Test()
        {
            // A
            string intro = @"Zwei Freunde in einem kleinen Boot, die sich einen lang gehegten Traum erfüllen: Aus den Tiefen des Nordatlantiks wollen sie einen Eishai ziehen, jenes sagenumwobene Ungeheuer, das sich nur selten an der Oberfläche zeigt. Während sie warten, branden wie Wellen die Meeresmythen und Legenden an das Boot, und Morten A. Strøksnes erzählt von echten und erfundenen Wesen, von Quallenarten mit dreihundert Mägen, von Seegurken und Teufelsanglern. Von mutigen Polarforschern, Walfängern und Kartografen und natürlich vom harten Leben an arktischen Ufern, vom Skrei, der vielen Generationen das Überleben auf den Lofoten sicherte, von der Farbe und dem Klang des Meeres. Übersetzt wurde 'Das Buch vom Meer' für Deutsche Verlags-Anstalt DVA / Randomhouse von Ina Kronenberger und Sylvia Kall. Die Künstlerin AK Dolven, die im Berliner Museum für Naturkunde im Herbst 2015 in der Nass-Sammlung bereits mit einer Installation und Aufnahmen von Paarungsgeräuschen vom Kabeljau vor Lofoten eine eindrucksvolle Entdeckungsreise in die Unterwasserwelt unternahm, wird eine Einführung in unbekannte sinnliche und ästhetische Aspekte der nordatlantischen Tiefsee geben. Der Autor Morten A. Strøksnes, 1965 in Kirkenes an der Barentssee geboren, hat Philosophie, Literaturwissenschaft und Geschichte in Oslo und Cambridge studiert. Er lebt heute als Journalist und Autor in Oslo.Er publiziert in renommierten Medien und hat mehrere viel beachtete Sachbücher vorgelegt. 2011 bekam Strøksnes den Preis des Sprachrats, der für Sachbücher von herausragender literarischer Qualität vergeben wird. „Das Buch vom Meer“ ist sein neuntes Buch und wurde in Norwegen zum Nr.- 1 - Bestseller.Es erhielt den Kritiker - Preis und den Brage - Preis, den wichtigsten norwegischen Buchpreis, und erscheint in über 15 Ländern. Der Eintritt zur Veranstaltung ist frei, eine Anmeldung erforderlich. Weitere Informationen: Verlagsgruppe Randomhouse, Nordische Botschaften Berlin Zeit: Dienstag, 27.September 2016, 19:00 Uhr Ort: Nordische Botschaften Berlin, Felleshus - Rauchstraße 1, 10787 Berlin. Zurück zum Anfang";

            // Act
            string result = BasicPageBase.TeaserOrFallbackToIntro(string.Empty, intro);

            // Assert
            Assert.That(result.Length, Is.LessThan(250));
            Assert.That(result, Is.EqualTo("Zwei Freunde in einem kleinen Boot, die sich einen lang gehegten Traum erfüllen: Aus den Tiefen des Nordatlantiks wollen sie einen Eishai ziehen, jenes sagenumwobene Ungeheuer, das sich nur selten an der Oberfläche zeigt."));
        }


    }
}