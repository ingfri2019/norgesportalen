﻿
using NUnit.Framework;
using Norgesportalen.Models.Blocks;
using Moq;
using EPiServer.SpecializedProperties;

namespace Norgesportalen.Tests.Models
{
    [TestFixture]
    public class TopTaskLinkListBlockTests
    {

        const bool Small = true;
        const bool Large = false;

        // Cases when block is in small mode
        //[TestCase(Small, 0, 1,false)]
        //[TestCase(Small, 1, 2,false)]
        //[TestCase(Small, 2, 3,false)]
        //[TestCase(Small, 3, 3, false)]
        //[TestCase(Small, 4, 3, false)]
        //// Cases when block is not in small mode, spanning two rows
        //[TestCase(Large, 0, 1,false)]
        //[TestCase(Large, 1, 2,false)]
        //[TestCase(Large, 2, 3,false)]
        //[TestCase(Large, 3, 4,false)]
        //[TestCase(Large, 4, 5,false)]
        //[TestCase(Large, 5, 5, false)]

        public void ItemsToRender_Test(bool isSmallCase, int linkItemsCase, int excpectedResult, bool excpectedReplacedReadMore)
        {

            // ARRANGE

            // Mock up block
            var topTaskLinkListBlockMock = new Mock<TopTaskLinkListBlock>();

            var linkListMock = new Mock<LinkItemCollection>();
            for (int i = 1; i <= linkItemsCase; i++)
            {
                var fakeLinkItem = new Mock<LinkItem>();

                fakeLinkItem.Object.Text = string.Format("Link {0}",i);
                fakeLinkItem.Object.Title = "has a subtext";
                linkListMock.Object.Add(fakeLinkItem.Object);
            }

            topTaskLinkListBlockMock
                .Setup(t=>t.LinkList)
                .Returns(linkListMock.Object);

            topTaskLinkListBlockMock
                .Setup(t => t.IsSmall)
                .Returns(isSmallCase);

            // Mock header for block
            topTaskLinkListBlockMock
                .Setup(h => h.TitleAndReadmoreLink)
                .Returns(new EPiServer.Core.PageReference());
            topTaskLinkListBlockMock
                .Setup(t => t.ReadMoreLinkTitle)
                .Returns("Read more");

            topTaskLinkListBlockMock
                .Setup(h => h.GetUrlForContentID(It.IsAny<string>()))
                .Returns("/alinkurl");

            topTaskLinkListBlockMock
                .Setup(h => h.TitleAndReadmoreLink)
                .Returns(new EPiServer.Core.PageReference());

            // ACT
            var result = topTaskLinkListBlockMock.Object.ItemsToRender;

            // ASSERT
            Assert.That(result, Is.Not.Null);
            Assert.That(result.Count, Is.EqualTo(excpectedResult));



            // assert that the title is converted correctly from LinkItem to TopTaskLink
            
            var resultAssertArray = result.ToArray();

            for (int k = 0; k < resultAssertArray.Length; k++)
            {
                if (k == 0)
                {
                    Assert.That(resultAssertArray[k].Title, Does.StartWith("Read more"));
                    Assert.That(resultAssertArray[k].Href, Does.StartWith("/alinkurl"));
                }
             
            }



        }
    }
}
