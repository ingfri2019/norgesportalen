﻿using EPiServer;
using EPiServer.Core;
using NUnit.Framework;
using Moq;
using Norgesportalen.Models.Blocks;
using Norgesportalen.Models.Pages;

namespace Norgesportalen.Tests.Models
{
    public class LinkWithTitleBlockTests
    {


        // uncomplete test
        [TestCase("","",null)]
        [TestCase("Some title", "", "Some title")]
        //[TestCase("", "Some page name", "Some page name")]
        public void LinkTitleTest(string title, string pageName, string excpected)
        {
            // Arrange
            var linkWithTitleBlockMock = new Mock<LinkWithTitleBlock>();
            if (title!="")
            {
                linkWithTitleBlockMock.Setup(x => x.Title).Returns(title);
            }
            if (pageName!="")
            {
                var pageRefMock = new Mock<Url>();
                linkWithTitleBlockMock.Setup(x => x.LinkUrl).Returns(pageRefMock.Object);
            }

            // Act
            string result = linkWithTitleBlockMock.Object.Title;

            // Assert
            Assert.AreEqual(excpected, result);

        }


    }
}
