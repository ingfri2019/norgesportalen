﻿using Norgesportalen.Models.ViewModels;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Norgesportalen.Tests.Models
{
    class PagerModelTests
    {
        [TestCase(0, 0, 0, 0)]
        [TestCase(1, 0, 12, 0)]
        [TestCase(1, 12, 12, 1)]
        [TestCase(1, 120, 12, 10)]
        [TestCase(2, 120, 12, 10)]
        [TestCase(3, 120, 12, 10)]
        [TestCase(4, 120, 12, 10)]
        [TestCase(5, 77, 10, 8)]
        [TestCase(6, 55, 10, 6)]
        [TestCase(3, 44, 10, 5)]
        [TestCase(2, 33, 10, 4)]
        [TestCase(50, 1000, 10, 100)]
        public void PagingTest(int activePageCase, int numberOfHitsCase, int pageSizeCase, int pagesShouldBe)
        {
            // Arrange

            // Act
            var paging = new PagingModel(
                                    numberOfHitsCase, 
                                    pageSizeCase, 
                                    activePageCase);

            // Assert
            Assert.That(paging, Is.Not.Null);

            if (numberOfHitsCase <= pageSizeCase)
            {
                Assert.That(paging.PagerLinks, Is.Null);
                Assert.That(paging.PreviousPage, Is.Null);
                Assert.That(paging.NextPage, Is.Null);

                return;
            }

            Assert.That(paging.PagerLinks.Count(), Is.EqualTo(pagesShouldBe));

            var activePages = paging.PagerLinks.Where(x => x.IsActivePage == true);
            Assert.That(activePages.Count(), Is.EqualTo(1));
            Assert.That(activePages.First().LinkText, Is.EqualTo(activePageCase.ToString()));

            if (activePageCase == 1)
                Assert.That(paging.PreviousPage, Is.Null);
            else
            {
                Assert.That(paging.PreviousPage, Is.Not.Null);
                Assert.That(paging.PreviousPage.LinkText, Is.Not.Empty);
                Console.Write("<< ");
            }

            int i = 1;
            foreach (var p in paging.PagerLinks)
            {
                Assert.That(p.LinkText, Is.EqualTo("" + i));
                Assert.That(p.Route, Is.Not.Null);
                //Assert.That(p.Route.Values.Count(), Is.EqualTo(2));
                Assert.That(p.Route["Page"], Is.EqualTo(i));
                //Assert.That(p.Route["Filter"], Is.EqualTo("news"));

                if (p.IsActivePage)
                {
                    Console.Write(" (" + p.LinkText + ") ");
                }
                else
                {
                    Console.Write(" _" + p.LinkText + "_ ");
                }
                i++;
            }

            if (activePageCase < pagesShouldBe)
            {
                Assert.That(paging.NextPage, Is.Not.Null);
                Assert.That(paging.NextPage.LinkText, Is.Not.Empty);
                Console.Write(" >>");
            }
            else
                Assert.That(paging.NextPage, Is.Null);

            Console.WriteLine("");

        }

    }
}
