﻿using EPiFakeMaker;
using EPiServer;
using EPiServer.Core;
using EPiServer.ServiceLocation;
using Moq;
using Norgesportalen.Business.Helpers;
using Norgesportalen.Models.Pages;
using Norgesportalen.Models.Pages.BaseClasses;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using Er = NUnit.Framework.Is;
using Påstå = NUnit.Framework.Assert;

namespace Norgesportalen.Business.Tests.Helpers
{
    /*
         AKSEPTANSEKRITERIE
        Når man er på en underside skal brødsmulestien vises. 
        Forsiden skal være første del av brødsmulestien.
        Alle delene i brødsmulestien skal være klikkbare og navigere besøkeren til riktig sted.
        Brødsmulestien skal ikke være sticky. 
        Brødsmulestien skal presenteres som - forsiden\valgt-område-1\valgt-område-2** - for eksempel: Norway in Canada\ Values and Priorites\ Areas in the north
        Brødsmulestien skal maks inneholde 4 nivåer inkludert forsiden og siste nivå.
        Hvis et femte nivå introduseres i stien skal nivået som vises etter forsiden erstattes med "...".
        Ved introduksjon av et sjette og sjuende nivå skal nivå "forsiden" fortsatt vises men nivå tre og fire flyttes inn under nivået "..."
        Ved reduksjon av nivåer fra sjuende til sjette og til femte så skal de respektive nivåene (nivå 4, 3 og 2) dukke opp fra "..." 
        nivået.
        Nivået man befinner seg på skal ikke være klikkbart.
        Eksempel
        Forsiden>nivå-2>nivå-3>nivå-4
        Forsiden>...>nivå-3>nivå-4>nivå-5
        Forsiden>...>nivå-4>nivå-5>nivå-6
        Forsiden>...>nivå-5>nivå-6>nivå-7
        (Som redaktør skal jeg kunne velge om en seksjon skal vises i brødsmulestien eller ikke ved å huke av "Show in menues".)
    */
    [TestFixture()]
    public class BreadcrumbTests
    {
        [TestCase(0,"")]
        [TestCase(1, "")]
        [TestCase(2, "Forsiden>nivå-2")]
        [TestCase(3, "Forsiden>nivå-2>nivå-3")]
        [TestCase(4, "Forsiden>nivå-2>nivå-3>nivå-4")]
        [TestCase(5, "Forsiden>..>nivå-3>nivå-4>nivå-5")]
        [TestCase(6, "Forsiden>..>..>nivå-4>nivå-5>nivå-6")]
        [TestCase(7, "Forsiden>..>..>..>nivå-5>nivå-6>nivå-7")]
        [TestCase(8, "Forsiden>..>..>..>..>nivå-6>nivå-7>nivå-8")]
        [TestCase(9, "Forsiden>..>..>..>..>..>nivå-7>nivå-8>nivå-9")]
        public void Brødsmuleti_Nivåer_Erstattets_Av_Prikker_Test(int antallNivåerCase, string forventetResultat)
        {
            #region Arrange
            FakeMaker _fake;
            FakePage _root;
            _fake = new FakeMaker();
            _root = FakePage.Create("root");
            var mfaPage = FakePage.Create<FolderPage>("MFA").ChildOf(_root).PublishedOn(DateTime.Now.AddDays(-1));
            var worldPage = FakePage.Create<CountriesOverviewPage>("World").ChildOf(mfaPage).PublishedOn(DateTime.Now.AddDays(-1));
            var forsiden = FakePage.Create<HomePage>("Forsiden").ChildOf(worldPage).PublishedOn(DateTime.Now.AddDays(-1)).AsStartPage();


            List<FakePage> sider = new List<FakePage>();
            var parent = forsiden;
            for (int i = 2; i < antallNivåerCase + 1; i++)
            {
                var nyttNivå = FakePage.Create<InformationArticlePage>("nivå-" + i).ChildOf(parent).PublishedOn(DateTime.Now.AddDays(-1));
                sider.Add(nyttNivå);
                parent = nyttNivå;
            }

            FakePage currentPage = forsiden;

            if (sider.Count() > 0) currentPage = sider.Last();

            var mockRepository = new Mock<IContentRepository>();

            var ancestors = new List<IContent>();
            ancestors.AddRange(new List<IContent>() {
                    _root.Page,
                    mfaPage.Page,
                    worldPage.Page,
                    forsiden.Page
                });

            SetIds(ancestors);

            ancestors.AddRange(sider.Select(s => s.Page));
            ancestors.Remove(ancestors.Last()); // remove current from ancestors
            ancestors.Reverse();

            mockRepository
            .Setup(repo => repo.GetAncestors(currentPage.Page.ContentLink))
            .Returns(ancestors);

            foreach (var page in ancestors)
            {
                if (page is SitePageData)
                {
                    mockRepository
                        .Setup(repo => repo.Get<SitePageData>(page.ContentLink))
                        .Returns((SitePageData)page);
                }
            }
            mockRepository
                .Setup(repo => repo.Get<SitePageData>(currentPage.Page.ContentLink))
                .Returns((SitePageData)currentPage.Page);

            Mock<IServiceLocator> _mockLocator = new Mock<IServiceLocator>();

            _mockLocator
                .Setup(l => l.GetInstance<IContentRepository>())
                .Returns(mockRepository.Object);

            ServiceLocator.SetLocator(_mockLocator.Object);

            #endregion

            // Act
            var breadcrumb = HtmlHelpers.CreateBreadcrumb((SitePageData)currentPage.Page, false, false, false, false);
            var breadcrumbText = string.Join(">", breadcrumb.Select(x => x.Text).ToList());
            Console.WriteLine(breadcrumbText);
            // Påstå


            if (breadcrumb.Count() > 0)
            {
                Påstå.That(breadcrumb.Count(), Er.EqualTo(antallNivåerCase));
                Påstå.That(breadcrumb.First().Text, Er.EqualTo(forsiden.Page.PageName));
                Påstå.That(breadcrumb.Last().Text, Er.EqualTo(currentPage.Page.PageName));
                Påstå.That(breadcrumb.Last().LinkUrl, Er.Null);
            }

            Påstå.That(breadcrumbText, Er.EqualTo(forventetResultat));
        }

        [Test]
        public void BreadcrumbTest()
        {
            #region Arrange
            FakeMaker _fake;
            FakePage _root;
            _fake = new FakeMaker();
            _root = FakePage.Create("root");
            var mfaPage = FakePage.Create<FolderPage>("MFA").ChildOf(_root).PublishedOn(DateTime.Now.AddDays(-1));
            var worldPage = FakePage.Create<CountriesOverviewPage>("World").ChildOf(mfaPage).PublishedOn(DateTime.Now.AddDays(-1));
            var breadcrumbLandPage = FakePage.Create<HomePage>("BreadcrumbLand").ChildOf(worldPage).PublishedOn(DateTime.Now.AddDays(-1)).AsStartPage();
            var newsListPage = FakePage.Create<FilteredListPage>("News and events list").ChildOf(breadcrumbLandPage).PublishedOn(DateTime.Now.AddDays(-1));
            var newscontainerPage = FakePage.Create<FilteredListPage>("News").ChildOf(newsListPage).PublishedOn(DateTime.Now.AddDays(-1));
            var news2016Page = FakePage.Create<FilteredListPage>("Nyheter 2016").ChildOf(newscontainerPage).PublishedOn(DateTime.Now.AddDays(-1));
            var newsAprilPage = FakePage.Create<FilteredListPage>("Nyheter April").ChildOf(news2016Page).PublishedOn(DateTime.Now.AddDays(-1));
            var news1Page = FakePage.Create<FilteredListPage>("Nyheter 1.").ChildOf(newsAprilPage).PublishedOn(DateTime.Now.AddDays(-1));
            var currentPage = FakePage.Create<NewsPage>("Great news!").ChildOf(news1Page).PublishedOn(DateTime.Now.AddDays(-1));

            var mockRepository = new Mock<IContentRepository>();

            var ancestors =
            new List<IContent>()
                {
                 newsAprilPage.Page,
                 news1Page.Page,
                    news2016Page.Page,
                    newscontainerPage.Page,
                    newsListPage.Page,
                    breadcrumbLandPage.Page,
                    worldPage.Page,
                    mfaPage.Page,
                    _root.Page
                };

            SetIds(ancestors);

            // returns descents of root
            mockRepository
                .Setup(repo => repo.GetAncestors(currentPage.Page.ContentLink))
                .Returns(ancestors);

            foreach (var page in ancestors)
            {
                if (page is SitePageData)
                {
                    mockRepository
                        .Setup(repo => repo.Get<SitePageData>(page.ContentLink))
                        .Returns((SitePageData)page);
                }
            }

            Mock<IServiceLocator> _mockLocator;
            _mockLocator = new Mock<IServiceLocator>();

            _mockLocator
                .Setup(l => l.GetInstance<IContentRepository>())
                .Returns(mockRepository.Object);

            // Make use of our mock objects throughout EPiServer
            ServiceLocator.SetLocator(_mockLocator.Object);

            #endregion

            // Act
            var crumbs = HtmlHelpers.CreateBreadcrumb((SitePageData)currentPage.Page,false,false, false, false);
            var breadcrumb = crumbs.Select(x => x.Text).ToList();
            string brødsmulestien = string.Join(" / ", breadcrumb);
            Console.WriteLine(brødsmulestien);

            // Påstå

            Påstå.That(crumbs, Er.Not.Empty);
            Påstå.That(crumbs.Count(), Er.EqualTo(7));
            Påstå.That(crumbs.First().Text, Er.EqualTo(breadcrumbLandPage.Page.PageName));
            Påstå.That(crumbs.Last().Text, Er.EqualTo(currentPage.Page.PageName));
            Påstå.That(crumbs.Last().LinkUrl, Er.Null);
            Påstå.That(brødsmulestien, Is.EqualTo("BreadcrumbLand / .. / .. / .. / Nyheter 1. / Nyheter April / Great news!"));



        }

        [Test]
        public void Breadcrumb_CurrentPage_Is_Null_Test()
        {
            // Arrange
            SitePageData currentPage = null;

            // Act
            var result = HtmlHelpers.CreateBreadcrumb(currentPage,false,false);

            // Påstå
            Påstå.That(result, Er.EqualTo(string.Empty));

        }

        static int pageId = 100;
        private static void SetIds(List<IContent> contents)
        {
            //BUG: FakePage.Create contains this code: .WithReferenceId(Randomizer.Next(10, 1000));
            //Once in a while, a list with more than one page will generate same id, so tests will fail
            //Hence this code; setting the ID's manually
            pageId += 100;
            foreach (var content in contents)
                content.ContentLink.ID = pageId++;
        }
    }
}