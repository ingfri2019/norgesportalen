﻿using System;
using System.Collections.Generic;
using System.Linq;
using EPiFakeMaker;
using EPiServer.Core;
using Norgesportalen.Business.Services;
using Norgesportalen.Controllers;
using Norgesportalen.Models.Pages;
using NUnit.Framework;
using Should;

namespace Norgesportalen.Tests.Controllers
{
    [TestFixture]
    public class CountriesOverviewControllerTests
    {
        // https://github.com/DavidVujic/EPiServer-FakeMaker
        private FakeMaker _fake;

        [SetUp]
        public void Setup()
        {
            _fake = new FakeMaker();
        }

        //[Test]
        //public void CreateAllUtenriksstasjonListTest()
        //{

        //    // Arrange 
        //    PageDataCollection pages = new PageDataCollection();

        //    // include
        //    pages.Add(FakePage.Create<EmbassyStartPage>("Angola").PublishedOn(DateTime.Now.AddDays(-1)).Page);
        //    pages.Add(FakePage.Create<EmbassyStartPage>("Sri Lanka").PublishedOn(DateTime.Now.AddDays(-2)).Page);
        //    pages.Add(FakePage.Create<EmbassyStartPage>("Denmark").PublishedOn(DateTime.Now.AddDays(-3)).Page);
        //    pages.Add(FakePage.Create<EmbassyStartPage>("USA").PublishedOn(DateTime.Now.AddDays(-4)).Page);
        //    pages.Add(FakePage.Create<EmbassyStartPage>("Ireland").PublishedOn(DateTime.Now.AddDays(-5)).Page);
        //    pages.Add(FakePage.Create<EmbassyStartPage>("Algeria").PublishedOn(DateTime.Now.AddDays(-1)).Page);
        //    pages.Add(FakePage.Create<EmbassyStartPage>("ÆØÅ").PublishedOn(DateTime.Now.AddDays(-1)).Page);
        //    pages.Add(FakePage.Create<EmbassyStartPage>("123").PublishedOn(DateTime.Now.AddDays(-1)).Page);
        //    // excluded
        //    pages.Add(FakePage.Create<EmbassyStartPage>("Italy").PublishedOn(DateTime.Now.AddDays(-6)).HiddenFromMenu().Page);
        //    pages.Add(FakePage.Create<EmbassyStartPage>("Canada").StopPublishOn(DateTime.Now.AddDays(-1)).Page);


        //    // Act
        //    var categorizedEmbassiesList = EmbassiesService.CreateAlphaCategorizedEmbassiesList(pages);

        //    // Assert
        //    categorizedEmbassiesList.ShouldNotBeNull();
        //    categorizedEmbassiesList.Count.ShouldEqual(7);

        //    // Sorted ?
        //    EmbassySiteCategory[] categories = categorizedEmbassiesList.ToArray();

        //    categories[0].Letter.ShouldEqual('1');
        //    categories[0].Embassies.Count.ShouldEqual(1);

        //    categories[1].Letter.ShouldEqual('Æ');
        //    categories[1].Embassies.Count.ShouldEqual(1);

        //    categories[2].Letter.ShouldEqual('A');
        //    categories[2].Embassies.Count.ShouldEqual(2);
        //    categories[2].Embassies[0].Name.ShouldEqual("Algeria");
        //    categories[2].Embassies[1].Name.ShouldEqual("Angola");

        //    categories[3].Letter.ShouldEqual('D');
        //    categories[3].Embassies.Count.ShouldEqual(1);
        //    categories[3].Embassies[0].Name.ShouldEqual("Denmark");

        //    categories[4].Letter.ShouldEqual('I');
        //    categories[4].Embassies.Count.ShouldEqual(1);
        //    categories[4].Embassies[0].Name.ShouldEqual("Ireland");

        //    categories[5].Letter.ShouldEqual('S');
        //    categories[5].Embassies[0].Name.ShouldEqual("Sri Lanka");

        //    categories[6].Letter.ShouldEqual('U');
        //    categories[6].Embassies[0].Name.ShouldEqual("USA");

        //    // A.....print
        //    foreach (var category in categories)
        //    {
        //        Console.WriteLine(category.Letter);
        //        foreach (var embassy in category.Embassies)
        //            Console.WriteLine(" * " + embassy.Name + " (" + embassy.Website+ ")");
        //    }

        //}
    }
}

