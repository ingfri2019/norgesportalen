﻿using System;
using System.Collections.Generic;
using System.Linq;
using EPiFakeMaker;
using EPiServer;
using EPiServer.Core;
using EPiServer.ServiceLocation;
using Norgesportalen.Controllers;
using Norgesportalen.Models.Pages;
using Norgesportalen.Models.Pages.BaseClasses;
using Norgesportalen.Models.ViewModels;
using NUnit.Framework;
using Moq;

namespace Norgesportalen.Tests.Controllers
{
    [TestFixture]
    public class FilteredListPageControllerTest
    {

        [SetUp]
        public void Setup()
        {

            // Mock up fake repositery
            FakeMaker _fake;
            FakePage _root;
            _fake = new FakeMaker();
            _root = FakePage.Create("root");
            var mfa = FakePage.Create<FolderPage>("MFA").ChildOf(_root);
            var newsAndEventsListPage = FakePage.Create<FilteredListPage>("News and events list").ChildOf(mfa);
            // N
            var newscontainer = FakePage.Create<ContainerPage>("News").ChildOf(newsAndEventsListPage);
            var news2016container = FakePage.Create<ContainerPage>("2016").ChildOf(newscontainer);
            var news1 = FakePage.Create<NewsPage>("Great news!").ChildOf(news2016container).PublishedOn(DateTime.Now.AddDays(-1));
            var news2 = FakePage.Create<NewsPage>("Bad news!").ChildOf(news2016container).PublishedOn(DateTime.Now.AddDays(-1));
            // E
            var eventscontainer = FakePage.Create<ContainerPage>("Events").ChildOf(newsAndEventsListPage);
            var event2016container = FakePage.Create<ContainerPage>("2016").ChildOf(eventscontainer);
            var event1 = FakePage.Create<EventPage>("Meeting event").ChildOf(event2016container).PublishedOn(DateTime.Now.AddDays(-1));
            // S
            var statementscontainer = FakePage.Create<ContainerPage>("statements").ChildOf(newsAndEventsListPage);
            var statement2016container = FakePage.Create<ContainerPage>("2016").ChildOf(statementscontainer);
            var statement1 = FakePage.Create<StatementPage>("Statement 1 from the clergy").ChildOf(statement2016container).PublishedOn(DateTime.Now.AddDays(-1));
            // A
            var articlescontainer = FakePage.Create<ContainerPage>("articles").ChildOf(newsAndEventsListPage);
            var articles2016container = FakePage.Create<ContainerPage>("2016").ChildOf(articlescontainer);
            var articles1 = FakePage.Create<InformationArticlePage>("Article 1 from the clergy").ChildOf(articles2016container).PublishedOn(DateTime.Now.AddDays(-1));
            var articles2 = FakePage.Create<InformationArticlePage>("Article 2 from the clergy").ChildOf(articles2016container).PublishedOn(DateTime.Now.AddDays(-1));
            //var articles3 = FakePage.Create<InformationArticlePage>("Article 3 from the clergy").ChildOf(articles2016container).PublishedOn(DateTime.Now.AddDays(-1));

            var mockRepository = new Mock<IContentRepository>();

            // returns descents of root
            mockRepository
                .Setup(repo => repo.GetDescendents(It.IsAny<ContentReference>()))
                .Returns(new List<ContentReference> {
                    newscontainer.Page.ContentLink,
                    news2016container.Page.ContentLink,
                    news1.Page.ContentLink,
                    news2.Page.ContentLink,
                    eventscontainer.Page.ContentLink,
                    event2016container.Page.ContentLink,
                    event1.Page.ContentLink,
                    statementscontainer.Page.ContentLink,
                    statement2016container.Page.ContentLink,
                    statement1.Page.ContentLink,
                    articlescontainer.Page.ContentLink,
                    articles2016container.Page.ContentLink,
                    articles1.Page.ContentLink,
                    articles2.Page.ContentLink
                });

            // Todo: do something generic setup here:
            mockRepository
                .Setup(repo => repo.Get<PageData>(news1.Page.ContentLink))
                .Returns(news1.Page);
            mockRepository
                .Setup(repo => repo.Get<PageData>(news2.Page.ContentLink))
                .Returns(news2.Page);
            mockRepository
                .Setup(repo => repo.Get<PageData>(event1.Page.ContentLink))
                .Returns(event1.Page);
            mockRepository
                .Setup(repo => repo.Get<PageData>(statement1.Page.ContentLink))
                .Returns(statement1.Page);
            mockRepository
                .Setup(repo => repo.Get<PageData>(articles1.Page.ContentLink))
                .Returns(articles1.Page);
            mockRepository
                .Setup(repo => repo.Get<PageData>(articles2.Page.ContentLink))
                .Returns(articles2.Page);

            // Setup the service locator to return our mock repository when an IContentRepository is requested
            Mock<IServiceLocator> _mockLocator;
            _mockLocator = new Mock<IServiceLocator>();
            _mockLocator.Reset();

            _mockLocator
                .Setup(l => l.GetInstance<IContentRepository>())
                .Returns(mockRepository.Object);

            // Make use of our mock objects throughout EPiServer
            ServiceLocator.SetLocator(_mockLocator.Object);
        }


        // TODO: Det er noe rart med test case runing og setup... feiler random noen ganger!
        [TestCase("", 4)]
        [TestCase(null, 4)]
        [TestCase("View all", 4)] // NewsAndEventsListViewModel.Filters.ViewAll
        public void GetNewsEventsStatements(string filterCase, int excepected)
        {

            // Arrange
            var controllerMock = new Mock<FilteredListPageController>();
            var currentPage = new Mock<FilteredListPage>();

            // Act
            var _contentRepository = ServiceLocator.Current.GetInstance<IContentRepository>();
            var descendents = _contentRepository.GetDescendents(currentPage.Object.ContentLink);
            IEnumerable<PageData> descendentPages = descendents
                                                    .Select(descendant => _contentRepository.Get<PageData>(descendant));
            //.FilterForDisplay(); <- Cannot mock extension method

            var result = FilteredListPageController.FilterDescendents(descendentPages);

            // Assert
            Assert.That(result, Is.Not.Null);

            // Feiler random:
            Assert.That(result.Count, Is.GreaterThanOrEqualTo(excepected));
        }

        [TestCase("", 200)]
        [TestCase(null, 200)]
        [TestCase("View all", 200)] // NewsAndEventsListViewModel.Filters.ViewAll
        public void FilterManyTest(string filterCase, int excpected)
        {

            // Arrange
            var controllerMock = new Mock<FilteredListPageController>();
            var currentPage = new Mock<FilteredListPage>();

            var descendentPages = new List<PageData>();
            for (int i = 0; i < 200; i++)
            {
                descendentPages.Add(
                    FakePage
                        .Create<NewsPage>("Article 3 from the clergy")
                        .PublishedOn(DateTime.Now.AddDays(-1))
                        .Page
                    );
            }

            // Act

            DateTime start = DateTime.Now;

            var result = FilteredListPageController.FilterDescendents(descendentPages);

            TimeSpan time = DateTime.Now - start;

            Console.WriteLine("Filtered time {0} ms", time.TotalMilliseconds);

            // Assert
            Assert.That(result.Count, Is.EqualTo(excpected));


        }




        [TestCase("", "")]
        [TestCase(null, "")]
        [TestCase("Berlin", " | Berlin")]
        [TestCase("Frankfurt am main airport", " | Frankfurt am main airport")]
        public void FilterListViewModel_Location(string locationCase, string excpected)
        {
            // Arrange
            var pageMock = new Mock<BasicPageBase>();
            pageMock
                .Setup(x => x.Location)
                .Returns(locationCase);
            pageMock
                .Setup(x => x.HideDate)
                .Returns(false);

            // Act
            string result = FilteredListViewModel.FormatLocation(pageMock.Object);

            // Assert
            Assert.That(result, Is.EqualTo(excpected));
        }



    }
}