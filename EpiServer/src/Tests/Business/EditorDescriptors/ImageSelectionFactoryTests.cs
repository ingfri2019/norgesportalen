﻿using EPiFakeMaker;
using EPiServer;
using EPiServer.Core;
using EPiServer.ServiceLocation;
using Moq;
using Norgesportalen.Business.EditorDescriptors;
using Norgesportalen.Models.Media;
using Norgesportalen.Models.Pages;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using EPiServer.Shell.ObjectEditing;

namespace Norgesportalen.Tests.Business.EditorDescriptors
{
    [TestFixture]
    public class ImageSelectionFactoryTests
    {
        [SetUp]
        public void Setup()
        {
            /*
            Root
                - Empty Folder
                    -
                - Test folder
                   - Image1
                   - Image2
                   - Image3
                - MFA
                    - World
                        - Argentina
                        - Denmark
            */
            FakeMaker _fake;
            FakePage _root;
            _fake = new FakeMaker();
            _root = FakePage.Create("root");
            var emptyFolder = FakePage.Create((new EmptyFolderSetting()).AssetFolderName()).ChildOf(_root);
            var testFolder = FakePage.Create((new TestFolderSetting()).AssetFolderName()).ChildOf(_root);
            var image1 = FakePage.Create("Image1.png").ChildOf(testFolder);
            var image2 = FakePage.Create("Image2.png").ChildOf(testFolder);
            var image3 = FakePage.Create("Image3.png").ChildOf(testFolder);
            var mfa = FakePage.Create<FolderPage>("MFA").ChildOf(_root);
            var world = FakePage.Create<CountriesOverviewPage>("World").ChildOf(mfa).AsStartPage();
            var argentina = FakePage.Create<HomePage>("Argentina").ChildOf(world);
            var denmark = FakePage.Create<HomePage>("Denmark").ChildOf(world);

            // https://tedgustaf.com/blog/2013/unit-testing-in-episerver-7/
            // ...
            var mockRepository = new Mock<IContentRepository>();

            // Setup the repository to return a root page with a preset property value
            mockRepository
                .Setup(r => r.Get<PageData>(ContentReference.RootPage))
                .Returns(_root.Page);

            // returns descents of root
            var rootDescendents = new List<ContentReference> {
                image1.Page.ContentLink,
                image2.Page.ContentLink,
                image3.Page.ContentLink,
                world.Page.ContentLink,
                mfa.Page.ContentLink,
                denmark.Page.ContentLink,
                argentina.Page.ContentLink,
                emptyFolder.Page.ContentLink,
                testFolder.Page.ContentLink
            };

            mockRepository
                .Setup(repo => repo.GetDescendents(ContentReference.RootPage))
                .Returns(rootDescendents);

            // mock up get content...
            // TODO : Mock to return any content referensie in _root !

            mockRepository
                .Setup(repo => repo.Get<IContent>(It.IsAny<ContentReference>()))
                .Returns(mfa.Page);

            mockRepository
                .Setup(repo => repo.Get<IContent>(emptyFolder.Page.ContentLink))
                .Returns(emptyFolder.Page);

            mockRepository
                .Setup(repo => repo.Get<IContent>(testFolder.Page.ContentLink))
                .Returns(testFolder.Page);

            // mock up the call to get images in test folder
            var imagesInTestFolder = new List<ImageFile>();
            imagesInTestFolder.Add(new ImageFile { Name = "Image1.png", ContentLink = image1.Page.ContentLink });
            imagesInTestFolder.Add(new ImageFile { Name = "Image2.png", ContentLink = image2.Page.ContentLink });
            imagesInTestFolder.Add(new ImageFile { Name = "Image3.png", ContentLink = image2.Page.ContentLink });

            mockRepository
                .Setup(repo => repo.GetChildren<IContent>(testFolder.Page.ContentLink))
                .Returns(imagesInTestFolder);

            // mock up when asking for children in emtpy folder
            mockRepository
               .Setup(repo => repo.GetChildren<IContent>(emptyFolder.Page.ContentLink))
               .Returns(new List<ImageFile>());

            // Create a mock service locator
            var mockLocator = new Mock<IServiceLocator>();

            // Setup the service locator to return our mock repository when an IContentRepository is requested
            mockLocator
                .Setup(l => l.GetInstance<IContentRepository>())
                .Returns(mockRepository.Object);

            // Make use of our mock objects throughout EPiServer
            ServiceLocator.SetLocator(mockLocator.Object);
        }

        public class TestFolderSetting : IImageSelectorSettings
        {
            public string AssetFolderName()
            {
                return "Test folder";
            }
        }

        [Test]
        public void Test_Folder_Should_Have_Four_Options()
        {
            // Arrange
            var ImageSelectionFactory = new ImageSelectionFactory<TestFolderSetting>();
            ExtendedMetadata dontCare = null;

            // ACt
            var selections = ImageSelectionFactory.GetSelections(dontCare);

            // Assert
            Assert.IsNotNull(selections);
            Assert.AreEqual(4, selections.Count());

            var images = selections.ToArray();

            Assert.AreEqual(4, images.Count());
            Assert.AreEqual("None", images[0].Text);
            Assert.AreEqual(string.Empty, images[0].Value);
            Assert.AreEqual("Image1.png", images[1].Text);
            Assert.AreEqual("Image2.png", images[2].Text);
            Assert.AreEqual("Image3.png", images[3].Text);

        }

        public class EmptyFolderSetting : IImageSelectorSettings
        {
            public string AssetFolderName()
            {
                return "EmptyFolder";
            }
        }

        [Test]
        public void Empty_Folder_Should_Give_None_Option_Test()
        {
            // Arrange
            var ImageSelectionFactory = new ImageSelectionFactory<EmptyFolderSetting>();
            ExtendedMetadata dontCare = null;

            // Act
            var selections = ImageSelectionFactory.GetSelections(dontCare).ToArray();

            // Assert
            Assert.IsNotNull(selections);
            Assert.AreEqual(1, selections.Count());
            Assert.AreEqual("None", selections[0].Text);
            Assert.AreEqual(string.Empty, selections[0].Value);
        }

        #region Helper test to understand Fakemaker / Moq

        [Test]
        public void Locate_IContentRepository_Test()
        {
            // Act
            IContentRepository contentRepository = ServiceLocator.Current.GetInstance<IContentRepository>();

            // assert
            Assert.IsNotNull(contentRepository);
        }

        [Test]
        public void GetDescendents_From_Root_Test()
        {
            // Act
            IContentRepository contentRepository = ServiceLocator.Current.GetInstance<IContentRepository>();
            var somethingPlease = contentRepository.GetDescendents(ContentReference.RootPage);

            // Assert
            Assert.IsNotNull(somethingPlease);
            Assert.IsTrue(somethingPlease.Any());
            Assert.GreaterOrEqual(9, somethingPlease.Count());
        }

        #endregion Helper test to understand Fakemaker / Moq
    }
}