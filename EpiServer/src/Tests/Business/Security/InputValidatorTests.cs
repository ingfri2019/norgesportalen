﻿using System;
using Norgesportalen.Business.Security;
using NUnit.Framework;

namespace Norgesportalen.Tests.Business.Security
{
    [TestFixture()]
    public class InputValidatorTests
    {
        #region IsValidEmail

        [Test]
        public void IsValidEmail_null_returnsfalse()
        {
            //Arrange
            InputValidator validator = new InputValidator();

            //Act
            var result = validator.IsValidEmail(null);

            //Assert
            Assert.IsFalse(result);
        }

        [Test]
        public void IsValidEmail_emptystring_returnsfalse()
        {
            //Arrange
            InputValidator validator = new InputValidator();

            //Act
            var result = validator.IsValidEmail(string.Empty);

            //Assert
            Assert.IsFalse(result);
        }

        [Test]
        public void IsValidEmail_validemail_returnstrue()
        {
            //Arrange
            InputValidator validator = new InputValidator();
            string input = "test@test.no";

            //Act
            var result = validator.IsValidEmail(input);

            //Assert
            Assert.IsTrue(result);
        }

        [TestCase("email@test.com")]
        [TestCase("firstname.lastname@test.com")]
        [TestCase("email@subdomain.test.com")]
        [TestCase("email@123.123.123.123")]
        [TestCase("email@[123.123.123.123]")]
        [TestCase("1234567890@test.com")]
        [TestCase("email@test-one.com")]
        [TestCase("_______@example.com")]
        [TestCase("email@test.name")]
        [TestCase("email@test.co.jp")]
        [TestCase("firstname-lastname@test.com")]
        public void IsValidEmail_validemaillist_returnstrue(string input)
        {
            //Arrange
            InputValidator validator = new InputValidator();

            //Act
            var result = validator.IsValidEmail(input);

            //Assert
            Assert.IsTrue(result);
        }

        [Test]
        public void IsValidEmail_invalidemail_returnsfalse()
        {
            //Arrange
            InputValidator validator = new InputValidator();
            string input = "test@test@test.no";

            //Act
            var result = validator.IsValidEmail(input);

            //Assert
            Assert.IsFalse(result);
        }

        [TestCase("plainaddress")]
        [TestCase("#@%^%#$@#$@#.com")]
        [TestCase("@test.com")]
        [TestCase("Joe Smith <email@test.com>")]
        [TestCase("email.test.com")]
        [TestCase("email@test@test.com")]
        [TestCase("あいうえお@test.com")]
        [TestCase("email@test.com (Joe Smith)")]
        [TestCase("email@test")]
        [TestCase("email@111.222.333.44444")]
        [TestCase("email@test..com")]
        [TestCase("“(),:;<>[\\]@test.com")]
        public void IsValidEmail_invalidemaillist_returnsfalse(string input)
        {
            //Arrange
            InputValidator validator = new InputValidator();

            //Act
            var result = validator.IsValidEmail(input);

            //Assert
            Assert.IsFalse(result);
        }



        [Test]
        public void IsValidEmail_helloworldscript_returnsfalse()
        {
            //Arrange
            InputValidator validator = new InputValidator();
            string input = "<script>alert('Hello, World!')</script>";

            //Act
            var result = validator.IsValidEmail(input);

            //Assert
            Assert.IsFalse(result);
        }



        #endregion


        #region IsValidGuid


        [Test]
        public void IsValidGuid_null_returnsfalse()
        {
            //Arrange
            InputValidator validator = new InputValidator();

            //Act
            var result = validator.IsValidGuid(null);

            //Assert
            Assert.IsFalse(result);
        }

        [Test]
        public void IsValidGuid_emptystring_returnsfalse()
        {
            //Arrange
            InputValidator validator = new InputValidator();

            //Act
            var result = validator.IsValidGuid(string.Empty);

            //Assert
            Assert.IsFalse(result);
        }



        [Test]
        public void IsValidGuid_helloworldscript_returnsfalse()
        {
            //Arrange
            InputValidator validator = new InputValidator();
            string input = "<script>alert('Hello, World!')</script>";

            //Act
            var result = validator.IsValidGuid(input);

            //Assert
            Assert.IsFalse(result);
        }


        #endregion
    }
}
