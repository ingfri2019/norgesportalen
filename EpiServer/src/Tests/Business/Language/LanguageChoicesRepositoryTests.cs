﻿using System;
using System.Collections.Generic;
using System.Globalization;
using EPiFakeMaker;
using EPiServer.Core;
using Moq;
using Norgesportalen.Business.Language;
using Norgesportalen.Business.Wrappers;
using Norgesportalen.Models.Pages;
using NUnit.Framework;

namespace Norgesportalen.Tests.Business.Language
{
    [TestFixture]
    public class LanguageChoicesRepositoryTests
    {
        private FakeMaker _fake;
        private FakePage _root;

        [SetUp]
        public void Setup()
        {
            _fake = new FakeMaker();
        }

        private IEnumerable<CultureInfo> Create3CultureInfo()
        {
            List<CultureInfo> cultureInfoList = new List<CultureInfo>();

            cultureInfoList.Add(new CultureInfo("en"));
            cultureInfoList.Add(new CultureInfo("sv"));
            cultureInfoList.Add(new CultureInfo("fr"));

            return cultureInfoList;
        }

        private IEnumerable<CultureInfo> Create4CultureInfo()
        {
            List<CultureInfo> cultureInfoList = new List<CultureInfo>();

            cultureInfoList.Add(new CultureInfo("en"));
            cultureInfoList.Add(new CultureInfo("sv"));
            cultureInfoList.Add(new CultureInfo("fr"));
            cultureInfoList.Add(new CultureInfo("de"));

            return cultureInfoList;
        }



        [Test]
        public void CreateLanguageOptions_2EnableLanguages3PageLanguages_Returns2Options()
        {
            //Arrange
            _root = FakePage.Create("root");

            var start = FakePage
                .Create("CountryOverview")
                .ChildOf(_root)
                .AsStartPage();
            var canada = FakePage.Create<HomePage>("Canada").ChildOf(start).PublishedOn(DateTime.Now.AddDays(-2));
            var canadaAsHomePage = canada.Page as HomePage;
            var canadaSubPage = FakePage.Create<ThemePage>("ASubPage").ChildOf(canada).PublishedOn(DateTime.Now.AddDays(-1)).WithLanguageBranch("EN").Page;

            var canadaEnabledLanguages = new List<CultureInfo> { new CultureInfo("EN"), new CultureInfo("FR") };
            IEnumerable<CultureInfo> canadaSubPagePublishedLanguages = Create3CultureInfo();

            Mock<IUrlResolverWrapper> mockUrlResolverWrapper = new Mock<IUrlResolverWrapper>();
            mockUrlResolverWrapper
                .Setup(x => x.GetVirtualPath(It.IsAny<ContentReference>(), It.IsAny<string>()))
                .Returns("/randompagepath");

            mockUrlResolverWrapper
                .Setup(x => x.GetCurrentSiteUrl())
                .Returns("http://localhost:10000");

            //Act
            LanguageChoicesRepository langRepository = new LanguageChoicesRepository(mockUrlResolverWrapper.Object);

            var result = langRepository
                        .CreateLanguageOptions(canadaSubPage, canadaAsHomePage, canadaSubPagePublishedLanguages,
                        canadaEnabledLanguages);

            //Assert
            Assert.AreEqual(2, result.Options.Count);
        }

        [Test]
        public void CreateLanguageOptions_2EnabledLanguages3PageLanguages2OfTheSamePageLanguages_Returns2OptionsWith2PageLinksForThese2Languages()
        {
            //Arrange
            _root = FakePage.Create("root");

            var start = FakePage
                .Create("CountryOverview")
                .ChildOf(_root)
                .AsStartPage();
            var canada = FakePage.Create<HomePage>("Canada").ChildOf(start).PublishedOn(DateTime.Now.AddDays(-2));
            var canadaAsHomePage = canada.Page as HomePage;
            var canadaSubPage = FakePage.Create<ThemePage>("ASubPage").ChildOf(canada).PublishedOn(DateTime.Now.AddDays(-1)).WithLanguageBranch("EN").Page;

            var canadaEnabledLanguages = new List<CultureInfo> { new CultureInfo("EN"), new CultureInfo("FR") };
            IEnumerable<CultureInfo> canadaSubPagePublishedLanguages = Create3CultureInfo();

            Mock<IUrlResolverWrapper> mockUrlResolverWrapper = new Mock<IUrlResolverWrapper>();
            mockUrlResolverWrapper.Setup(x => x.GetVirtualPath(It.IsAny<ContentReference>(), It.IsAny<string>()))
                .Returns("/randompagepath");
            mockUrlResolverWrapper.Setup(x => x.GetCurrentSiteUrl()).Returns("http://localhost:10000");

            //Act
            LanguageChoicesRepository langRepository = new LanguageChoicesRepository(mockUrlResolverWrapper.Object);

            var result = langRepository.CreateLanguageOptions(
                                                    canadaSubPage, 
                                                    canadaAsHomePage, 
                                                    canadaSubPagePublishedLanguages,
                                                    canadaEnabledLanguages);

            //Assert
            Assert.AreEqual(true, result.MoreThanOneOption);

            Assert.AreEqual(2, result.Options.Count);

            Assert.AreEqual(true, result.Options[0].CurrentPageIsPublishedInThisLanguageCode);
            Assert.AreEqual("en", result.Options[0].LanguageCode);

            Assert.AreEqual(true, result.Options[1].CurrentPageIsPublishedInThisLanguageCode);
            Assert.AreEqual("fr", result.Options[1].LanguageCode);

        }

        [Test]
        public void CreateLanguageOptions_4EnabledLanguages3PageLanguages4OfTheSamePageLanguages_Returns3OptionsWith3PageLinksForThese3LanguagesAnd1OptionWithLinkToHomePage()
        {
            //Arrange
            _root = FakePage.Create("root");

            var start = FakePage
                .Create("CountryOverview")
                .ChildOf(_root)
                .AsStartPage();
            var canada = FakePage.Create<HomePage>("Canada").ChildOf(start).PublishedOn(DateTime.Now.AddDays(-2));
            var canadaAsHomePage = canada.Page as HomePage;
            var canadaSubPage = FakePage.Create<ThemePage>("ASubPage").ChildOf(canada).PublishedOn(DateTime.Now.AddDays(-1)).WithLanguageBranch("EN").Page;

            var canadaEnabledLanguages = new List<CultureInfo> {
                new CultureInfo("EN"),
                new CultureInfo("SV"),
                new CultureInfo("FR"),
                new CultureInfo("DE")
            };
            IEnumerable<CultureInfo> canadaSubPagePublishedLanguages = Create3CultureInfo();

            Mock<IUrlResolverWrapper> mockUrlResolverWrapper = new Mock<IUrlResolverWrapper>();
            mockUrlResolverWrapper.Setup(x => x.GetVirtualPath(It.IsAny<ContentReference>(), It.IsAny<string>()))
                .Returns("/randompagepath");
            mockUrlResolverWrapper.Setup(x => x.GetCurrentSiteUrl()).Returns("http://localhost:10000");

            //Act
            LanguageChoicesRepository langRepository = new LanguageChoicesRepository(mockUrlResolverWrapper.Object);

            var result = langRepository.CreateLanguageOptions(canadaSubPage, canadaAsHomePage, canadaSubPagePublishedLanguages,
                canadaEnabledLanguages);

            //Assert
            Assert.AreEqual(true, result.MoreThanOneOption);
            Assert.AreEqual(4, result.Options.Count);

            Assert.AreEqual(true, result.Options[0].CurrentPageIsPublishedInThisLanguageCode);
            Assert.AreEqual("en", result.Options[0].LanguageCode);

            Assert.AreEqual(true, result.Options[1].CurrentPageIsPublishedInThisLanguageCode);
            Assert.AreEqual("sv", result.Options[1].LanguageCode);

            Assert.AreEqual(true, result.Options[2].CurrentPageIsPublishedInThisLanguageCode);
            Assert.AreEqual("fr", result.Options[2].LanguageCode);

            Assert.AreEqual(false, result.Options[3].CurrentPageIsPublishedInThisLanguageCode);
            Assert.AreEqual("de", result.Options[3].LanguageCode);

        }

        [Test]
        public void CreateLanguageOptions_1EnabledLanguage_ReturnsMoreThenOneOptionFalse()
        {
            //Arrange
            _root = FakePage.Create("root");

            var start = FakePage
                .Create("CountryOverview")
                .ChildOf(_root)
                .AsStartPage();
            var canada = FakePage.Create<HomePage>("Canada").ChildOf(start).PublishedOn(DateTime.Now.AddDays(-2));
            var canadaAsHomePage = canada.Page as HomePage;
            var canadaSubPage = FakePage.Create<ThemePage>("ASubPage").ChildOf(canada).PublishedOn(DateTime.Now.AddDays(-1)).WithLanguageBranch("EN").Page;

            var canadaEnabledLanguages = new List<CultureInfo> {
                new CultureInfo("EN")
            };
            IEnumerable<CultureInfo> canadaSubPagePublishedLanguages = Create3CultureInfo();

            Mock<IUrlResolverWrapper> mockUrlResolverWrapper = new Mock<IUrlResolverWrapper>();
            mockUrlResolverWrapper
                .Setup(x => x.GetVirtualPath(It.IsAny<ContentReference>(), It.IsAny<string>()))
                .Returns("/randompagepath");
            mockUrlResolverWrapper
                .Setup(x => x.GetCurrentSiteUrl())
                .Returns("http://localhost:10000");

            //Act
            LanguageChoicesRepository langRepository = new LanguageChoicesRepository(mockUrlResolverWrapper.Object);

            var result = langRepository
                            .CreateLanguageOptions(
                                            canadaSubPage, 
                                            canadaAsHomePage, 
                                            canadaSubPagePublishedLanguages,
                                            canadaEnabledLanguages);


            //Assert
            Assert.That(result.MoreThanOneOption, Is.False);
        }

    }
}
