﻿using EPiServer.Core;
using NUnit.Framework;

namespace Norgesportalen.Business.Tests
{
    /// <summary>
    /// Experimenting with unit test set up for EPiServer business logic. -Jon
    /// </summary>
    [TestFixture()] 
    public class ContentExtensionsTests
    {

        [Test]
        public void VisibleInMenuNullInputTest()
        {
            bool visibleInMenu = ContentExtensions.VisibleInMenu(null);

            Assert.IsTrue(visibleInMenu);

        }


        [Test]
        public void VisibleInMenuTest()
        {
            // Arrange
            IContent page = new PageData();

            // Act
            bool visibleInMenu = ContentExtensions.VisibleInMenu(page);

            // Assert
            Assert.IsFalse(visibleInMenu);
        }

        [Test]
        public void VisibleInMenuTestPageIsDeleted()
        {
            // Arrange
            IContent page = new PageData();

            // Act
            bool visibleInMenu = ContentExtensions.VisibleInMenu(page);

            // Assert 
            Assert.IsFalse(visibleInMenu); // deleted?
        }


    }
}
