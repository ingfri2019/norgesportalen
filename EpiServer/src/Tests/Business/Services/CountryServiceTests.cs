﻿using EPiFakeMaker;
using EPiServer.Core;
using EPiServer.ServiceLocation;
using EPiServer.Web.Routing;
using Moq;
using Norgesportalen.Business.Services;
using Norgesportalen.Models.Pages;
using Norgesportalen.Models.ViewModels;
using NUnit.Framework;
using Should;
using System;
using System.Collections.Generic;
using System.Linq;


namespace Norgesportalen.Tests.Business.Services
{
    [TestFixture]
    public class CountryServiceTests
    {
        // https://github.com/DavidVujic/EPiServer-FakeMaker
        private FakeMaker _fake;
        private FakePage _root;

        [SetUp]
        public void Setup()
        {
            _fake = new FakeMaker();

            var mockUrlResolver = new Mock<UrlResolver>();

            mockUrlResolver
                .Setup(res => res.GetUrl(It.IsAny<ContentReference>(), It.IsAny<string>()))
                .Returns("Some url");

            var mockLocator = new Mock<IServiceLocator>();
            ServiceLocator.SetLocator(mockLocator.Object);

            mockLocator
                .Setup(l => l.GetInstance<UrlResolver>())
                .Returns(mockUrlResolver.Object);


        }


        public List<HomePage> GetEmbassies()
        {
            _root = FakePage.Create("root");

            var start = FakePage
                .Create("NorwayInfo")
                .ChildOf(_root)
                .AsStartPage();

            var sriLanka = FakePage.Create<HomePage>("Sri Lanka").ChildOf(start).PublishedOn(DateTime.Now.AddDays(-2));
            var denmark = FakePage.Create<HomePage>("Denmark").ChildOf(start).PublishedOn(DateTime.Now.AddDays(-3));
            var usa = FakePage.Create<HomePage>("USA").ChildOf(start).PublishedOn(DateTime.Now.AddDays(-4));
            var ireland = FakePage.Create<HomePage>("Ireland").ChildOf(start).PublishedOn(DateTime.Now.AddDays(-5));
            var algeria = FakePage.Create<HomePage>("Algeria").ChildOf(start).PublishedOn(DateTime.Now.AddDays(-1));
            var special = FakePage.Create<HomePage>("ÆØÅ").ChildOf(start).PublishedOn(DateTime.Now.AddDays(-1));
            var numbers = FakePage.Create<HomePage>("123").ChildOf(start).PublishedOn(DateTime.Now.AddDays(-1));
            // excluded
            var italy = FakePage.Create<HomePage>("Italy").ChildOf(start).PublishedOn(DateTime.Now.AddDays(-6)).HiddenFromMenu();
            var canada = FakePage.Create<HomePage>("Canada").ChildOf(start).StopPublishOn(DateTime.Now.AddDays(-1));
            var germany = FakePage.Create<HomePage>("Germany").ChildOf(_root).PublishedOn(DateTime.Now.AddDays(-1));
            var japan = FakePage.Create<HomePage>("Japan").ChildOf(_root).PublishedOn(DateTime.Now.AddDays(-1));


            List<HomePage> pages = new List<HomePage>() {sriLanka.Page as HomePage};
            _fake.AddToRepository(_root);
            return pages;
        }



        [Test]
        public void CreateEmbassySiteCategoryList_WithNullParam_ReturnsEmbassySiteCategoryListWithCountZero()
        {
            //Arrange
            CountryService embService = new CountryService(_fake.ContentRepository);

            //Act
            var actual = embService.CreateEmbassySiteCategoryList(null);

            //Assert
            actual.ShouldNotBeNull();
            Assert.AreEqual(0, actual.Count());
        }

        [Test]
        public void CreateEmbassySiteCategoryList_WithNoPages_ReturnsEmbassySiteCategoryListWithCountZero()
        {
            //Arrange
            List<HomePage> pages = new List<HomePage>();
            CountryService embService = new CountryService(_fake.ContentRepository);

            //Act
            var actual = embService.CreateEmbassySiteCategoryList(pages);

            //Assert
            actual.ShouldNotBeNull();
            Assert.AreEqual(0, actual.Count());
        }

        [Test]
        public void CreateEmbassySiteCategoryList_With1Page_ReturnsEmbassySiteCategoryListWithCount1()
        {
            //Arrange
            var ireland = FakePage.Create<HomePage>("Ireland").PublishedOn(DateTime.Now.AddDays(-5));
            List<HomePage> pages = new List<HomePage>() {ireland.Page as HomePage};
            CountryService embService = new CountryService(_fake.ContentRepository);

            //Act
            var actual = embService.CreateEmbassySiteCategoryList(pages);

            //Assert
            actual.ShouldNotBeNull();
            Assert.AreEqual(1, actual.Count());
        }

        [Test]
        public void CreateEmbassySiteCategoryList_With2Pages_ReturnsEmbassySiteCategoryListWithCount2()
        {
            //Arrange
            var ireland = FakePage.Create<HomePage>("Ireland").PublishedOn(DateTime.Now.AddDays(-5));
            var canada = FakePage.Create<HomePage>("Canada").PublishedOn(DateTime.Now.AddDays(-4));

            List<HomePage> pages = new List<HomePage>() { ireland.Page as HomePage, canada.Page as HomePage };
            CountryService embService = new CountryService(_fake.ContentRepository);

            //Act
            var actual = embService.CreateEmbassySiteCategoryList(pages);

            //Assert
            Assert.AreEqual(2, actual.Count());
        }

        [Test]
        public void AlphaSort_NullInput_ReturnsNonEmptyList ()
        {
            //Arrange
            CountryService embService = new CountryService(_fake.ContentRepository);

            //Act
            var actual = embService.AlphaSort(null);

            //Assert
            actual.ShouldNotBeNull();
            Assert.AreEqual(0, actual.Count());

        }

        [Test]
        public void AlphaSort_EmptyInput_ReturnsNonEmptyList()
        {
            //Arrange
            CountryService embService = new CountryService(_fake.ContentRepository);

            //Act
            var actual = embService.AlphaSort(new List<CountriesOverviewViewModel.CountrySiteCategory.CountrySite>());

            //Assert
            actual.ShouldNotBeNull();
            Assert.AreEqual(0, actual.Count());

        }

        [Test]
        public void AlphaSort_Ireland_ReturnsListWithCount1()
        {
            //Arrange
            var ireland = FakePage.Create<HomePage>("Ireland").PublishedOn(DateTime.Now.AddDays(-5));
            List<HomePage> pages = new List<HomePage>() { ireland.Page as HomePage };
            CountryService embService = new CountryService(_fake.ContentRepository);
            IEnumerable<CountriesOverviewViewModel.CountrySiteCategory.CountrySite> listOfEmbassies = embService.CreateEmbassySiteCategoryList(pages);

            //Act
            List<CountriesOverviewViewModel.CountrySiteCategory> actual = embService.AlphaSort(listOfEmbassies);

            //Assert
            actual.ShouldNotBeNull();
            Assert.AreEqual(1, actual.Count());

        }

        [Test]
        public void AlphaSort_Ireland_ReturnsListWithIreland()
        {
            //Arrange
            string embName = "Ireland";
            var ireland = FakePage.Create<HomePage>(embName).PublishedOn(DateTime.Now.AddDays(-5));
            List<HomePage> pages = new List<HomePage>() { ireland.Page as HomePage };
            CountryService embService = new CountryService(_fake.ContentRepository);
            IEnumerable<CountriesOverviewViewModel.CountrySiteCategory.CountrySite> listOfEmbassies = embService.CreateEmbassySiteCategoryList(pages);

            //Act
            List<CountriesOverviewViewModel.CountrySiteCategory> actual = embService.AlphaSort(listOfEmbassies);

            //Assert
            actual.ShouldNotBeNull();
            Assert.AreEqual(embName, actual[0].Embassies[0].Name);

        }

        [Test]
        public void AlphaSort_Ireland_ReturnsListWithOneLetterWhichIsI()
        {
            //Arrange
            string embName = "Ireland";
            var ireland = FakePage.Create<HomePage>(embName).PublishedOn(DateTime.Now.AddDays(-5));
            List<HomePage> pages = new List<HomePage>() { ireland.Page as HomePage };
            CountryService embService = new CountryService(_fake.ContentRepository);
            IEnumerable<CountriesOverviewViewModel.CountrySiteCategory.CountrySite> listOfEmbassies = embService.CreateEmbassySiteCategoryList(pages);

            //Act
            List<CountriesOverviewViewModel.CountrySiteCategory> actual = embService.AlphaSort(listOfEmbassies);

            //Assert
            actual.ShouldNotBeNull();
            Assert.AreEqual(1, actual.Count);
            Assert.AreEqual(1, actual[0].Embassies.Count);
            Assert.AreEqual('I',actual[0].Letter);

        }

        [Test]
        public void AlphaSort_IrelandAndCanada_ReturnsListWithCount2()
        {
            //Arrange
            var ireland = FakePage.Create<HomePage>("Ireland").PublishedOn(DateTime.Now.AddDays(-5));
            var canada = FakePage.Create<HomePage>("Canada").PublishedOn(DateTime.Now.AddDays(-4));

            List<HomePage> pages = new List<HomePage>() { ireland.Page as HomePage, canada.Page as HomePage };
            CountryService embService = new CountryService(_fake.ContentRepository);
            IEnumerable<CountriesOverviewViewModel.CountrySiteCategory.CountrySite> listOfEmbassies = embService.CreateEmbassySiteCategoryList(pages);

            //Act
            List<CountriesOverviewViewModel.CountrySiteCategory> actual = embService.AlphaSort(listOfEmbassies);

            //Assert
            actual.ShouldNotBeNull();
            Assert.AreEqual(2, actual.Count);
        }


        [Test]
        public void AlphaSort_IrelandAndCanada_ReturnsCanadaBeforeIreland()
        {
            //Arrange
            var ireland = FakePage.Create<HomePage>("Ireland").PublishedOn(DateTime.Now.AddDays(-5));
            var canada = FakePage.Create<HomePage>("Canada").PublishedOn(DateTime.Now.AddDays(-4));

            List<HomePage> pages = new List<HomePage>() { ireland.Page as HomePage, canada.Page as HomePage };
            CountryService embService = new CountryService(_fake.ContentRepository);
            IEnumerable<CountriesOverviewViewModel.CountrySiteCategory.CountrySite> listOfEmbassies = embService.CreateEmbassySiteCategoryList(pages);

            //Act
            List<CountriesOverviewViewModel.CountrySiteCategory> actual = embService.AlphaSort(listOfEmbassies);

            //Assert
            Assert.AreEqual('C', actual[0].Letter);
            Assert.AreEqual('I', actual[1].Letter);
        }


        [Test]
        public void AlphaSort_IrelandCanadaUSANorwaySwedenFrance_ReturnsCorrectAlphaSorting()
        {
            //Arrange
            var ireland = FakePage.Create<HomePage>("Ireland").PublishedOn(DateTime.Now.AddDays(-5));
            var canada = FakePage.Create<HomePage>("Canada").PublishedOn(DateTime.Now.AddDays(-4));
            var usa = FakePage.Create<HomePage>("United States").PublishedOn(DateTime.Now.AddDays(-4));
            var norway = FakePage.Create<HomePage>("Norway").PublishedOn(DateTime.Now.AddDays(-4));
            var nigeria = FakePage.Create<HomePage>("Nigeria").PublishedOn(DateTime.Now.AddDays(-4));
            var sweden = FakePage.Create<HomePage>("Sweden").PublishedOn(DateTime.Now.AddDays(-4));
            var france = FakePage.Create<HomePage>("France").PublishedOn(DateTime.Now.AddDays(-4));
            var tanzania = FakePage.Create<HomePage>("Tanzania").PublishedOn(DateTime.Now.AddDays(-4));

            List<HomePage> pages = new List<HomePage>() { ireland.Page as HomePage, canada.Page as HomePage, usa.Page as HomePage, norway.Page as HomePage, sweden.Page as HomePage,france.Page as HomePage, nigeria.Page as HomePage, tanzania.Page as HomePage };
            CountryService embService = new CountryService(_fake.ContentRepository);
            IEnumerable<CountriesOverviewViewModel.CountrySiteCategory.CountrySite> listOfEmbassies = embService.CreateEmbassySiteCategoryList(pages);

            //Act
            List<CountriesOverviewViewModel.CountrySiteCategory> actual = embService.AlphaSort(listOfEmbassies);

            //Assert
            Assert.AreEqual('C', actual[0].Letter);
            Assert.AreEqual('F', actual[1].Letter);
            Assert.AreEqual('I', actual[2].Letter);
            Assert.AreEqual('N', actual[3].Letter);
            Assert.AreEqual('S', actual[4].Letter);
            Assert.AreEqual('T', actual[5].Letter);
            Assert.AreEqual('U', actual[6].Letter);

        }

        [Test]
        public void AlphaSort_3CountriesThatStartWithTheSameLetter_ReturnsListWIthOneLetterAndEmbassyCount3()
        {
            //Arrange
            var norway = FakePage.Create<HomePage>("Norway").PublishedOn(DateTime.Now.AddDays(-4));
            var nigeria = FakePage.Create<HomePage>("Nigeria").PublishedOn(DateTime.Now.AddDays(-4));
            var netherland = FakePage.Create<HomePage>("Netherlands").PublishedOn(DateTime.Now.AddDays(-4));

            List<HomePage> pages = new List<HomePage>() {  norway.Page as HomePage,nigeria.Page as HomePage,netherland.Page as HomePage };
            CountryService embService = new CountryService(_fake.ContentRepository);
            IEnumerable<CountriesOverviewViewModel.CountrySiteCategory.CountrySite> listOfEmbassies = embService.CreateEmbassySiteCategoryList(pages);

            //Act
            List<CountriesOverviewViewModel.CountrySiteCategory> actual = embService.AlphaSort(listOfEmbassies);

            //Assert
            Assert.AreEqual('N', actual[0].Letter);
            Assert.AreEqual(3, actual[0].Embassies.Count);
        }

        [Test]
        public void AlphaSort_3CountriesThatStartWithTheSameLetter_ReturnsListWithAlphaSortesEmbassies()
        {
            //Arrange
            string embNorway = "Norway";
            string embNigeria = "Nigeria";
            string embNetherlands = "Netherlands";
            var norway = FakePage.Create<HomePage>(embNorway).PublishedOn(DateTime.Now.AddDays(-4));
            var nigeria = FakePage.Create<HomePage>(embNigeria).PublishedOn(DateTime.Now.AddDays(-4));
            var netherland = FakePage.Create<HomePage>(embNetherlands).PublishedOn(DateTime.Now.AddDays(-4));

            List<HomePage> pages = new List<HomePage>() { norway.Page as HomePage, nigeria.Page as HomePage, netherland.Page as HomePage };
            CountryService embService = new CountryService(_fake.ContentRepository);
            IEnumerable<CountriesOverviewViewModel.CountrySiteCategory.CountrySite> listOfEmbassies = embService.CreateEmbassySiteCategoryList(pages);

            //Act
            List<CountriesOverviewViewModel.CountrySiteCategory> actual = embService.AlphaSort(listOfEmbassies);

            //Assert
            Assert.That(actual,Is.Not.Null);
            Assert.AreEqual('N', actual[0].Letter);
            Assert.AreEqual(embNetherlands, actual[0].Embassies[0].Name);
            Assert.AreEqual(embNigeria, actual[0].Embassies[1].Name);
            Assert.AreEqual(embNorway, actual[0].Embassies[2].Name);
        }
    }
}
