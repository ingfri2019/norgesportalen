﻿using EPiFakeMaker;
using EPiServer.Core;
using Norgesportalen.Business.Services;
using Norgesportalen.Models.Pages;
using Norgesportalen.Models.ViewModels;
using NUnit.Framework;
using Should;
using System;
using System.Collections.Generic;

namespace Norgesportalen.Tests.Business.Services
{
    [TestFixture]
    public class DelegationsServiceTests
    {
        // https://github.com/DavidVujic/EPiServer-FakeMaker
        private FakeMaker _fake;

        [SetUp]
        public void Setup()
        {
            _fake = new FakeMaker();
        }

        [Test]
        public void CreateSimpleDelegationInfo_WithNullParam_ShouldNotReturnNull()
        {
            //Arrange
            DelegationsService embService = new DelegationsService(_fake.ContentRepository);

            //Act
            var actual = embService.CreateSimpleDelegationInfo(null);

            //Assert
            actual.ShouldNotBeNull();
        }

        [Test]
        public void CreateSimpleDelegationInfo_WithNullParam_ShouldReturnEmptyList()
        {
            //Arrange
            DelegationsService embService = new DelegationsService(_fake.ContentRepository);

            //Act
            var actual = embService.CreateSimpleDelegationInfo(null);

            //Assert
            Assert.AreEqual(new List<CountriesOverviewViewModel.SimplePageInfo>(), actual);
        }

        [Test]
        public void CreateSimpleDelegationInfo_WithEmptyList_ShouldNotReturnNull()
        {
            //Arrange
            DelegationsService embService = new DelegationsService(_fake.ContentRepository);

            //Act
            var actual = embService.CreateSimpleDelegationInfo(new List<PageData>());

            //Assert
            actual.ShouldNotBeNull();
        }

        [Test]
        public void CreateSimpleDelegationInfo_WithEmptyList_ShouldReturnEmptyList()
        {
            //Arrange
            DelegationsService embService = new DelegationsService(_fake.ContentRepository);

            //Act
            var actual = embService.CreateSimpleDelegationInfo(new List<PageData>());

            //Assert
            Assert.AreEqual(new List<CountriesOverviewViewModel.SimplePageInfo>(), actual);
        }

        [Test]
        public void CreateSimpleDelegationInfo_With1DelegationPageAsInput_ShouldReturnListWithCount1()
        {
            //Arrange
            DelegationsService embService = new DelegationsService(_fake.ContentRepository);
            var un = FakePage.Create<HomePage>("UN").PublishedOn(DateTime.Now.AddDays(-5)).Page;

            var delegations = new List<PageData>() {un as HomePage };

            //Act
            var actual = embService.CreateSimpleDelegationInfo(delegations);

            //Assert
            Assert.AreEqual(1, actual.Count);
        }

        [Test]
        public void CreateSimpleDelegationInfo_WithUNDelegation_ShouldReturnListWithUNDelegation()
        {
            //Arrange
            DelegationsService embService = new DelegationsService(_fake.ContentRepository);
            string unName = "UN";
            var un = FakePage.Create<HomePage>(unName).PublishedOn(DateTime.Now.AddDays(-5)).Page;

            var delegations = new List<PageData>() { un as HomePage };

            //Act
            var actual = embService.CreateSimpleDelegationInfo(delegations);

            //Assert
            Assert.AreEqual(unName, actual[0].Name);
        }

        [Test]
        public void CreateSimpleDelegationInfo_WithUNDelegation_ShouldReturnListWithUNDelegationShortText()
        {
            //Arrange
            DelegationsService embService = new DelegationsService(_fake.ContentRepository);
            string unName = "UN";
            string unShortText = "This is the UN.";
            var un = FakePage.Create<HomePage>(unName).PublishedOn(DateTime.Now.AddDays(-5)).Page as HomePage;
            un.ShortText = unShortText;
            

            var delegations = new List<PageData>() { un };

            //Act
            var actual = embService.CreateSimpleDelegationInfo(delegations);

            //Assert
            Assert.AreEqual(unShortText, actual[0].ShortText);
        }

        [Test]
        public void CreateSimpleDelegationInfo_WithU2Delegations_ShouldReturnListWitchCount2()
        {
            //Arrange
            DelegationsService embService = new DelegationsService(_fake.ContentRepository);

            string unName = "UN";
            string unShortText = "This is the UN.";
            var un = FakePage.Create<HomePage>(unName).PublishedOn(DateTime.Now.AddDays(-5)).Page as HomePage;
            un.ShortText = unShortText;

            string natoName = "NATO";
            string natoShortText = "This is NATO.";
            var nato = FakePage.Create<HomePage>(natoName).PublishedOn(DateTime.Now.AddDays(-5)).Page as HomePage;
            nato.ShortText = natoShortText;


            var delegations = new List<PageData>() { un, nato };

            //Act
            var actual = embService.CreateSimpleDelegationInfo(delegations);

            //Assert
            Assert.AreEqual(2, actual.Count);
        }

    }
}
