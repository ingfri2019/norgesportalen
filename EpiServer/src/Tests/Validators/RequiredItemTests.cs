﻿using EPiServer.Core;
using NUnit.Framework;
using System;

namespace Norgesportalen.Tests.Validators
{
    [TestFixture]
    public class RequiredItemTests
    {

        [Test]
        public void ValidateContentReference()
        {
            var validator = new Norgesportalen.Business.Validation.RequiredItem();

            object value = new ContentReference();

            bool result = validator.IsValid(value);

            Assert.That(result, Is.True);
        }

        [Test]
        public void ValidateDateTime()
        {
            var validator = new Norgesportalen.Business.Validation.RequiredItem();

            object value = new DateTime(2016,9,15);

            bool result = validator.IsValid(value);
            
            Assert.That(result, Is.True);
            Assert.That(validator.ErrorMessage, Is.Null);

        }

        [Test]
        public void ValidateInt()
        {
            var validator = new Norgesportalen.Business.Validation.RequiredItem();

            object value = 5;

            bool result = validator.IsValid(value);

            Assert.That(result, Is.False);
            Assert.That(validator.ErrorMessage, Does.StartWith("Validator not implemented for"));

        }

        [Test]
        public void ValidateNull()
        {
            var validator = new Norgesportalen.Business.Validation.RequiredItem();

            object value = null;

            bool result = validator.IsValid(value);

            Assert.That(result, Is.False);
        }
    }
}
