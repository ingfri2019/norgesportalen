﻿namespace SSO.Security
{
    public static class Constants
    {
        public const string FormsAuthenticationType = "Forms";
        public const string ReturnUrl = "ReturnUrl";
        public const string ClaimBasedLoginUrl = "/Custom/Authentication/ClaimsBasedLogin.aspx";
    }
}