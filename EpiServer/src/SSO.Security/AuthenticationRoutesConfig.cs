﻿using System.Web.Mvc;
using System.Web.Routing;

namespace SSO.Security
{
    public static class AuthenticationRoutesConfig
    {
        public static void Register(RouteCollection routes)
        {
            routes.MapRoute(
                "Login",
                "Security/Login/{action}",
                new {controller = "Login", action = "Index"});

            routes.MapRoute(
                "Logout",
                "Security/Logout/{action}",
                new {controller = "Logout", action = "Index"});
        }
    }
}