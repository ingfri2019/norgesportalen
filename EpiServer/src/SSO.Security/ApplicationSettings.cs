﻿using System;
using System.Web.UI.WebControls;
using EPiServer.Events;
using EPiServer.Events.Clients;
using EPiServer.PlugIn;
using EPiServer.Security;

namespace SSO.Security
{
    [GuiPlugIn(Area = PlugInArea.None, DisplayName = "Application Settings", RequiredAccess = AccessLevel.Administer, RequireLicenseForLoad = false)]
    public class ApplicationSettings
    {
        private static ApplicationSettings _instance;
        internal static Guid BroadcastSettingsChangedEventId = new Guid("75714741-2ec2-4317-8110-1b1b63818602");

        [PlugInProperty(Description = "Filebane til XML fil for import", AdminControl = typeof (TextBox), AdminControlValue = "Text")]
        public string XmlImportUrl { get; set; }

        [PlugInProperty(Description = "Redaktørgruppenavn", AdminControl = typeof (TextBox), AdminControlValue = "Text")]
        public string WebEditorRoleName { get; set; }

        [PlugInProperty(Description = "Stasjonsgruppenavn", AdminControl = typeof(TextBox), AdminControlValue = "Text")]
        public string StationEditorRoleName { get; set; }

        [PlugInProperty(Description = "Url til innloggingside", AdminControl = typeof (TextBox), AdminControlValue = "Text")]
        public string FormsLoginUrl { get; set; }


        private ApplicationSettings()
        {
            PlugInSettings.SettingsChanged += PlugInSettings_SettingsChanged;
            Event broadcastSettingsChangedEvent = Event.Get(BroadcastSettingsChangedEventId);
            broadcastSettingsChangedEvent.Raised += BroadcastSettingsChangedEvent_Raised;
        }

        private static void PlugInSettings_SettingsChanged(object sender, EventArgs e)
        {
            //Broadcast event to all servers
            Event settingsChangedEvent = Event.Get(BroadcastSettingsChangedEventId);
            settingsChangedEvent.Raise(BroadcastSettingsChangedEventId, null);
        }

        private void BroadcastSettingsChangedEvent_Raised(object sender, EventNotificationEventArgs e)
        {
            _instance = null;
        }

        public static ApplicationSettings Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ApplicationSettings();
                    PlugInSettings.AutoPopulate(_instance);
                }
                return _instance;
            }
        }
    }
}