﻿namespace SSO.Security.Entities
{
    public class User
    {
        public string Username { get; set; }
        public string SiteID { get; set; }
    }
}