﻿using Autofac;
using SSO.Security.Contracts;
using SSO.Security.Services;
using IContainer = Autofac.IContainer;

namespace SSO.Security.IOC
{
    public static class DependencyResolver
    {
        public static IContainer Container { get; set; }

        public static void Initialize()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<UserAndRoleService>().As<IUserAndRoleService>();
            builder.RegisterType<XmlImportService>().As<IXmlImportService>();
            builder.RegisterType<IpAddressResolver>().As<IIpAddressResolver>();
            builder.RegisterType<ClaimsHelper>().As<IClaimsHelper>();
            Container = builder.Build();
        }
    }
}