﻿using System.Web.Mvc;
using EPiServer.Framework;
using EPiServer.Framework.Initialization;

namespace SSO.Security.IOC
{
    [InitializableModule]
    public class IOCInitializer : IInitializableModule
    {
        public void Initialize(InitializationEngine context)
        {
            DependencyResolver.Initialize();
        }

        public void Uninitialize(InitializationEngine context) { }
        public void Preload(string[] parameters) { }
    }
}
