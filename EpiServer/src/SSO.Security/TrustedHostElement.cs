﻿using System.Configuration;

namespace SSO.Security
{
    public class TrustedHostElement :  ConfigurationElement
    {
        [ConfigurationProperty("name", IsRequired = true, IsKey = true)]
        public string Name { get { return (string)this["name"]; } }

        [ConfigurationProperty("lower", IsRequired = true, IsKey = true)]
        public string Lower { get { return (string)this["lower"]; } }

        [ConfigurationProperty("upper", IsRequired = true, IsKey = true)]
        public string Upper { get { return (string)this["upper"]; } }
    }
}