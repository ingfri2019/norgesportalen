﻿using System.Collections.Generic;
using SSO.Security.Entities;

namespace SSO.Security.Contracts
{
    public interface IUserAndRoleService
    {
        void CreateOrUpdate(IEnumerable<User> users);
        void RemoveOldStationUsers(IEnumerable<User> users);
    }
}