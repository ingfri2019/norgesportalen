﻿using System.Net;
using System.Web;

namespace SSO.Security.Contracts
{
    public interface IIpAddressResolver
    {
        IPAddress GetClientIpAddressFor(HttpRequest request);
        IPAddress GetClientIpAddressFor(HttpRequestBase request);
    }
}