﻿using System.Security.Claims;
using System.Security.Principal;

namespace SSO.Security.Contracts
{
    public interface IClaimsHelper
    {
        ClaimsPrincipal CreateClaimsPrincipal(IIdentity user, string username, string authenticationType);
    }
}