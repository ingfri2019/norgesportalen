﻿using System.Collections.Generic;
using SSO.Security.Entities;

namespace SSO.Security.Contracts
{
    public interface IXmlImportService
    {
        List<User> GetUsersToImport();
    }
}