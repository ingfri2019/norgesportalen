﻿using System;
using System.Linq;
using System.Web.Mvc;
using EPiServer.Logging.Compatibility;
using EPiServer.ServiceLocation;
using SSO.Security.Contracts;

namespace SSO.Security.Plugins
{
    [EPiServer.PlugIn.ScheduledPlugIn(DisplayName = "XML-import av AD brukere og stasjoner")]
    public class ImportStationUsers
    {
        private static IXmlImportService _xmlImportService;
        private static IUserAndRoleService _userAndRoleService;
        private static readonly ILog Log = LogManager.GetLogger("ImportStationUsers");

        public static string Execute()
        {
            string message = string.Empty;
            try
            {
                message = ImportUsersAndStations();
                Log.Debug(message);
            }
            catch (Exception ex)
            {
                message += string.Format("Unable to import users due to unexpected error. Exception: {0}. Stack trace: {1}", ex.Message, ex.StackTrace);
                Log.Error(message);
            }
            return message;
        }

        private static string ImportUsersAndStations()
        {
            //_xmlImportService = DependencyResolver.Container.Resolve<IXmlImportService>();
            //_userAndRoleService = DependencyResolver.Container.Resolve<IUserAndRoleService>();

            _xmlImportService = ServiceLocator.Current.GetInstance<IXmlImportService>();
            _userAndRoleService = ServiceLocator.Current.GetInstance<IUserAndRoleService>();

            //retrieves users from xml
            var users = _xmlImportService.GetUsersToImport();

            //loop over all current station users and remove those not included in the import.
            _userAndRoleService.RemoveOldStationUsers(users);

            //create and updates users and roles from imported xml
            _userAndRoleService.CreateOrUpdate(users);

            return string.Format("{0} users imported from the XML file.", users.Count());
        }
    }
}