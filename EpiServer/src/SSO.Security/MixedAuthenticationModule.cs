﻿using System;
using System.IdentityModel.Services;
using System.IdentityModel.Tokens;
using System.Security.Principal;
using System.Web;
using System.Web.Security;
using Autofac;
using EPiServer.Logging.Compatibility;
using EPiServer.ServiceLocation;
using SSO.Security.Contracts;
using SSO.Security.IOC;


namespace SSO.Security
{
    public class MixedAuthenticationModule : SessionAuthenticationModule
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof (MixedAuthenticationModule));
        private IClaimsHelper _claimsHelper;


        protected override void OnAuthenticateRequest(object sender, EventArgs eventArgs)
        {
            _claimsHelper = DependencyResolver.Container.Resolve<IClaimsHelper>();

            //_claimsHelper = ServiceLocator.Current.GetInstance<IClaimsHelper>();
            //creating claims for SQL users
            if (HttpContext.Current.User != null && HttpContext.Current.User.Identity is FormsIdentity)
            {
                SessionAuthenticationModule sessionAuthenticationModule = FederatedAuthentication.SessionAuthenticationModule;
                IIdentity user = HttpContext.Current.User.Identity;

                Log.DebugFormat("Creating claims for user {0}", user.Name);
                var claimsPrincipalForms = _claimsHelper.CreateClaimsPrincipal(user, user.Name, Constants.FormsAuthenticationType);

                Log.DebugFormat("Writing custom session cookie for user {0}", user.Name);
                var token = new SessionSecurityToken(claimsPrincipalForms);
                sessionAuthenticationModule.WriteSessionTokenToCookie(token);

                if (HttpContext.Current.User.Identity.IsAuthenticated && !string.IsNullOrEmpty(HttpContext.Current.Request.QueryString[Constants.ReturnUrl]))
                {
                    string redirectUrl = HttpContext.Current.Request.QueryString[Constants.ReturnUrl];
                    HttpContext.Current.Response.Redirect(redirectUrl);
                }
            }

            base.OnAuthenticateRequest(sender, eventArgs);
        }
    }
}