﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Services;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using System.Web.Security;
using EPiServer.Logging.Compatibility;
using EPiServer.ServiceLocation;
using EPiServer.Web;
using SSO.Security.Contracts;

namespace SSO.Security.Controllers
{
    public class LoginController : Controller
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(LoginController));
        private readonly IIpAddressResolver _ipAddressResolver;

        public LoginController(IIpAddressResolver ipAddressResolver)
        {
            _ipAddressResolver = ipAddressResolver;
        }

        public LoginController()
        {
            _ipAddressResolver = ServiceLocator.Current.GetInstance<IIpAddressResolver>();
        }

        public ActionResult Index()
        {
            IPAddress ip = _ipAddressResolver.GetClientIpAddressFor(Request);
            Log.DebugFormat($"Incomming IP address: {ip}");

            if (ip != null)
            {
                List<TrustedHostElement> trustedHosts = Configuration.Current.TrustedHosts.Cast<TrustedHostElement>().ToList();
                bool isTrustedHost = trustedHosts.Any(th => new IPAddressRange(IPAddress.Parse(th.Lower), IPAddress.Parse(th.Upper)).IsInRange(ip));

                Log.DebugFormat($"IP address: {ip}. Is trusted host?: {isTrustedHost}");

                if (isTrustedHost)
                {
                    Log.DebugFormat($"Transferring to federated/adfs for trusted host: {ip}");
                    string redirectUrl = string.IsNullOrEmpty(Request.QueryString[Constants.ReturnUrl]) ? ApplicationSettings.Instance.FormsLoginUrl : Request.QueryString[Constants.ReturnUrl];

                    //INFO: redirects to federated/ADFS login
                    var signInRequestMessage = FederatedAuthentication.WSFederationAuthenticationModule.CreateSignInRequest(SiteDefinition.Current.SiteUrl.ToString(), redirectUrl, true);
                    var signInUri = signInRequestMessage.WriteQueryString();
                    return new RedirectResult(signInUri);

                }
            }
            else
            {
                Log.DebugFormat("Incomming IP address is null");
            }

            if (string.IsNullOrEmpty(FormsAuthentication.LoginUrl))
                throw new Exception("Forms login URL is missing from authentication settings.");

            //INFO: redirects to episerver default login page
            //So if ip == null then just redirect to default login. And if not a trusted host, then redirect to default login page. However, this code is run no matter what. The RedirectToIdentityProvider code runs, but it does not break out of the code, it keeps going..
            string url = $"{SiteDefinition.Current.SiteUrl.ToString()}Util/Login.aspx?{Constants.ReturnUrl}={Request.QueryString[Constants.ReturnUrl]}";
            Log.DebugFormat($"Redirecting to forms authentication url: {url}");
            return new RedirectResult(url);
        }
    }
}