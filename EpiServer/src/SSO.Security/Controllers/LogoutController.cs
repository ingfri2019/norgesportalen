﻿using System.IdentityModel.Services;
using System.Web.Mvc;
using System.Web.Security;

namespace SSO.Security.Controllers
{
    public class LogoutController : Controller
    {
        public ActionResult Index()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();

            SessionAuthenticationModule sessionAuthenticationModule = FederatedAuthentication.SessionAuthenticationModule;
            sessionAuthenticationModule.SignOut();

            return new RedirectResult("/");
        }
    }
}