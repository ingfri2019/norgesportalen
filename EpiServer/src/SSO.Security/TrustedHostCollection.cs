﻿using System.Configuration;

namespace SSO.Security
{
    public class TrustedHostCollection: ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new TrustedHostElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((TrustedHostElement)element).Name;
        }
    }
}