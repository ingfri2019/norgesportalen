﻿using System;
using System.Web;
using System.Web.Caching;
using EPiServer.Logging.Compatibility;

namespace SSO.Security
{
    public class CacheManager
    {
        private readonly ILog _iLog = LogManager.GetLogger(typeof (CacheManager));
        private readonly HttpContext _context;

        public CacheManager(HttpContext httpContext)
        {
            _context = httpContext;
        }


        public T Get<T>(string key) where T : class
        {
            return _context.Cache[key] as T;
        }


        public void InsertIntoCache(string key, object cacheObject, TimeSpan timespan)
        {
            if (String.IsNullOrEmpty(key) || cacheObject == null)
                return;

            _iLog.Debug(new {Key = key, Item = cacheObject, Seconds = timespan.Duration().TotalSeconds});
            _context.Cache.Insert(key, cacheObject, null, DateTime.UtcNow.Add(timespan), Cache.NoSlidingExpiration);
        }


        public void Remove(string key)
        {
            if (String.IsNullOrEmpty(key))
                return;

            _iLog.Debug("Remove " + new {Key = key});
            _context.Cache.Remove(key);
        }
    }
}