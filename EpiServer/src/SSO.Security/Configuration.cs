﻿using System.Configuration;

namespace SSO.Security
{
    public class Configuration : ConfigurationSection
    {
        private static Configuration _current;

        public static Configuration Current
        {
            get { return _current ?? (_current = ConfigurationManager.GetSection("sso.security") as Configuration ?? new Configuration()); }
        }

        [ConfigurationProperty("trustedhosts", IsRequired = true)]
        [ConfigurationCollection(typeof(TrustedHostCollection))]
        public virtual TrustedHostCollection TrustedHosts { get { return (TrustedHostCollection)this["trustedhosts"]; } }
    }
}