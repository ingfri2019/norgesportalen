﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Web.Security;
using SSO.Security.Contracts;
using EPiServer.ServiceLocation;

namespace SSO.Security
{
    [ServiceConfiguration(typeof(IClaimsHelper))]
    public class ClaimsHelper : IClaimsHelper
    {
        public ClaimsPrincipal CreateClaimsPrincipal(IIdentity user, string username, string authenticationType)    
        {
            var claims = new List<Claim>();
            string[] rolesForUser = Roles.GetRolesForUser(username);

            claims.Add(new Claim(ClaimTypes.Name, username));
            claims.AddRange(rolesForUser.Select(role => new Claim(ClaimTypes.Role, role)));

            var claimsIdentity = new ClaimsIdentity(claims, authenticationType);
            return  new ClaimsPrincipal(new[] { claimsIdentity });
        }
    }
}
