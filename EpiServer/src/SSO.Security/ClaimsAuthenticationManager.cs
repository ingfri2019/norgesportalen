﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Security;
using EPiServer.Logging.Compatibility;
using EPiServer.ServiceLocation;
using SSO.Security.Contracts;

namespace SSO.Security
{
    public class ClaimsAuthenticationManager : System.Security.Claims.ClaimsAuthenticationManager
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(ClaimsAuthenticationManager));
        private IClaimsHelper _claimsHelper;

        public override ClaimsPrincipal Authenticate(string resourceName, ClaimsPrincipal incomingPrincipal)
        {
            _claimsHelper = ServiceLocator.Current.GetInstance<IClaimsHelper>();

            ClaimsPrincipal claimsPrincipal = incomingPrincipal;
            if (incomingPrincipal?.Identity != null && incomingPrincipal.Identity.IsAuthenticated)
            {
                ClaimsIdentity claimsId = incomingPrincipal.Identity as ClaimsIdentity;
                HttpCookie formsAuthCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];

                if (claimsId != null && formsAuthCookie == null)
                {
                    Claim claimIwant = claimsId.Claims.FirstOrDefault(a => a.Type == ClaimTypes.WindowsAccountName);
                    if (claimIwant == null)
                    {
                        Log.DebugFormat("Unable to retrieve ClaimType WindowsAccountName");
                        return claimsPrincipal;
                    }

                    string username = claimIwant.Value;

                    //checking if the user actually exists as an SQL user, and is in the correct role
                    MembershipUser user = Membership.GetUser(username);
                    if (user == null)
                    {
                        Log.Debug($"No user with username {username} was found in the SQL database");
                        throw new HttpException(401, "Unauthorized");
                    }

                    if (Roles.IsUserInRole(user.UserName, ApplicationSettings.Instance.WebEditorRoleName))
                    {
                        Log.Debug($"User {username} was authenticated with ADFS and SQL user was found. Logging user in as SQL user with forms authentication.");

                        Log.DebugFormat("Adding encrypted forms auth ticket for user {0}", user.UserName);
                        FormsAuthenticationTicket formsAuthenticationTicket = new FormsAuthenticationTicket(1, user.UserName, DateTime.Now, DateTime.Now.AddMinutes(120), false, FormsAuthentication.FormsCookiePath);
                        string encryptedTicket = FormsAuthentication.Encrypt(formsAuthenticationTicket);
                        HttpContext.Current.Response.Cookies.Add(new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket));

                        claimsPrincipal = _claimsHelper.CreateClaimsPrincipal(claimsId, user.UserName, claimsId.AuthenticationType);
                    }
                    else
                    {
                        Log.DebugFormat($"User {username} is not a member of required role {ApplicationSettings.Instance.WebEditorRoleName}. Users roles = {Roles.GetRolesForUser(user.UserName)}");
                        throw new HttpException(401, "Unauthorized");
                    }
                }
            }
            return claimsPrincipal;
        }
    }
}