﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using EPiServer.ServiceLocation;
using SSO.Security.Contracts;
using SSO.Security.Entities;

namespace SSO.Security.Services
{
    [ServiceConfiguration(typeof(IXmlImportService))]
    public class XmlImportService : IXmlImportService
    {
        public List<User> GetUsersToImport()
        {
            if (string.IsNullOrEmpty(ApplicationSettings.Instance.XmlImportUrl))
                throw new FileNotFoundException("No xml file path is defined in module settings");

            var usernodes = from u in XDocument.Load(ApplicationSettings.Instance.XmlImportUrl).Descendants("users").Elements("user") select u;
            var users = usernodes.Select(node => new User
            {
                Username = node.Element("Username").Value,
                SiteID = node.Element("SiteID").Value
            });

            return users.ToList();
        }
    }
}