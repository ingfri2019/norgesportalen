﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Security;
using EPiServer.Logging.Compatibility;
using EPiServer.ServiceLocation;
using SSO.Security.Contracts;
using SSO.Security.Entities;

namespace SSO.Security.Services
{
    [ServiceConfiguration(typeof(IUserAndRoleService))]
    public class UserAndRoleService : IUserAndRoleService
    {
        private static readonly ILog Log = LogManager.GetLogger("UserAndRoleService");

        public void CreateOrUpdate(IEnumerable<User> users)
        {
            if (string.IsNullOrEmpty(ApplicationSettings.Instance.WebEditorRoleName) || !Roles.RoleExists(ApplicationSettings.Instance.WebEditorRoleName))
                throw new Exception("Missing configuration of WebEditorRoleName in Application Settings. Configure with correct role and try again");

            if (string.IsNullOrEmpty(ApplicationSettings.Instance.StationEditorRoleName) || !Roles.RoleExists(ApplicationSettings.Instance.StationEditorRoleName))
                throw new Exception("Missing configuration of StationEditorRoleName in Application Settings. Configure with correct role and try again");

            foreach (User user in users)
            {
                if (Membership.GetUser(user.Username) == null)
                    Membership.CreateUser(user.Username, Membership.GeneratePassword(8, 2));
                
                if (!Roles.RoleExists(user.SiteID))
                    Roles.CreateRole(user.SiteID);

                if (!Roles.IsUserInRole(user.Username,user.SiteID))
                    Roles.AddUserToRole(user.Username, user.SiteID);

                if (!Roles.IsUserInRole(user.Username,"WebEditors"))
                    Roles.AddUserToRole(user.Username,"WebEditors");

                if (!Roles.IsUserInRole(user.Username,ApplicationSettings.Instance.StationEditorRoleName))
                    Roles.AddUserToRole(user.Username, ApplicationSettings.Instance.StationEditorRoleName);

                Log.DebugFormat("User {0} was created/updated with roles {1}", user.Username, new[] {ApplicationSettings.Instance.StationEditorRoleName, ApplicationSettings.Instance.WebEditorRoleName});
            }
        }

        public void RemoveOldStationUsers(IEnumerable<User> users)
        {
            if (string.IsNullOrEmpty(ApplicationSettings.Instance.StationEditorRoleName) || !Roles.RoleExists(ApplicationSettings.Instance.StationEditorRoleName))
                throw new Exception("Missing configuration of StationEditorRoleName in Application Settings. Configure with correct role and try again");

            string[] currentStationUsers = Roles.GetUsersInRole(ApplicationSettings.Instance.StationEditorRoleName);

            foreach (User user in users.Where(user => !currentStationUsers.Contains(user.Username)))
            {
                Log.DebugFormat("User {0} is being deleted since he/she is no longer in the XML station-users import file", user.Username);
                Membership.DeleteUser(user.Username);
            }
        }
    }
}