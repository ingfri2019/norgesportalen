﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using EPiServer.Logging.Compatibility;
using EPiServer.ServiceLocation;
using SSO.Security.Contracts;

namespace SSO.Security.Services
{
    [ServiceConfiguration(typeof(IIpAddressResolver))]
    public class IpAddressResolver : IIpAddressResolver
    {
        private static readonly ILog Log = LogManager.GetLogger("IpAddressResolver");

        private static readonly IEnumerable<string> OrderedHeadersToCheckForClientIp = new[]
        {
            "HTTP_CLIENT_IP",
            "HTTP_X_FORWARDED_FOR",
            "X_FORWARDED_FOR",
            "X-Forwarded-For",
            "HTTP_X_FORWARDED",
            "HTTP_X_CLUSTER_CLIENT_IP",
            "HTTP_FORWARDED_FOR",
            "HTTP_FORWARDED",
            "HTTP_VIA",
            "REMOTE_ADDR"
        };

        public IPAddress GetClientIpAddressFor(HttpRequest request)
        {
            return GetIpAddress(request.ServerVariables, request.UserHostAddress);
        }

        public IPAddress GetClientIpAddressFor(HttpRequestBase request)
        {
            return GetIpAddress(request.ServerVariables, request.UserHostAddress);
        }

        private static IPAddress GetIpAddress(NameValueCollection serverVariables, string fallbackIp)
        {
            //serverVariables["HTTP_X_FORWARDED"] = "132.150.226.76";
            //serverVariables["X-Forwarded-For"] = "132.150.226.76";
            //serverVariables["REMOTE_ADDR"] = "132.150.226.76";

            if (Log.IsDebugEnabled)
                DebugServerVariables(serverVariables);
            IPAddress addr = null;

            // Loop through candidate server variable keys
            if (OrderedHeadersToCheckForClientIp
                // Check if key is defined
                .Where(key => serverVariables.AllKeys.Contains(key))
                // Get values
                .Select(serverVariables.GetValues)
                // Filter out empty entires (BSTS)
                .Where(values => values != null && values.Any())
                // Get all values (over several lines)
                .SelectMany(values => values
                    // Filter out empty values again (BSTS)
                    .Where(v => !string.IsNullOrEmpty(v))
                    // Split potential multiple values on one line
                    .SelectMany(value => value.Split(',')
                        // Remove empty values yet again (BSTS)
                        .Where(p => !string.IsNullOrEmpty(p))
                        // Trim
                        .Select(p => p.Trim())
                        // Reverse (just the split values on single line)
                        .Reverse()))
                // Check if valid IP address (BSTS)
                .Any(part => IPAddress.TryParse(part, out addr)))
            {
                Log.DebugFormat("Best guess for client IP: {0}", addr);
                return addr;
            }

            Log.DebugFormat("Fallback for client IP: {0}", fallbackIp);
            return IPAddress.TryParse(fallbackIp, out addr) ? addr : null;
        }

        public static IPAddress StringToIpAddress(string ipAddress)
        {
            IPAddress address;
            return !IPAddress.TryParse(ipAddress, out address) ? null : address;
        }

        private static void DebugServerVariables(NameValueCollection serverVariables)
        {
            try
            {
                // Get names of all keys into a string array. 
                string[] keys = serverVariables.AllKeys;

                StringBuilder sb = new StringBuilder();

                sb.AppendLine("Server variables for request:");
                foreach (string key in keys)
                {
                    sb.Append($"Key: {key}");
                    string[] values = serverVariables.GetValues(key);
                    if (values == null) continue;

                    int i = 1;
                    foreach (string value in values)
                    {
                        sb.Append($"Value {i++}: {value}");
                    }
                }

                Log.Debug(sb);
            }
            catch (Exception e)
            {
                Log.Debug("Logging server variables failed", e);
            }
        }
    }
}