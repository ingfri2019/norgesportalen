var path = require('path');
var webpack = require('webpack');
var csso = require('postcss-csso');

module.exports = {
  entry: {
    'app': './src/app.js',
    'styles': "./src/styles.scss"
  },
  output: {
		path: path.join(__dirname, 'build'),
		filename: 'server.js'
	},
  plugins: [
    new webpack.NoErrorsPlugin()
  ],
  module: {
    loaders: [
      {
        test: /.jsx?$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        query: {
          presets: ['es2015', 'react']
        }
      },
      {
        test: /\.scss$/,
        include: path.join(__dirname, 'build'),
        loaders: ['style', 'css', 'postcss', 'sass']
      }
    ]
  },
  postcss: () => [autoprefixer({ browsers: ['last 1 versions'] }), csso]
};
