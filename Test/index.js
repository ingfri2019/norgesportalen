"use strict";
const express = require('express');
const bodyParser = require('body-parser');

const app = express();

const server = app.listen(3000, () => {
  console.log('listening on *:3000');
});

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/views/index.html');
});
