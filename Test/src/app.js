import 'babel-polyfill';

import ReactDOM from 'react-dom';
import React from 'react';

global.React = React;
global.ReactDOM = ReactDOM;

import * as Components from './components/components.js';
global.Components = Components;
